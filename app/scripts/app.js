'use strict';
/**
 * @ngdoc brandingModule
 * @name se-branding
 * @description
 * # se-branding
 *
 * Main module of the application.
 */
angular
  .module('se-branding', [
    'seWebUI',
    'gettext',
    'ui.bootstrap',
    'seHeaderComponent',
    'ngMessages',
    'ngMaterial',
    'ngAnimate',
    'ngDialog',
    'angularMoment',
    'googlechart', // For Google Chart
    'commonUtilsModule', //Custom Module
    'dataModelModule', //Custom Module
    'smoothScroll'
    //			'serverDataValidationUtils'
  ])
  .value('googleChartApiConfig', {
    version: '1.1',
    optionalSettings: {
//            packages: ['corechart', 'bar', 'line', 'treemap'],
      packages: ['bar', 'line', 'treemap'],
      language: 'en'
    }
  })
  .config(['$routeProvider', "$sceDelegateProvider",
    function ($routeProvider, $sceDelegateProvider) {
      $routeProvider
        .when('/summary', {
          templateUrl: 'views/summary.html',
          controller: 'summaryCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService', 'authorizationService', 'implicitGrantLoginService','$q',
              function ($rootScope, seLandingMetaInfo, authenticationService, authorizationService, implicitGrantLoginService, $q) {
//                alert(JSON.stringify($rootScope.loginUser));
               // authenticationService.checkLogin();
                var deferred = $q.defer();
                if (authorizationService.getLoginUser() === undefined && $rootScope.env !== 'LOCAL') {
                  implicitGrantLoginService.login();
                }
                if($rootScope.loginUser === undefined && $rootScope.env !== 'LOCAL') {
                  authenticationService.checkLogin().then(function (data) {
                    $rootScope.loginUser = data;
                    deferred.resolve(data);
                  });
                }else{
                  deferred.resolve(true);
                }
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.homepage;
                });
                return deferred.promise;
              }]
          }
        })
        .when('/byapps', {
          templateUrl: 'views/byapps.html',
          // templateUrl: 'views/byappsBs.html',
          controller: 'byappsCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService',
              function ($rootScope, seLandingMetaInfo, authenticationService) {
//                alert(JSON.stringify($rootScope.loginUser));
                //authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.homepage;
                });
              }]
          }
        })
        .when('/byapps/:appid', {
          templateUrl: 'views/byapps.html',
          // templateUrl: 'views/byappsBs.html',
          controller: 'byappsCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService',
              function ($rootScope, seLandingMetaInfo, authenticationService) {
//                alert(JSON.stringify($rootScope.loginUser));
                authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.homepage;
                });
              }]
          }
        })
        .when('/home/:pageNo', {
          templateUrl: 'views/home.html',
          controller: 'homeCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService',
              function ($rootScope, seLandingMetaInfo, authenticationService) {
                authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.innerpage;
                });
              }]
          }
        })
        .when('/work/:pageNo', {
          templateUrl: 'views/work.html',
          controller: 'workCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService',
              function ($rootScope, seLandingMetaInfo, authenticationService) {
                authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.homepage;
                });
              }]
          }
        })
        .when('/report/:pageNo', {
          templateUrl: 'views/report.html',
          controller: 'reportCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService',
              function ($rootScope, seLandingMetaInfo, authenticationService) {
                authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.homepage;
                });
              }]
          }
        })
        .when('/admin', {
          templateUrl: 'views/admin.html',
          controller: 'adminCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService',
              function ($rootScope, seLandingMetaInfo, authenticationService) {
                authenticationService.checkLogin().then(function (data) {
                  $rootScope.loginUser = data;
                }, function (error) {
                  console.log('\n')
                });
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.homepage;
                });
              }]
          }
        })
        .when('/cce', {
          templateUrl: 'views/cce.html',
          controller: 'cceCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService',
              function ($rootScope, seLandingMetaInfo, authenticationService) {
                authenticationService.checkLogin().then(function (data) {
                  $rootScope.loginUser = data;
                });
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.homepage;
                });
              }]
          }
        })
        .when('/others', {
          templateUrl: 'views/others.html',
          controller: 'othersCtrl',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService',
              function ($rootScope, seLandingMetaInfo, authenticationService) {
                authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function (data) {
                  $rootScope.metatags = data.homepage;
                });
              }]
          }
        })
        .otherwise({
          redirectTo: '/summary'

        });
      $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'http://10.194.156.27:9019/**'
      ]);
    }])
  .constant('loginConstants', (function () {
    return {
      LOGIN_URL: '/authorize',
      LOGOUT_URL: '/oauth/disconnect',
      USER_SERVICE_URL: '/api/v1/user'
    }
  })())
  .run(['$rootScope', '$location', '$http', '$q', 'authenticationService', 'seClient', 'implicitGrantLoginService',
    'authorizationCodeGrantService', 'loginConstants', 'authenticationConstants', 'authorizationService',
    function ($rootScope, $location, $http, $q, authenticationService, seClient, implicitGrantLoginService,
              authorizationCodeGrantService, loginConstants, authenticationConstants, authorizationService) {
      $rootScope.env = 'LOCAL'; // LOCAL, SIT, PROD
      var authenticationUrlList = {
        'PROD': 'https://login.schneider-electric.com',
        'DEFAULT': 'https://login-ppr.schneider-electric.com'
      };
      var gaTrackingIds = {
        'PROD': 'UA-92634761-2',
        'DEFAULT': 'UA-92634761-1'
      };
      var authUrl = authenticationUrlList[$rootScope.env] || authenticationUrlList['DEFAULT'];
      var gaId = gaTrackingIds[$rootScope.env] || gaTrackingIds['DEFAULT']

      //Setting up GA tracking
      ga('create', gaId, 'auto');

      authenticationService.setAuthenticationUrl(authUrl);
      authenticationService.setClientId(seClient.CLIENT_ID);
      authenticationService.setGmrCode(seClient.GMR_CODE);
      authenticationService.setLoginService(implicitGrantLoginService);
      authenticationService.setLoginConstants(loginConstants);

      if ($rootScope.loginUser === undefined && $rootScope.env !== 'LOCAL') {
        implicitGrantLoginService.login();
      }else if($rootScope.env === 'LOCAL'){
        $rootScope.stateIsLoading = false;
      }

      $rootScope.$on(authenticationConstants.LOGIN_CONFIRMED_EVENT, function (event, data) {
        if($rootScope.loginUser === undefined) {
          $rootScope.loginUser = data.loginUser;
        }
      });

      $rootScope.$on('$routeChangeStart', function (event, route) {
        ga('set', 'page', $location['$$url']);
        ga('send', 'pageview');
        $rootScope.stateIsLoading = true;
        if($rootScope.loginUser === undefined && $rootScope.env !== 'LOCAL'){
          authenticationService.checkLogin().then(function (data) {
            $rootScope.loginUser = data;
          });
        }
      });
      $rootScope.$on('$routeChangeSuccess', function() {
        $rootScope.stateIsLoading = false;
      });
    }]);
