(function () {
  'use strict';

  var factoryId = '';

  angular.module('commonUtilsModule').factory('commonUtils', commonUtils);

  commonUtils.$inject = ['$rootScope', 'logger', 'appConstants'];


  /**
   *
   * @param {type} $rootScope
   * @param {type} logger
   * @param {type} appConstants
   * @returns {commonUtils_L1.commonUtils.factory}
   */
  function commonUtils($rootScope, logger, appConstants) {

    //#Factory Module Definition
    var factory = {
      initAllRootScopeVariables: initAllRootScopeVariables,
      getNodeRESTFulURL: getNodeRESTFulURL,
      getDateDiff: getDateDiff,
      getYesterday: getYesterday,
      getServiceFailErrorObj: getServiceFailErrorObj,
      getAuthFailErrorObj: getAuthFailErrorObj,
      getLaunchDateFormat: getLaunchDateFormat,
      getMoneyFormat: getMoneyFormat,
      getNumberFormat: getNumberFormat,
      getPaceChartOptions: getPaceChartOptions,
      getPieChartOptions: getPieChartOptions,
      getColumnChartOptions: getColumnChartOptions,
      getBarChartOptions: getBarChartOptions,
      getGoogleChartOptionsOneCol: getGoogleChartOptionsOneCol,
      getGoogleChartOptionsTwoCol: getGoogleChartOptionsTwoCol,
      getPaceChartStyle: getPaceChartStyle,
      getStartDateOfCurrrentYear: getStartDateOfCurrrentYear,
      getDateStringFromDate: getDateStringFromDate,
      logout: logout,
      getFormattedDayString: getFormattedDayString,
      getFormattedWeekString: getFormattedWeekString,
      getFormattedMonthString: getFormattedMonthString,
      nullValidationPaceData: nullValidationPaceData,
      roundToTwoDecimal: roundToTwoDecimal,
      getDateYMD: getDateYMD,
      getYearWeek: getYearWeek,
      getWeekToDate: getWeekToDate,
      getMonthYear: getMonthyear,
      // common functions
      getLastThirtyDaysArray: getLastThirtyDaysArray,
      getLastWeeksArray: getLastWeeksArray,
      getLastYearArray: getLastYearArray,

      computeLast30DaysData: computeLast30DaysData,
      computeWeekData: computeWeekData,
      computeMonthData: computeMonthData
    };
    return factory;
    //#Factory Module Definition


    /**
     *
     * @returns {undefined}
     */
    function initAllRootScopeVariables() {
      $rootScope.env = appConstants.ENVIRONMENT.LOCAL; //LOCAL , DEBUG, SIT, PROD
      $rootScope.validUser = true;
      $rootScope.adminUser = true;
      $rootScope.isLoginProgress = true;
      $rootScope.validatePACEToken = true;
    }

    /**
     *
     * @param {string} urlConst
     * @returns string
     */
    function getNodeRESTFulURL(urlConst) {
      var finalUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + appConstants.NODE_REST_URL[urlConst];
      return finalUrl;
    }


    /**
     *
     * @param {type} t1
     * @param {type} t2
     * @returns {Number}
     */
    function getDateDiff(t1, t2) {
      var diffMS = t1 - t2;
      var diffS = diffMS / 1000;
      var diffM = diffS / 60;
      var diffH = diffM / 60;
      var diffD = Math.round(diffH / 24);
      return (diffD);
    }

    /**
     *
     * @returns {unresolved}
     */
    function getYesterday() {
      var today = new Date(new Date().getFullYear(), (new Date().getMonth()), new Date().getDate());
      var yesterday = today.setDate(today.getDate() - 1);
      return yesterday;
    }


    /**
     *
     * @param {type} msgTitle
     * @param {type} response
     * @returns {commonUtils_L7.getServiceFailErrorObj.errObj}
     */
    function getServiceFailErrorObj(msgTitle, response) {
      var title = "Error";
      var errCode = "";
      var errMsg = "";

      if (msgTitle !== undefined && msgTitle !== null && msgTitle !== "") {
        title = title + " : " + msgTitle;
      }

      if (response !== undefined && response !== null
              && response.error !== undefined
              && response.error !== null) {

        errCode = response.error.code;
        errMsg = response.error.message + " (" + errCode + ")";
      } else {
        errMsg = appConstants.MESSAGE.SERVICE_FAIL_ERROR_MSG;
      }

      var errObj = {
        'title': title,
        'type': appConstants.ERROR.TYPE.WARNING,
        'message': errMsg
      };

      return errObj;
    }


    /**
     *
     * @param {string} msg
     * @returns {commonUtils_L7.getServiceFailErrorObj.errObj}
     */
    function getAuthFailErrorObj(msg) {
      var externalMsg = "";

      if (msg !== undefined && msg !== null && msg !== "") {
        externalMsg = msg + " ";
      }

      var errObj = {
        'title': 'Warning',
        'type': appConstants.ERROR.TYPE.WARNING,
        'message': externalMsg + appConstants.MESSAGE.AUTH_FAIL_ERROR_MSG
      };

      return errObj;
    }

    /**
     * Get lauch date fromat
     *
     * @param {type} data
     * @returns {String}
     */
    function getLaunchDateFormat(data) {
      var temp = new Date(data);
      var returnDate = temp.toDateString();
      returnDate = returnDate.split(" ");
      return returnDate[2] + " " + returnDate[1] + " " + returnDate[3];
    }

    /**
     * Get money format
     *
     * @param {type} number
     * @param {type} places
     * @param {type} symbol
     * @param {type} thousand
     * @param {type} decimal
     * @returns {@var;symbol|@var;decimal|@var;thousand|String}
     */
    function getMoneyFormat(number, places, symbol, thousand, decimal) {
      number = Math.round(number / 1000) || 0;
      places = !isNaN(places = Math.abs(places)) ? places : 2;
      symbol = symbol !== undefined ? symbol : "$";
      thousand = thousand || ",";
      decimal = decimal || ".";
      var negative = number < 0 ? "-" : "",
              i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
              j = (j = i.length) > 3 ? j % 3 : 0;
      return negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "") + symbol;
    }

    /**
     * Get money format
     *
     * @param {type} number
     * @param {type} places
     * @param {type} thousand
     * @param {type} decimal
     * @returns {@var;symbol|@var;decimal|@var;thousand|String}
     */
    function getNumberFormat(number, places, thousand, decimal) {
      number = number || 0;
      places = !isNaN(places = Math.abs(places)) ? places : 2;

      thousand = thousand || ",";
      decimal = decimal || ".";
      var negative = number < 0 ? "-" : "",
              i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
              j = (j = i.length) > 3 ? j % 3 : 0;
      return negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
    }


    /**
     *
     * @returns {Date}
     */
    function getStartDateOfCurrrentYear() {
      var dt = new Date();
      var year = dt.getFullYear();
      var newDate = new Date(year, 0, 1);

      return newDate;
    }



    function getDateStringFromDate(dt) {
      var year = dt.getFullYear();
      var month = dt.getMonth() + 1;
      var day = dt.getDate();

      var yearStr = year.toString();
      var monthStr = "";
      var dayStr = "";

      if (month <= 9) {
        monthStr = "0" + month.toString();
      } else {
        monthStr = month.toString();
      }

      if (day <= 9) {
        dayStr = "0" + day.toString();
      } else {
        dayStr = day.toString();
      }

      var dtStr = yearStr + "-" + monthStr + "-" + dayStr;
      return dtStr;
    }


    /**
     *
     * @returns {commonUtils_L7.getChartOptions.chartOption}
     */
    function getPaceChartOptions() {
      var chartOption = {};

      chartOption.fill = 20;
      chartOption.displayExactValues = "displayExactValues";

      chartOption.lineWidth = 4;

      chartOption.colors = ['#058DC7'];

      chartOption.areaOpacity = 0.1;

      chartOption.pointShape = "circle";
      chartOption.pointSize = 6;

      chartOption.vAxis = {};
      chartOption.vAxis.textPosition = 'in';

      chartOption.hAxis = {};
      chartOption.hAxis.textPosition = 'out';
      chartOption.hAxis.showTextEvery = 2;

      chartOption.legend = {};
      chartOption.legend.position = "top";

      chartOption.chartArea = {};
      chartOption.chartArea.left = 20;
      chartOption.chartArea.top = 50;
      chartOption.chartArea.width = '90%';
      chartOption.chartArea.height = '75%';
      chartOption.slantedText = true;

      return chartOption;
    }


    /**
     *
     * @returns {commonUtils_L7.getChartOptions.chartOption}
     */
    function getPieChartOptions() {
      var chartOption = {};

      chartOption.fill = 20;
      chartOption.displayExactValues = "displayExactValues";

      chartOption.lineWidth = 4;

      chartOption.is3D = true;

      chartOption.vAxis = {};
      chartOption.vAxis.textPosition = 'out';

      chartOption.hAxis = {};
      chartOption.hAxis.textPosition = 'out';
      chartOption.hAxis.showTextEvery = 3;

      chartOption.legend = {};
      chartOption.legend.alignment = "left";
      chartOption.legend.position = "bottom";

      chartOption.chartArea = {};
      chartOption.chartArea.left = 20;
      chartOption.chartArea.top = 50;
      chartOption.chartArea.width = '93%';
      chartOption.chartArea.height = '75%';

      return chartOption;
    }


    /**
     *
     * @returns {commonUtils_L7.getChartOptions.chartOption}
     */
    function getColumnChartOptions() {
      var chartOption = {};

      chartOption.isStacked = "false";

      chartOption.colors = ['#058DC7', '#C1504C'];

      chartOption.vAxis = {};
      chartOption.vAxis.textPosition = 'out';

      chartOption.hAxis = {};
      chartOption.hAxis.textPosition = 'out';
      chartOption.hAxis.showTextEvery = 2;

      chartOption.hAxis.gridlines = {};
      chartOption.hAxis.gridlines.color = '#333';
      chartOption.hAxis.gridlines.count = 12;

      chartOption.legend = {};
      chartOption.legend.alignment = "left";
      chartOption.legend.position = "top";


      chartOption.chartArea = {};
      chartOption.chartArea.left = 55;
      chartOption.chartArea.top = 50;
      chartOption.chartArea.width = '92%';
      chartOption.chartArea.height = '75%';

      return chartOption;
    }


    /**
     *
     * @returns {commonUtils_L7.getChartOptions.chartOption}
     */
    function getBarChartOptions() {

      var chartOption = {};

      chartOption.isStacked = "false";

      chartOption.colors = ['#058DC7', '#C1504C'];

      chartOption.vAxis = {};
      chartOption.vAxis.textPosition = 'out';

      chartOption.hAxis = {};
      chartOption.hAxis.textPosition = 'out';
      chartOption.hAxis.showTextEvery = 2;

      chartOption.hAxis.gridlines = {};
      chartOption.hAxis.gridlines.color = '#333';
      chartOption.hAxis.gridlines.count = 12;

      chartOption.legend = {};
      chartOption.legend.alignment = "left";
      chartOption.legend.position = "top";

      chartOption.chartArea = {};
      chartOption.chartArea.left = 120;
      chartOption.chartArea.top = 50;
      chartOption.chartArea.width = '80%';
      chartOption.chartArea.height = '90%';

      return chartOption;
    }

    /**
     *
     * @returns {commonUtils_L7.getChartOptions.chartOption}
     */
    function getGoogleChartOptionsOneCol() {
      var chartOption = {};

      chartOption.isStacked = "true";
      chartOption.fill = 20;
      chartOption.displayExactValues = "displayExactValues";

      chartOption.lineWidth = 4;

      chartOption.colors = ['#058DC7'];

      chartOption.areaOpacity = 0.1;

      chartOption.pointShape = "circle";
      chartOption.pointSize = 6;

      chartOption.vAxis = {};
      chartOption.vAxis.textPosition = 'in';

      chartOption.hAxis = {};
      chartOption.hAxis.textPosition = 'out';
      chartOption.hAxis.showTextEvery = 3;

      chartOption.legend = {};
      chartOption.legend.position = "top";

      chartOption.chartArea = {};
      chartOption.chartArea.left = 20;
      chartOption.chartArea.top = 50;
      chartOption.chartArea.width = '95%';
      chartOption.chartArea.height = '75%';

      return chartOption;
    }

    /**
     *
     * @returns {commonUtils_L7.getChartOptions.chartOption}
     */
    function getGoogleChartOptionsTwoCol() {
      var chartOption = {};

      chartOption.isStacked = "false";
      chartOption.fill = 20;
      chartOption.displayExactValues = "displayExactValues";

      chartOption.lineWidth = 4;

      chartOption.colors = ['#058DC7', '#C0504D'];

      chartOption.areaOpacity = 0.1;

      chartOption.pointShape = "circle";
      chartOption.pointSize = 6;

      chartOption.vAxis = {};
      chartOption.vAxis.textPosition = 'out';//"in";

      chartOption.hAxis = {};
      chartOption.hAxis.textPosition = 'out';
      chartOption.hAxis.showTextEvery = 3;

      chartOption.legend = {};
      chartOption.legend.position = "top";

      chartOption.chartArea = {};
      chartOption.chartArea.left = 45; //20
      chartOption.chartArea.top = 50;
      chartOption.chartArea.width = '90%'; // 95
      chartOption.chartArea.height = '75%';

      return chartOption;
    }


    /**
     *
     * @returns {String}
     */
    function getPaceChartStyle() {
      return  "height:400px; width:100%;";
    }


    /**
     *
     * @returns {undefined}
     */
    function logout() {
      var authenticationServerUrl = appConstants.PACE_LOGIN_REDIRECT_URL.AUTHENTICATION_SERVER_URL;
      var appUrl = appConstants.PACE_LOGIN_REDIRECT_URL[$rootScope.env].APP_URL;
      window.location.href = authenticationServerUrl + '/oauth/disconnect?state=mgmt-dashboard&redirect=' + appUrl + '%2F%23';
    }

    /**
     *
     * @param {type} dateStr
     * @returns {String}
     */
    function getFormattedDayString(dateStr) {
      var yearStr = dateStr.substr(0, 4);
      var monthStr = dateStr.substr(4, 2);
      var dayStr = dateStr.substr(6, 2);
      var dtStr = yearStr + "-" + monthStr + "-" + dayStr;

      return dtStr;
    }

    /**
     *
     * @param {type} dateStr
     * @returns {String}
     */
    function getFormattedWeekString(dateStr) {
      var yearStr = dateStr.substr(0, 4);
      var weekStr = dateStr.substr(4, 2);

      var dtStr = yearStr + "-" + weekStr;

      return dtStr;
    }

    /**
     *
     * @param {type} dateStr
     * @returns {String}
     */
    function getFormattedMonthString(dateStr) {
      var yearStr = dateStr.substr(0, 4);
      var weekStr = dateStr.substr(4, 2);
      var dayStr = "01";

      var dtStr = yearStr + "-" + weekStr + "-" + dayStr;

      return dtStr;
    }


    /**
     *
     * @param {type} data
     * @returns {undefined}
     */
    function nullValidationPaceData(data) {

    }

    /**
     * Round To Two Decimal Place
     *
     *
     * @param {type} num
     * @returns {String}
     */
    function roundToTwoDecimal(num) {
      return +(Math.round(num + "e+2") + "e-2");
    }

    function getDateYMD(cdate) {
      var yr = cdate.getFullYear();
      var mo = cdate.getMonth() + 1;
      var dd = cdate.getDate();
      return yr + "-" + (mo < 10 ? '0' + mo : mo) + '-' + (dd < 10 ? '0' + dd : dd);
    }

    // get year-week: e.g., 2016-01, etc
    function getYearWeek(year, weekno) {
      return year + '-' + (weekno < 10 ? '0' + weekno : weekno);
    }

    function getWeekToDate(startDate) {
      var d = new Date(+startDate);
      // Set to nearest Thursday: current date + 4 - current day number
      // Make Sunday's day number 7
      d.setDate(d.getDate() + 4 - (d.getDay() || 7));
      // Get first day of year
      var yearStart = new Date(d.getFullYear(), 0, 1);
      // Calculate full weeks to nearest Thursday
      var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
      return weekNo;
    }

    function getMonthyear(monthYearStr) {
      var months = appConstants.DATES.MONTH_ARR_SHORT;
      var arr = monthYearStr.split('-');
      return months[+arr[1]-1] + "'" + arr[0].substr(2);
    }

    function getLastThirtyDaysArray(fromdate, xField, yField) {
      var data = [];
      var fromdt = fromdate.split('-');
      var today = new Date(fromdt[0], fromdt[1] - 1, fromdt[2]);
      var todayYmd = getDateYMD(today);
//			console.log('--> getLastThirtyDaysArray: fromdate: ', fromdate, ' | today :: ', todayYmd);
      var startdt = new Date(today.getFullYear(), today.getMonth(), 1);
//			console.log('--> startdt: ', startdt);
      var startYmd = getDateYMD(startdt);
      var d = startdt.getDate();
//			console.log('--> startYmd: ', startYmd, ' | todayYmd: ', todayYmd);
      while (startYmd <= todayYmd) {
        var t = {};
        t[xField] = startYmd;
        t[yField] = 0;
        data.push(t);
        startdt.setDate(++d);
        startYmd = getDateYMD(startdt);
      }
      return data;
    }
    // create last 24 weeks (weekly data for last 6 months)
    function getLastWeeksArray(fromdate, xField, yField) {
      var data = [];
      var today = new Date(fromdate);

//			var todayWeekno = getWeekToDate(today);
//			var todayYearWeek = getYearWeek(today.getFullYear(), todayWeekno + 1);

      var weektodate = getWeekToDate(today);
      var todayWeekno = weektodate > 52 ? 1 : weektodate;
      var todayYearWeek = getYearWeek(today.getFullYear(), todayWeekno + 1);

      var weekdays = (-1 * 24 * 7);
      var startdt = new Date(today.getFullYear(), today.getMonth() + 1, weekdays);
      var startYr = startdt.getFullYear();
      var startDateno = startdt.getDate();
      var startWeekno = getWeekToDate(startdt);
      var startYearWeek = getYearWeek(startYr, startWeekno);
//			alert('~~~ getLastWeeksArray: weekdays: ' + weekdays + '\nstartdt: ' + getDateYMD(startdt) + '\nstartDateno: ' + startDateno + '\nstartWeekno: ' + startWeekno + ' \nstartYearWeek: ' + startYearWeek + ' \ntodayYearWeek: ' + todayYearWeek);
      while (startYearWeek <= todayYearWeek) {
        var t = {};
        t[xField] = startYearWeek;
        t[yField] = 0;
        data.push(t);
        startWeekno++;
        if (startWeekno > 52) {
          startWeekno = 1;
          startYr++;
        }
        startYearWeek = getYearWeek(startYr, startWeekno);
      }
      return data;
    }

    function getLastYearArray(fromdate, xField, yField) {
      var data = [];
      var today = new Date(fromdate);
      var todayYmd = getDateYMD(today).substr(0, 7);
      var startdt = new Date(today.getFullYear() - 1, today.getMonth() + 1, 1);
      var startYmd = getDateYMD(startdt).substr(0, 7);
      var mm = startdt.getMonth();
      var n = 0;
      while (startYmd <= todayYmd) {
        var yymm = getDateYMD(startdt).substr(0, 7);
        var t = {};
        t[xField] = startYmd;
        t[yField] = 0;
        data.push(t);
        if (mm > 11) {
          mm = 0;
        }
        mm++;
        startdt.setMonth(mm);
        startYmd = getDateYMD(startdt).substr(0, 7);
      }
      return data;
    }

    function computeLast30DaysData(chartData, last30daysData) {
      chartData.forEach(function (obj, indx) {
        last30daysData.forEach(function (ldobj, lyindx) {
          if (ldobj.createdate === obj.createdate) {
            ldobj.users = obj.users;
          }
          ;
        });
      });
    }


    function computeWeekData(chartData, lastWeekData) {
      for (var i = 0; i < lastWeekData.length; i++) {
        chartData.forEach(function (obj, indx) {
          if (lastWeekData[i].createdate === obj.createdate) {
            lastWeekData[i].users = obj.users;
          }
        });
      }
    }

    function computeMonthData(chartData, lyData) {
      // map current data with the mapped array
      chartData.forEach(function (obj, indx) {
        lyData.forEach(function (lyobj, lyindx) {
          if (lyobj.createdate === obj.createdate) {
            lyobj.users = obj.users;
          }
          ;
        });
      });
    }

  }

})();
