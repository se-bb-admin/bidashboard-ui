(function () {
  'use strict';


  angular.module('commonUtilsModule')
          .factory('serverDataValidationUtils', serverDataValidationUtils);

  serverDataValidationUtils.$inject = ['$rootScope', 'logger', 'appConstants', '$location'];


  /**
   *
   * @param {type} $rootScope
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $location
   * @returns {serverDataValidationUtils_L1.serverDataValidationUtils.factory}
   */

  function serverDataValidationUtils($rootScope, logger, appConstants, $location) {



    //#Factory Module Definition
    var factory = {
      validatePaceRegUserOnCurrentYear: validatePaceRegUserOnCurrentYear,
      validatePaceRegUser: validatePaceRegUser
    };
    return factory;
    //#Factory Module Definition




    /**
     *
     * @param {type} data
     * @returns {undefined}
     */
    function validatePaceRegUserOnCurrentYear(data) {
      var isValid = true;

      return isValid;

      if (data === undefined || data === null || data.length === 0) {
        $location.path('/error/E010');
        //Hide Spinner
        $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
        isValid = false;
      }

      return isValid;
    }



    function validatePaceRegUser(data) {
      var isValid = true;

      return isValid;

      if (data === undefined || data === null ||
              data.appRegistrationsStatsWrapper === undefined || data.appRegistrationsStatsWrapper === null ||
              data.appRegistrationsStatsWrapper.applicationStats === undefined || data.appRegistrationsStatsWrapper.applicationStats === null) {
        $location.path('/error/E010');
        //Hide Spinner
        $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
        isValid = false;
      }

      return isValid;
    }



  }

})();


