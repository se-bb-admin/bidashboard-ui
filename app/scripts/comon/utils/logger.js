(function () {

  'use strict';

  angular.module('commonUtilsModule').factory('logger', logger);

  logger.$inject = ['$log'];

  /**
   *
   * @param {type} $log
   * @returns {logger_L1.logger.factory}
   */
  function logger($log) {

    //#region Factory Definition
    var _logToConsole = true;

    var factory = {
      log: log,
      info: info,
      error: error,
      warn: warn,
      debug: debug,
      success: success,
      logToConsole: logToConsole
    };
    return factory;

    //#endregion

    //#region Exposed Members

    function log(message, context, data, showToast) {
      // Log to info by default
      info(message, context, data, showToast);
    }

    function info(message, context, data, showToast) {
      _log(message, context, data, 'info', showToast);
    }

    function error(message, context, data, showToast) {
      _log(message, context, data, 'error', showToast);
    }

    function warn(message, context, data, showToast) {
      _log(message, context, data, 'warning', showToast);
    }

    function success(message, context, data, showToast) {
      _log(message, context, data, 'success', showToast);
    }

    function debug(message, context, data) {
      // Don't show toast on debug.
      _log(message, context, data, 'warning', false);
    }

    function logToConsole(isEnabled) {
      _logToConsole = isEnabled;
    }

    //#endregion

    //#region Private Members

    function _log(message, context, data, toastType, showToast) {
      // Write out the log entry
      if (_logToConsole) {
        var write = (toastType === 'error') ? $log.error : $log.log;
        var source = context ? '[' + context + '] ' : '';
        if (data === undefined) {
          write(source, message);
        } else {
          write(source, message, data);
        }
      }

      // Check if the showToast variable is set, if not default to false
      if (showToast === null || showToast === undefined) {
        showToast = false;
      }

    }

  }
})();