(function () {
  'use strict';

  angular.module('commonUtilsModule').factory('authUtil', authUtil);

  authUtil.$inject = ['$rootScope', 'logger', 'appConstants', 'paceService'];

  function authUtil($rootScope, logger, appConstants, paceService) {

    var _loggedInUser = null;
    var _isValidUser = false;

    //#Factory Module Definition
    var factory = {
      validateUserByPaceAuthToken: validateUserByPaceAuthToken,
      getUserDetails: getUserDetails
              //isAdminUser: isAdminUser,
              //isValidUser: isValidUser
    };
    return factory;
    //#Factory Module Definition


    /**
     *
     * @returns {undefined}
     */
    function validateUserByPaceAuthToken() {
      //Int RootScope Values.
      $rootScope.validUser = false;
      $rootScope.adminUser = false;

      var userToken = paceService.getAuthToken();
      paceService.getUserAuthentication(userToken).then(
              function (result) {
                _loggedInUser = result;
                logger.log("PaceLoinRedirector::PACE::Result::Success");
                var id = result.id;
                //Check Validity
                if (id.indexOf("SESA") === -1)
                {
                  if (result.email === "schneider.analytic@gmail.com" || result.email === 'analytics.mphasis@gmail.com') {
                    _isValidUser = true;
                  } else {
                    _isValidUser = false;
                  }

                } else {
                  _isValidUser = true;
                }

                //Dispatch event as per valid/invalid auth
                if (_isValidUser === true) {
                  $rootScope.validUser = true;
                  $rootScope.adminUser = isAdminUser();
                  $rootScope.isLoginProgress = false;
                  $rootScope.$broadcast('login_success');
                } else {
                  //for first time loging or refreshing the page, redirected to /error page.
                  $rootScope.$broadcast(appConstants.EVENTS.INVALID_LOGIN);
                }

              },
              function (error) {
                logger.log("PaceLoinRedirector::PACE::Result::FAIL");
                $rootScope.$broadcast(appConstants.EVENTS.INVALID_LOGIN);
              }
      );
    }


    /**
     *
     * @returns {Boolean}
     */
    function isValidUser() {
      return _isValidUser;
    }


    /**
     *
     * @returns {Boolean}
     */
    function isAdminUser() {
      var isAdmin = false;
      var loggedInUserSESA_Id = "";
      var loggedInUserEmailId = "";
      var ADMIN_IDs = appConstants.ADMIN_ID;
      var tmpAdmin = null;


      if (_loggedInUser !== undefined && _loggedInUser !== null) {
        loggedInUserSESA_Id = _loggedInUser.id;
        loggedInUserEmailId = _loggedInUser.email;

        for (var i = 0; i < ADMIN_IDs.length; i++) {
          tmpAdmin = ADMIN_IDs[i];

          if (tmpAdmin.id === loggedInUserSESA_Id || tmpAdmin.email === loggedInUserEmailId) {
            isAdmin = true;
            break;
          }
        }
      }
      return isAdmin;
    }


    /**
     *
     * @returns {result}
     */
    function getUserDetails() {
      return _loggedInUser;
    }


  }
})();


