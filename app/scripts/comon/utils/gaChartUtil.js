(function () {
  'use strict';


  angular.module('commonUtilsModule').factory('gaChartUtil', gaChartUtil);

  gaChartUtil.$inject = ['$rootScope', 'logger', 'appConstants', 'moment'];

  /**
   *
   * @param {type} $rootScope
   * @param {type} logger
   * @param {type} appConstants
   * @returns {gaChartUtil_L1.gaChartUtil.factory}
   */
  function gaChartUtil($rootScope, logger, appConstants, moment) {

    var MONTH_ARR = appConstants.CHART_OPTIONS.MONTH_ARR;

    //#Factory Module Definition
    var factory = {
      populateGAChartData: populateGAChartData,
      populateLinearChartData: populateLinearChartData,
      populateGAChartDataTwoCol: populateGAChartDataTwoCol,
      populateTwoFieldChartData: populateTwoFieldChartData,
      populateThreeFieldChartData: populateThreeFieldChartData,
      populateDayChartData: populateDayChartData,
      populateWeekChartData: populateWeekChartData,
      populateMonthChartData: populateMonthChartData,
      populateYearChartData: populateYearChartData,
      populateTreeMapData: populateTreeMapData,
      populateMultiLineChartData: populateMultiLineChartData
    };
    return factory;
    //#Factory Module Definition


    //------------------------------------------------------------------
    //----------------------- PUBLIC METHODS ---------------------------
    //------------------------------------------------------------------


    /**
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @param {type} labelX
     * @param {type} labelY
     * @returns {gaChartUtil_L1.gaChartUtil.populateGAChartData.chartData}
     */
    function populateGAChartData(data, fieldNameX, fieldNameY, labelX, labelY) {
      var chartData = {};

      var dayChartData = [];
      var weekChartData = [];
      var monthChartData = [];

      if (data) {
        if (data.day !== undefined && data.day !== null) {
          dayChartData = populateDayChartData(data.day, fieldNameX, fieldNameY, labelX, labelY);
        }
        if (data.week !== undefined && data.week !== null) {
          weekChartData = populateWeekChartData(data.week, fieldNameX, fieldNameY, labelX, labelY);
        }
        if (data.month !== undefined && data.month !== null) {
          monthChartData = populateMonthChartData(data.month, fieldNameX, fieldNameY, labelX, labelY);
        }
        chartData.day = dayChartData;
        chartData.week = weekChartData;
        chartData.month = monthChartData;
      }

      return chartData;
    }


    /**
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @param {type} labelX
     * @param {type} labelY
     * @returns {gaChartUtil_L1.gaChartUtil.populateLinearChartData.linierChartData}
     */
    function populateLinearChartData(data, fieldNameX, fieldNameY, labelX, labelY) {

      var linierChartData = {};
      var columns = getLinearChartColumnsData(labelX, labelY);
      var rows = getLinearChartRowsData(data, fieldNameX, fieldNameY);
      linierChartData.cols = columns;
      linierChartData.rows = rows;

      return linierChartData;
    }

    /**
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY1
     * @param {type} fieldNameY2
     * @param {type} labelX
     * @param {type} labelY1
     * @param {type} labelY2
     * @returns {gaChartUtil_L1.gaChartUtil.populateGAChartDataTwoCol.linierChartData}
     */
    function populateGAChartDataTwoCol(data, fieldNameX, fieldNameY1, fieldNameY2, labelX, labelY1, labelY2) {

      var linierChartData = {};
      var columns = getLinearChartColumnsDataTwoCol(labelX, labelY1, labelY2);
      var rows = getLinearChartRowsDataTwoCol(data, fieldNameX, fieldNameY1, fieldNameY2);
      linierChartData.cols = columns;
      linierChartData.rows = rows;

      return linierChartData;
    }


    /**
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @param {type} labelX
     * @param {type} labelY
     * @returns {undefined}
     */
    function populateTwoFieldChartData(data, fieldNameX, fieldNameY, labelX, labelY) {
      var twoFieldChartData = {};
      var columns = getTwoFieldColumnsData(labelX, labelY);
      var rows = getTwoFieldChartRowsData(data, fieldNameX, fieldNameY);
      twoFieldChartData.cols = columns;
      twoFieldChartData.rows = rows;

      return twoFieldChartData;


    }

    //------------------------------------------------------------------
    //----------------------- PRIVATE METHODS --------------------------
    //------------------------------------------------------------------


    //------------------ Populate Day Chart Data -----------------------

    /**
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @param {type} labelX
     * @param {type} labelY
     * @returns {gaChartUtil_L7.populateDayChartData.dayChartData}
     */
    function populateDayChartData(data, fieldNameX, fieldNameY, labelX, labelY) {
      var dayChartData = {};
      if (data) {
        var columns = getDayChartColumnsData(labelX, labelY);
        var rows = getDayChartRowsData(data, fieldNameX, fieldNameY);
        dayChartData.cols = columns;
        dayChartData.rows = rows;
      }

      return dayChartData;

    }


    /**
     *
     * @param {type} labelX
     * @param {type} labelY
     * @returns {Array}
     */
    function getDayChartColumnsData(labelX, labelY) {
      var cols = [];
      var col1 = {};
      var col2 = {};
      //Populate Col1
      col1.id = labelX;
      col1.label = labelX;
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = labelY;
      col2.label = labelY;
      col2.type = "number";
      //Push
      cols.push(col2);

      return cols;
    }


    /**
     *
     * OUTPUT Single Object: {c:[{v: "2015-03-25"},{v: 14}]}
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @returns {Array}
     */
    function getDayChartRowsData(data, fieldNameX, fieldNameY) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = getMonthDateFromDateStr(dataRow[fieldNameX]);
        //Push to Array
        row.c.push(rowValDate);

        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow[fieldNameY];
        //Push to Array
        row.c.push(rowValUser);

        //push into Rows
        rows.push(row);
      }

      return rows;
    }

    /**
     *
     * @param {type} dateStr
     * @returns {String}
     */
    function getMonthDateFromDateStr(dateStr) {
      var monthArr = MONTH_ARR;
      var dtArr = dateStr.split("-");
      var month = parseInt(dtArr[1]);
      var monthStr = monthArr[(month - 1)];
      var date = dtArr[2];

      return monthStr + " " + date;
    }

    //------------------------------------------------------------------
    //------------------ Populate Week Chart Data ----------------------
    //------------------------------------------------------------------
    /**
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @param {type} labelX
     * @param {type} labelY
     * @returns {gaChartUtil_L7.populateWeekChartData.weekChartData}
     */
    function populateWeekChartData(data, fieldNameX, fieldNameY, labelX, labelY) {
      var weekChartData = {};
      var columns = getWeekChartColumnsData(labelX, labelY);
      var rows = getWeekChartRowsData(data, fieldNameX, fieldNameY);

      weekChartData.cols = columns;
      weekChartData.rows = rows;

      return weekChartData;

    }

    /**
     *
     * @param {type} labelX
     * @param {type} labelY
     * @returns {Array}
     */
    function getWeekChartColumnsData(labelX, labelY) {
      var cols = [];
      var col1 = {};
      var col2 = {};
      //Populate Col1
      col1.id = labelX;
      col1.label = labelX;
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = labelY;
      col2.label = labelY;
      col2.type = "number";
      //Push
      cols.push(col2);

      return cols;
    }

    /**
     *
     *
     * OUTPUT Single Object: {c:[{v: "2015-03-25"},{v: 14}]}
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @returns {Array}
     */
    function getWeekChartRowsData(data, fieldNameX, fieldNameY) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = getWeekYearFromDateStr(dataRow[fieldNameX]);
        //Push to Array
        row.c.push(rowValDate);

        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow[fieldNameY];
        //Push to Array
        row.c.push(rowValUser);


        //push into Rows
        rows.push(row);
      }

      return rows;
    }


    /**
     *
     * @param {type} dateStr
     * @returns {String}
     */
    function getWeekYearFromDateStr(dateStr) {
      var dtArr = dateStr.split("-");
      var wek = parseInt(dtArr[1]);
      var yearWeek = "";
      var year = dtArr[0];

      var week = parseInt(wek);
      var weekStr = week > 9 ? week : "0" + week;

      yearWeek = year + "-" + weekStr;

      return yearWeek;
    }


    //------------------------------------------------------------------
    //------------------ Populate Month Chart Data ---------------------
    //------------------------------------------------------------------
    /**
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @param {type} labelX
     * @param {type} labelY
     * @returns {gaChartUtil_L7.populateMonthChartData.monthChartData}
     */
    function populateMonthChartData(data, fieldNameX, fieldNameY, labelX, labelY) {
      var monthChartData = {};
      var columns = getMonthChartColumnsData(labelX, labelY);
      var rows = getMonthChartRowsData(data, fieldNameX, fieldNameY);
      monthChartData.cols = columns;
      monthChartData.rows = rows;

      return monthChartData;

    }

    /**
     *
     * @param {type} labelX
     * @param {type} labelY
     * @returns {Array}
     */
    function getMonthChartColumnsData(labelX, labelY) {
      var cols = [];
      var col1 = {};
      var col2 = {};
      //Populate Col1
      col1.id = labelX;
      col1.label = labelX;
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = labelY;
      col2.label = labelY;
      col2.type = "number";
      //Push
      cols.push(col2);

      return cols;
    }

    /**
     *
     *
     * OUTPUT Single Object: {c:[{v: "2015-03-25"},{v: 14}]}
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @returns {Array}
     */
    function getMonthChartRowsData(data, fieldNameX, fieldNameY) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = getYearMonthFromDateStr(dataRow[fieldNameX]);
        //Push to Array
        row.c.push(rowValDate);

        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow[fieldNameY];
        //Push to Array
        row.c.push(rowValUser);


        //push into Rows
        rows.push(row);
      }

      return rows;
    }


    /**
     *
     * @param {type} dateStr
     * @returns {String}
     */
    function getYearMonthFromDateStr(dateStr) {
      var monthArr = MONTH_ARR;
      var dtArr = dateStr.split("-");
      var month = parseInt(dtArr[1]);
      var monthStr = monthArr[(month - 1)];
      var year = dtArr[0];
      var yearMonth = "";

      yearMonth = monthStr + "'" + year.substring(2, 4);

      return yearMonth;
    }

    function populateYearChartData(data, fieldNameX, fieldNameY, labelX, labelY) {
      var yearChartData = {rows: []};
      yearChartData.cols = [{id: labelX, label: labelX, type: 'string'}, {id: labelY, label: labelY, type: 'number'}];
      data.forEach(function (row) {
        yearChartData.rows.push({'c': [{v: row[fieldNameX]}, {v: row[fieldNameY]}]});
      });
      return yearChartData;
    }

    function populateMultiLineChartData(data, fields, labels){
      var multiChartData = {rows: [], cols:[]};

      labels.forEach(function(label, cKey){
        var type = cKey === 0 ? 'string' : 'number';
        multiChartData.cols.push({'id': label, 'label': label, 'type': type});
      });
      data.forEach(function (row, key) {
        var rows = [];
        fields.forEach(function(field, fKey){
          var value = row[field] || 0;
          if(field.indexOf('Month') !== -1){
            value = moment(value).format("MMM 'YY");
          }else if(field.indexOf('Date') !== -1){
            value = moment(value).format("MMM DD");
          }
          rows.push({v: value});
        });
        multiChartData.rows.push({'c': rows});
      });
      return multiChartData;
    }
    //------------------------------------------------------------------
    //------------------ Populate Linear Chart Data ---------------------
    //------------------------------------------------------------------

    /**
     *
     * @param {type} labelX
     * @param {type} labelY
     * @returns {Array}
     */
    function getLinearChartColumnsData(labelX, labelY) {
      var cols = [];
      var col1 = {};
      var col2 = {};
      //Populate Col1
      col1.id = labelX;
      col1.label = labelX;
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = labelY;
      col2.label = labelY;
      col2.type = "number";
      //Push
      cols.push(col2);

      return cols;
    }


    /**
     * OUTPUT Single Object: {c:[{v: "2015-03-25"},{v: 14}]}
     *
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY
     * @returns {Array}
     */
    function getLinearChartRowsData(data, fieldNameX, fieldNameY) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = dataRow[fieldNameY];
        //Push to Array
        row.c.push(rowValDate);

        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow[fieldNameX];
        //Push to Array
        row.c.push(rowValUser);


        //push into Rows
        rows.push(row);
      }

      return rows;
    }


    //----------------------------------------------------------------------
    //----------- Populate Linear Chart Data ( Two Column) -----------------
    //----------------------------------------------------------------------

    /**
     *
     * @param {type} labelX
     * @param {type} labelY1
     * @param {type} labelY2
     * @returns {Array}
     */
    function getLinearChartColumnsDataTwoCol(labelX, labelY1, labelY2) {
      var cols = [];
      var col1 = {};
      var col2 = {};
      var col3 = {};

      //Populate Col1
      col1.id = labelX;
      col1.label = labelX;
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = labelY1;
      col2.label = labelY1;
      col2.type = "number";
      //Push
      cols.push(col2);

      //Populate Col1
      col3.id = labelY2;
      col3.label = labelY2;
      col3.type = "number";
      //Push
      cols.push(col3);


      return cols;
    }


    /**
     * OUTPUT Single Object: {c:[{v: "2015-03-25"},{v: 14}, {v: 20}]}
     *
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY1
     * @param {type} fieldNameY2
     * @returns {Array}
     */
    function getLinearChartRowsDataTwoCol(data, fieldNameX, fieldNameY1, fieldNameY2) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = dataRow[fieldNameX];

        if (fieldNameX === 'registrationMonth' || fieldNameX === 'featureMonth') {
          rowValDate.v = getYearMonthFromDateStr(rowValDate.v);
        }else if(fieldNameX === 'accessedDate' || fieldNameX === 'featureDate') {
          rowValDate.v = getMonthDateFromDateStr(rowValDate.v);
        }
        //Push to Array
        row.c.push(rowValDate);


        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow[fieldNameY1];
        //Push to Array
        row.c.push(rowValUser);


        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow[fieldNameY2];
        //Push to Array
        row.c.push(rowValUser);


        //push into Rows
        rows.push(row);
      }

      return rows;
    }


    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------

    /**
     *
     * @param {type} labelX
     * @param {type} labelY
     * @returns {Array}
     */
    function getTwoFieldColumnsData(labelX, labelY) {
      var cols = [];
      var col1 = {};
      var col2 = {};
      //Populate Col1
      col1.id = labelX;
      col1.label = labelX;
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = labelY;
      col2.label = labelY;
      col2.type = "number";
      //Push
      cols.push(col2);

      return cols;
    }


    function getTwoFieldChartRowsData(data, fieldNameX, fieldNameY) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = dataRow[fieldNameX];
        if (fieldNameX.indexOf('Month') !== -1) {
          rowValDate.v = getYearMonthFromDateStr(rowValDate.v);
        }else if(fieldNameX === 'accessedDate'){
          rowValDate.v = getMonthDateFromDateStr(rowValDate.v);
        }
        //Push to Array
        row.c.push(rowValDate);

        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow[fieldNameY];
        //Push to Array
        row.c.push(rowValUser);


        //push into Rows
        rows.push(row);
      }

      return rows;
    }


    /**
     *
     * @param {type} data
     * @param {type} fieldNameX
     * @param {type} fieldNameY1
     * @param {type} fieldNameY2
     * @param {type} labelX
     * @param {type} labelY1
     * @param {type} labelY2
     * @returns {gaChartUtil_L1.gaChartUtil.populateGAChartDataTwoCol.linierChartData}
     */
    function populateThreeFieldChartData(data, fieldNameX, fieldNameY1, fieldNameY2, labelX, labelY1, labelY2) {

      var linierChartData = {};
      var columns = getLinearChartColumnsDataTwoCol(labelX, labelY1, labelY2);
      var rows = getLinearChartRowsDataTwoCol(data, fieldNameX, fieldNameY1, fieldNameY2);
      linierChartData.cols = columns;
      linierChartData.rows = rows;

      return linierChartData;
    }


    function populateTreeMapData(parentObj, childObj, valueObj, chartHeight, chartHeaderHeight, axisFontSize) {
      var chart = {
        type: 'TreeMap',
        data: {
          cols: [
            {id: parentObj.field, label: parentObj.label, type: 'string'},
            {id: childObj.field, label: childObj.label, type: 'string'},
            {id: valueObj.field, label: valueObj.label, type: 'number'}
          ],
          rows: [{
            c: [
              {v: parentObj.field},
              {v: null},
              {v: null}
            ]
          }]
        },
        options: {
          title: '',
          titleTextStyle: {
            fontSize: 14,
            color: '#666',
            bold: false
          },
          theme: 'material',
          backgroundColor: '#999',
          minColor: '#0ff',
          midColor: '#0aa',
          maxColor: '#099',
          fontColor: 'black',
          fontWeight: 'bold',
          headerHeight: chartHeaderHeight,
          showScale: false,
          showTooltips: true,
          highlightOnMouseOver: true,
          height: chartHeight,
          vAxis: {
            textPosition: 'out',
            textStyle: {
              fontSize: axisFontSize || 14
            }
          },
          hAxis: {
            textPosition: 'out',
            textStyle: {
              fontSize: axisFontSize || 14
            }
          }
        }
      };

      return chart;
    }


  }

})();


