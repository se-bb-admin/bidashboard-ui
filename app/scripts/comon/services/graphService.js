(function() {
  'use strict';

  angular.module('se-branding',[])
    .service('graphService', function() {
      function baseChart(graphType, graphLegendPosition) {
        var chart = {
          type: graphType,
          data: [],
          options: {
            legend: {
              position: graphLegendPosition || 'none'
            }
          }
        };
        return chart;
      }
      this.getChart = function(graphType, graphLegendPosition) {
        var chart = baseChart(graphType, graphLegendPosition);
      }
    })
})();
