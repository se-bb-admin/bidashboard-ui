/**
 * Created by SESA431390 on 3/21/2017.
 */
/**
 *
 */
(function () {
  'use strict';


  angular.module('se-branding').factory('appsDataService', appsDataService);

  appsDataService.$inject = ['$rootScope', '$q', '$http', 'logger', 'commonUtils', 'appConstants', 'dataCacheService'];

  function appsDataService($rootScope, $q, $http, logger, commonUtils, appConstants, dataCacheService) {
    //------------------------------------------------------------------
    //--------------------- Private Properties -------------------------
    //------------------------------------------------------------------

    var _summaryData = [];
    var _countryData = [];
    var _userData = {};

    //------------------------------------------------------------------
    //------------------------ Public  Methods -------------------------
    //------------------------------------------------------------------
    var factory = {
      getSummaryData: getSummaryData,
      getUsersData: getUsersData,
      getUsersByCountryData: getUsersByCountryData,
      getGaFeaturesList: getGaFeaturesList,
      getAppGaFeatureData: getAppGaFeatureData,
      getAppGaTrafficData: getAppGaTrafficData,
      getAppCountryTargetData: getAppCountryTargetData
    };

    return factory;

    function getSummaryData() {
      var deferred = $q.defer();
      var url = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'summary/app';
      var cache = dataCacheService.get('appSummary');
      if (cache) {
        deferred.resolve(cache);
      } else {
        $http.get(url)
          .success(function(data) {
            logger.log("appsDataService :: getSummaryData :: Success");
            dataCacheService.put('appSummary', data);
            deferred.resolve(data);
          })
          .error(function (data, status) {
            logger.log("appsDataService :: getSummaryData :: Fail");
            dispatchServiceError('getGlobalTotal');
            deferred.reject(data);
          });
      }
      return deferred.promise;
    }

    function getUsersData(appId, countryCode, year) {
      var deferred = $q.defer();
      var userData = {};
      userData[appId] = {daily: [], weekly: [], monthly: []};

      var dailyUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'summary/app/' + appId + '/daily?year='+year;
      var weeklyUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'summary/app/' + appId + '/weekly?year='+year;
      var monthlyUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'summary/app/' + appId + '/monthly?year='+year;

      var cacheKey = 'appUserData_' + appId + '_' + year;
      if(countryCode !== 'All'){
        var countryParam = '&country=' + countryCode;
        dailyUrl += countryParam;
        weeklyUrl += countryParam;
        monthlyUrl += countryParam;
        cacheKey += '_' + countryCode;
      }
      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        console.log("Returned From Cache - ", cacheKey);
        deferred.resolve(cache);
      } else {
        console.log("Couldnt find in Cache - ", cacheKey);
        $http.get(weeklyUrl).success(function (data) {
          userData[appId].weekly = data;
          $http.get(dailyUrl).success(function (data) {
            userData[appId].daily = data;
            $http.get(monthlyUrl).success(function (data) {
              userData[appId].monthly = data;
              dataCacheService.put(cacheKey, userData);
              deferred.resolve(userData);
            });
          });
        });
      }
      return deferred.promise;
    }

    function getUsersByCountryData(appId, countryCode, year) {
      var deferred = $q.defer();

      var countryUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'summary/country/app/' + appId + '?year=' + year;
      var cacheKey = ['appCountryData', appId, year].join("_");
      var countryData = {};
      if(countryCode !== 'All'){
        countryUrl += '&country=' + countryCode;
        cacheKey += '_' + countryCode;
      }
      var cache = dataCacheService.get(cacheKey);
      if (cache) {
        deferred.resolve(cache);
      } else {
        countryData[appId] = {};
        $http.get(countryUrl).success(function (data) {
          countryData[appId] = data;
          deferred.resolve(countryData);
        });
      }
      return deferred.promise;
    }

    function getGaFeaturesList(appId) {
      var deferred = $q.defer();
      var featureListUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'summary/ga/features/' + appId;
      var cacheKey = 'gaFeaturesList_' + appId;
      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        deferred.resolve(cache);
      } else {
        $http.get(featureListUrl).success(function (data) {
          dataCacheService.put(cacheKey, data);
          deferred.resolve(data);
        });
      }
      return deferred.promise;
    }

    function getAppGaFeatureData(appId, countryCode, year) {
      var deferred = $q.defer();
      var periods = ['daily', 'weekly', 'monthly'];
      var featureUrl = '';
      var featurePromises = {};
      var cacheKey = ['gaAppFeatures', appId, year, countryCode].join('_');
      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        console.log("Returning from Cache for ", appId, countryCode);
        deferred.resolve(cache);
        return deferred.promise;
      }
      periods.forEach(function(period){
        var api = ['summary/ga/app', appId, period].join('/');
        featureUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + api + '?year=' + year;
        if(countryCode !== 'All') {
          featureUrl += '&country=' + countryCode;
        }
        featurePromises[period] = $http.get(featureUrl);
      });
      $q.all(featurePromises).then(function(data){
        dataCacheService.put(cacheKey, data);
        deferred.resolve(data);
      });
      return deferred.promise;
    }

    function getAppGaTrafficData(appId, countryCode, year) {
      var deferred = $q.defer();
      var periods = ['daily', 'weekly', 'monthly'];
      var trafficUrl = '';
      var featurePromises = {};
      var cacheKey = ['gaAppTraffic', appId, year, countryCode].join('_');
      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        console.log("Returning from Cache for ", appId, countryCode);
        deferred.resolve(cache);
        return deferred.promise;
      }
      periods.forEach(function(period){
        var api = ['summary/ga/traffic', appId, period].join('/');
        trafficUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + api + '?year=' + year;
        if(countryCode !== 'All') {
          trafficUrl += '&country=' + countryCode;
        }
        featurePromises[period] = $http.get(trafficUrl);
      });
      $q.all(featurePromises).then(function(data){
        dataCacheService.put(cacheKey, data);
        deferred.resolve(data);
      });
      return deferred.promise;
    }

    function getAppCountryTargetData(appId, year) {
      var deferred = $q.defer();
      var cacheKey = ['appCountryTargets', appId, year].join('_');
      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        console.log("Returning from Cache for ", cacheKey);
        deferred.resolve(cache);
      }else{
        var apiUrl = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'summary/country/target/' + appId  + '?year=' + year;
        $http.get(apiUrl).success(function(data){
          dataCacheService.put(cacheKey, data);
          deferred.resolve(data);
        });
      }
      return deferred.promise;
    }

    //------------------------------------------------------------------
    //----------------------- #Private Methods -------------------------
    //------------------------------------------------------------------

    function dispatchServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }
  }
})();
