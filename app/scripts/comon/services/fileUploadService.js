/**
 *
 */
(function () {
  'use strict';


  angular.module('se-branding').factory('fileUploadService', fileUploadService);

  fileUploadService.$inject = ['$rootScope', '$q', 'logger', 'appConstants',
    '$http', 'commonUtils', '$timeout'];


  /**
   *
   * @param {type} $rootScope
   * @param {type} $q
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $http
   * @param {type} commonUtils
   * @param {type} $timeout
   * @returns {fileUploadService_L4.fileUploadService.factory}
   */
  function fileUploadService($rootScope, $q, logger, appConstants, $http, commonUtils, $timeout) {

    //------------------------------------------------------------------
    //--------------------- Private Properties -------------------------
    //------------------------------------------------------------------

    var _ePlanProductGrouplist = null;

    //------------------------------------------------------------------
    //------------------------ Public  Methods -------------------------
    //------------------------------------------------------------------
    var factory = {
      getEplanProductGroupList: getEplanProductGroupList,
      uploadTracePartFile: uploadTracePartFile,
      getEplanDataSummary: getEplanDataSummary
    };

    return factory;


    //------------------------------------------------------------------
    //----------------------  Public Methods ---------------------------
    //------------------------------------------------------------------

    function getEplanProductGroupList() {

      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('EPLAN_PRODUCT_GROUP_LIST_URL');
      url = url + "?isForced=true";
      $http.get(url)
              .success(function (data, status) {
                logger.log("PACE Service :: getEplanProductGrouplist :: Success ");
                _ePlanProductGrouplist = data;
                deferred.resolve(data);
              })
              .error(function (data, status) {
                logger.log("PACE Service :: getEplanProductGrouplist :: Fail ");
                _ePlanProductGrouplist = null;
                dispatchNodeGAServiceError('ePlan Product Group List');
                deferred.reject(data);
              });

      return deferred.promise;

    }



    /**
     * File Object should be stored in "uploadedFile"
     *
     * @param {Object} dataToBePost
     * @param {String} qryString
     * @returns {$q@call;defer.promise}
     */

    function uploadTracePartFile(dataToBePost, qryString) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('TRACE_PART_FILE_UPLOAD_URL');

      //Pass Form DATA as a query string Param
      //TODO:: Need to send form data as a POST param
      url = url + qryString;

      var config = {
        headers: {
        }
      };

      $http.post(url, dataToBePost, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      })
              .success(function (data) {
                alert("File Upload SUCCESS" + data);
                deferred.resolve(data);
              })
              .error(function (err) {
                alert("File Upload FAIL" + err);
                deferred.reject(err);
              });
      return deferred.promise;
    }


    function getEplanDataSummary() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('EPLAN_DATA_SUMMARY_URL');
      url = url + "?isForced=true";

      $http.get(url)
              .success(function (data, status) {
                logger.log("getEplanDataSummary :: Success ");
                deferred.resolve(data);
              })
              .error(function (data, status) {
                logger.log("getEplanDataSummary :: Fail ");
                dispatchNodeGAServiceError('ePlan Product Group List');
                deferred.reject(data);
              });

      return deferred.promise;

    }

    //------------------------------------------------------------------
    //----------------------- #Private Methods -------------------------
    //------------------------------------------------------------------

    function dispatchNodeGAServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }

  }

})();