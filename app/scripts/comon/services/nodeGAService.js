/**
 *
 */
(function () {
  'use strict';


  angular.module('se-branding').factory('nodeGAService', nodeGAService);

  nodeGAService.$inject = ['$rootScope', '$q', 'logger', 'appConstants',
    '$http', 'commonUtils'];

  /**
   *
   * @param {type} $rootScope
   * @param {type} $q
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $http
   * @param {type} commonUtils
   * @returns {nodeGAService_L4.nodeGAService.factory}
   */
  function nodeGAService($rootScope, $q, logger, appConstants, $http, commonUtils) {

    //------------------------------------------------------------------
    //--------------------- Private Properties -------------------------
    //------------------------------------------------------------------

    var _BLMReportData = null;
    var _BLMChartData = null;
    var _easyQuoteReportData = null;
    var _easyQuoteChartData = null;
    var _quickRefReportData = null;
    var _quickRefChartData = null;
    var _clipsalWishlistReportData = null;
    var _clipsalWishlistChartData = null;
    var _cadLibraryReportData = null;
    var _cadLibraryChartData = null;
    var _globalMeterReportData = null;
    var _globalMeterChartData = null;
    var _myPactReportData = null;
    var _myPactChartData = null;
    var _ecorealQuickQuotationReportData = null;
    var _ecorealQuickQuotationChartData = null;
    var _ecoreal630ReportData = null;
    var _ecoreal630ChartData = null;
    var _cbtReportData = null;
    var _cbtChartData = null;
    var _closedLoopChartData = null;
    var _ecoReachCountryList = null;
    var _ecoReachReportData = null;
    var _ecoReachChartData = null;
    var _smartSelectorsReportData = null;
    var _smartSelectorsChartData = null;
    var _berriSoftReportData = null;
    var _berriSoftChartData = null;
    var _summaryReportData = null;
    var _cccAppReportData = null;
    var _cccAppChartData = null;
    var _meaReportData = null;
    var _meaChartData = null;
    var _registeredUsersData = null;
    var _chartData = null;
    var _maxdate = null;
    var _mynovabuddyAppReportData = null;
    var _mynovabuddyAppChartData = null;


    //------------------------------------------------------------------
    //------------------------ Public  Methods -------------------------
    //------------------------------------------------------------------
    var factory = {
      getBLMReportData: getBLMReportData,
      getBLMChartData: getBLMChartData,
      getEasyQuoteReportData: getEasyQuoteReportData,
      getEasyQuoteChartData: getEasyQuoteChartData,
      getQuickRefReportData: getQuickRefReportData,
      getQuickRefChartData: getQuickRefChartData,
      getClipsalWishlistReportData: getClipsalWishlistReportData,
      getClipsalWishlistChartData: getClipsalWishlistChartData,
      getCadLibraryReportData: getCadLibraryReportData,
      getCadLibraryChartData: getCadLibraryChartData,
      getGlobalMeterReportData: getGlobalMeterReportData,
      getGlobalMeterChartData: getGlobalMeterChartData,
      getMyPactReportData: getMyPactReportData,
      getMyPactChartData: getMyPactChartData,
      getEcorealQuickQuotationReportData: getEcorealQuickQuotationReportData,
      getEcorealQuickQuotationChartData: getEcorealQuickQuotationChartData,
      getEcoreal630ReportData: getEcoreal630ReportData,
      getEcoreal630ChartData: getEcoreal630ChartData,
      getCbtReportData: getCbtReportData,
      getCbtChartData: getCbtChartData,
      getSmartSelectorsReportData: getSmartSelectorsReportData,
      getSmartSelectorsChartData: getSmartSelectorsChartData,
      getBerriSoftReportData: getBerriSoftReportData,
      getBerriSoftChartData: getBerriSoftChartData,
      getSummaryReportData: getSummaryReportData,
      getClosedLoopReportData: getClosedLoopReportData,
      getClosedLoopChartData: getClosedLoopChartData,
      getEcoReachCountryList: getEcoReachCountryList,
      getEcoReachReportData: getEcoReachReportData,
      getEcoReachGAReportData: getEcoReachGAReportData,
      getEcoReachChartData: getEcoReachChartData,
      getEcoReachGAChartData: getEcoReachGAChartData,
      getCCCAppReportData: getCCCAppReportData,
      getCCCAppChartData: getCCCAppChartData,
      getMEAReportData: getMEAReportData,
      getMEAChartData: getMEAChartData,
      getRegisteredUsersData: getRegisteredUsersData,
      getChartData: getChartData,
      getRegisteredUsersMaxCreationDate: getRegisteredUsersMaxCreationDate,
      getMyNovaBuddyAppReportData: getMyNovaBuddyAppReportData,
      getMyNovaBuddyAppChartData: getMyNovaBuddyAppChartData
    };

    return factory;


    //------------------------------------------------------------------
    //----------------------  Public Methods ---------------------------
    //------------------------------------------------------------------


    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getBLMReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('BLM_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getBLMReportData :: Success");
                _BLMReportData = data;
                deferred.resolve(_BLMReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getBLMReportData :: Fail");
                _BLMReportData = null;
                dispatchNodeGAServiceError('BLMReportData');
                deferred.reject(_BLMReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getBLMChartData() {

      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('BLM_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getBLMChartData :: Success");
                _BLMChartData = data;
                deferred.resolve(_BLMChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getBLMChartData :: Fail");
                _BLMChartData = null;
                dispatchNodeGAServiceError('BLMChartData');
                deferred.reject(_BLMChartData);
              });
      return deferred.promise;

    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getEasyQuoteReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('EASY_QUOTE_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEasyQuoteReportData :: Success");
                _easyQuoteReportData = data;
                deferred.resolve(_easyQuoteReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEasyQuoteReportData :: Fail");
                _easyQuoteReportData = null;
                dispatchNodeGAServiceError('EasyQuoteReportData');
                deferred.reject(_easyQuoteReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getEasyQuoteChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('EASY_QUOTE_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEasyQuoteChartData :: Success");
                _easyQuoteChartData = data;
                deferred.resolve(_easyQuoteChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEasyQuoteChartData :: Fail");
                _easyQuoteChartData = null;
                dispatchNodeGAServiceError('EasyQuoteChartData');
                deferred.reject(_easyQuoteChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getQuickRefReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('QUICK_REF_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getQuickRefReportData :: Success");
                _quickRefReportData = data;
                deferred.resolve(_quickRefReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getQuickRefReportData :: Fail");
                _quickRefReportData = null;
                dispatchNodeGAServiceError('QuickRefReportData');
                deferred.reject(_quickRefReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getQuickRefChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('QUICK_REF_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getQuickRefChartData :: Success");
                _quickRefChartData = data;
                deferred.resolve(_quickRefChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getQuickRefChartData :: Fail");
                _quickRefChartData = null;
                dispatchNodeGAServiceError('QuickRefChartData');
                deferred.reject(_quickRefChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getClipsalWishlistReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CLIPSAL_WISHLIST_REPORT_DATA_URL');
//      url += '?isForced=true';
      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getClipsalWishlistReportData :: Success");
                _clipsalWishlistReportData = data;
                deferred.resolve(_clipsalWishlistReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getClipsalWishlistReportData :: Fail");
                _clipsalWishlistReportData = null;
                dispatchNodeGAServiceError('ClipsalWishlistReportData');
                deferred.reject(_clipsalWishlistReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getClipsalWishlistChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CLIPSAL_WISHLIST_CHART_DATA_URL');
//      url += '?isForced=true';
      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getClipsalWishlistChartData :: Success");
                _clipsalWishlistChartData = data;
                deferred.resolve(_clipsalWishlistChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getClipsalWishlistChartData :: Fail");
                _clipsalWishlistChartData = null;
                dispatchNodeGAServiceError('ClipsalWishlistChartData');
                deferred.reject(_clipsalWishlistChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getCadLibraryReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CAD_LIBRARY_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getCadLibraryReportData :: Success");
                _cadLibraryReportData = data;
                deferred.resolve(_cadLibraryReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getCadLibraryReportData :: Fail");
                _cadLibraryReportData = null;
                dispatchNodeGAServiceError('CadLibraryReportData');
                deferred.reject(_cadLibraryReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getCadLibraryChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CAD_LIBRARY_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getCadLibraryChartData :: Success");
                _cadLibraryChartData = data;
                deferred.resolve(_cadLibraryChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getCadLibraryChartData :: Fail");
                _cadLibraryChartData = null;
                dispatchNodeGAServiceError('CadLibraryChartData');
                deferred.reject(_cadLibraryChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getGlobalMeterReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('GLOBAL_METER_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getGlobalMeterReportData :: Success");
                _globalMeterReportData = data;
                deferred.resolve(_globalMeterReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getGlobalMeterReportData :: Fail");
                _globalMeterReportData = null;
                dispatchNodeGAServiceError('GlobalMeterReportData');
                deferred.reject(_globalMeterReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getGlobalMeterChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('GLOBAL_METER_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getGlobalMeterChartData :: Success");
                _globalMeterChartData = data;
                deferred.resolve(_globalMeterChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getGlobalMeterChartData :: Fail");
                _globalMeterChartData = null;
                dispatchNodeGAServiceError('GlobalMeterChartData');
                deferred.reject(_globalMeterChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getMyPactReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('MY_PACT_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getMyPactReportData :: Success");
                _myPactReportData = data;
                deferred.resolve(_myPactReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getMyPactReportData :: Fail");
                _myPactReportData = null;
                dispatchNodeGAServiceError('MyPactReportData');
                deferred.reject(_myPactReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getMyPactChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('MY_PACT_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getMyPactChartData :: Success");
                _myPactChartData = data;
                deferred.resolve(_myPactChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getMyPactChartData :: Fail");
                _myPactChartData = null;
                dispatchNodeGAServiceError('MyPactChartData');
                deferred.reject(_myPactChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getEcorealQuickQuotationReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECOREAL_QUICK_QUOTATION_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcorealQuickQuotationReportData :: Success");
                _ecorealQuickQuotationReportData = data;
                deferred.resolve(_ecorealQuickQuotationReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcorealQuickQuotationReportData :: Fail");
                _ecorealQuickQuotationReportData = null;
                dispatchNodeGAServiceError('EcorealQuickQuotationReportData');
                deferred.reject(_ecorealQuickQuotationReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getEcorealQuickQuotationChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECOREAL_QUICK_QUOTATION_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcorealQuickQuotationChartData :: Success");
                _ecorealQuickQuotationChartData = data;
                deferred.resolve(_ecorealQuickQuotationChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcorealQuickQuotationChartData :: Fail");
                _ecorealQuickQuotationChartData = null;
                dispatchNodeGAServiceError('EcorealQuickQuotationChartData');
                deferred.reject(_ecorealQuickQuotationChartData);
              });
      return deferred.promise;
    }


    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getEcoreal630ReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECOREAL_630_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcoreal630ReportData :: Success");
                _ecoreal630ReportData = data;
                deferred.resolve(_ecoreal630ReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcoreal630ReportData :: Fail");
                _ecoreal630ReportData = null;
                dispatchNodeGAServiceError('Ecoreal630ReportData');
                deferred.reject(_ecoreal630ReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getEcoreal630ChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECOREAL_630_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcoreal630ChartData :: Success");
                _ecoreal630ChartData = data;
                deferred.resolve(_ecoreal630ChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcoreal630ChartData :: Fail");
                _ecoreal630ChartData = null;
                dispatchNodeGAServiceError('Ecoreal630ChartData');
                deferred.reject(_ecoreal630ChartData);
              });
      return deferred.promise;
    }


    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getCbtReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CBT_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getCbtReportData :: Success");
                _cbtReportData = data;
                deferred.resolve(_cbtReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getCbtReportData :: Fail");
                _cbtReportData = null;
                dispatchNodeGAServiceError('CbtReportData');
                deferred.reject(_cbtReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getCbtChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CBT_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getCbtChartData :: Success");
                _cbtChartData = data;
                deferred.resolve(_cbtChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getCbtChartData :: Fail");
                _cbtChartData = null;
                dispatchNodeGAServiceError('CbtChartData');
                deferred.reject(_cbtChartData);
              });
      return deferred.promise;
    }



    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getSmartSelectorsReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('SMART_SELECTORS_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getSmartSelectorsReportData :: Success");
                _smartSelectorsReportData = data;
                deferred.resolve(_smartSelectorsReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getSmartSelectorsReportData :: Fail");
                _smartSelectorsReportData = null;
                dispatchNodeGAServiceError('getSmartSelectorsReportData');
                deferred.reject(_smartSelectorsReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getSmartSelectorsChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('SMART_SELECTORS_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getSmartSelectorsChartData :: Success");
                _smartSelectorsChartData = data;
                deferred.resolve(_smartSelectorsChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getSmartSelectorsChartData :: Fail");
                _smartSelectorsChartData = null;
                dispatchNodeGAServiceError('SmartSelectorsChartData');
                deferred.reject(_smartSelectorsChartData);
              });
      return deferred.promise;
    }



    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getBerriSoftReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('BERRI_SOFT_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getBerriSoftReportData :: Success");
                _berriSoftReportData = data;
                deferred.resolve(_berriSoftReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getSmartSelectorsReportData :: Fail");
                _berriSoftReportData = null;
                dispatchNodeGAServiceError('getBerriSoftReportData');
                deferred.reject(_berriSoftReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getBerriSoftChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('BERRI_SOFT_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getBerriSoftChartData :: Success");
                _berriSoftChartData = data;
                deferred.resolve(_berriSoftChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getBerriSoftChartData :: Fail");
                _berriSoftChartData = null;
                dispatchNodeGAServiceError('getBerriSoftChartData');
                deferred.reject(_berriSoftChartData);
              });
      return deferred.promise;
    }










    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getClosedLoopReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CLOSED_LOOP_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getCbtReportData :: Success");
                _cbtReportData = data;
                deferred.resolve(_cbtReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getCbtReportData :: Fail");
                _cbtReportData = null;
                dispatchNodeGAServiceError('CbtReportData');
                deferred.reject(_cbtReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getClosedLoopChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CLOSED_LOOP_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getClosedLoopChartData :: Success");
                _closedLoopChartData = data;
                deferred.resolve(_closedLoopChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getClosedLoopChartData :: Fail");
                _closedLoopChartData = null;
                dispatchNodeGAServiceError('getClosedLoopChartData');
                deferred.reject(_closedLoopChartData);
              });
      return deferred.promise;
    }



    /**
     *
     * @param {type} isForce
     * @returns {Array}
     */
    function getEcoReachCountryList(isForce) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECO_REACH_COUNTRY_LIST_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };

      if (isForce !== undefined && isForce !== null && isForce === true) {
//        url += "?isForced=true";
      }

      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachCountryList :: Success");
                _ecoReachCountryList = data;
                deferred.resolve(_ecoReachCountryList);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachCountryList :: Fail");
                _ecoReachCountryList = null;
                dispatchNodeGAServiceError('getEcoReachCountryList');
                deferred.reject(_ecoReachCountryList);
              });
      return deferred.promise;
    }


    /**
     *
     * @param {type} isForce
     * @returns {$q@call;defer.promise}
     */
    function getEcoReachReportData(isForce) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECO_REACH_REPORT_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };

      if (isForce !== undefined && isForce !== null && isForce === true) {
//        url += "?isForced=true";
      }

      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachReportData :: Success");
                _ecoReachReportData = data;
                deferred.resolve(_ecoReachReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachReportData :: Fail");
                _ecoReachReportData = null;
                dispatchNodeGAServiceError('getEcoReachReportData');
                deferred.reject(_ecoReachReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @param {type} isForce
     * @returns {$q@call;defer.promise}
     */
    function getEcoReachGAReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECO_REACH_GA_REPORT_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };

//      url += "?isForced=true";

      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachReportData :: Success");
                _ecoReachReportData = data;
                deferred.resolve(_ecoReachReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachReportData :: Fail");
                _ecoReachReportData = null;
                dispatchNodeGAServiceError('getEcoReachReportData');
                deferred.reject(_ecoReachReportData);
              });
      return deferred.promise;
    }


    /**
     *
     * @param {type} isForce
     * @returns {$q@call;defer.promise}
     */
    function getEcoReachChartData(isForce) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECO_REACH_CHART_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      if (isForce !== undefined && isForce !== null && isForce === true) {
//        url += "?isForced=true";
      }
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachChartData :: Success");
                _ecoReachChartData = data;
                deferred.resolve(_ecoReachChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachChartData :: Fail");
                _ecoReachChartData = null;
                dispatchNodeGAServiceError('getEcoReachChartData');
                deferred.reject(_ecoReachChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @param {type} isForce
     * @returns {$q@call;defer.promise}
     */
    function getEcoReachGAChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('ECO_REACH_GA_CHART_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
//      url += "?isForced=true";

      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachChartData :: Success");
                _ecoReachChartData = data;
                deferred.resolve(_ecoReachChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getEcoReachChartData :: Fail");
                _ecoReachChartData = null;
                dispatchNodeGAServiceError('getEcoReachChartData');
                deferred.reject(_ecoReachChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getCCCAppReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CCC_APP_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getCCCAppReportData :: Success");
                _cccAppReportData = data;
                deferred.resolve(_cccAppReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getCCCAppReportData :: Fail");
                _cccAppReportData = null;
                dispatchNodeGAServiceError('getCCCAppReportData');
                deferred.reject(_cccAppReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getCCCAppChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CCC_APP_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getCCCAppChartData :: Success");
                _cccAppChartData = data;
                deferred.resolve(_cccAppChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getCCCAppChartData :: Fail");
                _cccAppChartData = null;
                dispatchNodeGAServiceError('getCCCAppChartData');
                deferred.reject(_cccAppChartData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getMEAReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('MEA_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getMEAReportData :: Success");
                _meaReportData = data;
                deferred.resolve(_meaReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getMEAReportData :: Fail");
                _meaReportData = null;
                dispatchNodeGAServiceError('getMEAReportData');
                deferred.reject(_meaReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getMEAChartData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('MEA_CHART_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getMEAChartData :: Success");
                _meaChartData = data;
                deferred.resolve(_meaChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getMEAChartData :: Fail");
                _meaChartData = null;
                dispatchNodeGAServiceError('getMEAChartData');
                deferred.reject(_meaChartData);
              });
      return deferred.promise;
    }
    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getMyNovaBuddyAppReportData(device) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('MY_NOVA_BUDDY_APP_REPORT_DATA_URL');
      url += "/" + device;
//      "/?isForced=true";

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getMyNovaBuddyAppReportData :: Success");
                _mynovabuddyAppReportData = data;
                deferred.resolve(_mynovabuddyAppReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getMyNovaBuddyAppReportData :: Fail");
                _mynovabuddyAppReportData = null;
                dispatchNodeGAServiceError('getMyNovaBuddyAppReportData');
                deferred.reject(_mynovabuddyAppReportData);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getMyNovaBuddyAppChartData(device) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('MY_NOVA_BUDDY_APP_CHART_DATA_URL');
      url += "/" + device;
//      "/?isForced=true";
      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getMyNovaBuddyAppChartData :: Success");
                _mynovabuddyAppChartData = data;
                deferred.resolve(_mynovabuddyAppChartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getMyNovaBuddyAppChartData :: Fail");
                _mynovabuddyAppChartData = null;
                dispatchNodeGAServiceError('getMyNovaBuddyAppChartData');
                deferred.reject(_mynovabuddyAppChartData);
              });
      return deferred.promise;
    }

    //

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getRegisteredUsersData(restfulurl) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL(restfulurl);
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getRegisteredUsersData :: Success");
                _registeredUsersData = data;
                deferred.resolve(_registeredUsersData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getRegisteredUsersData :: Fail");
                _registeredUsersData = null;
                dispatchNodeGAServiceError('getRegisteredUsersData');
                deferred.reject(_registeredUsersData);
              });
      return deferred.promise;
    }

    /**
     * @param {string} charttype chart type -> DAY, WEEK, MONTH
     * @returns {$q@call;defer.promise}
     */
    function getChartData(charturl) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL(charturl);
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getChartData :: Success");
                _chartData = data;
                deferred.resolve(_chartData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getChartData :: Fail");
                _chartData = null;
                dispatchNodeGAServiceError('getChartData');
                deferred.reject(data);
              });
      return deferred.promise;
    }

    function getRegisteredUsersMaxCreationDate(filter) {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('REGISTERED_USERS_MAX_DATE_URL');
//      url += '?isForced=true&filter=' + filter;
      url += '?filter=' + filter;
      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getRegisteredUsersMaxDate :: Success");
                _maxdate = data;
                deferred.resolve(_maxdate);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getRegisteredUsersMaxDate :: Fail");
                _maxdate = null;
                dispatchNodeGAServiceError('getRegisteredUsersMaxDate');
                deferred.reject(data);
              });
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getSummaryReportData() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('SUMMARY_REPORT_DATA_URL');
//      url += '?isForced=true';

      var config = {
        headers: {
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("nodeGAService Service :: getSummaryReportData :: Success");
                _summaryReportData = data;
                deferred.resolve(_summaryReportData);
              })
              .error(function (data, status) {
                logger.log("nodeGAService Service :: getSummaryReportData :: Fail");
                _summaryReportData = null;
                dispatchNodeGAServiceError('SummaryReportData');
                deferred.reject(_summaryReportData);
              });
      return deferred.promise;
    }

    //------------------------------------------------------------------
    //----------------------- #Private Methods -------------------------
    //------------------------------------------------------------------

    function dispatchNodeGAServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }

  }

})();