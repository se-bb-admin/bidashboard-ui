/**
 *
 */
(function () {
  'use strict';


  angular.module('se-branding').factory('globalSummaryService', globalSummaryService);

  globalSummaryService.$inject = ['$rootScope', '$q', '$http', 'logger', 'commonUtils', 'appConstants', 'dataCacheService'];

  function globalSummaryService($rootScope, $q, $http, logger, commonUtils, appConstants, dataCacheService) {
    //------------------------------------------------------------------
    //--------------------- Private Properties -------------------------
    //------------------------------------------------------------------

    // var _globalTotal = [];
    // var _summaryData = [];
    // var _countryData = [];
    // var _appsData = {};
    var _lastUpdatedDate = '';

    //------------------------------------------------------------------
    //------------------------ Public  Methods -------------------------
    //------------------------------------------------------------------
    var factory = {
      getGlobalTotal: getGlobalTotal,
      getRegistrationData: getRegistrationData,
      getCountryData: getCountryData,
      getAppData: getAppData,
      getAppGroups: getAppGroups,
      getLastUpdatedDate: getLastUpdatedDate,
      getBomSummary: getBomSummary,
      getAppConfig: getAppConfig
    };

    return factory;

    function getGlobalTotal() {
      var deferred = $q.defer();
      var url = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'registrations/global';
      var cache = dataCacheService.get('sumGlobalTotal');

      if (cache) {
        deferred.resolve(cache);
      } else {
        $http.get(url)
          .success(function (data, status) {
            logger.log("globalSummaryService :: getGlobalTotal :: Success");
            dataCacheService.put('sumGlobalTotal', data);
            $rootScope.lastUpdatedDate = data.lastUpdatedDate;
            deferred.resolve(data);
          })
          .error(function (data, status) {
            logger.log("globalSummaryService :: getGlobalTotal :: Fail");
            dispatchServiceError('getGlobalTotal');
            deferred.reject(data);
          });
      }

      return deferred.promise;
    }

    function getLastUpdatedDate() {
      return _lastUpdatedDate;
    }

    /**
     *
     * @param {type} period
     * @param {type} year
     * @returns {.$q@call;defer.promise}
     */
    function getRegistrationData(period, year) {
      var deferred = $q.defer();
      var url = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'registrations/global/' + period + '?year=' + year;
      var config = {
        headers: {}
      };

      var cacheKey = 'sumRegData_' + period + '_' + year;

      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        deferred.resolve(cache);
      } else {
//      $http.get(url, config)
        $http.get(url)
          .success(function (data, status) {
            logger.log("globalSummaryService :: getRegistrationData :: Success");
            dataCacheService.put(cacheKey, data);
            deferred.resolve(data);
          })
          .error(function (data, status) {
            logger.log("globalSummaryService :: getRegistrationData :: Fail");
            dispatchServiceError('getRegistrationData');
            deferred.reject(data);
          });
      }
      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise|deferred.promise}
     */
    function getCountryData(year) {
      var deferred = $q.defer();
      var url = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'registrations/country';
      if (year !== undefined && year !== 'All') {
        url += "?year=" + year;
      }
      var config = {
        headers: {}
      };

      var cacheKey = 'sumCountryData_' + year;

      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        deferred.resolve(cache);
      } else {
        $http.get(url, config)
          .success(function (data, status) {
            logger.log("globalSummaryService :: getCountryData :: Success");
            dataCacheService.put(cacheKey, data);
            deferred.resolve(data);
          })
          .error(function (data, status) {
            logger.log("globalSummaryService :: getCountryData :: Fail");
            dispatchServiceError('getCountryData');
            deferred.reject(data);
          });
      }

      return deferred.promise;
    }

    /**
     *
     * @returns {.$q@call;defer.promise}
     */
    function getAppData(year) {
      var deferred = $q.defer();
      var url = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'registrations/users/app?year=' + year;

      var config = {
        headers: {}
      };

      var cacheKey = 'sumAppData_' + year;

      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        deferred.resolve(cache);
      } else {
        $http.get(url, config)
          .success(function (data, status) {
            logger.log("globalSummaryService :: getAppsData :: Success");
            dataCacheService.put(cacheKey, data);
            deferred.resolve(data);
          })
          .error(function (data, status) {
            logger.log("globalSummaryService :: getAppsData :: Fail");
            dispatchServiceError('getAppsData');
            deferred.reject(data);
          });
      }
      return deferred.promise;
    }

    function getAppGroups() {
      var deferred = $q.defer();
      var dataPath = "bidata/appGroups.json";
      $http.get(dataPath).success(function (data) {
        deferred.resolve(data);
      });
      return deferred.promise;
    }

    function getAppConfig() {
      var deferred = $q.defer();
      var dataPath = "bidata/appConfig.json";
      $http.get(dataPath).success(function (data) {
        deferred.resolve(data);
      });
      return deferred.promise;
    }

    function getBomSummary(period, year){
      var deferred = $q.defer();
      var url = appConstants.NODE_REST_DOMAIN[$rootScope.env].BASE_DOMAIN + 'summary/bom/'+ period;
      if(year !== undefined){
        url += '?year=' + year;
      }
      var cacheKey = 'bomData_' + period;
      if(year !== undefined){
        cacheKey += '_' + year;
      }
      var cache = dataCacheService.get(cacheKey);

      if (cache) {
        deferred.resolve(cache);
      } else {
        $http.get(url)
          .success(function (data, status) {
            logger.log("globalSummaryService :: getBomSummary :: Success");
            dataCacheService.put(cacheKey, data);
            deferred.resolve(data);
          })
          .error(function (data, status) {
            logger.log("globalSummaryService :: getBomSummary :: Fail");
            dispatchServiceError('getAppsData');
            deferred.reject(data);
          });
      }
      return deferred.promise;
    }

    //------------------------------------------------------------------
    //----------------------- #Private Methods -------------------------
    //------------------------------------------------------------------

    function dispatchServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }
  }
})();
