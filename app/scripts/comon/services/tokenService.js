/**
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').factory('tokenService', tokenService);

  tokenService.$inject = ['$rootScope', '$q', 'logger', 'appConstants',
    '$http', 'commonUtils'];


  /**
   *
   * @param {type} $rootScope
   * @param {type} $q
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $http
   * @param {type} commonUtils
   * @returns {tokenService_L4.tokenService.factory}
   */
  function tokenService($rootScope, $q, logger, appConstants, $http, commonUtils) {



    var factory = {
      getToken: getToken,
      refreshToken: refreshToken
    };

    return factory;



    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getToken() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('GET_TOKEN_URL');

      $http.get(url)
              .success(function (data, status) {
                logger.log("Token Service :: getToken :: Success ");
                deferred.resolve(data);
              })
              .error(function (data, status) {
                logger.log("Token Service :: getToken :: Fail ");
                dispatchTokenServiceError('getToken');
                deferred.reject(data);
              });

      return deferred.promise;
    }




    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function refreshToken() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('REFRESH_TOKEN_URL');
      $http.get(url)
              .success(function (data, status) {
                logger.log("Token Service :: refreshToken :: Success ");
                deferred.resolve(data);
              })
              .error(function (data, status) {
                logger.log("Token Service :: refreshToken :: Fail ");
                dispatchTokenServiceError('refreshToken');
                deferred.reject(data);
              });

      return deferred.promise;
    }





    //------------------------------------------------------------------
    //----------------------- #Private Methods -------------------------
    //------------------------------------------------------------------

    function dispatchTokenServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }

  }

})();