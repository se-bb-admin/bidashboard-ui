/**
 * Created by SESA431390 on 3/30/2017.
 */
'use strict';


angular.module('se-branding').factory('dataCacheService', ['$cacheFactory', function ($cacheFactory) {
  return $cacheFactory("bidata");
}]);
