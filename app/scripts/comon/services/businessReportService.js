/**
 *
 */
(function () {
	'use strict';

	angular.module('se-branding').factory('businessReportService', businessReportService);

	businessReportService.$inject = ['$rootScope', '$q', 'logger', 'appConstants',
		'$http', 'commonUtils'];

	/**
	 *
	 * @param {type} $rootScope
	 * @param {type} $q
	 * @param {type} logger
	 * @param {type} appConstants
	 * @param {type} $http
	 * @param {type} commonUtils
	 * @returns {businessReportService_L4.businessReportService.factory}
	 */
	function businessReportService($rootScope, $q, logger, appConstants,
			$http, commonUtils) {

		/**
		 * Local properties
		 */
		var _registeredUsers = null;




		var factory = {
			getEplanCountryList: getEplanCountryList,
			getEplanCompanyWiseReportData: getEplanCompanyWiseReportData,
			getEplanCompanyWiseGrandTotalDownloads: getEplanCompanyWiseGrandTotalDownloads,
			getEplanCompanyCounts: getEplanCompanyCounts,
			getEplanProductGroupList: getEplanProductGroupList,
			getEplanProductGroupTypes: getEplanProductGroupTypes,
			getEplanProductGroupReportData: getEplanProductGroupReportData,
			getEplanProductGroupGrandTotalDownloads: getEplanProductGroupGrandTotalDownloads,
			getEplanProductGroupCounts: getEplanProductGroupCounts,
			getBIMProductList: getBIMProductList,
			getBIMCountryList: getBIMCountryList,
			getBIMGeneralDownloadReportData: getBIMGeneralDownloadReportData,
			getBIMViewDownloadReportData: getBIMViewDownloadReportData,
			getBIMUserCountryList: getBIMUserCountryList,
			getBIMUsersDownloadReportData: getBIMUsersDownloadReportData,
			getBIMCompanyWiseGrandTotalDownloads: getBIMCompanyWiseGrandTotalDownloads,
			getMnMCountryList: getMnMCountryList,
			getMnMReportData: getMnMReportData,
			// ad-hoc data
			getMnMAdhocData: getMnMAdhocData,
			getMnMAdhocQuarterlyData: getMnMAdhocQuarterlyData,

			/* traceparts functions */
			getTracepartsTrendCountries: getTracepartsTrendCountries,
			getTracepartsTotals: getTracepartsTotals,
			getTracepartsDownloadList: getTracepartsDownloadList,
			getTracepartsTopTenProductDownloads: getTracepartsTopTenProductDownloads,
			// trend chart functions
			getTracepartsTrendTopTenProducts: getTracepartsTrendTopTenProducts,
			getTracepartsTrendTopTenCountries: getTracepartsTrendTopTenCountries,
			getTracepartsTrendDownloads: getTracepartsTrendDownloads,
			getTracepartsTrendDownloadsAndUsers: getTracepartsTrendDownloadsAndUsers
					/* END: traceparts functions */
		};

		return factory;


		/**
		 *
		 * @param {type} isForce
		 * @returns {$q@call;defer.promise}
		 */
		function getEplanCountryList(isForce) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_COUNTRY_LIST_URL');

			if (isForce !== undefined && isForce !== null && isForce === true) {
				url += "?isForced=true";
			}
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCountryList :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCountryList :: Fail ");
						dispatchServiceError('RegisteredUsers');
						deferred.reject(data);
					});

			return deferred.promise;
		}



		/**
		 *
		 * countryCode=all&month=all&year=2015&&filterBy=country
		 *
		 * @param {type} countryCode
		 * @param {type} month
		 * @param {type} year
		 * @param {type} filterBy
		 * @returns {$q@call;defer.promise}
		 */
		function getEplanCompanyWiseReportData(countryCode, month, year, filterBy) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_COMPANY_WISE_REPORT_URL');

			//Add QueryString Param.
			if (filterBy !== undefined && filterBy !== null && filterBy !== "") {
				url += "?countryCode=" + countryCode + "&month=" + month + "&year=" + year + "&filterBy=" + filterBy + "&isForced=true";
			} else {
				url += "?countryCode=" + countryCode + "&month=" + month + "&year=" + year + "&isForced=true";
			}

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Fail ");
						dispatchServiceError('EplanCompanyWiseReportData');
						deferred.reject(data);
					});

			return deferred.promise;
		}



		/**
		 *
		 * @param {type} countryCode
		 * @returns {$q@call;defer.promise}
		 */
		function getEplanCompanyWiseGrandTotalDownloads(countryCode) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_COMPANY_WISE_LIFETIMT_DOWNLOADS_URL');

			url += "?countryCode=" + countryCode + "&isForced=true";

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseGrandTotalDownloads :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseGrandTotalDownloads :: Fail ");
						dispatchServiceError('getEplanCompanyWiseGrandTotalDownloads');
						deferred.reject(data);
					});

			return deferred.promise;
		}



		/**
		 *
		 * @param {type} countryCode
		 * @param {type} month
		 * @param {type} year
		 * @returns {undefined}
		 */
		function getEplanCompanyCounts(countryCode, month, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_COMPANY_COUNT_URL');

			url += "?countryCode=" + countryCode + "&month=" + month + "&year=" + year + "&isForced=true";

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Fail ");
						dispatchServiceError('EplanCompanyWiseReportData');
						deferred.reject(data);
					});

			return deferred.promise;
		}


		/**
		 *
		 * @returns {$q@call;defer.promise}
		 */
		function getEplanProductGroupList() {

			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_PRODUCT_GROUP_LIST_URL');
			url = url + "?isForced=true";

			$http.get(url)
					.success(function (data, status) {
						logger.log("PACE Service :: getEplanProductGrouplist :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("PACE Service :: getEplanProductGrouplist :: Fail ");
						dispatchNodeGAServiceError('ePlan Product Group List');
						deferred.reject(data);
					});

			return deferred.promise;

		}

		function getEplanProductGroupTypes() {

			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_PRODUCT_GROUP_TYPES_URL');
			url = url + "?isForced=true";

			$http.get(url)
					.success(function (data, status) {
						logger.log("Service :: getEplanProductGroupTypes :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Service :: getEplanProductGrouplist :: Fail ");
						dispatchServiceError('ePlan Product Group List');
						deferred.reject(data);
					});

			return deferred.promise;

		}


		/**
		 *
		 * @param {type} productGroup
		 * @param {type} month
		 * @param {type} year
		 * @returns {$q@call;defer.promise}
		 */
		function getEplanProductGroupReportData(productGroup, month, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_PRODUCT_GROUP_REPORT_URL');

			url += "?productGroup=" + productGroup + "&month=" + month + "&year=" + year + "&isForced=true";

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Fail ");
						dispatchServiceError('EplanCompanyWiseReportData');
						deferred.reject(data);
					});

			return deferred.promise;
		}




		/**
		 *
		 * @param {type} productGroup
		 * @returns {$q@call;defer.promise}
		 */
		function getEplanProductGroupGrandTotalDownloads(productGroup) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_PRODUCT_GROUP_LIFETIME_DOWNLOADS_URL');

			url += "?productGroup=" + productGroup + "&isForced=true";

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseGrandTotalDownloads :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseGrandTotalDownloads :: Fail ");
						dispatchServiceError('getEplanCompanyWiseGrandTotalDownloads');
						deferred.reject(data);
					});

			return deferred.promise;
		}



		/**
		 *
		 * @param {type} productGroup
		 * @param {type} month
		 * @param {type} year
		 * @returns {undefined}
		 */
		function getEplanProductGroupCounts(productGroup, month, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('EPLAN_PRODUCT_GROUP_COUNT_URL');

			url += "?productGroup=" + productGroup + "&month=" + month + "&year=" + year + "&isForced=true";

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanProductGroupCounts :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanProductGroupCounts :: Fail ");
						dispatchServiceError('getEplanProductGroupCounts');
						deferred.reject(data);
					});

			return deferred.promise;
		}


		/**
		 *
		 * @param {type} isForce
		 * @returns {$q@call;defer.promise}
		 */
		function getBIMProductList(isForce) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('BIM_PRODUCT_LIST_URL');


			if (isForce !== undefined && isForce !== null && isForce === true) {
				url += "?isForced=true";
			}

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCountryList :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCountryList :: Fail ");
						dispatchServiceError('RegisteredUsers');
						deferred.reject(data);
					});

			return deferred.promise;
		}





		/**
		 *
		 * @param {type} isForce
		 * @returns {$q@call;defer.promise}
		 */
		function getBIMCountryList(isForce) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('BIM_COUNTRY_LIST_URL');

			if (isForce !== undefined && isForce !== null && isForce === true) {
				url += "?isForced=true";
			}
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCountryList :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCountryList :: Fail ");
						dispatchServiceError('RegisteredUsers');
						deferred.reject(data);
					});

			return deferred.promise;
		}




		/**
		 *
		 * country=all&product=all&month=all&year=2015&&filterBy=country
		 *
		 * @param {type} country
		 * @param {type} product
		 * @param {type} month
		 * @param {type} year
		 * @returns {$q@call;defer.promise}
		 */
		function getBIMGeneralDownloadReportData(country, product, month, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('BIM_GENERAL_DOWNLOAD_REPORT_URL');

			//Add QueryString Param.
			url += "?country=" + country + "&product=" + product + "&month=" + month + "&year=" + year;

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Fail ");
						dispatchServiceError('EplanCompanyWiseReportData');
						deferred.reject(data);
					});

			return deferred.promise;
		}



		/**
		 *
		 * month=all&year=2015
		 *
		 * @param {type} month
		 * @param {type} year
		 * @returns {$q@call;defer.promise}
		 */
		function getBIMViewDownloadReportData(month, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('BIM_VIEW_DOWNLOAD_REPORT_URL');

			//Add QueryString Param.
			url += "?month=" + month + "&year=" + year;

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Fail ");
						dispatchServiceError('EplanCompanyWiseReportData');
						deferred.reject(data);
					});

			return deferred.promise;
		}




		/**
		 *
		 * @param {type} isForce
		 * @returns {$q@call;defer.promise}
		 */
		function getBIMUserCountryList(isForce) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('BIM_USER_COUNTRY_LIST_URL');

			if (isForce !== undefined && isForce !== null && isForce === true) {
				url += "?isForced=true";
			}
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCountryList :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCountryList :: Fail ");
						dispatchServiceError('RegisteredUsers');
						deferred.reject(data);
					});

			return deferred.promise;
		}



		/**
		 *
		 * country=all
		 *
		 * @param {type} country
		 * @returns {$q@call;defer.promise}
		 */
		function getBIMUsersDownloadReportData(country) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('BIM_USER_DOWNLOAD_REPORT_URL');

			//Add QueryString Param.
			url += "?country=" + country;

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getEplanCompanyWiseReportData :: Fail ");
						dispatchServiceError('EplanCompanyWiseReportData');
						deferred.reject(data);
					});

			return deferred.promise;
		}


		/**
		 *
		 * @param {type} country
		 * @param {type} product
		 * @returns {$q@call;defer.promise}
		 */
		function getBIMCompanyWiseGrandTotalDownloads(country, product) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('BIM_GENERAL_DOWNLOAD_LIFETIME_DOWNLOADS_URL');

			//Add QueryString Param.
			url += "?country=" + country + "&product=" + product;

			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getBIMCompanyWiseGrandTotalDownloads :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getBIMCompanyWiseGrandTotalDownloads :: Fail ");
						dispatchServiceError('getBIMCompanyWiseGrandTotalDownloads');
						deferred.reject(data);
					});

			return deferred.promise;
		}


		/**
		 *
		 * @param {type} isForce
		 * @returns {$q@call;defer.promise}
		 */
		function getMnMCountryList(isForce) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('MNM_COUNTRY_LIST_URL');

			//var url = "https://localhost:9000/data/MixNMatchCountryList.json";

			if (isForce !== undefined && isForce !== null && isForce === true) {
				url += "?isForced=true";
			}
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getMnMCountryList :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getMnMCountryList :: Fail ");
						dispatchServiceError('getMnMCountryList');
						deferred.reject(data);
					});

			return deferred.promise;
		}

		/**
		 *
		 * country=all&month=all&year=2015
		 *
		 * @param {type} country
		 * @param {type} month
		 * @param {type} year
		 * @returns {$q@call;defer.promise}
		 */
		function getMnMReportData(country, month, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('MNM_REPORT_URL');
			url += "?country=" + country + "&month=" + month + "&year=" + year + "&isForced=true";


			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getMnMReportData :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getMnMReportData :: Fail ");
						dispatchServiceError('getMnMReportData');
						deferred.reject(data);
					});

			return deferred.promise;
		}

		/**
		 *
		 * @returns {.$q@call;defer.promise}
		 */
		function getMnMAdhocData() {
			var deferred = $q.defer();
			var url = 'bidata/mix_n_match.json';

			$http.get(url)
					.success(function (data) {
						deferred.resolve(data);
					})
					.error(function (error) {
						deferred.reject(error);
					});
			return deferred.promise;
		}

		/**
		 *
		 * @param {type} year
		 * @returns {.$q@call;defer.promise}
		 */
		function getMnMAdhocQuarterlyData(year) {
			var deferred = $q.defer();

			getMnMAdhocData().then(function (data) {
				var qtrData = {
					registeredUsers: {
						T: {total: 0, Q1: 0, Q2: 0, Q3: 0, Q4: 0},
						A: {total: 0, Q1: 0, Q2: 0, Q3: 0, Q4: 0}
					}
				}
				var d = data[year];
				if (d['total'] !== undefined) {
					qtrData.registeredUsers.A.total = d['total'].registeredUsers;
					qtrData.registeredUsers.T.total = d['total'].registeredUsersTarget;
				} else {
					qtrData.registeredUsers.A.Q1 = d['Jan'].registeredUsers + d['Feb'].registeredUsers + d['Mar'].registeredUsers;
					qtrData.registeredUsers.A.Q2 = d['Apr'].registeredUsers + d['May'].registeredUsers + d['Jun'].registeredUsers;
					qtrData.registeredUsers.A.Q3 = d['Jul'].registeredUsers + d['Aug'].registeredUsers + d['Sep'].registeredUsers;
					qtrData.registeredUsers.A.Q4 = (d['Oct'] ? d['Oct'].registeredUsers : 0) + (d['Nov'] ? d['Nov'].registeredUsers : 0) + (d['Dec'] ? d['Dec'].registeredUsers : 0);

					qtrData.registeredUsers.T.Q1 = d['Jan'].registeredUsersTarget + d['Feb'].registeredUsersTarget + d['Mar'].registeredUsersTarget;
					qtrData.registeredUsers.T.Q2 = d['Apr'].registeredUsersTarget + d['May'].registeredUsersTarget + d['Jun'].registeredUsersTarget;
					qtrData.registeredUsers.T.Q3 = d['Jul'].registeredUsersTarget + d['Aug'].registeredUsersTarget + d['Sep'].registeredUsersTarget;
					qtrData.registeredUsers.T.Q4 = (d['Oct'] ? d['Oct'].registeredUsersTarget : 0) + (d['Nov'] ? d['Nov'].registeredUsersTarget : 0) + (d['Dec'] ? d['Dec'].registeredUsersTarget : 0);

					qtrData.registeredUsers.A.total = qtrData.registeredUsers.A.Q1 + qtrData.registeredUsers.A.Q2 + qtrData.registeredUsers.A.Q3 + qtrData.registeredUsers.A.Q4;
					qtrData.registeredUsers.T.total = qtrData.registeredUsers.T.Q1 + qtrData.registeredUsers.T.Q2 + qtrData.registeredUsers.T.Q3 + qtrData.registeredUsers.T.Q4;
				}
				deferred.resolve(qtrData);
			});
			return deferred.promise;
		}


		/**
		 * returns country list for trace parts
		 *
		 * @returns {$q@call;defer.promise}
		 */
		function getTracepartsTrendCountries(businessUnit) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('TRACEPART_TREND_COUNTRIES_URL');
			url += "?bu=" + businessUnit;
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendCountries :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendCountries :: Fail ");
						dispatchServiceError('getTracepartsTrendCountries');
						deferred.reject(data);
					});
			return deferred.promise;
		}

		/**
		 * returns users list
		 *
		 *
		 * @returns {$q@call;defer.promise}
		 */
		function getTracepartsTotals(businessUnit, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('TRACEPART_TOTALS');
			url += "?bu=" + businessUnit + "&year=" + year;
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTotals :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTotals :: Fail ");
						dispatchServiceError('getTracepartsTotals');
						deferred.reject(data);
					});

			return deferred.promise;
		}

		/**
		 *
		 * returns downloadstats list
		 *
		 *
		 * @returns {$q@call;defer.promise}
		 */
		function getTracepartsDownloadList(businessUnit, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('TRACEPART_DOWNLOADS_LIST_URL');
			url += "?bu=" + businessUnit + "&year=" + year;
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getTracepartsDownloadList :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getTracepartsDownloadList :: Fail ");
						dispatchServiceError('getTracepartsDownloadList');
						deferred.reject(data);
					});

			return deferred.promise;
		}

		function getTracepartsTopTenProductDownloads(businessUnit, year) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('TRACEPART_TOP_TEN_PRODUCT_DOWNLOADS_URL');
			url += "?bu=" + businessUnit + "&year=" + year;
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTopTenProductDownloads :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTopTenProductDownloads :: Fail ");
						dispatchServiceError('getTracepartsTopTenProductDownloads');
						deferred.reject(data);
					});

			return deferred.promise;
		}

		/**
		 * returns download stats for selection country and business unit
		 *
		 * @param {string} country
		 * @param {string} businessUnit
		 * @returns {$q@call;defer.promise}
		 */
		function getTracepartsTrendTopTenProducts(businessUnit, country) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('TRACEPART_TREND_PRODUCT_DOWNLOADS_URL');
			url += "?bu=" + businessUnit + "&country=" + country;
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendTopTenProducts :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendTopTenProducts :: Fail ");
						dispatchServiceError('getTracepartsTrendTopTenProducts');
						deferred.reject(data);
					});
			return deferred.promise;
		}


		function getTracepartsTrendTopTenCountries(businessUnit) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('TRACEPART_TREND_TOPTEN_COUNTRIES_URL');
			url += "?bu=" + businessUnit;
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendTopTenCountries :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendTopTenCountries :: Fail ");
						dispatchServiceError('getTracepartsTrendTopTenCountries');
						deferred.reject(data);
					});
			return deferred.promise;
		}

		function getTracepartsTrendDownloads(businessUnit, country) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('TRACEPART_TREND_DOWNLOADS_URL');
			url += "?bu=" + businessUnit + "&country=" + country;
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendDownloads :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendDownloads :: Fail ");
						dispatchServiceError('getTracepartsTrendDownloads');
						deferred.reject(data);
					});
			return deferred.promise;
		}

		function getTracepartsTrendDownloadsAndUsers(businessUnit, country) {
			var deferred = $q.defer();
			var url = commonUtils.getNodeRESTFulURL('TRACEPART_TREND_DOWNLOADS_AND_USERS_URL');
			url += "?bu=" + businessUnit + "&country=" + country;
			$http.get(url)
					.success(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendDownloadsAndUsers :: Success ");
						deferred.resolve(data);
					})
					.error(function (data, status) {
						logger.log("Business Report Service :: getTracepartsTrendDownloadsAndUsers :: Fail ");
						dispatchServiceError('getTracepartsTrendDownloadsAndUsers');
						deferred.reject(data);
					});
			return deferred.promise;
		}



		//------------------------------------------------------------------
		//----------------------- #Private Methods -------------------------
		//------------------------------------------------------------------

		/**
		 *
		 * @param {type} serviceName
		 * @returns {undefined}
		 */
		function dispatchServiceError(serviceName) {
			var errObj = commonUtils.getServiceFailErrorObj(serviceName);
			$rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
		}

	}

})();