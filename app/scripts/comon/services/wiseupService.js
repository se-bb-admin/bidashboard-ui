/**
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').factory('wiseupService', wiseupService);

  wiseupService.$inject = ['$rootScope', '$q', 'logger', 'appConstants', '$http', 'commonUtils'];

  /**
   *
   * @param {type} $rootScope
   * @param {type} $q
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $http
   * @param {type} commonUtils
   * @returns {businessReportService_L4.businessReportService.factory}
   */
  function wiseupService($rootScope, $q, logger, appConstants, $http, commonUtils) {

    var factory = {
      getData: getData
    }

    return factory;

    function getData() {
      var deferred = $q.defer();
      var url = 'bidata/wiseup.json';

      $http.get(url)
              .success(function (data) {
                deferred.resolve(data);
              })
              .error(function (error) {
                deferred.reject(error);
              });
      return deferred.promise;
    }
  }
})();