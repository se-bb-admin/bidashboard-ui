/**
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').factory('closedLoopService', closedLoopService);

  closedLoopService.$inject = ['$q', '$http', 'logger', 'commonUtils'];

  function closedLoopService($q, $http, logger, commonUtils) {
    var factory = {
      getRegisteredOrdersData: getRegisteredOrdersData
    };

    return factory;

    /**
     * get quarterly data for the year requested
     *
     * @param {type} year
     * @returns {$q@call;defer.promise}
     */
    function getRegisteredOrdersData() {
      var deferred = $q.defer();

      var url = 'bidata/ClosedLoop_orders.json';

      $http.get(url)
              .success(function (data) {
                logger.log("closedLoopService  :: getOrderData :: Success ");
                deferred.resolve(data);
              })
              .error(function (data) {
                logger.error("closedLoopService  :: getOrderData :: Fail ");
                dispatchSummaryServiceError('getOrderData');
                deferred.reject(data);
              });
      return deferred.promise;
    }
  }
})();