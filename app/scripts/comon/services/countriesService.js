/**
 *
 */
(function () {
  'use strict';


  angular.module('se-branding').factory('countriesService', countriesService);

  countriesService.$inject = ['$rootScope', '$q', '$http', 'logger', 'commonUtils', 'appConstants'];

  function countriesService($rootScope, $q, $http, logger, commonUtils, appConstants) {
    //------------------------------------------------------------------
    //--------------------- Private Properties -------------------------
    //------------------------------------------------------------------

    var _list = null;

    //------------------------------------------------------------------
    //------------------------ Public  Methods -------------------------
    //------------------------------------------------------------------
    var factory = {
      getNamesList: getNamesList
    };

    return factory;

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getNamesList() {
      var deferred = $q.defer();
      var url = 'bidata/countries.json';

      var config = {
        headers: {
        }
      };

      $http.get(url, config)
              .success(function (data, status) {
                logger.log("countriesService :: getNameList :: Success");
                _list = data;
                deferred.resolve(_list);
              })
              .error(function (data, status) {
                logger.log("countriesService :: getNameList :: Fail");
                _list = null;
                dispatchServiceError('getNameList');
                deferred.reject(_list);
              });
      return deferred.promise;
    }

    //------------------------------------------------------------------
    //----------------------- #Private Methods -------------------------
    //------------------------------------------------------------------

    function dispatchServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }
  }
})();