/**
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').factory('mynovabuddyService', mynovabuddyService);

  mynovabuddyService.$inject = ['$q', '$http', 'logger', 'commonUtils', 'paceService', 'appConstants'];

  function mynovabuddyService($q, $http, logger, commonUtils, paceService, appConstants) {
    var factory = {
      getRegisteredUsers: getRegisteredUsers
    };
    return factory;

    function getRegisteredUsers() {
      var deferred = $q.defer();
      paceService.getRegisteredUsers().then(
              function (result) {
                var usersReg = 0;
                for (var i = 0; i < result.length; i++) {
                  var tempNode = result[i];
                  var projName = tempNode.projName;
                  switch (projName) {
                    case appConstants.SITE_NAME.MY_NOVA_BUDDY:
                      usersReg = tempNode.users;
                      break;
                  }
                }
                deferred.resolve({registeredUsers: usersReg});
              },
              function (error) {
                deferred.resolve({});
              }
      );
      return deferred.promise;
    }
  }
})();