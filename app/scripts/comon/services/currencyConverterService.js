/**
 *
 */
(function () {
  'use strict';

  angular.module('se-branding')
          .factory('currencyConverterService', currencyConverterService);

  currencyConverterService.$inject = ['$rootScope', '$q', 'logger',
    'appConstants', '$http', 'commonUtils'];

  /**
   *
   * @param {type} $rootScope
   * @param {type} $q
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $http
   * @param {type} commonUtils
   * @returns {currencyConverterService_L4.currencyConverterService.factory}
   */
  function currencyConverterService($rootScope, $q, logger,
          appConstants, $http, commonUtils) {
    /**
     *
     */

    var oneRubleToEuroMultiplier = 0;
    var oneRealToEuroMultiplier = 0;

    var factory = {
      initCurrencyConverterService: initCurrencyConverterService,
      getRubleToEuroConversionVal: getRubleToEuroConversionVal,
      getRealToEuroConversionVal: getRealToEuroConversionVal
              //convertRubleToEuro: convertRubleToEuro,
              //convertRealToEuro: convertRealToEuro
    };

    return factory;




    function initCurrencyConverterService() {
      convertRubleToEuro().then(
              function () {
                convertRealToEuro();
              },
              function () {
                convertRealToEuro();
              }
      );
    }



    /**
     *
     * @returns {undefined}
     */
    function getRubleToEuroConversionVal() {
      return oneRubleToEuroMultiplier;
    }


    /**
     *
     * @returns {Number|refObj.val}
     */
    function getRealToEuroConversionVal() {
      return oneRealToEuroMultiplier;
    }


    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function convertRubleToEuro() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CURRENCY_COVERTER_RUB_EUR');
      $http.get(url)
              .success(function (data, status) {
                logger.log("currency Converter Service :: convertRubleToEuro :: Success ");
                var refObj = data.RUB_EUR;
                oneRubleToEuroMultiplier = refObj.val;
                deferred.resolve(refObj.val);
              })
              .error(function (data, status) {
                logger.log("currency Converter Service :: convertRubleToEuro :: Fail ");
                oneRubleToEuroMultiplier = 0;
                dispatchCurrencyConvertServiceError('ConvertRubleToEuro');
                deferred.reject(data);
              });

      return deferred.promise;
    }


    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function convertRealToEuro() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('CURRENCY_COVERTER_BRL_EUR');
      $http.get(url)
              .success(function (data, status) {
                logger.log("currency Converter Service :: convertRealToEuro :: Success ");
                var refObj = data.BRL_EUR;
                oneRealToEuroMultiplier = refObj.val;
                deferred.resolve(refObj.val);
              })
              .error(function (data, status) {
                logger.log("currency Converter Service :: convertRealToEuro :: Fail ");
                oneRealToEuroMultiplier = 0;
                dispatchCurrencyConvertServiceError('convertRealToEuro');
                deferred.reject(data);
              });

      return deferred.promise;
    }



    //------------------------------------------------------------------
    //----------------------- #Private Methods -------------------------
    //------------------------------------------------------------------

    function dispatchCurrencyConvertServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }

  }


})();