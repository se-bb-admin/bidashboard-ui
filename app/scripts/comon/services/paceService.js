/**
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').factory('paceService', paceService);

  paceService.$inject = ['$rootScope', '$q', 'logger', 'appConstants',
    '$http', 'commonUtils'];

  /**
   *
   * @param {type} $rootScope
   * @param {type} $q
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $http
   * @param {type} commonUtils
   * @returns {paceService_L4.paceService.factory}
   */
  function paceService($rootScope, $q, logger, appConstants,
          $http, commonUtils) {
    /**
     *
     */
    var _registeredUsers = null;
    var _registeredUsersOverPeriod = null;
    var _registeredUsersOnCurrentYear = null;
    var _registeredUsersByYear = null;
    var _loginUser = null;
    var _userData = null;
    var _paceAuthToken = null;

    var authenticationServerUrl = appConstants.PACE_LOGIN_REDIRECT_URL.AUTHENTICATION_SERVER_URL;


    var factory = {
      getRegisteredUsers: getRegisteredUsers,
      getRegisteredUsersOverPeriod: getRegisteredUsersOverPeriod,
      getRegUserOnCurrentYear: getRegUserOnCurrentYear,
      getUserAuthentication: getUserAuthentication,
      setAuthToken: setAuthToken,
      getAuthToken: getAuthToken,
      getUserInfo: getUserInfo,
      getRegUserByYear: getRegUserByYear,
      getRegisteredUsersForProject: getRegisteredUsersForProject
    };

    return factory;


    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getRegisteredUsers() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('PACE_REG_USER_URL');
      url += "?isForced=true";

      if (_registeredUsers !== null && _registeredUsers !== undefined) {
        deferred.resolve(_registeredUsers);
      } else {
        $http.get(url)
                .success(function (data, status) {
                  logger.log("PACE Service :: getRegisteredUsers :: Success ");
                  _registeredUsers = data;
                  deferred.resolve(data);
                })
                .error(function (data, status) {
                  logger.log("PACE Service :: getRegisteredUsers :: Fail ");
                  _registeredUsers = null;
                  dispatchPaceServiceError('RegisteredUsers');
                  deferred.reject(data);
                });

      }

      return deferred.promise;
    }

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getRegisteredUsersOverPeriod() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('PACE_REG_USER_OVER_PERIOD_URL');
      url += '?isForced=true';

      if (_registeredUsersOverPeriod !== null && _registeredUsersOverPeriod !== undefined) {
        deferred.resolve(_registeredUsersOverPeriod);
      } else {
        $http.get(url)
                .success(function (data, status) {
                  logger.log("PACE Service :: getRegisteredUsersOverPeriod :: Success ");
                  _registeredUsersOverPeriod = data;
                  deferred.resolve(data);
                })
                .error(function (data, status) {
                  logger.log("PACE Service :: getRegisteredUsersOverPeriod :: Fail ");
                  _registeredUsersOverPeriod = null;
                  dispatchPaceServiceError('RegisteredUsersOverPeriod');
                  deferred.reject(data);
                });
      }

      return deferred.promise;
    }


    /**
     * getRegUserOnCurrentYear
     *
     * @returns {$q@call;defer.promise}
     */
    function getRegUserOnCurrentYear() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('PACE_REG_USER_ON_CURRENT_YEAR_URL');

      if (_registeredUsersOnCurrentYear !== null && _registeredUsersOnCurrentYear !== undefined) {
        deferred.resolve(_registeredUsersOnCurrentYear);
      } else {
        $http.get(url)
                .success(function (data, status) {
                  logger.log("PACE Service :: getRegUserOnCurrentYear :: Success ");
                  _registeredUsersOnCurrentYear = data;
                  deferred.resolve(data);
                })
                .error(function (data, status) {
                  logger.log("PACE Service :: getRegUserOnCurrentYear :: Fail ");
                  _registeredUsersOnCurrentYear = null;
                  dispatchPaceServiceError('RegUserOnCurrentYear');
                  deferred.reject(data);
                });
      }

      return deferred.promise;
    }

    /**
     *
     * @param {type} userToken
     * @returns {$q@call;defer.promise}
     */
    function getUserAuthentication(userToken) {
      var deferred = $q.defer();
      var url = authenticationServerUrl + '/api/v1/user';
      var config = {headers: {
          'Authorization': userToken,
          'Accept': 'application/json'
        }
      };
      $http.get(url, config)
              .success(function (data, status) {
                logger.log("PACE Service :: getUserAuthentication :: Success");
                _userData = data;
                deferred.resolve(_userData);
              })
              .error(function (data, status) {
                logger.log("PACE Service :: getUserAuthentication :: Fail");
                _userData = null;
                dispatchPaceServiceError('UserAuthentication');
                deferred.reject(_userData);
              });
      return deferred.promise;
    }


    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getRegUserByYear() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('PACE_REG_USER_BY_YEAR_BY_QTR');

      if (_registeredUsersByYear !== null && _registeredUsersByYear !== undefined) {
        deferred.resolve(_registeredUsersByYear);
      } else {
        $http.get(url)
                .success(function (data, status) {
                  logger.log("PACE Service :: getRegUserByYear :: Success ");
                  _registeredUsersByYear = data;
                  deferred.resolve(data);
                })
                .error(function (data, status) {
                  logger.log("PACE Service :: getRegUserByYear :: Fail ");
                  _registeredUsersByYear = null;
                  dispatchPaceServiceError('getRegUserByYear');
                  deferred.reject(data);
                });
      }

      return deferred.promise;
    }


    /**
     *
     * @param {type} userToken
     * @returns {undefined}
     */
    function setAuthToken(userToken) {
      _paceAuthToken = userToken;
    }


    /**
     *
     * @returns {userToken}
     */
    function getAuthToken() {
      return _paceAuthToken;
    }


    /**
     *
     * @returns {undefined}
     */
    function getUserInfo() {
      return _userData;
    }


    function getRegisteredUsersForProject(projectName) {
      var deferred = $q.defer();
      getRegisteredUsers().then(function (data) {
        var project = {};
        for (var d = 0; d < data.length; d++) {
          var obj = data[d];
          if (obj.projName.toLowerCase() === projectName.toLowerCase()) {
            project = obj;
            break;
          }
        }
        deferred.resolve(project);
      });
      return deferred.promise;
    }
    //------------------------------------------------------------------
    //----------------------- #Private Methods -------------------------
    //------------------------------------------------------------------

    function dispatchPaceServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }

  }

})();