/**
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').factory('ecodialService', ecodialService);

  ecodialService.$inject = ['$q', '$http', 'logger', 'commonUtils'];

  function ecodialService($q, $http, logger, commonUtils) {
    var factory = {
      getQtrData: getQtrData,
      getWorldWideDayTotal: getWorldWideDayTotal,
      getWorldWideWeekTotal: getWorldWideWeekTotal,
      getWorldWideMonthTotal: getWorldWideMonthTotal,
      getWorldWideTotal: getWorldWideTotal,
      getMaxDate: getMaxDate
    };

    return factory;

    /**
     * get quarterly data for the year requested
     *
     * @param {type} year
     * @returns {$q@call;defer.promise}
     */
    function getQtrData(year) {
      var deferred = $q.defer();

      var url = commonUtils.getNodeRESTFulURL('ECO_DIAL_DESKTOP_QTR_DATA_URL');
      url += '?year=' + year;

      $http.get(url)
              .success(function (data) {
                logger.log("ecodialService  :: getQtrData :: Success ");
                deferred.resolve(data);
              })
              .error(function (data) {
                logger.error("ecodialService  :: getQtrData :: Fail ");
                dispatchSummaryServiceError('getQtrData');
                deferred.reject(data);
              });
      return deferred.promise;
    }

    /**
     * get world-wide data by day
     *
     * @returns {$q@call;defer.promise}
     */
    function getWorldWideDayTotal() {
      var deferred = $q.defer();

      var url = commonUtils.getNodeRESTFulURL('ECO_DIAL_DESKTOP_WORLDWIDE_DAY_URL');

      $http.get(url)
              .success(function (data) {
                logger.log("ecodialService  :: getWorldWideTotal :: Success ");
                deferred.resolve(data);
              })
              .error(function (data) {
                logger.error("ecodialService  :: getWorldWideTotal :: Fail ");
                dispatchSummaryServiceError('getQtrData');
                deferred.reject(data);
              });
      return deferred.promise;
    }

    /**
     * get world-wide data by week
     *
     * @returns {$q@call;defer.promise}
     */
    function getWorldWideWeekTotal() {
      var deferred = $q.defer();

      var url = commonUtils.getNodeRESTFulURL('ECO_DIAL_DESKTOP_WORLDWIDE_WEEK_URL');

      $http.get(url)
              .success(function (data) {
                logger.log("ecodialService  :: getWorldWideTotal :: Success ");
                deferred.resolve(data);
              })
              .error(function (data) {
                logger.error("ecodialService  :: getWorldWideTotal :: Fail ");
                dispatchSummaryServiceError('getQtrData');
                deferred.reject(data);
              });
      return deferred.promise;
    }

    /**
     * get world-wide data by month
     *
     * @returns {$q@call;defer.promise}
     */
    function getWorldWideMonthTotal() {
      var deferred = $q.defer();

      var url = commonUtils.getNodeRESTFulURL('ECO_DIAL_DESKTOP_WORLDWIDE_MONTH_URL');

      $http.get(url)
              .success(function (data) {
                logger.log("ecodialService  :: getWorldWideTotal :: Success ");
                deferred.resolve(data);
              })
              .error(function (data) {
                logger.error("ecodialService  :: getWorldWideTotal :: Fail ");
                dispatchSummaryServiceError('getQtrData');
                deferred.reject(data);
              });
      return deferred.promise;
    }

    function getWorldWideTotal() {
      var deferred = $q.defer();

      var url = commonUtils.getNodeRESTFulURL('ECO_DIAL_DESKTOP_WORLDWIDE_TOTAL_URL');

      $http.get(url)
              .success(function (data) {
                logger.log("ecodialService  :: getWorldWideTotal :: Success ");
                deferred.resolve(data);
              })
              .error(function (data) {
                logger.error("ecodialService  :: getWorldWideTotal :: Fail ");
                dispatchSummaryServiceError('getQtrData');
                deferred.reject(data);
              });
      return deferred.promise;
    }

    function getMaxDate(filter) {
      var deferred = $q.defer();

      var url = commonUtils.getNodeRESTFulURL('ECO_DIAL_DESKTOP_WORLDWIDE_TOTAL_URL');
      url += '?filter=' + filter;

      $http.get(url)
              .success(function (data) {
                logger.log("ecodialService  :: getWorldWideTotal :: Success ");
                deferred.resolve(data);
              })
              .error(function (data) {
                logger.error("ecodialService  :: getWorldWideTotal :: Fail ");
                dispatchSummaryServiceError('getQtrData');
                deferred.reject(data);
              });
      return deferred.promise;
    }
  }
})();