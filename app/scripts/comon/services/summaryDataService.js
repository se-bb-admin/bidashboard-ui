/**
 *
 */
(function () {
  'use strict';
  var factoryId = 'summaryDataService';
  angular.module('se-branding').factory('summaryDataService', summaryDataService);
  summaryDataService.$inject = ['$rootScope', '$q', 'logger', 'appConstants',
    '$http', 'commonUtils', 'paceService', 'nodeGAService', 'currencyConverterService'];

  /**
   *
   * @param {type} $rootScope
   * @param {type} $q
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $http
   * @param {type} commonUtils
   * @param {type} paceService
   * @param {type} nodeGAService
   * @param {type} currencyConverterService
   * @returns {summaryDataService_L4.summaryDataService.factory}
   */
  function summaryDataService($rootScope, $q, logger, appConstants,
          $http, commonUtils, paceService, nodeGAService, currencyConverterService) {

    var SITE_DETAILS_FOR_SUMMARY_PAGE = appConstants.SITE_DETAILS_FOR_SUMMARY_PAGE;
    var _KPISummeryFrmDB = null;
    var _KPISummeryParsed = null;
    var _CURRENT_YEAR = new Date().getFullYear();

    //--------------------- Total Registered  Users --------------------
    var _summaryBasicData_TOTAL_REG_USER = {
      'work': {
        "appName": "Registered user in @WORK",
        "path": "/pace"
      },
      'home': {
        "appName": "Registered user in @HOME",
        "path": "/pace"
      }
    };

    /**
     *
     */

    var factory = {
      getSummaryBasicData: getSummaryBasicData,
      resetKPI_SummaryData: resetKPI_SummaryData,
      getManualTargets: getManualTargets
    };

    return factory;

    //------------------------------------------------------------------
    //-------------------------- PUBLIC METHODS ------------------------
    //------------------------------------------------------------------

    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getSummaryBasicData() {
      var deferred = $q.defer();

      getKPI_SummeryFromServer().then(
              function (data) {
                _KPISummeryFrmDB = data;
                //Parsed Summary Data By Year.
                parseKPISummeryData();

                //Get And Update Current Year  Reg user data from PACE
                getPaceRegUserForCurrentYear().then(
                        function (data) {

                          //Get Google Anyalytic Data For Current Year
                          // 'No. of Project Created', 'BOM (YTD)', 'No. of BOM Exported'
                          getGaSummeryDataForCurrentYear().then(
                                  function () {
                                    //Calculate Reg. User (YTD)
                                    getTotAchiveTaregetRegUserYTD();


                                    //Return Parsed data
                                    deferred.resolve(_KPISummeryParsed);
                                  },
                                  function () {
                                    logger.error("Error :: getSummaryBasicData:: getGaSummeryDataForCurrentYear ");
                                  }
                          );
                        },
                        function () {
                          logger.error("Error :: getSummaryBasicData:: getPaceRegUserForCurrentYear ");
                        }
                );
              },
              function () {
                logger.error("FAIL::getSummaryBasicData");
                //Popuate Blank Data..
                _KPISummeryFrmDB = null;

                //Return Null Data..
                deferred.reject(null);
              }
      );

      return deferred.promise;
    }


    /**
     *
     * @returns {undefined}
     */
    function resetKPI_SummaryData() {
      _KPISummeryFrmDB = null;
    }

    function getManualTargets(jsonFileName, year) {
      var deferred = $q.defer();
      var url = "bidata/" + jsonFileName + ".json";
      $http.get(url).then(function (response) {
        deferred.resolve(response.data[year]);
      });
      return deferred.promise;
    }



    //------------------------------------------------------------------
    //------------------------ PRIVATE PROPERTIES ----------------------
    //------------------------------------------------------------------


    /**
     *
     * @returns {$q@call;defer.promise}
     */
    function getKPI_SummeryFromServer() {
      var deferred = $q.defer();
      var url = commonUtils.getNodeRESTFulURL('SUMMARY_KPI_URL');
//			console.log('---> getKPI_SummeryFromServer: url: ', url);
      if (_KPISummeryFrmDB !== null && _KPISummeryFrmDB !== undefined) {
        deferred.resolve(_KPISummeryFrmDB);
      } else {
        $http.get(url)
                .success(function (data, status) {
                  logger.log("summaryDataService  :: getKPI_SummeryFromServer :: Success ");
                  deferred.resolve(data);
                })
                .error(function (data, status) {
                  logger.error("summaryDataService  :: getKPI_SummeryFromServer :: Fail ");
                  dispatchSummaryServiceError('getKPI_SummeryFromServer');
                  deferred.reject(data);
                });
      }

      return deferred.promise;
    }




    function parseKPISummeryData() {

      var kpiYears = [];
      var kpiYear = "";
      var tmpKpiYearRawData = null;
      var tmpKpiYearParsedData = null;

      _KPISummeryParsed = {};

      //Get Years from Raw Data..
      if (_KPISummeryFrmDB !== undefined || _KPISummeryFrmDB !== null) {
        kpiYears = Object.keys(_KPISummeryFrmDB);
      }

      //Loop through each years and populate data.
      for (var j = 0; j < kpiYears.length; j++) {
        kpiYear = kpiYears[j];

        //Create Blank Object for that Year
        _KPISummeryParsed[kpiYear] = {};

        tmpKpiYearRawData = _KPISummeryFrmDB[kpiYear];
//				console.log("\n---->> tmpKpiYearRawData: ", tmpKpiYearRawData);
        tmpKpiYearParsedData = parseKPISummeryDataForAYear(tmpKpiYearRawData);

        //Set Parsed data into  Blank Object for that Year
        _KPISummeryParsed[kpiYear] = tmpKpiYearParsedData;
      }

      //Add Total Registered user At Root Lavel...
      _KPISummeryParsed.TOTAL_REG_USER = _summaryBasicData_TOTAL_REG_USER;

      console.log("Parsed Complete..");
    }



    function parseKPISummeryDataForAYear(kpiDataByYear) {
      var kpiSummeryParsedForAYear = {};
      if (kpiDataByYear !== undefined || kpiDataByYear !== null) {
        kpiSummeryParsedForAYear.EASY_QUOTE = getSummeryDataBySiteName(kpiDataByYear, 'EASY_QUOTE');
        kpiSummeryParsedForAYear.CLIPSAL_WISH_LIST = getSummeryDataBySiteName(kpiDataByYear, 'CLIPSAL_WISH_LIST');
        kpiSummeryParsedForAYear.GLOBAL_METER_APP = getSummeryDataBySiteName(kpiDataByYear, 'GLOBAL_METER_APP');
        kpiSummeryParsedForAYear.CLOSED_LOOP = getSummeryDataBySiteName(kpiDataByYear, 'CLOSED_LOOP');
        kpiSummeryParsedForAYear.MIX_N_MATCH = getSummeryDataBySiteName(kpiDataByYear, 'MIX_N_MATCH');
        kpiSummeryParsedForAYear.WISE_UP = getSummeryDataBySiteName(kpiDataByYear, 'WISE_UP');
        kpiSummeryParsedForAYear.WISER_LINK = getSummeryDataBySiteName(kpiDataByYear, 'WISER_LINK');
        kpiSummeryParsedForAYear.NULL_HOME = getSummeryDataBySiteName(kpiDataByYear, 'NULL_HOME');
        kpiSummeryParsedForAYear.QUICK_REF = getSummeryDataBySiteName(kpiDataByYear, 'QUICK_REF');
        kpiSummeryParsedForAYear.MY_PACT = getSummeryDataBySiteName(kpiDataByYear, 'MY_PACT');
        kpiSummeryParsedForAYear.QUICK_QUOTATION_630 = getSummeryDataBySiteName(kpiDataByYear, 'QUICK_QUOTATION_630');
        kpiSummeryParsedForAYear.CBT = getSummeryDataBySiteName(kpiDataByYear, 'CBT');
        kpiSummeryParsedForAYear.BLM = getSummeryDataBySiteName(kpiDataByYear, 'BLM');
        kpiSummeryParsedForAYear.CAD_LIBRARY = getSummeryDataBySiteName(kpiDataByYear, 'CAD_LIBRARY');
        kpiSummeryParsedForAYear.SMART_SELECTORS = getSummeryDataBySiteName(kpiDataByYear, 'SMART_SELECTORS');
        kpiSummeryParsedForAYear.BERRI_SOFT = getSummeryDataBySiteName(kpiDataByYear, 'BERRI_SOFT');
        kpiSummeryParsedForAYear.ECOREAL_630 = getSummeryDataBySiteName(kpiDataByYear, 'ECOREAL_630');
        kpiSummeryParsedForAYear.NULL_WORK = getSummeryDataBySiteName(kpiDataByYear, 'NULL_WORK');
        kpiSummeryParsedForAYear.RAPSODY = getSummeryDataBySiteName(kpiDataByYear, 'RAPSODY');
        kpiSummeryParsedForAYear.ECO_DIAL_DESKTOP = getSummeryDataBySiteName(kpiDataByYear, 'ECO_DIAL_DESKTOP');
        kpiSummeryParsedForAYear.ECO_DIAL_CHINA = getSummeryDataBySiteName(kpiDataByYear, 'ECO_DIAL_CHINA');
        kpiSummeryParsedForAYear.ECO_DIAL_AUTOCAD_PLUGIN = getSummeryDataBySiteName(kpiDataByYear, 'ECO_DIAL_AUTOCAD_PLUGIN');
        kpiSummeryParsedForAYear.ECO_REACH = getSummeryDataBySiteName(kpiDataByYear, 'ECO_REACH');
        kpiSummeryParsedForAYear.ECO_REACH_2 = getSummeryDataBySiteName(kpiDataByYear, 'ECO_REACH_2');
        kpiSummeryParsedForAYear.MY_NOVA_BUDDY = getSummeryDataBySiteName(kpiDataByYear, 'MY_NOVA_BUDDY');
//				console.log("\n=======> kpiSummeryParsedForAYear.ECO_REACH: ", kpiSummeryParsedForAYear.ECO_REACH, '\n');
        //kpiSummeryParsedForAYear.TOTAL_REG_USER = _summaryBasicData_TOTAL_REG_USER;
      }

      return  kpiSummeryParsedForAYear;
    }


    /**
     *
     * @param {type} kpiData
     * @param {type} siteName
     * @returns {summaryDataService_L4.summaryDataService.populateSiteDataForSummery.siteData}
     */
    function getSummeryDataBySiteName(kpiData, siteName) {
      var siteDetails = SITE_DETAILS_FOR_SUMMARY_PAGE[siteName];
      var rawData = getProjSummeryDataByClientID(kpiData, siteDetails.CLIENT_ID);
      var siteData = populateSiteDataForSummery(siteDetails, rawData);
      return siteData;
    }


    /**
     *
     * @param {type} kpiData
     * @param {type} clientId
     * @returns {unresolved}
     */
    function getProjSummeryDataByClientID(kpiData, clientId) {
      var projData = null;
      var tmpData = null;
      for (var i = 0; i < kpiData.length; i++) {
        tmpData = kpiData[i];
        //Check for Match
        if (tmpData.CLIENT_ID === clientId) {
          projData = angular.copy(tmpData);
          break;
        }
      }

      return projData;
    }


    /**
     *
     * @param {type} siteDetails
     * @param {type} rawData
     * @returns {summaryDataService_L4.summaryDataService.populateSiteDataForSummery.siteData}
     */
    function populateSiteDataForSummery(siteDetails, rawData) {
      var siteData = {};
      if (rawData) {
        siteData.appName = rawData.APP_NAME;
        siteData.clientId = rawData.CLIENT_ID;
        siteData.path = siteDetails.PATH;
        siteData.KpiYear = rawData.KPI_YEAR;
        siteData.Q1Target = rawData.KPI_Q1_TARGET;
        siteData.Q1Achive = rawData.KPI_Q1_ACHIEVED;
        siteData.Q2Target = rawData.KPI_Q2_TARGET;
        siteData.Q2Achive = rawData.KPI_Q2_ACHIEVED;
        siteData.Q3Target = rawData.KPI_Q3_TARGET;
        siteData.Q3Achive = rawData.KPI_Q3_ACHIEVED;
        siteData.Q4Target = rawData.KPI_Q4_TARGET;
        siteData.Q4Achive = rawData.KPI_Q4_ACHIEVED;
        siteData.RegUserTarget = "";
        siteData.RegUserAchive = "";
        siteData.projCreatedTarget = "";
        siteData.projCreatedAchive = rawData.NO_OF_PRJ_CREATED_ACHIEVED;
        siteData.BOMTarget = "";
        siteData.BOMYTDAchive = rawData.BOM_YTD_ACHIEVED;
        siteData.BOMExportedTarget = "";
        siteData.BOMExportedAchive = rawData.NO_BOM_EXPORTED_ACHIEVED;
      }
      return siteData;
    }


    //----------------------------------------------------------------------

    /**
     * Get registered user from the bigining of time
     * @returns {undefined}
     */
    function getPaceRegUserForCurrentYear() {
      var deferred = $q.defer();
      paceService.getRegUserOnCurrentYear().then(
              function (result) {
                logger.log("summaryDataService::getPaceRegUserForCurrentYear::Result::Success");
                parsePaceRegUserDataForCurrentYear(result);
                deferred.resolve(_KPISummeryParsed);
              },
              function (error) {
                logger.error("summaryDataService::getPaceRegUserForCurrentYear::Result::FAIL");
                deferred.reject(error);
              }
      );
      return deferred.promise;
    }


    /**
     *
     * @param {type} data
     * @returns {undefined}
     */
    function parsePaceRegUserDataForCurrentYear(data) {

      var applicationStats = angular.copy(data);
      var tempNode = "";
      var projName = "";
      var currentYearKpiData = _KPISummeryParsed[_CURRENT_YEAR];

      for (var i = 0; i < applicationStats.length; i++) {
        tempNode = applicationStats[i];
        projName = tempNode.projName;

        switch (projName) {
          case appConstants.SITE_NAME.QUICK_QUOTATION_630_SPAIN:
          case appConstants.SITE_NAME.QUICK_QUOTATION_630_RUSSIA:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.QUICK_QUOTATION_630);
            break;

          case appConstants.SITE_NAME.MY_PACT:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.MY_PACT);
            break;

          case appConstants.SITE_NAME.BLM:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.BLM);
            break;

          case appConstants.SITE_NAME.CAD_LIBRARY:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.CAD_LIBRARY);
            break;

          case appConstants.SITE_NAME.RAPSODY:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.RAPSODY);
            break;

          case appConstants.SITE_NAME.SMART_SELECTORS:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.SMART_SELECTORS);
            break;

          case appConstants.SITE_NAME.BERRI_SOFT:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.BERRI_SOFT);
            break;

          case appConstants.SITE_NAME.ECOREAL_630:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.ECOREAL_630);
            break;

          case appConstants.SITE_NAME.ECO_DIAL_DESKTOP:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.ECO_DIAL_DESKTOP);
            break;

          case appConstants.SITE_NAME.ECO_DIAL_CHINA:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.ECO_DIAL_CHINA);
            break;

          case appConstants.SITE_NAME.ECO_DIAL_AUTOCAD_PLUGIN:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.ECO_DIAL_AUTOCAD_PLUGIN);
            break;

          case appConstants.SITE_NAME.ECO_REACH:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.ECO_REACH);
            break;

          case appConstants.SITE_NAME.CBT:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.CBT);
            break;

          case appConstants.SITE_NAME.ECO_REACH_2:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.ECO_REACH_2);
            break;

          case appConstants.SITE_NAME.MY_NOVA_BUDDY:
            updateCurrentYearPaceRegUsrAchiveData(tempNode, currentYearKpiData.MY_NOVA_BUDDY);
            break;

          default:
            console.log("parsePaceRegUserDataForCurrentYear: Not Found ", projName);
            break;
        }// End Switch

      }
    }


    /**
     *
     * @param {type} srcData
     * @param {type} trgData
     * @returns {undefined}
     */
    function updateCurrentYearPaceRegUsrAchiveData(srcData, trgData) {

      var q1 = srcData.month[0].users + srcData.month[1].users + srcData.month[2].users;
      var q2 = srcData.month[3].users + srcData.month[4].users + srcData.month[5].users;
      var q3 = srcData.month[6].users + srcData.month[7].users + srcData.month[8].users;
      var q4 = srcData.month[9].users + srcData.month[10].users + srcData.month[11].users;

      trgData.Q1Achive += q1;
      trgData.Q2Achive += q2;
      trgData.Q3Achive += q3;
      trgData.Q4Achive += q4;
    }



    //--------------------------------------------------------------------------------------

    /**
     * Get Google Anyalytic Data For Current Year
     * 'No. of Project Created', 'BOM (YTD)', 'No. of BOM Exported'
     *
     * @returns {undefined}
     */
    function getGaSummeryDataForCurrentYear() {
      var deferred = $q.defer();
      nodeGAService.getSummaryReportData().then(
              function (result) {
                logger.log("summaryDataService::getGaDataForCurrentYear::Result::Success");
                parseGaSummeryDataForCurrentYear(result);
                deferred.resolve(_KPISummeryParsed);
              },
              function (error) {
                logger.error("summaryDataService::getGaDataForCurrentYear::Result::FAIL");
                deferred.reject(error);
              }
      );
      return deferred.promise;
    }


    /**
     *
     * @param {type} data
     * @returns {undefined}
     */
    function parseGaSummeryDataForCurrentYear(data) {

      var currentYearKpiData = _KPISummeryParsed[_CURRENT_YEAR];
      var oneRubleToEuroMultiplier = currencyConverterService.getRubleToEuroConversionVal();
      var oneRealToEuroMultiplier = currencyConverterService.getRealToEuroConversionVal();

      populateGARegUserSummaryData(data.easyQuote_RegUser.rows, currentYearKpiData.EASY_QUOTE, 3);
      populateGARegUserSummaryData(data.quickRef_ActiveUser.rows, currentYearKpiData.QUICK_REF, 2);
      populateGARegUserSummaryData(data.clipsalWishList_RegUser.rows, currentYearKpiData.CLIPSAL_WISH_LIST, 4);
      populateGARegUserSummaryData(data.globalMeterApp_RegUser.rows, currentYearKpiData.GLOBAL_METER_APP, 2);

      currentYearKpiData.BLM.projCreatedAchive = parseInt(data.BLM_ProjCreatedAchive.totalsForAllResults['ga:totalEvents']);
      currentYearKpiData.EASY_QUOTE.projCreatedAchive = parseInt(data.EASY_QUOTE_projCreatedAchive.totalsForAllResults['ga:totalEvents']);
      currentYearKpiData.QUICK_QUOTATION_630.projCreatedAchive = data.QQ630_ProjCreatedAchive;
      currentYearKpiData.CLIPSAL_WISH_LIST.projCreatedAchive = parseInt(data.clipsalWishList_ProjCreatedAchive.totalsForAllResults['ga:totalEvents']);
      currentYearKpiData.CBT.projCreatedAchive = data.CBT_ProjCreatedAchive;


      var ecorealQQSpainBOM = data.ecorealQQ_SpainBOM.totalsForAllResults['ga:eventValue'];
      var ecorealQQRussiaBOM = data.ecorealQQ_RussiaBOM.totalsForAllResults['ga:eventValue'];
      currentYearKpiData.QUICK_QUOTATION_630.BOMExportedAchive = (parseInt(data.ecorealQQ_SpainBOM.totalsForAllResults['ga:totalEvents']) + parseInt(data.ecorealQQ_RussiaBOM.totalsForAllResults['ga:totalEvents']));
      currentYearKpiData.QUICK_QUOTATION_630.BOMYTDAchive = (parseInt(ecorealQQSpainBOM) + parseInt((ecorealQQRussiaBOM) * oneRubleToEuroMultiplier));




      currentYearKpiData.EASY_QUOTE.bomValBrazil = data.easyQuoteBomValBrazil.totalsForAllResults['ga:eventValue'];
      currentYearKpiData.EASY_QUOTE.bomValRussia = data.easyQuoteBomValRussia.totalsForAllResults['ga:eventValue'];
      currentYearKpiData.EASY_QUOTE.bomValSpain = data.easyQuoteBomValSpain.totalsForAllResults['ga:eventValue'];
      currentYearKpiData.EASY_QUOTE.bomCountBrazil = data.easyQuoteBomCountBrazil.totalsForAllResults['ga:totalEvents'];
      currentYearKpiData.EASY_QUOTE.bomCountRussia = data.easyQuoteBomCountRussia.totalsForAllResults['ga:totalEvents'];
      currentYearKpiData.EASY_QUOTE.bomCountSpain = data.easyQuoteBomCountSpain.totalsForAllResults['ga:totalEvents'];
      currentYearKpiData.EASY_QUOTE.BOMYTDAchive = ((currentYearKpiData.EASY_QUOTE.bomValBrazil) * oneRealToEuroMultiplier) + ((currentYearKpiData.EASY_QUOTE.bomValRussia) * oneRubleToEuroMultiplier) + parseInt(currentYearKpiData.EASY_QUOTE.bomValSpain);
      currentYearKpiData.EASY_QUOTE.BOMExportedAchive = parseInt(currentYearKpiData.EASY_QUOTE.bomCountBrazil) + parseInt(currentYearKpiData.EASY_QUOTE.bomCountRussia) + parseInt(currentYearKpiData.EASY_QUOTE.bomCountSpain);
    }


    /**
     *
     * @param {type} srcData
     * @param {type} trgData
     * @param {type} qryParam
     * @returns {undefined}
     */
    function populateGARegUserSummaryData(srcData, trgData, qryParam) {

      if (srcData === undefined || srcData === null) {
        return;
      }

      for (var i = 0; i < srcData.length; i++) {
        var temp = srcData[i];
        var tempMonth = 0;
        var tempUser = 0;
        if (qryParam === 4) {
          tempMonth = parseInt(temp[3]);
          tempUser = parseInt(temp[4]);
        } else if (qryParam === 3) {
          tempMonth = parseInt(temp[1]);
          tempUser = parseInt(temp[2]);
        } else if (qryParam === 2) {
          tempMonth = parseInt(temp[0]);
          tempUser = parseInt(temp[1]);
        }
        if (tempMonth >= 1 && tempMonth <= 3) {
          trgData.Q1Achive += tempUser;
        } else if (tempMonth >= 4 && tempMonth <= 6) {
          trgData.Q2Achive += tempUser;
        } else if (tempMonth >= 7 && tempMonth <= 9) {
          trgData.Q3Achive += tempUser;
        } else if (tempMonth >= 10 && tempMonth <= 12) {
          trgData.Q4Achive += tempUser;
        }
      }
    }



    function getTotAchiveTaregetRegUserYTD() {
      var tYearData = null;
      var tProjData = null;

      //Loop Through Year
      for (var key in _KPISummeryParsed) {
        tYearData = _KPISummeryParsed[key];

        for (var keyS in tYearData) {
          tProjData = tYearData[keyS];
          updateTotalAchiveTaregetRegUserYTDByProj(tProjData);
        }


      }
    }



    /**
     *
     * @param {type} targetData
     * @returns {undefined}
     */
    function updateTotalAchiveTaregetRegUserYTDByProj(targetData) {

      targetData.RegUserAchive = targetData.Q1Achive + targetData.Q2Achive + targetData.Q3Achive + targetData.Q4Achive;
      targetData.RegUserTarget = targetData.Q1Target + targetData.Q2Target + targetData.Q3Target + targetData.Q4Target;

      /*
       var month = (new Date().getMonth() + 1);
       var crntQtr = 1;

       if (month >= 1 && month <= 3) {
       crntQtr = 1;
       } else if (month >= 4 && month <= 6) {
       crntQtr = 2;
       } else if (month >= 6 && month <= 9) {
       crntQtr = 3;
       } else if (month >= 9 && month <= 12) {
       crntQtr = 4;
       }


       if (crntQtr === 1) {
       targetData.RegUserAchive = targetData.Q1Achive;
       targetData.RegUserTarget = targetData.Q1Target;
       } else if (crntQtr === 2) {
       targetData.RegUserAchive = targetData.Q1Achive + targetData.Q2Achive;
       targetData.RegUserTarget = targetData.Q1Target + targetData.Q2Target;
       } else if (crntQtr === 3) {
       targetData.RegUserAchive = targetData.Q1Achive + targetData.Q2Achive + targetData.Q3Achive;
       targetData.RegUserTarget = targetData.Q1Target + targetData.Q2Target + targetData.Q3Target;
       } else if (crntQtr === 4) {
       targetData.RegUserAchive = targetData.Q1Achive + targetData.Q2Achive + targetData.Q3Achive + targetData.Q4Achive;
       targetData.RegUserTarget = targetData.Q1Target + targetData.Q2Target + targetData.Q3Target + targetData.Q4Target;
       }
       */
    }




    //------------------------------------------------------------------
    //------------------------ PRIVATE METHODS -------------------------
    //------------------------------------------------------------------

    function dispatchSummeryServiceError(serviceName) {
      var errObj = commonUtils.getServiceFailErrorObj(serviceName);
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR, errObj);
    }

  }

})();

