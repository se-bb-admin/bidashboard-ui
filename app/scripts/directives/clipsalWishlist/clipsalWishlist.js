/**
 * @ngdoc directive
 * @name Clipspec Wishlist directive
 *
 * @description
 *  Clipspec Wishlist directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('clipsalWishlist', clipsalWishlist);

  function clipsalWishlist($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, ClipsalWishlistCountryVO) {

    var directive = {
      restrict: 'E', replace: true,
      scope: {
        vm: "=vm"
      },
      templateUrl: 'scripts/directives/clipsalWishlist/clipsalWishlist.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Australia'];

      var _clipsalWishlistReportData = null;
      var _clipsalWishlistChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];

      scope.regUsers = 0;
      scope.uniqueActiveUsers = 0;
      scope.productsAddedToBOM = 0;
      scope.projectsCreated = 0;
      scope.projectsSentToElectricians = 0;
      scope.bomSentToElectricians = "Data is not available in GA";
      scope.launchDate = appConstants.DATES.CLIPSAL_WISH_LIST_AUSTRALIA_LAUNCHED_DATE;

      scope.clipsalWishlistLaunchDays = 0;
      scope.clipsalWishlistLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, appConstants.DATES.CLIPSAL_WISH_LIST_AUSTRALIA_LAUNCHED_DATE);


      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      //------------------------------------------------------------------
      //-------------- MY PACT CHART INIT SHOW HIDE PROPERTIES -----------
      //------------------------------------------------------------------
      var NO_OF_REG_USERS = "No. of registered users";
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_PROJ_CREATED = "No. of project created";
      var NO_OF_PROJ_TO_ELECTRICIANS = "No. of projects sent to electricians";

      var DEFAULT_CHART_NAME = "ChartRegisteredUser"; // "ChartActiveUser";

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};


      //------------------------------------------------------------------
      //-----------------------  For Chart Options -----------------------
      //------------------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.clipsalWishlistChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.clipsalWishlistChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.clipsalWishlistChartName = DEFAULT_CHART_NAME;
      scope.clipsalWishlistChartLabel = NO_OF_REG_USERS;//NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;

      scope.onClipsalWishlistChartLinkChange = onClipsalWishlistChartLinkChange;
      scope.onClipsalWishlistChartOptionChange = onClipsalWishlistChartOptionChange;

      scope.chartReady = chartReady;

      init();


      /**
       *
       * @returns {undefined}
       */
      function init() {
        //--- Init Countries data
        initCountries();
        // Get Report Data
        getClipsalWishlistReportData();
        // Get Chart Data
        getClipsalWishlistChartData();

        //Add Event Listener  When page resize
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }



      /**
       * Get Clipsal Wishlist report data from NODE
       *
       * @returns {undefined}
       */
      function getClipsalWishlistReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getClipsalWishlistReportData().then(
                function (result) {
                  logger.log("Clipsal Wishlist::getClipsalWishlistReportData::Result::Success");
                  _clipsalWishlistReportData = result;
                  parseReportData(_clipsalWishlistReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _clipsalWishlistReportData = null;
                  logger.log("Clipsal Wishlist::getClipsalWishlistReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }



      /**
       * Get Clipsal Wishlist CHART data from NODE
       *
       * @returns {undefined}
       */
      function getClipsalWishlistChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getClipsalWishlistChartData().then(
                function (result) {
                  logger.log("Clipsal Wishlist::getClipsalWishlistChartData::Result::Success");
                  _clipsalWishlistChartData = result;
                  parseChartData(_clipsalWishlistChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _clipsalWishlistChartData = null;
                  logger.log("Clipsal Wishlist::getClipsalWishlistChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }



      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }



      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          console.log("clipsalWishlist.js :: loadDefaultChart :: Timer :: Start ");
          onClipsalWishlistChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("clipsalWishlist.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }



      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }


      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var clipsalWishlist = {};
        //--- clipsalWishlist
        for (var i = 0; i < countryNameList.length; i++) {
          var capsName = countryNameList[i].toUpperCase();
          clipsalWishlist.countryName = countryNameList[i];
          clipsalWishlist.countryGaId = eval('appConstants.SITE_GA_IDS.CLIPSAL_WISH_LIST_' + capsName + '_ID');
          clipsalWishlist.launchDate = eval('appConstants.DATES.CLIPSAL_WISH_LIST_' + capsName + '_LAUNCHED_DATE');
          clipsalWishlist.launchDateStr = eval('appConstants.DATES.CLIPSAL_WISH_LIST_' + capsName + '_LAUNCHED_DATE_STR');
          clipsalWishlist.regUsers = 0;
          clipsalWishlist.uniqueActiveUsers = 0;
          clipsalWishlist.productsAddedToBOM = 0;
          clipsalWishlist.projectsCreated = 0;
          clipsalWishlist.projectsSentToElectricians = 0;
          clipsalWishlist.bomSentToElectricians = 0;
          clipsalWishlist.regUsersChart = 0;
          clipsalWishlist.uniqueActiveUsersChart = 0;
          clipsalWishlist.projectsCreatedChart = 0;
          clipsalWishlist.projectsSentToElectriciansChart = 0;
          clipsalWishlist.bomSentToElectricians = 0;
          //--- Push country to Countries Array
          scope.countries.push(new ClipsalWishlistCountryVO(clipsalWishlist));
        }
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload ClipsalWishlist Chart.." + scope.selectedChartTab);
        onClipsalWishlistChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.countries[0].regUsers = parseFloat(reportData.regUsers.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
        scope.countries[0].projectsCreated = parseFloat(reportData.projectsCreated.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].projectsSentToElectricians = parseFloat(reportData.projectsSentToElectricians.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].productsAddedToBOM = parseFloat(reportData.productsAddedToBOM.totalsForAllResults['ga:totalEvents']);

        //----------------- SET Default Value ---------------------
        scope.regUsers = scope.countries[0].regUsers;
        scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
        scope.projectsCreated = scope.countries[0].projectsCreated;
        scope.projectsSentToElectricians = scope.countries[0].projectsSentToElectricians;
        scope.productsAddedToBOM = scope.countries[0].productsAddedToBOM;
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        scope.countries[0].regUsersChart = gaChartUtil.populateGAChartData(chartData.regUsers, "dateFormatted", "regUsers", "Date", "Registered Users");
        scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        scope.countries[0].projectsCreatedChart = gaChartUtil.populateGAChartData(chartData.projectsCreated, "dateFormatted", "projectsCreated", "Date", "Projects Created");
        scope.countries[0].projectsSentToElectriciansChart = gaChartUtil.populateGAChartData(chartData.projectsSentToElectricians, "dateFormatted", "projectsSentToElectricians", "Date", "Projects Sent to Electricians");
        console.log("Chart Data.. Prepare...");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onClipsalWishlistChartLinkChange(chartName) {                             //When you click on a link Chart will reset to Month Chart.
        scope.clipsalWishlistChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.clipsalWishlistChartName = chartName;
        onClipsalWishlistChartOptionChange(null, chartOptionDfltSelInx);
      }



      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onClipsalWishlistChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.clipsalWishlistChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.clipsalWishlistChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.clipsalWishlistChartTypeSelected;
        var chartName = scope.clipsalWishlistChartName;

        if (isInitChartData === false) {
          initChartData();
        }


        // ADD Chart
        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.clipsalWishlistChartLabel = NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartRegisteredUser":
          {
            scope.clipsalWishlistChartLabel = NO_OF_REG_USERS;
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].regUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].regUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].regUsersChart.month;
            }
            break;
          }
          case "ChartProjectCreated":
          {
            scope.clipsalWishlistChartLabel = NO_OF_PROJ_CREATED;
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].projectsCreatedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].projectsCreatedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].projectsCreatedChart.month;
            }
            break;
          }
          case "ChartProjectSendToElectician":
          {
            scope.clipsalWishlistChartLabel = NO_OF_PROJ_TO_ELECTRICIANS;
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].projectsSentToElectriciansChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].projectsSentToElectriciansChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].projectsSentToElectriciansChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }



      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.clipsalWishlistChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }


    }//END Link..

  }

})();