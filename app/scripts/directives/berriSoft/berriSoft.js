/**
 * @ngdoc directive
 * @name Berri Soft directive
 *
 * @description
 *  Quick Ref directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('berriSoft', berriSoft);

  function berriSoft($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, BerriSoftCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/berriSoft/berriSoft.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['France', 'Italy', 'Spain', 'Netherlands',
        'Brazil', 'Indonesia', 'Germany', 'United Kingdom', 'Belgium',
        'Turkey', 'Russia', 'United States', 'India', 'Australia', 'Mexico',
        'Poland', 'China', 'South Africa', 'Saudi Arabia', 'Thailand'];


      var _berriSoftReportData = null;
      var _berriSoftChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];

      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];

      scope.uniqueActvUsers = 0;
      scope.configLaunched = 0;
      scope.regUsers = 0;
      scope.launchDate = appConstants.DATES.BERRI_SOFT_LAUNCHED_DATE;

      scope.berriSoftLaunchDays = 0;
      scope.berriSoftLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, appConstants.DATES.BERRI_SOFT_LAUNCHED_DATE);

      //------------------------------------------------------------------
      //-------------- MY PACT CHART INIT SHOW HIDE PROPERTIES -----------
      //------------------------------------------------------------------
      scope.isGAChartVisible = true;
      scope.isPaceChartVisible = false;

      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_CONFIGURATION_LAUNCHED = "No. of Configuration launched";
      var NO_OF_BOM_ISSUED = "No of Times BOM Issued";

      scope.onCountryDropDownChange = onCountryDropDownChange;

      scope.paceChartStyle = commonUtils.getPaceChartStyle();
      scope.paceChartSource = {};
      scope.showPaceChart = showPaceChart;
      var isInitPaceChartData = false;
      var paceChartObj = {};

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};


      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.berriSoftChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.berriSoftChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.berriSoftChartName = "ChartActiveUser";
      scope.berriSoftChartLabel = NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;

      scope.onBerriSoftChartOptionChange = onBerriSoftChartOptionChange;
      scope.onBerriSoftChartLinkChange = onBerriSoftChartLinkChange;

      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        //--- Init Countries data
        initCountries();
        // Get Report Data
        getBerriSoftReportData();
        // Get Chart Data
        getBerriSoftChartData();

        //Add Event Listener  When page resize
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.berriSoftUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initPaceChartData();
          }
        });

      }

      /**
       * Get Quick Ref report data from NODE
       *
       * @returns {undefined}
       */
      function getBerriSoftReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getBerriSoftReportData().then(
                function (result) {
                  logger.log("Quick Ref::getBerriSoftReportData::Result::Success");
                  _berriSoftReportData = result;
                  parseReportData(_berriSoftReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _berriSoftReportData = null;
                  logger.log("Quick Ref::getBerriSoftReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get Quick Ref CHART data from NODE
       *
       * @returns {undefined}
       */
      function getBerriSoftChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getBerriSoftChartData().then(
                function (result) {
                  logger.log("Quick Ref::getBerriSoftChartData::Result::Success");
                  _berriSoftChartData = result;
                  parseChartData(_berriSoftChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _berriSoftChartData = null;
                  logger.log("Quick Ref::getBerriSoftChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("berriSoft.js :: loadDefaultChart :: Timer :: Start ");
          onBerriSoftChartLinkChange('ChartActiveUser');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("berriSoft.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initPaceChartData() {
        paceChartObj = {};
        paceChartObj.type = "AreaChart";
        paceChartObj.displayed = false;
        paceChartObj.options = commonUtils.getPaceChartOptions();

        isInitPaceChartData = true;
      }

      /***
       *  Onclick Registererd users link click..
       *  Show PACE Chart Hide GA Chart
       *
       * @returns {undefined}
       *
       */
      function showPaceChart() {
        scope.isGAChartVisible = false;
        scope.isPaceChartVisible = true;

        //Show Hide Chart
        onBerriSoftChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }



      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var berriSoft = {};
        for (var i = 0; i < countryNameList.length; i++) {
          berriSoft.countryName = countryNameList[i];
          var capsCountryName = constructName(countryNameList[i].toUpperCase());
          berriSoft.countryGaId = appConstants.SITE_GA_IDS.BERRI_SOFT_ID;

          var launchDate = 'BERRISOFT_' + capsCountryName + '_LAUNCHED_DATE';
          //console.log("VAR launchDate:: "+ launchDate);

          berriSoft.launchDate = appConstants.DATES[launchDate];
          //console.dir (berriSoft.launchDate);
          berriSoft.berriSoftLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, berriSoft.launchDate);

          berriSoft.regUsers = 0;
          berriSoft.uniqueActiveUsers = 0;
          berriSoft.configLaunched = 0;
          berriSoft.uniqueActiveUsersChart = 0;
          berriSoft.configLaunchedChart = 0;
          berriSoft.bomIssued = 0;
          //--- Push country to Countries Array
          scope.countries.push(new BerriSoftCountryVO(berriSoft));
        }

        //Set default Country to update data binding
        setDefaultCountry();
      }

      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }

      /**
       *
       * @param {type} name
       * @returns {unresolved}
       */
      function constructName(name) {
        var result = name.split(' ').join('_');
        return result;
      }

      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;
        logger.log(selectedCountryName);
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.launchDate = scope.countries[index].launchDate;
        scope.berriSoftLaunchDays = scope.countries[index].berriSoftLaunchDays;

        setTableDataAndLink('UNIQUE_ACTIVE_USERS', scope.countries[index].uniqueActiveUsers);
        setTableDataAndLink('CONFIG_LAUNCHED', scope.countries[index].configLaunched);
        setTableDataAndLink('BOM_ISSUED', scope.countries[index].bomIssued);

        onBerriSoftChartLinkChange('ChartActiveUser');
      }



      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload BerriSoft Chart.." + scope.selectedChartTab);
        onBerriSoftChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        //console.log("******** HELLO********");
        //console.dir(reportData);
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          scope.countries[i].configLaunched = parseFloat(reportData[keyName].configLaunched.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].bomIssued = parseFloat(reportData[keyName].bomIssued.totalsForAllResults['ga:totalEvents']);
        }

        //----------------- SET Default Value ---------------------
        scope.berriSoftLaunchDays = scope.countries[0].berriSoftLaunchDays;
        scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
        scope.configLaunched = scope.countries[0].configLaunched;
        scope.bomIssued = scope.countries[0].bomIssued;
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          scope.countries[i].configLaunchedChart = gaChartUtil.populateGAChartData(chartData[keyName].configLaunched, "dateFormatted", "configLaunched", "Date", "Configuration Launched");
          scope.countries[i].bomIssued = gaChartUtil.populateGAChartData(chartData[keyName].bomIssued, "dateFormatted", "bomIssued", "Date", "No of Times BOM Issued");
        }
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onBerriSoftChartLinkChange(chartName) {
        scope.isGAChartVisible = true;
        scope.isPaceChartVisible = false;

        //When you click on a link Chart will reset to Month Chart.
        scope.berriSoftChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.berriSoftChartName = chartName;
        onBerriSoftChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onBerriSoftChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.berriSoftChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.berriSoftChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.berriSoftChartTypeSelected;
        var chartName = scope.berriSoftChartName;

        if (isInitChartData === false) {
          initChartData();
        }

        //check for PACE Chart
        if (scope.isPaceChartVisible) {
          scope.quickQuotationChartLabel = NO_OF_REG_USERS;

          loadPaceChart(scope.selectedChartTab);
          return;
        }


        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.berriSoftChartLabel = NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartConfigLaunched":
          {
            scope.berriSoftChartLabel = NO_OF_CONFIGURATION_LAUNCHED;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].configLaunchedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].configLaunchedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].configLaunchedChart.month;
            }
            break;
          }
          case "ChartBomIssued":
          {
            scope.berriSoftChartLabel = NO_OF_BOM_ISSUED;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].bomIssued.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].bomIssued.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].bomIssued.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




      /**
       * Load PACE Chart as per selected tab (Day | Week | Month)
       *
       * @param {type} selTab
       * @returns {undefined}
       */
      function loadPaceChart(selTab) {
        var chartTypeSelected = scope.chartOptions[scope.selectedChartTab];

        //TODO :: Also Need To Check "scope.paceDataRegUserOverPeriod.myPactBasicUsersReg" val exist or not..
        if (isInitPaceChartData === false) {
          initPaceChartData();
        }

        if (chartTypeSelected.toLowerCase() === 'day') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.berriSoftUsersReg.day;
        } else if (chartTypeSelected.toLowerCase() === 'week') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.berriSoftUsersReg.week;
        } else if (chartTypeSelected.toLowerCase() === 'month') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.berriSoftUsersReg.month;
        }

        //Set value to Scope
        scope.paceChartSource = paceChartObj;
      }


      /**
       *
       * @param {type} filedName
       * @param {type} value
       * @returns {undefined}
       */
      function setTableDataAndLink(filedName, value) {
        if (filedName === "UNIQUE_ACTIVE_USERS") {
          if (value === 0) {
            scope.disableUniqueActiveUsers = true;
            angular.element("#uniqueActiveUsers").removeClass("link");
          } else {
            scope.disableUniqueActiveUsers = false;
            angular.element("#uniqueActiveUsers").addClass("link");
          }
          scope.uniqueActiveUsers = value;

        } else if (filedName === "CONFIG_LAUNCHED") {
          if (value === 0) {
            scope.disableConfLaunched = true;
            angular.element("#configLaunched").removeClass("link");
          } else {
            scope.disableConfLaunched = false;
            angular.element("#configLaunched").addClass("link");
          }
          scope.configLaunched = value;
        } else if (filedName === "BOM_ISSUED") {
          if (value === 0) {
            scope.disableBOMIssued = true;
            angular.element("#bomIssued").removeClass("link");
          } else {
            scope.disableBOMIssued = false;
            angular.element("#bomIssued").addClass("link");
          }
          scope.bomIssued = value;
        }
      }




    }//END Link..

  }
  ;
})();