/**
 * @ngdoc directive
 * @name Rapsody directive
 *
 * @description
 *  Rapsody directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('rapsody', rapsody);

  function rapsody($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, RapsodyCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/rapsody/rapsody.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Country1'];

      //var _RapsodyReportData = null;
      //var _RapsodyChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      //var selectedCountryGaId = appConstants.SITE_GA_IDS.RAPSODY_ID;
      //scope.uniqueActiveUsers = 0;
      //scope.rapsodyLaunchDays = 0;
      //scope.rapsodyLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, appConstants.DATES.RAPSODY_LAUNCHED_DATE);
      scope.launchDate = appConstants.DATES.RAPSODY_LAUNCHED_DATE;

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------

      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.rapsodyTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.rapsodyChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.rapsodyChartName = "ChartRegisteredUsers";
      scope.rapsodyChartLabel = NO_OF_REG_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = false;
      scope.loadDefaultChart = false;

      scope.onRapsodyChartOptionChange = onRapsodyChartOptionChange;
      scope.onRapsodyChartLinkChange = onRapsodyChartLinkChange;


      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
//                            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        // Init Countries data
        initCountries();
        // Get Report Data
        //getRapsodyReportData();
        // Get Chart Data
        //getRapsodyChartData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.rapsodyUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initChartData();
            //Load Default Chart
            loadDefaultChart();
          }
        });
      }

      /**
       * Get Rapsody report data from NODE
       *
       * @returns {undefined}

       function getRapsodyReportData() {
       $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
       nodeGAService.getRapsodyReportData().then(
       function (result) {
       logger.log("Rapsody::getRapsodyReportData::Result::Success");
       _RapsodyReportData = result;
       parseReportData(_RapsodyReportData);
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
       },
       function (error) {
       _RapsodyReportData = null;
       logger.log("Rapsody::getRapsodyReportData::Result::FAIL");
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
       }
       );
       }
       */

      /**
       * Get Rapsody CHART data from NODE
       *
       * @returns {undefined}

       function getRapsodyChartData() {

       $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

       nodeGAService.getRapsodyChartData().then(
       function (result) {
       logger.log("Rapsody::getRapsodyChartData::Result::Success");
       _RapsodyChartData = result;
       parseChartData(_RapsodyChartData);
       //Load Default Chart
       loadDefaultChart();
       //Hide Loader
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
       },
       function (error) {
       _RapsodyChartData = null;
       logger.log("Rapsody::getRapsodyChartData::Result::FAIL");
       //Hide Loader
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
       }
       );

       }
       */


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("rapsody.js :: loadDefaultChart :: Timer :: Start ");
          onRapsodyChartLinkChange('ChartRegisteredUsers');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("rapsody.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );


      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var rapsody = {};
        //--- Rapsody
        for (var i = 0; i < countryNameList.length; i++) {
          rapsody.countryName = countryNameList[i];
          rapsody.countryGaId = appConstants.SITE_GA_IDS.RAPSODY_ID;
          rapsody.launchDate = appConstants.DATES.RAPSODY_LAUNCHED_DATE;
          rapsody.launchDateStr = appConstants.DATES.RAPSODY_LAUNCHED_DATE_STR;
          rapsody.regUsers = 0;
          rapsody.uniqueActiveUsers = 0;
          rapsody.projectsCreated = 0;
          rapsody.uniqueActiveUsersChart = 0;
          rapsody.projectsCreatedChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new RapsodyCountryVO(rapsody));
        }
      }

      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload Rapsody Chart.." + scope.selectedChartTab);
        onRapsodyChartOptionChange(null, scope.selectedChartTab);

      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}

       function parseReportData(reportData) {
       scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
       scope.countries[0].projectsCreated = parseFloat(reportData.projectList.totalsForAllResults['ga:totalEvents']);
       //----------------- SET Default Value ---------------------
       scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
       scope.projectsCreated = scope.countries[0].projectsCreated;
       } */




      /**
       *
       * @param {type} chartData
       * @returns {undefined}

       function parseChartData(chartData) {
       scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
       scope.countries[0].projectsCreatedChart = gaChartUtil.populateGAChartData(chartData.projectList, "dateFormatted", "projectList", "Date", "Projects Created");

       console.log("Chart Data.. Prepare...");
       }*/




      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onRapsodyChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.rapsodyChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.rapsodyChartName = chartName;
        onRapsodyChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onRapsodyChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.rapsodyChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.rapsodyChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.rapsodyChartTypeSelected;
        var chartName = scope.rapsodyChartName;

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartRegisteredUsers":
          {
            scope.rapsodyChartLabel = NO_OF_REG_USERS;

            if (chartType === 'Day') {
              chartObj.data = scope.paceDataRegUserOverPeriod.rapsodyUsersReg.day;
            } else if (chartType === 'Week') {
              chartObj.data = scope.paceDataRegUserOverPeriod.rapsodyUsersReg.week;
            } else if (chartType === 'Month') {
              chartObj.data = scope.paceDataRegUserOverPeriod.rapsodyUsersReg.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




    }//END Link..

  }
  ;
})();