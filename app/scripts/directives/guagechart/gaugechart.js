(function () {
  'use strict';
  angular.module('se-branding')
          .directive('gaugeChart', function () {
            return {
              restrict: 'AE',
              scope: {
                target: '=',
                achive: '='
              },
              templateUrl: 'scripts/directives/guagechart/guagechart.html',
              link: function (scope) {
                alert(scope.target);
                scope.guageChartObject = {
                  type: 'Guage',
                  options: {
                    width: 200,
                    height: 200
                  },
                  data: [
                    ['Label', 'Value'],
                    ['Achieved %', 50]
                  ]
                };
              }
            };
          });
})();