/**
 * @ngdoc directive
 * @name Global Meter App directive
 *
 * @description
 *  Global Meter App directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('globalMeterApp', globalMeterApp);

  function globalMeterApp($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, GlobalMeterAppCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
      },
      templateUrl: 'scripts/directives/globalMeterApp/globalMeterApp.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Country1'];
      var _globalMeterReportData = null;
      var _globalMeterChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var timer = null;
      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      scope.uniqueActiveUsers = 0;
      scope.registeredUsers = 0;
      scope.productSearches = 0;
      scope.productViews = 0;
      scope.launchDate = appConstants.DATES.GLOBAL_METER_APP_LAUNCHED_DATE;
      scope.GLOBAL_METER_FOOTER_NOTE = appConstants.NOTE.GLOBAL_METER_FOOTER_NOTE;

      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_REG_USERS = "No. of registered users";
      var NO_OF_PROD_SEARCH = "No. of product searched";
      var NO_OF_PROD_VIEW = "No. of product views";
      var DEFAULT_CHART_NAME = "ChartRegisteredUsers";
      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};
      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.globalMeterTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.globalMeterAppChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.globalMeterAppChartName = DEFAULT_CHART_NAME;
      scope.globalMeterAppChartLabel = NO_OF_REG_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;
      scope.onGlobalMeterAppChartOptionChange = onGlobalMeterAppChartOptionChange;
      scope.onGlobalMeterAppChartLinkChange = onGlobalMeterAppChartLinkChange;
      scope.chartReady = chartReady;
      init();
      /**
       *
       * @returns {undefined}
       */
      function init() {
        // Init Countries data
        initCountries();
        // Get Report Data
        getGlobalMeterReportData();
        // Get Chart Data
        getGlobalMeterChartData();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);
        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }

      /**
       * Get Global Meter report data from NODE
       *
       * @returns {undefined}
       */
      function getGlobalMeterReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getGlobalMeterReportData().then(
                function (result) {
                  logger.log("Global Meter::getGlobalMeterReportData::Result::Success");
                  _globalMeterReportData = result;
                  parseReportData(_globalMeterReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _globalMeterReportData = null;
                  logger.log("Global Meter::getGlobalMeterReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get Global Meter CHART data from NODE
       *
       * @returns {undefined}
       */
      function getGlobalMeterChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getGlobalMeterChartData().then(
                function (result) {
                  logger.log("Global Meter::getGlobalMeterChartData::Result::Success");
                  _globalMeterChartData = result;
                  parseChartData(_globalMeterChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _globalMeterChartData = null;
                  logger.log("Global Meter::getGlobalMeterChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();
        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("globalMeter.js :: loadDefaultChart :: Timer :: Start ");
          onGlobalMeterAppChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("globalMeter.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }

      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var globalMeterApp = {};
        //--- clipsalWishlist
        for (var i = 0; i < countryNameList.length; i++) {
          globalMeterApp.countryName = countryNameList[i];
          globalMeterApp.countryGaId = appConstants.SITE_GA_IDS.GLOBAL_METER_APP_ID;
          globalMeterApp.launchDate = appConstants.DATES.GLOBAL_METER_APP_LAUNCHED_DATE;
          globalMeterApp.launchDateStr = appConstants.DATES.GLOBAL_METER_APP_LAUNCHED_DATE_STR;
          globalMeterApp.regUsers = 0;
          globalMeterApp.uniqueActiveUsers = 0;
          globalMeterApp.productSearches = 0;
          globalMeterApp.productViews = 0;
          globalMeterApp.uniqueActiveUsersChart = 0;
          globalMeterApp.registeredUsersChart = 0;
          globalMeterApp.productSearchesChart = 0;
          globalMeterApp.productViewsChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new GlobalMeterAppCountryVO(globalMeterApp));
        }
      }

      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload Global Meter App Chart.." + scope.selectedChartTab);
        onGlobalMeterAppChartOptionChange(null, scope.selectedChartTab);
      }

      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
        scope.countries[0].registeredUsers = parseFloat(reportData.registeredUsers.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].productSearches = parseFloat(reportData.productSearches.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].productViews = parseFloat(reportData.productViews.totalsForAllResults['ga:totalEvents']);

        //----------------- SET Default Value ---------------------
        scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
        scope.registeredUsers = scope.countries[0].registeredUsers;
        scope.productSearches = scope.countries[0].productSearches;
        scope.productViews = scope.countries[0].productViews;
      }

      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        scope.countries[0].registeredUsersChart = gaChartUtil.populateGAChartData(chartData.registeredUsers, "dateFormatted", "registeredUsers", "Date", "Registered Users");
        scope.countries[0].productSearchesChart = gaChartUtil.populateGAChartData(chartData.productSearches, "dateFormatted", "productSearches", "Date", "Product Searches");
        scope.countries[0].productViewsChart = gaChartUtil.populateGAChartData(chartData.productViews, "dateFormatted", "productViews", "Date", "Product Views");
        console.log("Chart Data.. Prepare...");
      }

      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onGlobalMeterAppChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.globalMeterAppChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.globalMeterAppChartName = chartName;
        onGlobalMeterAppChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onGlobalMeterAppChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;
        scope.selectedChartTab = selTab;
        scope.globalMeterAppChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.globalMeterAppChartToolTip = scope.chartToolTips[scope.selectedChartTab];
        var chartType = scope.globalMeterAppChartTypeSelected;
        var chartName = scope.globalMeterAppChartName;
        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.globalMeterAppChartLabel = NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartRegisteredUsers":
          {
            scope.globalMeterAppChartLabel = NO_OF_REG_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].registeredUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].registeredUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].registeredUsersChart.month;
            }
            break;
          }
          case "ChartProductSearches":
          {
            scope.globalMeterAppChartLabel = NO_OF_PROD_SEARCH;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].productSearchesChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].productSearchesChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].productSearchesChart.month;
            }
            break;
          }
          case "ChartProductViews":
          {
            scope.globalMeterAppChartLabel = NO_OF_PROD_VIEW;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].productViewsChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].productViewsChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].productViewsChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.globalMeterAppChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }

    }//END Link..
  }


})();