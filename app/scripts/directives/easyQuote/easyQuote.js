/**
 * @ngdoc directive
 * @name Easy Quote directive
 *
 * @description
 *  Easy Quote directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('easyQuote', easyQuote);

  function easyQuote($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, EasyQuoteCountryVO, currencyConverterService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceData: "=paceData"
      },
      templateUrl: 'scripts/directives/easyQuote/easyQuote.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
//			console.log('\n--> easyQuote: link: paceData: ', scope.paceData, '\n');
      var countryNameList = ['Brazil', 'Russia', 'Spain', 'South Africa'];
      var _easyQuoteReportData = null;
      var _easyQuoteChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var timer = null;


      //------------------------------------------------------------------
      //--------------------------- Global Properties --------------------
      //------------------------------------------------------------------

      var oneRubleToEuroMultiplier = currencyConverterService.getRubleToEuroConversionVal();
      var oneRealToEuroMultiplier = currencyConverterService.getRealToEuroConversionVal();
      var oneRSARandToEuroMultiplier = 0.060;

      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];
      scope.easyQuoteBrazilLaunchDate = appConstants.DATES.EASY_QUOTE_BRAZIL_LAUNCHED_DATE;
      scope.easyQuoteRussiaLaunchDate = appConstants.DATES.EASY_QUOTE_RUSSIA_LAUNCHED_DATE;
      scope.easyQuoteSpainLaunchDate = appConstants.DATES.EASY_QUOTE_SPAIN_LAUNCHED_DATE;
      scope.easyQuoteSouthAfricaLaunchDate = appConstants.DATES.EASY_QUOTE_SOUTH_AFRICA_LAUNCHED_DATE;
      scope.EASY_QUOTE_BOM_FOOTER_NOTE = appConstants.NOTE.EASY_QUOTE_BOM_FOOTER_NOTE;
      scope.regUsers = 0;
      scope.loginUsers = 0;
      scope.uniqueActiveUsers = 0;
      scope.createProject = 0;
      scope.projectCompleted = 0;
      scope.bomPrice = 0;
      scope.referenceCount = 0;
      scope.sendMail = 0;
      scope.easyQuoteTotalRegUsers = 0;
      scope.easyQuoteTotalLoginUsers = 0;
      scope.easyQuoteTotalUniqueActiveUsers = 0;
      scope.easyQuoteTotalCreateProject = 0;
      scope.easyQuoteTotalProjectCompleted = 0;
      scope.easyQuoteTotalBomPrice = 0;
      scope.easyQuoteTotalReferenceCount = 0;
      scope.easyQuoteTotalSendMail = 0;
      scope.easyQuoteLaunchDays = 0;
      scope.totalBomPrice = 0;

      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;


      //------------------------------------------------------------------
      //--------------- MY PACT CHART INIT SHOW HIDE PROPERTIES ----------
      //------------------------------------------------------------------

      var NO_OF_REG_USERS = "No. of registered users";
      var NO_OF_LOGGED_USER = "No. of logged in users";
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_PROJ_CREATED = "No. of projects created";
      var NO_OF_PROJ_COMPLETED_CONFIG = "No. of projects completed";
      var PRICE_SHARED = "Value of projects shared with customer";
      var NO_OF_WIRIN_DEVICES = "No. wiring devices";
      var NO_OF_PROJECT_SHARED = "No. of projects shared with customer";
      var DEFAULT_CHART_NAME = "ChartRegisteredUsers";
      scope.onCountryDropDownChange = onCountryDropDownChange;
      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------------------
      //----------------------  For Chart Options ------------------------
      //------------------------------------------------------------------

      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.easyQuoteTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.easyQuoteChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.easyQuoteChartName = DEFAULT_CHART_NAME;
      scope.easyQuoteChartLabel = NO_OF_REG_USERS;//NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;
      scope.disableRegUsers = false;
      scope.disableLoginUsers = false;
      scope.disableUniqueActiveUsers = false;
      scope.disableCreateProject = false;
      scope.disableBOMPice = false;
      scope.disableProjectCompleted = false;
      scope.disableBomPrice = false;
      scope.disableReferenceCount = false;
      scope.disableSendMail = false;
      scope.onEasyQuoteChartOptionChange = onEasyQuoteChartOptionChange;
      scope.onEasyQuoteChartLinkChange = onEasyQuoteChartLinkChange;
      scope.chartReady = chartReady;


      init();


      /**
       *
       * @returns {undefined}
       */
      function init() {
        // Init Countries data
        initCountries();
        // Get Report Data
        getEasyQuoteReportData();
        // Get Chart Data
        getEasyQuoteChartData();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);
        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }


      /**
       * Get Easy Quote report data from NODE
       *
       * @returns {undefined}
       */
      function getEasyQuoteReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getEasyQuoteReportData().then(
                function (result) {
                  logger.log("Easy Quote::getEasyQuoteReportData::Result::Success");
                  _easyQuoteReportData = result;
                  parseReportData(_easyQuoteReportData);
//							console.log('\n\n==> parseReportData: _easyQuoteReportData ::> ', _easyQuoteReportData, '\n\n ')
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _easyQuoteReportData = null;
                  logger.log("Easy Quote::getEasyQuoteReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       * Get Easy Quote CHART data from NODE
       *
       * @returns {undefined}
       */
      function getEasyQuoteChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getEasyQuoteChartData().then(
                function (result) {
                  logger.log("Easy Quote::getEasyQuoteChartData::Result::Success");
                  _easyQuoteChartData = result;
                  parseChartData(_easyQuoteChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _easyQuoteChartData = null;
                  logger.log("Easy Quote::getEasyQuoteChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          console.log("easyQuote.js :: loadDefaultChart :: Timer :: Start ");
          onEasyQuoteChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("easyQuote.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }


      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       *
       * @returns {undefined}
       */
      function initCountries() {
        var easyQuote = {};
        //--
        for (var i = 0; i < countryNameList.length; i++) {

          easyQuote.countryName = countryNameList[i];

          if (easyQuote.countryName.toUpperCase() === "BRAZIL") {
            easyQuote.launchDate = scope.easyQuoteBrazilLaunchDate;
          } else if (easyQuote.countryName.toUpperCase() === "RUSSIA") {
            easyQuote.launchDate = scope.easyQuoteRussiaLaunchDate;
          } else if (easyQuote.countryName.toUpperCase() === "SPAIN") {
            easyQuote.launchDate = scope.easyQuoteSpainLaunchDate;
          } else if (easyQuote.countryName.toUpperCase() === "SOUTH AFRICA") {
            easyQuote.launchDate = scope.easyQuoteSouthAfricaLaunchDate;
          }
          easyQuote.easyQuoteLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, easyQuote.launchDate);

          easyQuote.regUsers = 0;
          easyQuote.uniqueActiveUsers = 0;
          easyQuote.loginUsers = 0;
          easyQuote.createProject = 0;
          easyQuote.projectCompleted = 0;
          easyQuote.bomPrice = 0;
          easyQuote.referenceCount = 0;
          easyQuote.sendMail = 0;
          easyQuote.regUsersChart = 0;
          easyQuote.uniqueActiveUsersChart = 0;
          easyQuote.loginUsersChart = 0;
          easyQuote.createProjectChart = 0;
          easyQuote.projectCompletedChart = 0;
          easyQuote.bomPriceChart = 0;
          easyQuote.referenceCountChart = 0;
          easyQuote.sendMailChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new EasyQuoteCountryVO(easyQuote));
        }
        //Set default Country to update data binding
        setDefaultCountry();
      }


      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }


      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;
//				logger.log(selectedCountryName);
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }
        scope.launchDate = scope.countries[index].launchDate;
        scope.easyQuoteLaunchDays = scope.countries[index].easyQuoteLaunchDays;

        setTableDataAndLink('Reg. User', scope.countries[index].regUsers);
        setTableDataAndLink('Login Users', scope.countries[index].loginUsers);
        setTableDataAndLink('Active Users', scope.countries[index].uniqueActiveUsers);
        setTableDataAndLink('Create Project', scope.countries[index].createProject);
        setTableDataAndLink('Project Completed', scope.countries[index].projectCompleted);
        setTableDataAndLink('Bom Price', scope.countries[index].bomPrice);
        setTableDataAndLink('Reference Count', scope.countries[index].referenceCount);
        setTableDataAndLink('Send Mail', scope.countries[index].sendMail);
        onEasyQuoteChartLinkChange(DEFAULT_CHART_NAME);
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
//				logger.log("Reload EASY QUOTE Chart.." + scope.selectedChartTab);
        //->
        onEasyQuoteChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.totalBomPrice = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].regUsers = parseFloat(reportData[keyName].regUsers.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].loginUsers = parseFloat(reportData[keyName].loginUsers.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          scope.countries[i].createProject = parseFloat(reportData[keyName].createProject.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].projectCompleted = parseFloat(reportData[keyName].projectCompleted.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].bomPrice = parseFloat(reportData[keyName].bomPrice.totalsForAllResults['ga:eventValue']);
          scope.countries[i].referenceCount = parseFloat(reportData[keyName].referenceCount.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].sendMail = parseFloat(reportData[keyName].sendMail.totalsForAllResults['ga:totalEvents']);
          scope.easyQuoteTotalRegUsers = scope.easyQuoteTotalRegUsers + scope.countries[i].regUsers;
          scope.easyQuoteTotalLoginUsers = scope.easyQuoteTotalLoginUsers + scope.countries[i].loginUsers;
          scope.easyQuoteTotalUniqueActiveUsers = scope.easyQuoteTotalUniqueActiveUsers + scope.countries[i].uniqueActiveUsers;
          scope.easyQuoteTotalCreateProject = scope.easyQuoteTotalCreateProject + scope.countries[i].createProject;
          scope.easyQuoteTotalProjectCompleted = scope.easyQuoteTotalProjectCompleted + scope.countries[i].projectCompleted;
          scope.easyQuoteTotalBomPrice = scope.easyQuoteTotalBomPrice + scope.countries[i].bomPrice;
          scope.easyQuoteTotalReferenceCount = scope.easyQuoteTotalReferenceCount + scope.countries[i].referenceCount;
          scope.easyQuoteTotalSendMail = scope.easyQuoteTotalSendMail + scope.countries[i].sendMail;
        }

        scope.totalBomPrice = ((scope.countries[0].bomPrice) * oneRealToEuroMultiplier) + ((scope.countries[1].bomPrice) * oneRubleToEuroMultiplier) +
                (scope.countries[2].bomPrice) + ((scope.countries[2].bomPrice) * oneRSARandToEuroMultiplier);

        //----------------- SET Default Value ---------------------

        scope.launchDate = scope.countries[0].launchDate;
        scope.easyQuoteLaunchDays = scope.countries[0].easyQuoteLaunchDays;

        setTableDataAndLink('Reg. User', scope.countries[0].regUsers);
        setTableDataAndLink('Login Users', scope.countries[0].loginUsers);
        setTableDataAndLink('Active Users', scope.countries[0].uniqueActiveUsers);
        setTableDataAndLink('Create Project', scope.countries[0].createProject);
        setTableDataAndLink('Project Completed', scope.countries[0].projectCompleted);
        setTableDataAndLink('Bom Price', scope.countries[0].bomPrice);
        setTableDataAndLink('Reference Count', scope.countries[0].referenceCount);
        setTableDataAndLink('Send Mail', scope.countries[0].sendMail);
      }


      /**
       *
       * @param {type} filedName
       * @param {type} value
       * @returns {undefined}
       */
      function setTableDataAndLink(filedName, value) {
        if (filedName === "Reg. User") {
          if (value === 0) {
            scope.disableRegUsers = true;
            angular.element("#regUsers").removeClass("link");
          } else {
            scope.disableRegUsers = false;
            angular.element("#regUsers").addClass("link");
          }
          scope.regUsers = value;
        } else if (filedName === "Login Users") {
          if (value === 0) {
            scope.disableLoginUsers = true;
            angular.element("#loginUsers").removeClass("link");
          } else {
            scope.disableLoginUsers = false;
            angular.element("#loginUsers").addClass("link");
          }
          scope.loginUsers = value;
        } else if (filedName === "Active Users") {
          if (value === 0) {
            scope.disableUniqueActiveUsers = true;
            angular.element("#uniqueActiveUsers").removeClass("link");
          } else {
            scope.disableUniqueActiveUsers = false;
            angular.element("#uniqueActiveUsers").addClass("link");
          }
          scope.uniqueActiveUsers = value;
        } else if (filedName === "Create Project") {
          if (value === 0) {
            scope.disableCreateProject = true;
            angular.element("#createProject").removeClass("link");
          } else {
            scope.disableCreateProject = false;
            angular.element("#createProject").addClass("link");
          }
          scope.createProject = value;
        } else if (filedName === "Project Completed") {
          if (value === 0) {
            scope.disableProjectCompleted = true;
            angular.element("#projectCompleted").removeClass("link");
          } else {
            scope.disableProjectCompleted = false;
            angular.element("#projectCompleted").addClass("link");
          }
          scope.projectCompleted = value;
        } else if (filedName === "Bom Price") {
          if (value === 0) {
            scope.disableBomPrice = true;
            angular.element("#bomPrice").removeClass("link");
          } else {
            scope.disableBomPrice = false;
            angular.element("#bomPrice").addClass("link");
          }
          scope.bomPrice = value;
        } else if (filedName === "Reference Count") {
          if (value === 0) {
            scope.disableReferenceCount = true;
            angular.element("#referenceCount").removeClass("link");
          } else {
            scope.disableReferenceCount = false;
            angular.element("#referenceCount").addClass("link");
          }
          scope.referenceCount = value;
        } else if (filedName === "Send Mail") {
          if (value === 0) {
            scope.disableSendMail = true;
            angular.element("#sendMail").removeClass("link");
          } else {
            scope.disableSendMail = false;
            angular.element("#sendMail").addClass("link");
          }
          scope.sendMail = value;
        }
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].regUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].regUsers, "dateFormatted", "regUsers", "Date", "Registered Users");
          scope.countries[i].loginUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].loginUsers, "dateFormatted", "loginUsers", "Date", "No. of Logged In Users");
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          scope.countries[i].createProjectChart = gaChartUtil.populateGAChartData(chartData[keyName].createProject, "dateFormatted", "createProject", "Date", "Projects Created");
          scope.countries[i].projectCompletedChart = gaChartUtil.populateGAChartData(chartData[keyName].projectCompleted, "dateFormatted", "projectCompleted", "Date", "Projects Completed");
          scope.countries[i].bomPriceChart = gaChartUtil.populateGAChartData(chartData[keyName].bomPrice, "dateFormatted", "bomPrice", "Date", "Value Of Projects Shared");
          scope.countries[i].referenceCountChart = gaChartUtil.populateGAChartData(chartData[keyName].referenceCount, "dateFormatted", "referenceCount", "Date", "Wiring Devices");
          scope.countries[i].sendMailChart = gaChartUtil.populateGAChartData(chartData[keyName].sendMail, "dateFormatted", "sendMail", "Date", "Projects Shared");
        }
        console.log("Chart Data.. Prepare...");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onEasyQuoteChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.easyQuoteChartTypeSelected = scope.chartOptions[2];
        scope.easyQuoteChartName = chartName;
        onEasyQuoteChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onEasyQuoteChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;
        scope.selectedChartTab = selTab;
        scope.easyQuoteChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.easyQuoteChartToolTip = scope.chartToolTips[scope.selectedChartTab];
        var chartType = scope.easyQuoteChartTypeSelected;
        var chartName = scope.easyQuoteChartName;
        var countryPrefix = selectedCountryName + " : ";
        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.easyQuoteChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartRegisteredUsers":
          {
            scope.easyQuoteChartLabel = countryPrefix + NO_OF_REG_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].regUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].regUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].regUsersChart.month;
            }
            break;
          }
          case "ChartProjectCreated":
          {
            scope.easyQuoteChartLabel = countryPrefix + NO_OF_PROJ_CREATED;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].createProjectChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].createProjectChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].createProjectChart.month;
            }
            break;
          }
          case "ChartUserLogged":
          {
            scope.easyQuoteChartLabel = countryPrefix + NO_OF_LOGGED_USER;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].loginUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].loginUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].loginUsersChart.month;
            }
            break;
          }
          case "ChartProjectCompleted":
          {
            scope.easyQuoteChartLabel = countryPrefix + NO_OF_PROJ_COMPLETED_CONFIG;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].projectCompletedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].projectCompletedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].projectCompletedChart.month;
            }
            break;
          }
          case "ChartBomPrice":
          {
            scope.easyQuoteChartLabel = countryPrefix + PRICE_SHARED;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].bomPriceChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].bomPriceChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].bomPriceChart.month;
            }
            break;
          }
          case "ChartWiringDevices":
          {
            scope.easyQuoteChartLabel = countryPrefix + NO_OF_WIRIN_DEVICES;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].referenceCountChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].referenceCountChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].referenceCountChart.month;
            }
            break;
          }
          case "ChartProjectShared":
          {
            scope.easyQuoteChartLabel = countryPrefix + NO_OF_PROJECT_SHARED;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].sendMailChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].sendMailChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].sendMailChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.easyQuoteChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }




    }//END Link..
  }

})();