/**
 * @ngdoc directive
 * @name EcoDialDesktop directive
 *
 * @description
 *  EcoDialDesktop directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('ecoDialDesktop', ecoDialDesktop);

  function ecoDialDesktop($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, EcoDialDesktopCountryVO, countriesService, ecodialService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/ecoDialDesktop/ecoDialDesktop.html',
      link: link
    };

    return directive;

    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ["-- Unknown --", "Australia", "Belgium", "Bulgaria", "Brazil", "Colombia", "Czech Republic", "Denmark", "France", "Germany", "Hungary", "India", "Indonesia", "Netherlands", "Poland", "Saudi Arabia", "South Africa", "Spain", "Turkey", "Vietnam"
      ];
      var _registeredUsersData = null;
      var _EcoDialDesktopChartData = null;
      var _EcoDialDesktopWorldwideData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];
      var countryCodes = {};
      var countryNameCodes = {};
      var countryCodeList = [];
      scope.chartType = 'month';
      scope.countryCode = '';

      scope.maxCreationDate = '';

      scope.countrySelectedLabel = 'World wide';

      scope.launchDate = appConstants.DATES.ECO_DIAL_DESKTOP_LAUNCHED_DATE;
      scope.ecoDialDesktopBulgariaLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_BULGARIA_LAUNCHED_DATE;
      scope.ecoDialDesktopCzeckRepublicLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_CZECKREPUBLIC_LAUNCHED_DATE;
      scope.ecoDialDesktopDenmarkLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_DENMARK_LAUNCHED_DATE;
      scope.ecoDialDesktopGermanyLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_GERMANY_LAUNCHED_DATE;
      scope.ecoDialDesktopIndonesiaLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_INDONESIA_LAUNCHED_DATE;
      scope.ecoDialDesktopSpainLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_SPAIN_LAUNCHED_DATE;
      scope.ecoDialDesktopAustraliaLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_AUSTRALIA_LAUNCHED_DATE;
      scope.ecoDialDesktopColombiaLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_COLOMBIA_LAUNCHED_DATE;
      scope.ecoDialDesktopIndiaLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_INDIA_LAUNCHED_DATE;
      scope.ecoDialDesktopVietnamLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_VIETNAM_LAUNCHED_DATE;
      scope.ecoDialDesktopBelgiumLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_BELGIUM_LAUNCHED_DATE;
      scope.ecoDialDesktopBrazilLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_BRAZIL_LAUNCHED_DATE;
      scope.ecoDialDesktopNetherlandsLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_NETHERLANDS_LAUNCHED_DATE;
      scope.ecoDialDesktopPolandLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_POLAND_LAUNCHED_DATE;
      scope.ecoDialDesktopSaudiArabiaLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_SAUDI_ARABIA_LAUNCHED_DATE;
      scope.ecoDialDesktopTurkeyLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_TURKEY_LAUNCHED_DATE;
      scope.ecoDialDesktopFranceLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_FRANCE_LAUNCHED_DATE;
      scope.ecoDialDesktopHungaryLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_HUNGARY_LAUNCHED_DATE;
      scope.ecoDialDesktopSouthAfricaLaunchDate = appConstants.DATES.ECO_DIAL_DESKTOP_SOUTH_AFRICA_LAUNCHED_DATE;

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------

      var NO_OF_REG_USERS = " : Total No. of registered users";
      var USERS_BY_COUNTRY = "No. of registered users for ";

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.ecoDialDesktopTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.ecoDialDesktopChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.ecoDialDesktopChartName = "ChartRegisteredUsers";
      scope.ecoDialDesktopChartLabel = NO_OF_REG_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = false;
      scope.loadDefaultChart = false;

      scope.onEcoDialDesktopChartOptionChange = onEcoDialDesktopChartOptionChange;
      scope.onEcoDialDesktopChartLinkChange = onEcoDialDesktopChartLinkChange;

      scope.onCountryDropDownChange = onCountryDropDownChange;

      scope.chartReady = chartReady;

      scope.worldwideTotal = 0;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        // Init Countries data
        initCountries();
        // Get Report Data
        getEcoDialDesktopReportData();
        // Get Chart Data
        getEcoDialDesktopChartData();

        getMaxCreationDate();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.ecoDialDesktopUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initChartData();
            //Load Default Chart
            loadDefaultChart();
          }
        });
      }

      /**
       * Get EcoDialDesktop report data from NODE
       *
       * @returns {undefined}
       */
      function getEcoDialDesktopReportData() {
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getRegisteredUsersData('ECODIAL_USERS_URL').then(
                function (result) {
                  logger.log("EcoDialDesktop::getEcoDialDesktopReportData::Result::Success");
                  _registeredUsersData = result;
                  ecodialService.getWorldWideTotal().then(function (data) {
                    scope.worldwideTotal = data.users;
                  });
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _registeredUsersData = null;
                  logger.log("EcoDialDesktop::getEcoDialDesktopReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get day EcoDialDesktop CHART data from NODE
       * called by getEcoDialDesktopChartData
       *
       * @returns {undefined}
       */
      function getDayChartData() {
        _EcoDialDesktopChartData = _EcoDialDesktopChartData || {};
        nodeGAService.getChartData('ECODIAL_OFFLINE_CHART_DAY_URL').then(
                function (result) {
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-day::Success");
                  _EcoDialDesktopChartData.day = result;
                  if (!scope.maxCreationDate) {
                    getMaxCreationDate();
                  }
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _EcoDialDesktopChartData.day = null;
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-day::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get week chart data
       *
       * called by getEcoDialDesktopChartData
       *
       * @returns {undefined}
       */
      function getWeekChartData() {
        // week chart data
        nodeGAService.getChartData('ECODIAL_OFFLINE_CHART_WEEK_URL').then(
                function (result) {
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-week::Success");
                  _EcoDialDesktopChartData.week = result;
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _EcoDialDesktopChartData.week = null;
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-week::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get week chart data
       *
       * called by getEcoDialDesktopChartData
       *
       * @returns {undefined}
       */
      function getMonthChartData() {
        // month chart data
        nodeGAService.getChartData('ECODIAL_OFFLINE_CHART_MONTH_URL').then(
                function (result) {
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-month::Success");
                  _EcoDialDesktopChartData.month = result;
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  //$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _EcoDialDesktopChartData.month = null;
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-month::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function getWorldwideDayChartData() {
        _EcoDialDesktopWorldwideData = _EcoDialDesktopWorldwideData || {};
        // week chart data
        nodeGAService.getChartData('ECO_DIAL_DESKTOP_WORLDWIDE_DAY_URL').then(
                function (result) {
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-day::Success");
                  _EcoDialDesktopWorldwideData.day = result;
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                },
                function (error) {
                  _EcoDialDesktopWorldwideData.day = null;
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-day::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function getWorldwideWeekChartData() {
        // week chart data
        _EcoDialDesktopWorldwideData = _EcoDialDesktopWorldwideData || {};
        nodeGAService.getChartData('ECO_DIAL_DESKTOP_WORLDWIDE_WEEK_URL').then(
                function (result) {
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-week::Success");
                  _EcoDialDesktopWorldwideData.week = result;
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  //$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _EcoDialDesktopWorldwideData.week = null;
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-week::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function getWorldwideMonthChartData() {
        // month chart data
        _EcoDialDesktopWorldwideData = _EcoDialDesktopWorldwideData || {};
        nodeGAService.getChartData('ECO_DIAL_DESKTOP_WORLDWIDE_MONTH_URL').then(
                function (result) {
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-month::Success");
                  _EcoDialDesktopWorldwideData.month = result;
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  //$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _EcoDialDesktopWorldwideData.month = null;
                  logger.log("EcoDialDesktop::getEcoDialDesktopChartData-day::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get chart data by day, week, and month
       *
       * @returns {undefined}
       */
      function getEcoDialDesktopChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        getDayChartData();
        getWeekChartData();
        getMonthChartData();

        // get world wide data
        getWorldwideDayChartData();
        getWorldwideWeekChartData();
        getWorldwideMonthChartData();

        getMaxCreationDate();
        $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);

      }

      /**
       * to get max creation date
       * @returns {undefined}
       */
      function getMaxCreationDate() {
        scope.maxCreationDate = null;
        nodeGAService.getRegisteredUsersMaxCreationDate('ecodialad').then(
                function (result) {
                  logger.log("EcoDialDesktop::getMaxCreationDate::Success");
                  scope.maxCreationDate = result.maxcreatedate;
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  //$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _EcoDialDesktopWorldwideData.month = null;
                  logger.log("EcoDialDesktop::getMaxCreationDate::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("ecoDialDesktop.js :: loadDefaultChart :: Timer :: Start ");
          onEcoDialDesktopChartLinkChange('ChartRegisteredUsers');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoDialDesktop.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var ecoDialDesktop = {};

        // get countries key:value list
        initCountryCodes();

        //--- EcoDialDesktop
        for (var i = 0; i < countryNameList.length; i++) {
          var countryName = countryNameList[i];

          ecoDialDesktop.countryName = countryNameList[i];
          ecoDialDesktop.countryGaId = appConstants.SITE_GA_IDS.ECO_DIAL_DESKTOP_ID;
          ecoDialDesktop.launchDate = appConstants.DATES.ECODIAL_DESKTOP_LAUNCH_DATES[countryName];
//                    ecoDialDesktop.launchDateStr = appConstants.DATES.ECO_DIAL_DESKTOP_LAUNCHED_DATE_STR;
          ecoDialDesktop.regUsers = 0;
          ecoDialDesktop.uniqueActiveUsers = 0;
          ecoDialDesktop.projectsCreated = 0;
          ecoDialDesktop.uniqueActiveUsersChart = 0;
          ecoDialDesktop.projectsCreatedChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new EcoDialDesktopCountryVO(ecoDialDesktop));
        }
        countryName = countryNameList[0].toUpperCase();
        scope.countryCode = countryNameCodes[countryName];
        scope.chartType = 'month';
        scope.ecoDialDesktopChartName = 'CountryChartUsers';
        onEcoDialDesktopChartOptionChange(null, appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX);
      }

      /**
       *
       * @returns {undefined}
       */
      function initCountryCodes() {
        countryNameCodes = {};
        countryCodeList = [];
        countriesService.getNamesList().then(function (resp) {
          countryCodes = resp;
          // create name -> code list
          for (var co in countryCodes) {
            var countryName = countryCodes[co];
            countryNameCodes[countryName] = co;
          }
          ;

          // create codes list for EcoDial countries
          countryNameList.forEach(function (country, index) {
            var co = countryNameCodes[country];
            countryCodeList.push(co);
          });
        });
      }

      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload EcoDialDesktop Chart.." + scope.selectedChartTab);
        onEcoDialDesktopChartOptionChange(null, scope.selectedChartTab);
      }

      /**
       *
       * @param {type} reportData
       * @returns {undefined}

       function parseReportData(reportData) {
       scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
       scope.countries[0].projectsCreated = parseFloat(reportData.projectList.totalsForAllResults['ga:totalEvents']);
       //----------------- SET Default Value ---------------------
       scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
       scope.projectsCreated = scope.countries[0].projectsCreated;
       }*/


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
//			 scope.countries[].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
//			 scope.countries[0].projectsCreatedChart = gaChartUtil.populateGAChartData(chartData.projectList, "dateFormatted", "projectList", "Date", "Projects Created");

        console.log("Chart Data.. Prepare...");
      }

      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onEcoDialDesktopChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.ecoDialDesktopChartTypeSelected = scope.chartOptions[scope.selectedChartTab].toLowerCase();
        scope.ecoDialDesktopChartName = chartName;
//				onEcoDialDesktopChartOptionChange(null, chartOptionDfltSelInx);
        onEcoDialDesktopChartOptionChange(null, scope.selectedChartTab);
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onEcoDialDesktopChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;
        scope.selectedChartTab = selTab;
        scope.ecoDialDesktopChartTypeSelected = scope.chartOptions[selTab].toLowerCase();
        scope.ecoDialDesktopChartToolTip = scope.chartToolTips[selTab];

        var chartType = scope.ecoDialDesktopChartTypeSelected;
        scope.chartType = chartType;
        var chartName = scope.ecoDialDesktopChartName;

        var countryPrefix = selectedCountryName + " : ";
        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartRegisteredUsers":
          {
            scope.countrySelectedLabel = 'World wide';
            var chartData = _EcoDialDesktopWorldwideData[chartType];
            if (chartData === undefined || chartData === null) {
              scope.ecoDialDesktopChartToolTip = scope.chartToolTips[scope.selectedChartTab] + ' >> ** NOT AVAILABLE **';
              chartData = {};
            }
            switch (chartType) {
              case 'day':
                var last30daysData = commonUtils.getLastThirtyDaysArray(scope.maxCreationDate, 'createdate', 'users');
                commonUtils.computeLast30DaysData(chartData, last30daysData);
                // set chart data with the computed data
                chartObj.data = gaChartUtil.populateDayChartData(last30daysData, "createdate", "users", chartType, "Registered Users");
                break;

              case 'week':
                var lastWeeksData = commonUtils.getLastWeeksArray(scope.maxCreationDate, 'createdate', 'users');
                commonUtils.computeWeekData(chartData, lastWeeksData);
                // set chart data with the computed data
                chartObj.data = gaChartUtil.populateWeekChartData(lastWeeksData, "createdate", "users", chartType, "Registered Users");
                break;

              case 'month':
                // get base array for last year
                var lyData = commonUtils.getLastYearArray(scope.maxCreationDate, 'createdate', 'users');
                commonUtils.computeMonthData(chartData, lyData);
                // set chart data with the computed data
                chartObj.data = gaChartUtil.populateMonthChartData(lyData, "createdate", "users", chartType, "Registered Users");
                break;
            }
            break;
          }
          case 'ChartCountryUsers':
          {
            scope.countrySelectedLabel = scope.selectedCountry.countryName;
            var countryCode = scope.countryCode;
            var chartData = _EcoDialDesktopChartData[chartType][countryCode];
            if (chartData === undefined || chartData === null) {
              scope.ecoDialDesktopChartToolTip = scope.chartToolTips[scope.selectedChartTab] + ' >> ** NOT AVAILABLE **';
              chartData = {};
            }
            switch (chartType) {
              case 'day':
                var last30daysData = commonUtils.getLastThirtyDaysArray(scope.maxCreationDate, 'createdate', 'users');
                commonUtils.computeLast30DaysData(chartData, last30daysData);
                chartObj.data = gaChartUtil.populateDayChartData(last30daysData, "createdate", "users", chartType, "Registered Users");
                break;

              case 'week':
                var lastWeeksData = commonUtils.getLastWeeksArray(scope.maxCreationDate, 'createdate', 'users');
                commonUtils.computeWeekData(chartData, lastWeeksData);
                chartObj.data = gaChartUtil.populateWeekChartData(lastWeeksData, "createdate", "users", chartType, "Registered Users");
                break;

              case 'month':
                // get base array for last year
                var lyData = commonUtils.getLastYearArray(scope.maxCreationDate, 'createdate', 'users');
                commonUtils.computeMonthData(chartData, lyData);
                // set chart data with the computed data
                chartObj.data = gaChartUtil.populateMonthChartData(lyData, "createdate", "users", chartType, "Registered Users");
                break;
            }
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }

      /**
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        if (scope.selectedCountry) {
          selectedCountryName = scope.selectedCountry.countryName;
          var countryCode = selectedCountryName === '-- Unknown --' ? '' : countryNameCodes[selectedCountryName];
          scope.countryCode = countryCode;
          scope.regUsers = _registeredUsersData[countryCode].totRegUsers;
          scope.launchDate = scope.selectedCountry.launchDate;
          // show chart
          onEcoDialDesktopChartLinkChange('ChartCountryUsers');
        }
      }
    }//END Link..
  }
})();