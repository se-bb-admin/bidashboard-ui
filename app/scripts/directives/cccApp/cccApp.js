/**
 * @ngdoc directive
 * @name CCC App directive
 *
 * @description
 *  CCC App directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('cccApp', cccApp);

  function cccApp($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, CCCAppCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/cccApp/cccApp.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      var countryNameList = ['China', 'United States', 'France', 'India',
        'Indonesia', 'Spain', 'Canada', 'Brazil', 'Germany', 'Taiwan',
        'Turkey', 'Russia', 'Thailand', 'Italy', 'Morocco', 'Chile',
        'Australia', 'United Kingdom', 'New Zealand', 'South Korea',
        'Peru', 'Sweden', 'Malaysia', 'Argentina', 'Colombia'];


      var _cccAppReportData = null;
      var _cccAppChartData = null;

      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];

      //scope.uniqueActvUsers = 0;
      // scope.configLaunched = 0;
      scope.userSessions = 0;
      scope.regUsers = 0;
      scope.launchDate = appConstants.DATES.CCC_APP_GLOBAL_LAUNCHED_DATE;
      scope.cccAppLaunchDays = 0;

      scope.disableUniqueActiveUsers = false;
      scope.disableConfLaunched = false;

      //------------------------------------------------------------------
      //-------------- CCC APP CHART INIT SHOW HIDE PROPERTIES -----------
      //------------------------------------------------------------------
      scope.isGAChartVisible = true;
      scope.isPaceChartVisible = false;

      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      //var NO_OF_CONFIGURATION_LAUNCHED = "No. of Configuration launched";
      var NO_OF_USER_SESSIONS = "No. of user sessions";

      scope.onCountryDropDownChange = onCountryDropDownChange;

      scope.paceChartStyle = commonUtils.getPaceChartStyle();
      scope.paceChartSource = {};
      scope.showPaceChart = showPaceChart;
      var isInitPaceChartData = false;
      var paceChartObj = {};

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};


      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.cccAppChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.cccAppChartToolTip = scope.chartToolTips[scope.selectedChartTab];
//            scope.cccAppChartName = "ChartActiveUser";
      scope.cccAppChartLabel = NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;
      scope.noteUserSessions = appConstants.NOTE.USER_SESSIONS;

      scope.onCCCAppChartOptionChange = onCCCAppChartOptionChange;
      scope.onCCCAppChartLinkChange = onCCCAppChartLinkChange;

      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        //--- Init Countries data
        console.log("INIT CALLED.......");

        initCountries();
        // Get Report Data
        getCCCAppReportData();
        // Get Chart Data
        getCCCAppChartData();

        //Add Event Listener  When page resize
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        /*
         scope.$watch('paceDataRegUser.cccAppUsersReg', function (newValue, oldValue) {
         //console.log("newValue:::::: ***** "+newValue);
         if (newValue !== undefined && newValue !== null) {
         initPaceChartData();
         }
         });
         */
      }

      /**
       * Get Quick Ref report data from NODE
       *
       * @returns {undefined}
       */
      function getCCCAppReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getCCCAppReportData().then(
                function (result) {
                  logger.log("CCC App::getCCCAppReportData::Result::Success");
                  _cccAppReportData = result;
                  console.log("\n\n_cccAppReportData: ", _cccAppReportData);
                  parseReportData(_cccAppReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _cccAppReportData = null;
                  logger.log("CCC APP::getCCCAppReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get CCC APP CHART data from NODE
       *
       * @returns {undefined}
       */
      function getCCCAppChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getCCCAppChartData().then(
                function (result) {
                  logger.log("CCC App::getCCCAppChartData::Result::Success");
                  _cccAppChartData = result;
                  parseChartData(_cccAppChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _cccAppChartData = null;
                  logger.log("CCC App::getCCCAppChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("cccApp.js :: loadDefaultChart :: Timer :: Start ");
          onCCCAppChartLinkChange('ChartActiveUser');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("cccApp.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initPaceChartData() {
        paceChartObj = {};
        paceChartObj.type = "AreaChart";
        paceChartObj.displayed = false;
        paceChartObj.options = commonUtils.getPaceChartOptions();

        isInitPaceChartData = true;
      }

      /***
       *  Onclick Registererd users link click..
       *  Show PACE Chart Hide GA Chart
       *
       * @returns {undefined}
       *
       */
      function showPaceChart() {
        scope.isGAChartVisible = false;
        scope.isPaceChartVisible = true;

        //Show Hide Chart
        onCCCAppChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }



      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var cccApp = {};
        for (var i = 0; i < countryNameList.length; i++) {
          cccApp.countryName = countryNameList[i];

          var capsCountryName = constructName(countryNameList[i].toUpperCase());
          var launchDate = 'CCC_APP_' + capsCountryName + '_LAUNCHED_DATE';
          //console.log("launchDate:: "+ launchDate);
          cccApp.launchDate = appConstants.DATES[launchDate];//appConstants.DATES.CCC_APP_LAUNCHED_DATE;
          //console.log("launchDate 1:: "+ cccApp.launchDate);
          cccApp.cccAppLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, cccApp.launchDate);
          //console.log("cccAppLaunchDays:: "+ cccApp.cccAppLaunchDays);

          cccApp.regUsers = 0;
          cccApp.uniqueActiveUsers = 0;
          cccApp.userSessions = 0;
          //--- Push country to Countries Array
          scope.countries.push(new CCCAppCountryVO(cccApp));
        }

        //Set default Country to update data binding
        setDefaultCountry();
      }


      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }

      /**
       *
       * @param {type} name
       * @returns {unresolved}
       */
      function constructName(name) {
        var result = name.split(' ').join('_');
        return result;
      }


      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;
        //logger.log(selectedCountryName);
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.launchDate = scope.countries[index].launchDate;
        scope.cccAppLaunchDays = scope.countries[index].cccAppLaunchDays;

        setTableDataAndLink('UNIQUE_ACTIVE_USERS', scope.countries[index].uniqueActiveUsers);
        setTableDataAndLink('USER_SESSIONS', scope.countries[index].userSessions);

        onCCCAppChartLinkChange('ChartActiveUser');
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        //logger.log("Reload CCCApp Chart.." + scope.selectedChartTab);
        onCCCAppChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {

        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];

          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          scope.countries[i].userSessions = parseFloat(reportData[keyName].sessions.totalsForAllResults['ga:sessions']);
        }
        //----------------- SET Default Value ---------------------
        scope.cccAppLaunchDays = scope.countries[0].cccAppLaunchDays;
        setTableDataAndLink('UNIQUE_ACTIVE_USERS', scope.countries[0].uniqueActiveUsers);
        setTableDataAndLink('USER_SESSIONS', scope.countries[0].userSessions);
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          scope.countries[i].userSessionsChart = gaChartUtil.populateGAChartData(chartData[keyName].sessions, "dateFormatted", "sessions", "Date", "User Sessions");

        }
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onCCCAppChartLinkChange(chartName) {
        scope.isGAChartVisible = true;
        scope.isPaceChartVisible = false;

        //When you click on a link Chart will reset to Month Chart.
        scope.cccAppChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.cccAppChartName = chartName;
        onCCCAppChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onCCCAppChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.cccAppChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.cccAppChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.cccAppChartTypeSelected;
        var chartName = scope.cccAppChartName;

        var countryPrefix = selectedCountryName + " : ";
        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        if (isInitChartData === false) {
          initChartData();
        }

        //check for PACE Chart
        if (scope.isPaceChartVisible) {
          scope.quickQuotationChartLabel = NO_OF_REG_USERS;

          loadPaceChart(scope.selectedChartTab);
          return;
        }

        //console.log("chartName:: " + chartName);
        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.cccAppChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            //console.dir(chartObj.data);
            break;
          }
          case "ChartUserSessions":
          {
            console.log("inside ChartUserSessions.......");
            scope.cccAppChartLabel = countryPrefix + NO_OF_USER_SESSIONS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].userSessionsChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].userSessionsChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].userSessionsChart.month;
            }
            //console.dir(chartObj.data);
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




      /**
       * Load PACE Chart as per selected tab (Day | Week | Month)
       *
       * @param {type} selTab
       * @returns {undefined}
       */
      function loadPaceChart(selTab) {
        var chartTypeSelected = scope.chartOptions[scope.selectedChartTab];

        //TODO :: Also Need To Check "scope.paceDataRegUserOverPeriod.myPactBasicUsersReg" val exist or not..
        if (isInitPaceChartData === false) {
          initPaceChartData();
        }

        if (chartTypeSelected.toLowerCase() === 'day') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.cccAppUsersReg.day;
        } else if (chartTypeSelected.toLowerCase() === 'week') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.cccAppUsersReg.week;
        } else if (chartTypeSelected.toLowerCase() === 'month') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.cccAppUsersReg.month;
        }

        //Set value to Scope
        scope.paceChartSource = paceChartObj;
      }


      /**
       *
       * @param {type} filedName
       * @param {type} value
       * @returns {undefined}
       */
      function setTableDataAndLink(filedName, value) {
        if (filedName === "UNIQUE_ACTIVE_USERS") {
          if (value === 0) {
            scope.disableUniqueActiveUsers = true;
            angular.element("#uniqueActiveUsers").removeClass("link");
          } else {
            scope.disableUniqueActiveUsers = false;
            angular.element("#uniqueActiveUsers").addClass("link");
          }
          scope.uniqueActiveUsers = value;

        } else if (filedName === "USER_SESSIONS") {
          if (value === 0) {
            scope.disableConfLaunched = true;
            angular.element("#userSessions").removeClass("link");
          } else {
            scope.disableConfLaunched = false;
            angular.element("#userSessions").addClass("link");
          }
          scope.userSessions = value;
        }
      }




    }//END Link..

  }
  ;
})();