/**
 * @ngdoc directive
 * @name E_PLAN Report directive
 *
 * @description
 *  E_PLAN Report directive displays all list of information based on counties selection.
 *
 */
(function () {
	'use strict';

	angular.module('se-branding')
			.directive('ePlanCompany', ecoReach);

	function ecoReach($rootScope, $timeout, appConstants, commonUtils, logger,
			gaChartUtil, businessReportService) {

		var directive = {
			restrict: 'E',
			replace: true,
			scope: {
			},
			templateUrl: 'scripts/directives/ePlan/ePlanCompany.html',
			link: link
		};
		return directive;



		/**
		 *
		 * @param {type} scope
		 * @param {type} elem
		 * @param {type} attrs
		 * @returns {undefined}
		 */
		function link(scope, elem, attrs) {

			var TOP_X_DATA_FOR_CHART = 10;
			var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
			var CHART_LABLE_TOTAL_DOWNLOADS = 'Total Downloads';

			var timer = null;

			//------------------------------------------------------------------
			//---------------------------- Global Properties -------------------
			//------------------------------------------------------------------
			scope.countries = [];
			scope.selectedCountrye = "";

			scope.months = appConstants.DATES.MONTH_ARR_LONG;
			scope.selectedMonth = "";

			scope.years = appConstants.DATES.DATA_YEARS;
			scope.selectedYear = "";

			scope.selectedChartFilterBy = null;


			scope.ePlanCompanyWiseReportData = {};
			scope.ePlanCompanyWiseReportData.grandTotalDownloads = 0;
			scope.ePlanCompanyWiseReportData.totalDownloads = 0;
			scope.ePlanCompanyWiseReportData.companyCounts = 0;
			scope.ePlanCompanyWiseReportData.chartData = {};

			scope.onChartOptionChange = onChartOptionChange;
			scope.onEplanChartFilterByChange = onEplanChartFilterByChange;


			scope.chartSource = {};
			scope.ePlanChartLabel = "";
			scope.showEplanChartSubLinks = false;
			var isInitChartData = false;
			var chartObj = {};

			//------------------------------------------------------------------
			//--------- E_PLAN CHART INIT SHOW HIDE PROPERTIES --------------
			//------------------------------------------------------------------




			//------------------------------------------------------------------
			//-------------------  For Chart TYPE Options  ---------------------
			//------------------------------------------------------------------


			init();



			/**
			 * Function to initialized the repors
			 *
			 * @returns {undefined}
			 */
			function init() {
				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

				businessReportService.getEplanCountryList().then(
						function (data) {
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);

							scope.countries = angular.copy(data.countries);

							scope.months = angular.copy(appConstants.DATES.MONTH_ARR_LONG);
							scope.years = angular.copy(appConstants.DATES.DATA_YEARS);


							addAllParamsInDropDowns();

							setDefaultDropDownValues();

							getEplanCompanyWiseReportData();

							getEplanCompanyWiseGrandTotalDownloads();

							getEplanCompanyCounts();
						},
						function () {
							logger.log("E_PLAN::getEcoReachCountryList::Result::FAIL");
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
						}
				);

				//Add Event Listener
				var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reLoadChartDataAndSource);

				//Remove event Listener on Destroy
				scope.$on('$destroy', listenerReloadChart);
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function getEplanCompanyWiseGrandTotalDownloads() {
				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
				scope.ePlanCompanyWiseReportData.grandTotalDownloads = 0;

				var countryCode = scope.selectedCountry.countryCode;

				businessReportService.getEplanCompanyWiseGrandTotalDownloads(countryCode).then(
						function (data) {
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);

							scope.ePlanCompanyWiseReportData.grandTotalDownloads = data[0].TOT_DOWNLOADS;

						},
						function () {
							logger.log("E_PLAN::getEcoReachCountryList::Result::FAIL");
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
						}
				);
			}

			/**
			 *
			 * @returns {undefined}
			 */
			function addAllParamsInDropDowns() {
				scope.months.unshift({label: "All", value: "all"});
				scope.countries.unshift({countryCode: "all", countryName: "All"});
			}

			/**
			 *
			 * @returns {undefined}
			 */
			function setDefaultDropDownValues() {
				scope.selectedCountry = scope.countries[1];
				scope.selectedMonth = scope.months[1];
				scope.selectedYear = scope.years[0];
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function onChartOptionChange() {
				var countryCode = scope.selectedCountry.countryCode;
				var month = scope.selectedMonth.value;
				var year = scope.selectedYear;
				scope.selectedChartFilterBy = null;

				if (year.toString().toLowerCase() === "all") {
					alert("Please select a specific year");
					return;
					// 1. COUNTRY SPECIFIC| Month SPECIFIC | YEAR SPECIFIC
				} else if (countryCode.toLowerCase() !== "all" && parseInt(month) > 0 && parseInt(year) > 0) {
					// 2. COUNTRY ALL| Month SPECIFIC | YEAR SPECIFIC
				} else if (countryCode.toLowerCase() === 'all' && parseInt(month) > 0 && parseInt(year) > 0) {
					// 3. COUNTRY SPECIFIC| Month ALL | YEAR SPECIFIC
				} else if (countryCode.toLowerCase() !== "all" && month.toLowerCase() === "all" && parseInt(year) > 0) {
					// 4.a COUNTRY ALL| Month ALL | YEAR SPECIFIC | -- GROUP By Month (default)
				} else if (countryCode.toLowerCase() === "all" && month.toLowerCase() === "all" && parseInt(year) > 0) {
					scope.showEplanChartSubLinks = true;
					scope.selectedChartFilterBy = "month";
				}

				getEplanCompanyWiseReportData();

				//TODO:: ADD IF ELSE ONLY FOR COUNTRY CHANGE
				getEplanCompanyWiseGrandTotalDownloads();

				//Fetch company counts Data
				getEplanCompanyCounts();
			}


			/**
			 *
			 * @param {type} evnt
			 * @param {type} selFilter
			 * @returns {undefined}
			 */
			function onEplanChartFilterByChange(evnt, selFilter) {
				scope.selectedChartFilterBy = selFilter;

				getEplanCompanyWiseReportData();
			}


			/**
			 * Get E_PLAN report data from NODE
			 *
			 * @returns {undefined}
			 */
			function getEplanCompanyWiseReportData() {

				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

				scope.showEplanChartSubLinks = false;

				var countryCode = scope.selectedCountry.countryCode;
				var month = scope.selectedMonth.value;
				var year = scope.selectedYear;
				var filterBy = scope.selectedChartFilterBy;

				businessReportService.getEplanCompanyWiseReportData(countryCode, month, year, filterBy).then(
						function (result) {
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
							logger.log("E_PLAN::getEcoReachReportData::Result::Success");

							var tempResult = angular.copy(result);

							parseEplanCompanyWiseReportDataAndDisplayChart(tempResult, countryCode, month, year, filterBy);
						},
						function (error) {
							logger.log("E_PLAN::getEcoReachReportData::Result::FAIL");
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
						}
				);
			}



			/**
			 *
			 * @returns {undefined}
			 */
			function getEplanCompanyCounts() {
				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

				scope.ePlanCompanyWiseReportData.companyCounts = 0;

				var countryCode = scope.selectedCountry.countryCode;
				var month = scope.selectedMonth.value;
				var year = scope.selectedYear;

				businessReportService.getEplanCompanyCounts(countryCode, month, year).then(
						function (data) {
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
							logger.log("E_PLAN::getEplanCompanyCounts::Result::Success");

							scope.ePlanCompanyWiseReportData.companyCounts = data[0].DISTINCT_COMPANY_NAME;
						},
						function (error) {
							logger.log("E_PLAN::getEplanCompanyCounts::Result::FAIL");
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
						}
				);
			}








			/**
			 *  Init PIE  Chart Options..
			 *
			 * @returns {undefined}
			 */
			function initPieChartOption() {
				chartObj = {};
				chartObj.type = "PieChart";
				chartObj.displayed = false;
				chartObj.options = commonUtils.getPieChartOptions();

				isInitChartData = true;
			}


			/**
			 *  Init COLUMN  Chart Options..
			 *
			 * @returns {undefined}
			 */
			function initColumnChartOption() {
				chartObj = {};
				chartObj.type = "ColumnChart";
				chartObj.displayed = false;
				chartObj.options = commonUtils.getColumnChartOptions();

				isInitChartData = true;
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function loadDefaultChart() {
				scope.isChartReady = false;
				chartObj.data = {};

				//Start Timer
				timer = $timeout(function () {
					console.log("ecoReach.js :: loadDefaultChart :: Timer :: Start ");

					reLoadChartDataAndSource();

					scope.isChartReady = true;

				}, CHART_LOAD_DELAY);

				//Destroy Timer
				scope.$on("$destroy",
						function (event) {
							console.log("ecoReach.js :: loadDefaultChart :: Timer :: Stop ");
							$timeout.cancel(timer);
						}
				);
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function reLoadChartDataAndSource() {
				//Set Chart Data
				chartObj.data = scope.ePlanCompanyWiseReportData.chartData;
				//Set value to Scope
				scope.chartSource = chartObj;
			}





			/**
			 *
			 * @returns {undefined}
			 */
			function chartReady() {
				fixGoogleChartsBarsBootstrap();
			}

			/**
			 *
			 * @returns {undefined}
			 */
			function fixGoogleChartsBarsBootstrap() {
				$(".google-visualization-table-table img[width]").each(function (index, img) {
					$(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
				});
			}






			/**
			 *
			 * @param {type} reportData
			 * @param {type} countryCode
			 * @param {type} month
			 * @param {type} year
			 * @param {type} filterBy
			 * @returns {undefined}
			 */
			function parseEplanCompanyWiseReportDataAndDisplayChart(reportData, countryCode, month, year, filterBy) {

				//scope.ePlanCompanyWiseReportData = {};
				scope.ePlanCompanyWiseReportData.totalDownloads = 0;
				scope.ePlanCompanyWiseReportData.chartData = {};

				var tmpDataItem = {};
				var _totalDownloads = 0;

				//Calculate Total Downloads
				for (var i = 0; i < reportData.length; i++) {
					tmpDataItem = reportData[i];
					_totalDownloads += tmpDataItem.TOT_DOWNLOADS;
				}

				//Set Total Downloads Value for Displaying in Table
				scope.ePlanCompanyWiseReportData.totalDownloads = _totalDownloads;

				//Prepare Chart DATA and Display Chart
				groupCompanyWiseTop_X_DataForChartAndDisplayChart(reportData, countryCode, month, year, filterBy);
			}


			/**
			 *
			 * @param {type} reportData
			 * @param {type} countryCode
			 * @param {type} month
			 * @param {type} year
			 * @param {type} filterBy
			 * @returns {undefined}
			 */
			function groupCompanyWiseTop_X_DataForChartAndDisplayChart(reportData, countryCode, month, year, filterBy) {

				var _groupedChartData = null;
				var _chartData = null;

				scope.ePlanChartLabel = CHART_LABLE_TOTAL_DOWNLOADS;

				if (year.toString().toLowerCase() === "all") {
					alert("Please select a specific year");
					return;

					// 1. COUNTRY SPECIFIC| Month SPECIFIC | YEAR SPECIFIC
				} else if (countryCode.toLowerCase() !== "all" && parseInt(month) > 0 && parseInt(year) > 0) {
//                    console.log("1");

					_groupedChartData = groupTop_X_Chart_Data(reportData);

					_chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "COMPANY_NAME", "TOT_DOWNLOADS", "Company", "Total Downloads");

					//----------------- SET Chart Data ---------------------
					scope.ePlanCompanyWiseReportData.chartData = _chartData;

					// -------------Init Pie Chart Options-----------------------
					initPieChartOption();

					//------------- Load Chart -----------------------
					loadDefaultChart();

					// 2. COUNTRY ALL| Month SPECIFIC | YEAR SPECIFIC
				} else if (countryCode.toLowerCase() === 'all' && parseInt(month) > 0 && parseInt(year) > 0) {
//                    console.log("2");

					_groupedChartData = groupTop_X_Chart_Data(reportData);

					_chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "COUNTRY_NAME", "TOT_DOWNLOADS", "Country", "Total Downloads");

					//----------------- SET Default Value ---------------------
					scope.ePlanCompanyWiseReportData.chartData = _chartData;

					// -------------Init Pie Chart Options-----------------------
					initPieChartOption();

					//------------- Load Chart -----------------------
					loadDefaultChart();

					// 3. COUNTRY SPECIFIC| Month ALL | YEAR SPECIFIC
				} else if (countryCode.toLowerCase() !== "all" && month.toString().toLowerCase() === "all" && parseInt(year) > 0) {
//                    console.log("3");

					_groupedChartData = reportData;

					//Update Month number to Month String (1 -> Jan, 2 -> Feb)
					updateMonthNumberWithMonthString(_groupedChartData, 'MONTH', "TOT_DOWNLOADS");

					_chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "MONTH", "TOT_DOWNLOADS", "Country", "Total Downloads");
					//----------------- SET Default Value ---------------------
					scope.ePlanCompanyWiseReportData.chartData = _chartData;

					// -------------Init Pie Chart Options-----------------------
					initColumnChartOption();

					//------------- Load Chart -----------------------
					loadDefaultChart();

					// 4.a COUNTRY ALL| Month ALL | YEAR SPECIFIC | -- GROUP By Month
				} else if (countryCode.toLowerCase() === "all" && month.toString().toLowerCase() === "all"
						&& parseInt(year) > 0 && filterBy.toLowerCase() === "month") {
//                    console.log("4.a");

					scope.showEplanChartSubLinks = true;

					_groupedChartData = reportData;

					//Update Month number to Month String (1 -> Jan, 2 -> Feb
					updateMonthNumberWithMonthString(_groupedChartData, 'MONTH', "TOT_DOWNLOADS");

					_chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "MONTH", "TOT_DOWNLOADS", "Country", "Total Downloads");

					//----------------- SET Default Value ---------------------
					scope.ePlanCompanyWiseReportData.chartData = _chartData;

					// -------------Init Pie Chart Options-----------------------
					initColumnChartOption();

					//------------- Load Chart -----------------------
					loadDefaultChart();

					// 4.b COUNTRY ALL| Month ALL | YEAR SPECIFIC | -- GROUP By Country
				} else if (countryCode.toLowerCase() === "all" && month.toString().toLowerCase() === "all"
						&& parseInt(year) > 0 && filterBy.toLowerCase() === "country") {
//                    console.log("4.b");

					scope.showEplanChartSubLinks = true;

					_groupedChartData = groupTop_X_Chart_Data(reportData);

					_chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "COUNTRY_NAME", "TOT_DOWNLOADS", "Country", "Total Downloads");

					//----------------- SET Default Value ---------------------
					scope.ePlanCompanyWiseReportData.chartData = _chartData;

					// -------------Init Pie Chart Options-----------------------
					initPieChartOption();

					//------------- Load Chart -----------------------
					loadDefaultChart();
				}
			}




			/**
			 *
			 * @param {type} data
			 * @returns {undefined}
			 */
			function groupTop_X_Chart_Data(data) {

				var tmpData = angular.copy(data);
				var tmpDataItem = {};
				var _totalDownloads = 0;
				var newChartData = null;

				//Calculate TotalDownloads for Others
				if (tmpData.length > (TOP_X_DATA_FOR_CHART + 1)) {
					for (var i = TOP_X_DATA_FOR_CHART; i < tmpData.length; i++) {
						tmpDataItem = tmpData[i];
						_totalDownloads += tmpDataItem.TOT_DOWNLOADS;
					}

					//Truncate First X+1 Item
					newChartData = tmpData.slice(0, (TOP_X_DATA_FOR_CHART + 1));

					//Get Last data
					var lastDataItem = newChartData[TOP_X_DATA_FOR_CHART];

					//Update last data
					for (var key in lastDataItem) {
						if (key !== "key") {
							lastDataItem[key] = "Others";
						}
					}
					//Update Total Downloads;
					lastDataItem.TOT_DOWNLOADS = _totalDownloads;

				} else {
					newChartData = tmpData;
				}

				return newChartData;
			}


			/**
			 *
			 * @param {type} monthData
			 * @param {type} dateFieldName
			 * @param {type} otherFieldName
			 * @returns {undefined}
			 */
			function updateMonthNumberWithMonthString(monthData, dateFieldName, otherFieldName) {
				var tmpMonthObj = null;
				var tmpMonthVal = 0;
				var tmpMonthStr = "";
				var matchFound = false;

				var monthArrShort = appConstants.DATES.MONTH_ARR_SHORT;

				//Create 12 Month Array Data..
				for (var j = 1; j <= 12; j++) {
					matchFound = false;
					for (var i = 0; i < monthData.length; i++) {
						tmpMonthObj = monthData[i];
						tmpMonthVal = parseInt(tmpMonthObj[dateFieldName]);

						if (tmpMonthVal === j) {
							matchFound = true;
							//tmpMonthStr = monthArrShort[(tmpMonthVal - 1)];
							//tmpMonthObj[dateFieldName] = tmpMonthStr;
						}
					}
					if (matchFound === false) {
						tmpMonthObj = {};
						//tmpMonthStr = monthArrShort[j];
						tmpMonthObj[dateFieldName] = (j);
						tmpMonthObj[otherFieldName] = 0;
						monthData.push(tmpMonthObj);
					}
				}

				//SORT BY MONTH
				monthData.sort(function (a, b) {
					return a[dateFieldName] - b[dateFieldName];
				});


				for (var i = 0; i < monthData.length; i++) {
					tmpMonthObj = monthData[i];
					tmpMonthVal = parseInt(tmpMonthObj[dateFieldName]);
					matchFound = true;
					tmpMonthStr = monthArrShort[(tmpMonthVal - 1)];
					tmpMonthObj[dateFieldName] = tmpMonthStr;

				}

			}//End Function



		}
	}


})();