/**
 * @ngdoc directive
 * @name loginMenu directive
 *
 * @description
 *  loginMenu directive displays company name, user name, edit profile and logout function.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').directive('loginMenu', loginMenu);

  function loginMenu($rootScope, appConstants, commonUtils, logger,
          paceService, profileService, $timeout) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
      },
      templateUrl: 'scripts/directives/loginMenu/loginMenu.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      scope.user = null;
      scope.editLogoutVisible = false;

      scope.openEditLogout = openEditLogout;
      scope.modifyProfileSuccess = modifyProfileSuccess;
      scope.modifyProfileClose = modifyProfileClose;
      scope.logout = commonUtils.logout;

      init();

      /**
       * Constractor
       *
       * @returns {undefined}
       */
      function init() {
        $rootScope.$on('login_success', function () {
          scope.user = paceService.getUserInfo();
        });
      }

      /**
       *
       * @returns {undefined}
       */
      function modifyProfileSuccess() {
        profileService.load();
//                            var data = paceService.getUserData();
//                            $rootScope.$broadcast('userInfo', data);
        //$('#closeProfileModal').click();
      }
      ;

      /**
       *
       * @returns {undefined}
       */
      function modifyProfileClose() {
        $('#closeProfileModal').click();
      }

      /**
       *
       * @returns {undefined}
       */
      function openEditLogout() {
        if (scope.editLogoutVisible === false) {
          scope.editLogoutVisible = true;
        } else {
          scope.editLogoutVisible = false;
        }
      }

    }// #End link

  }

})();