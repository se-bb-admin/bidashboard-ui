/**
 * @ngdoc directive
 * @name loginMenuMobile directive
 *
 * @description
 *  loginMenuMobile directive displays company name, user name, edit profile and logout function for mobile.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding')
          .directive('loginMenuMobile', loginMenuMobile);

  function loginMenuMobile($rootScope, appConstants, commonUtils, logger) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        userData: '=userData'
      },
      templateUrl: 'scripts/directives/loginMenu/loginMenuMobile.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      scope.logout = commonUtils.logout;
    }
  }
  ;

})();