/**
 * @ngdoc directive
 * @name data loader directive
 *
 * @description Directive to show message and spinner image during data loading...
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('dataLoader', function() {
    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        message: "="
      },
      template: '<h2>{{message}} <img ng-src="images/ajax_loader_small.gif"/></h2>'
    };
    return directive;
  });
})();