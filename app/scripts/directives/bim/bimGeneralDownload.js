/**
 * @ngdoc directive
 * @name BIM Report directive
 *
 * @description
 *  BIM Report directive displays all list of information based on counties selection.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding')
          .directive('bimGeneralDownload', ecoReach);

  function ecoReach($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, businessReportService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
      },
      templateUrl: 'scripts/directives/bim/bimGeneralDownload.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      var TOP_X_DATA_FOR_CHART = 10;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var CHART_LABLE_TOTAL_DOWNLOADS = 'Total Downloads';

      var timer = null;

      //------------------------------------------------------------------
      //---------------------------- Global Properties -------------------
      //------------------------------------------------------------------
      scope.products = [];
      scope.selectedProduct = "";

      scope.countries = [];
      scope.selectedCountrye = "";


      scope.months = appConstants.DATES.MONTH_ARR_LONG;
      scope.selectedMonth = "";

      scope.years = appConstants.DATES.DATA_YEARS;
      scope.selectedYear = "";

      scope.selectedChartFilterBy = null;


      scope.bimGeneralDownloadReportData = {};
      scope.bimGeneralDownloadReportData.grandTotalDownloads = 0;
      scope.bimGeneralDownloadReportData.totalDownloads = 0;
      scope.bimGeneralDownloadReportData.companyCounts = 0;
      scope.bimGeneralDownloadReportData.chartData = {};

      scope.onChartOptionChange = onChartOptionChange;
      scope.onEplanChartFilterByChange = onEplanChartFilterByChange;


      scope.chartSource = {};
      scope.ePlanChartLabel = "";
      scope.showEplanChartSubLinks = false;
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------------------
      //--------- BIM CHART INIT SHOW HIDE PROPERTIES --------------
      //------------------------------------------------------------------




      //------------------------------------------------------------------
      //-------------------  For Chart TYPE Options  ---------------------
      //------------------------------------------------------------------


      init();



      /**
       * Function to initialized the repors
       *
       * @returns {undefined}
       */
      function init() {
        // GET BIM PRODUCT LIST
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        businessReportService.getBIMProductList().then(
                function (dataProd) {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);

                  scope.products = angular.copy(dataProd.products);

                  // GET BIM COUNTRY LIST
                  $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
                  businessReportService.getBIMCountryList().then(
                          function (data) {

                            $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);

                            scope.countries = angular.copy(data.countries);

                            scope.months = angular.copy(appConstants.DATES.MONTH_ARR_LONG);
                            scope.years = angular.copy(appConstants.DATES.DATA_YEARS);


                            addAllParamsInDropDowns();

                            setDefaultDropDownValues();

                            getBimGeneralDownloadReportData();

                            getBIMCompanyWiseGrandTotalDownloads();

                          },
                          function (err) {

                          }
                  );


                },
                function (err) {
                  logger.log("BIM::getEcoReachCountryList::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reLoadChartDataAndSource);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }





      /**
       *
       * @returns {undefined}
       */
      function getBIMCompanyWiseGrandTotalDownloads() {
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        scope.bimGeneralDownloadReportData.grandTotalDownloads = 0;

        var country = scope.selectedCountry.countryName;
        var product = scope.selectedProduct.productName;

        businessReportService.getBIMCompanyWiseGrandTotalDownloads(country, product).then(
                function (data) {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);

                  scope.bimGeneralDownloadReportData.grandTotalDownloads = data[0].TOT_DOWNLOADS;

                },
                function () {
                  logger.log("BIM::getEcoReachCountryList::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function addAllParamsInDropDowns() {
        scope.months.unshift({label: "All", value: "all"});
        scope.countries.unshift({countryName: "All"});
        scope.products.unshift({productName: "All"});
      }

      /**
       * Scenario 1:
       *     Country: Specific, Month: All, Year:Specific, Product Specific
       *
       * @returns {undefined}
       */
      function setDefaultDropDownValues() {
        scope.selectedMonth = scope.months[0];
        scope.selectedYear = scope.years[2];
        scope.selectedCountry = scope.countries[0];
        scope.selectedProduct = scope.products[0];
      }


      /**
       *
       * @returns {undefined}
       */
      function onChartOptionChange() {
        scope.selectedChartFilterBy = null;

        getBimGeneralDownloadReportData();

        getBIMCompanyWiseGrandTotalDownloads();
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selFilter
       * @returns {undefined}
       */
      function onEplanChartFilterByChange(evnt, selFilter) {
        scope.selectedChartFilterBy = selFilter;

        getBimGeneralDownloadReportData();
      }


      /**
       * Get BIM report data from NODE
       *
       * @returns {undefined}
       */
      function getBimGeneralDownloadReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        scope.showEplanChartSubLinks = false;

        var country = scope.selectedCountry.countryName;
        var product = scope.selectedProduct.productName;
        var month = scope.selectedMonth.value;
        var year = scope.selectedYear;

        businessReportService.getBIMGeneralDownloadReportData(country, product, month, year).then(
                function (result) {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  logger.log("BIM::getEcoReachReportData::Result::Success");

                  var tempResult = angular.copy(result);

                  parseBIMGeneralDownloadReportAndDisplayChart(tempResult, country, product, month, year);
                },
                function (error) {
                  logger.log("BIM::getEcoReachReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }



      /**
       *  Init PIE  Chart Options..
       *
       * @returns {undefined}
       */
      function initPieChartOption() {
        chartObj = {};
        chartObj.type = "PieChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPieChartOptions();

        isInitChartData = true;
      }


      /**
       *  Init COLUMN  Chart Options..
       *
       * @returns {undefined}
       */
      function initColumnChartOption() {
        chartObj = {};
        chartObj.type = "ColumnChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getColumnChartOptions();

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        scope.isChartReady = false;
        chartObj.data = {};

        //Start Timer
        timer = $timeout(function () {
          console.log("ecoReach.js :: loadDefaultChart :: Timer :: Start ");

          reLoadChartDataAndSource();

          scope.isChartReady = true;

        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoReach.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function reLoadChartDataAndSource() {
        //Set Chart Data
        chartObj.data = scope.bimGeneralDownloadReportData.chartData;
        //Set value to Scope
        scope.chartSource = chartObj;
      }





      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }






      /**
       *
       * @param {type} reportData
       * @param {type} country
       * @param {type} product
       * @param {type} month
       * @param {type} year
       * @returns {undefined}
       */
      function parseBIMGeneralDownloadReportAndDisplayChart(reportData, country, product, month, year) {

        //scope.bimGeneralDownloadReportData = {};
        scope.bimGeneralDownloadReportData.totalDownloads = 0;
        scope.bimGeneralDownloadReportData.chartData = {};

        var tmpDataItem = {};
        var _totalDownloads = 0;

        //Calculate Total Downloads
        for (var i = 0; i < reportData.length; i++) {
          tmpDataItem = reportData[i];
          _totalDownloads += tmpDataItem.TOT_DOWNLOADS;
        }

        //Set Total Downloads Value for Displaying in Table
        scope.bimGeneralDownloadReportData.totalDownloads = _totalDownloads;

        //Prepare Chart DATA and Display Chart
        groupGeneralDownloadTop_X_DataForChartAndDisplayChart(reportData, country, product, month, year);
      }


      /**
       *
       * @param {type} reportData
       * @param {type} country
       * @param {type} product
       * @param {type} month
       * @param {type} year
       * @returns {undefined}
       */
      function groupGeneralDownloadTop_X_DataForChartAndDisplayChart(reportData, country, product, month, year) {

        var _groupedChartData = null;
        var _chartData = null;

        scope.ePlanChartLabel = CHART_LABLE_TOTAL_DOWNLOADS;

        if (year.toString().toLowerCase() === "all") {

//                    console.log("0");

          // 1. Country: Specific, Month: All, Year:Specific, Product Specific - (OK)
        } else if (country.toLowerCase() !== "all" && product.toLowerCase() !== "all" && month.toString().toLowerCase() === "all" && parseInt(year) > 0) {
//                    console.log("1");

          _groupedChartData = reportData;

          updateMonthNumberWithMonthString(_groupedChartData, 'MONTH', "TOT_DOWNLOADS");

          _chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "MONTH", "TOT_DOWNLOADS", "Month", "Total Downloads");

          //----------------- SET Chart Data ---------------------
          scope.bimGeneralDownloadReportData.chartData = _chartData;

          // -------------Init Column Chart Options-----------------------
          initColumnChartOption();

          //------------- Load Chart -----------------------
          loadDefaultChart();

          // 2. Country: All, Product Specific, Month: Specific, Year:Specific - (OK)
        } else if (country.toLowerCase() === "all" && product.toLowerCase() !== "all" && month.toString().toLowerCase() !== "all" && parseInt(year) > 0) {
//                    console.log("2");

          _groupedChartData = groupTop_X_Chart_Data(reportData);

          _chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "COUNTRY", "TOT_DOWNLOADS", "Country", "Total Downloads");

          //----------------- SET Chart Data ---------------------
          scope.bimGeneralDownloadReportData.chartData = _chartData;

          // -------------Init Pie Chart Options-----------------------
          initPieChartOption();

          //------------- Load Chart -----------------------
          loadDefaultChart();

          // 3. Country: Specific, Product: All, Month: Specific, Year: Specific
        } else if (country.toLowerCase() !== "all" && product.toLowerCase() === "all" && month.toString().toLowerCase() !== "all" && parseInt(year) > 0) {
//                    console.log("3");

          _groupedChartData = groupTop_X_Chart_Data(reportData);

          _chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "PRODUCT_NAME", "TOT_DOWNLOADS", "Country", "Total Downloads");

          //----------------- SET Chart Data ---------------------
          scope.bimGeneralDownloadReportData.chartData = _chartData;

          // -------------Init Pie Chart Options-----------------------
          initPieChartOption();

          //------------- Load Chart -----------------------
          loadDefaultChart();


          // 4. Country: All, Product: All, Month: All, Year: Specific - BAR or COLUMN CHART
        } else if (country.toLowerCase() === "all" && product.toLowerCase() === "all" && month.toString().toLowerCase() === "all" && parseInt(year) > 0) {
//                    console.log("4");

          _groupedChartData = reportData;

          updateMonthNumberWithMonthString(_groupedChartData, 'MONTH', "TOT_DOWNLOADS");

          _chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "MONTH", "TOT_DOWNLOADS", "Month", "Total Downloads");

          //----------------- SET Chart Data ---------------------
          scope.bimGeneralDownloadReportData.chartData = _chartData;

          // -------------Init Column Chart Options-----------------------
          initColumnChartOption();

          //------------- Load Chart -----------------------
          loadDefaultChart();

          // 5. Country: All, Product: All, Month: Specific, Year: Specific
        } else if (country.toLowerCase() === "all" && product.toLowerCase() === "all" && month.toString().toLowerCase() !== "all" && parseInt(year) > 0) {
//                    console.log("5");

          _groupedChartData = groupTop_X_Chart_Data(reportData);

          _chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "COUNTRY", "TOT_DOWNLOADS", "Country", "Total Downloads");

          //----------------- SET Chart Data ---------------------
          scope.bimGeneralDownloadReportData.chartData = _chartData;

          // -------------Init Pie Chart Options-----------------------
          initPieChartOption();

          //------------- Load Chart -----------------------
          loadDefaultChart();

          // 6. Country: Specific, Product: All, Month: All, Year: Specific   BAR or COLUMN CHART
        } else if (country.toLowerCase() !== "all" && product.toLowerCase() === "all" && month.toString().toLowerCase() === "all" && parseInt(year) > 0) {
//                    console.log("6");

          _groupedChartData = reportData;

          updateMonthNumberWithMonthString(_groupedChartData, 'MONTH', "TOT_DOWNLOADS");

          _chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "MONTH", "TOT_DOWNLOADS", "Month", "Total Downloads");

          //----------------- SET Chart Data ---------------------
          scope.bimGeneralDownloadReportData.chartData = _chartData;

          // -------------Init Column Chart Options-----------------------
          initColumnChartOption();

          //------------- Load Chart -----------------------
          loadDefaultChart();

          // 7. Country: All, Product: Specific, Month: All, Year: Specific - BAR or COLUMN CHART
        } else if (country.toLowerCase() === "all" && product.toLowerCase() !== "all" && month.toString().toLowerCase() === "all" && parseInt(year) > 0) {
//                    console.log("7");

          _groupedChartData = reportData;

          updateMonthNumberWithMonthString(_groupedChartData, 'MONTH', "TOT_DOWNLOADS");

          _chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "MONTH", "TOT_DOWNLOADS", "Month", "Total Downloads");

          //----------------- SET Chart Data ---------------------
          scope.bimGeneralDownloadReportData.chartData = _chartData;

          // -------------Init Column Chart Options-----------------------
          initColumnChartOption();

          //------------- Load Chart -----------------------
          loadDefaultChart();

          // 8. Country: Specific, Product: Specific, Month: Specific, Year: Specific - PIE CHART
        } else if (country.toLowerCase() !== "all" && product.toLowerCase() !== "all" && month.toString().toLowerCase() !== "all" && parseInt(year) > 0) {
//                    console.log("8");

          _groupedChartData = groupTop_X_Chart_Data(reportData);

          _chartData = gaChartUtil.populateTwoFieldChartData(_groupedChartData, "PRODUCT_NAME", "TOT_DOWNLOADS", "Country", "Total Downloads");

          //----------------- SET Chart Data ---------------------
          scope.bimGeneralDownloadReportData.chartData = _chartData;

          // -------------Init Pie Chart Options-----------------------
          initPieChartOption();

          //------------- Load Chart -----------------------
          loadDefaultChart();

        }





      } //




      /**
       *
       * @param {type} data
       * @returns {undefined}
       */
      function groupTop_X_Chart_Data(data) {

        var tmpData = angular.copy(data);
        var tmpDataItem = {};
        var _totalDownloads = 0;
        var newChartData = null;

        //Calculate TotalDownloads for Others
        if (tmpData.length > (TOP_X_DATA_FOR_CHART + 1)) {

          //SORT BY TOT_DOWNLOADS
          tmpData.sort(function (a, b) {
            return b['TOT_DOWNLOADS'] - a['TOT_DOWNLOADS'];
          });

          for (var i = TOP_X_DATA_FOR_CHART; i < tmpData.length; i++) {
            tmpDataItem = tmpData[i];
            _totalDownloads += tmpDataItem.TOT_DOWNLOADS;
          }

          //Truncate First X+1 Item
          newChartData = tmpData.slice(0, (TOP_X_DATA_FOR_CHART + 1));

          //Get Last data
          var lastDataItem = newChartData[TOP_X_DATA_FOR_CHART];

          //Update last data
          for (var key in lastDataItem) {
            if (key !== "key") {
              lastDataItem[key] = "Others";
            }
          }
          //Update Total Downloads;
          lastDataItem.TOT_DOWNLOADS = _totalDownloads;

        } else {
          newChartData = tmpData;
        }

        return newChartData;
      }


      /**
       *
       * @param {type} monthData
       * @param {type} dateFieldName
       * @param {type} otherFieldName
       * @returns {undefined}
       */
      function updateMonthNumberWithMonthString(monthData, dateFieldName, otherFieldName) {
        var tmpMonthObj = null;
        var tmpMonthVal = 0;
        var tmpMonthStr = "";
        var matchFound = false;

        var monthArrShort = appConstants.DATES.MONTH_ARR_SHORT;

        //Create 12 Month Array Data..
        for (var j = 1; j <= 12; j++) {
          matchFound = false;
          for (var i = 0; i < monthData.length; i++) {
            tmpMonthObj = monthData[i];
            tmpMonthVal = parseInt(tmpMonthObj[dateFieldName]);

            if (tmpMonthVal === j) {
              matchFound = true;
              //tmpMonthStr = monthArrShort[(tmpMonthVal - 1)];
              //tmpMonthObj[dateFieldName] = tmpMonthStr;
            }
          }
          if (matchFound === false) {
            tmpMonthObj = {};
            //tmpMonthStr = monthArrShort[j];
            tmpMonthObj[dateFieldName] = (j);
            tmpMonthObj[otherFieldName] = 0;
            monthData.push(tmpMonthObj);
          }
        }

        //SORT BY MONTH
        monthData.sort(function (a, b) {
          return a[dateFieldName] - b[dateFieldName];
        });


        for (var i = 0; i < monthData.length; i++) {
          tmpMonthObj = monthData[i];
          tmpMonthVal = parseInt(tmpMonthObj[dateFieldName]);
          matchFound = true;
          tmpMonthStr = monthArrShort[(tmpMonthVal - 1)];
          tmpMonthObj[dateFieldName] = tmpMonthStr;

        }

      }//End Function



    }
  }


})();