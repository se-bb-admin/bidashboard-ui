/**
 * @ngdoc directive
 * @name BIM Report directive
 *
 * @description
 *  BIM Report directive displays all list of information based on counties selection.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding')
          .directive('bimViewDownload', ecoReach);

  function ecoReach($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, businessReportService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
      },
      templateUrl: 'scripts/directives/bim/bimViewDownload.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      var TOP_X_DATA_FOR_CHART = 40;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var CHART_LABLE_TOTAL_DOWNLOADS = 'Total Viewed & Total Downloads ';

      var timer = null;

      //------------------------------------------------------------------
      //---------------------------- Global Properties -------------------
      //------------------------------------------------------------------
      scope.products = [];
      scope.selectedProduct = "";

      scope.countries = [];
      scope.selectedCountrye = "";


      scope.months = appConstants.DATES.MONTH_ARR_LONG;
      scope.selectedMonth = "";

      scope.years = appConstants.DATES.DATA_YEARS;
      scope.selectedYear = "";

      scope.selectedChartFilterBy = null;


      scope.bimGeneralDownloadReportData = {};
      scope.bimGeneralDownloadReportData.totalDownloads = 0;
      scope.bimGeneralDownloadReportData.totalViewed = 0;
      scope.bimGeneralDownloadReportData.chartData = {};

      scope.onEplanChartFilterByChange = onEplanChartFilterByChange;

      scope.chartSource = {};
      scope.ePlanChartLabel = "";
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------------------
      //--------- BIM CHART INIT SHOW HIDE PROPERTIES --------------
      //------------------------------------------------------------------




      //------------------------------------------------------------------
      //-------------------  For Chart TYPE Options  ---------------------
      //------------------------------------------------------------------


      init();



      /**
       * Function to initialized the repors
       *
       * @returns {undefined}
       */
      function init() {

        scope.months = angular.copy(appConstants.DATES.MONTH_ARR_LONG);
        scope.years = angular.copy(appConstants.DATES.DATA_YEARS);

        addAllParamsInDropDowns();

        setDefaultDropDownValues();

        getBimViewDownloadReportData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reLoadChartDataAndSource);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }


      /**
       *
       * @returns {undefined}
       */
      function addAllParamsInDropDowns() {
        scope.months.unshift({label: "All", value: "all"});
        scope.years.unshift("All");
      }

      /**
       * Scenario 1:
       *     Country: Specific, Month: All, Year:Specific, Product Specific
       *
       * @returns {undefined}
       */
      function setDefaultDropDownValues() {
        scope.selectedMonth = scope.months[0];
        scope.selectedYear = scope.years[0];
      }



      /**
       *
       * @param {type} evnt
       * @param {type} selFilter
       * @returns {undefined}
       */
      function onEplanChartFilterByChange(evnt, selFilter) {
        scope.selectedChartFilterBy = selFilter;

        getBimViewDownloadReportData();
      }


      /**
       * Get BIM report data from NODE
       *
       * @returns {undefined}
       */
      function getBimViewDownloadReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        var month = scope.selectedMonth.value;
        var year = scope.selectedYear;

        businessReportService.getBIMViewDownloadReportData(month, year).then(
                function (result) {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  logger.log("BIM::getEcoReachReportData::Result::Success");

                  var tempResult = angular.copy(result);

                  parseBIMGeneralDownloadReportAndDisplayChart(tempResult, month, year);
                },
                function (error) {
                  logger.log("BIM::getEcoReachReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }



      /**
       *
       * @returns {undefined}
       */
      function getEplanCompanyCounts() {
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        scope.bimGeneralDownloadReportData.companyCounts = 0;

        var countryCode = scope.selectedCountry.countryName;
        var month = scope.selectedMonth.value;
        var year = scope.selectedYear;

        businessReportService.getEplanCompanyCounts(countryCode, month, year).then(
                function (data) {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  logger.log("BIM::getEplanCompanyCounts::Result::Success");

                  scope.bimGeneralDownloadReportData.companyCounts = data[0].DISTINCT_COMPANY_NAME;
                },
                function (error) {
                  logger.log("BIM::getEplanCompanyCounts::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       *  Init COLUMN  Chart Options..
       *
       * @returns {undefined}
       */
      function initBarChartOption() {
        chartObj = {};
        chartObj.type = "BarChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getBarChartOptions();

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        scope.isChartReady = false;
        chartObj.data = {};

        //Start Timer
        timer = $timeout(function () {
          console.log("ecoReach.js :: loadDefaultChart :: Timer :: Start ");

          reLoadChartDataAndSource();

          scope.isChartReady = true;

        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoReach.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function reLoadChartDataAndSource() {
        //Set Chart Data
        chartObj.data = scope.bimGeneralDownloadReportData.chartData;
        //Set value to Scope
        scope.chartSource = chartObj;
      }



      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }



      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseBIMGeneralDownloadReportAndDisplayChart(reportData) {

        scope.bimGeneralDownloadReportData.totalDownloads = 0;
        scope.bimGeneralDownloadReportData.chartData = {};

        var tmpDataItem = {};
        var _totalDownloads = 0;
        var _totalviewed = 0;

        //Calculate Total Downloads
        for (var i = 0; i < reportData.length; i++) {
          tmpDataItem = reportData[i];
          _totalDownloads += tmpDataItem.TOT_DOWNLOADS;
          _totalviewed += tmpDataItem.TOT_VIEWED;
        }

        //Set Total Downloads Value for Displaying in Table
        scope.bimGeneralDownloadReportData.totalDownloads = _totalDownloads;
        scope.bimGeneralDownloadReportData.totalViewed = _totalviewed;

        //Prepare Chart DATA and Display Chart
        groupGeneralDownloadTop_X_DataForChartAndDisplayChart(reportData);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function groupGeneralDownloadTop_X_DataForChartAndDisplayChart(reportData) {

        var _groupedChartData = null;
        var _chartData = null;

        scope.ePlanChartLabel = CHART_LABLE_TOTAL_DOWNLOADS;

        _groupedChartData = groupTop_X_Chart_Data(reportData);

        _chartData = gaChartUtil.populateThreeFieldChartData(_groupedChartData, "PRODUCT_NAME", "TOT_VIEWED", "TOT_DOWNLOADS", "Product Name", "Total Viewed", "Total Downloads");

        //----------------- SET Chart Data ---------------------
        scope.bimGeneralDownloadReportData.chartData = _chartData;

        // -------------Init Pie Chart Options-----------------------
        initBarChartOption();

        //------------- Load Chart -----------------------
        loadDefaultChart();


      } //




      /**
       *
       * @param {type} data
       * @returns {undefined}
       */
      function groupTop_X_Chart_Data(data) {

        var tmpData = angular.copy(data);
        var tmpDataItem = {};
        var _totalDownloads = 0;
        var _totalViwed = 0;
        var newChartData = null;

        //SORT BY TOT_DOWNLOADS
        tmpData.sort(function (a, b) {
          return b['TOT_DOWNLOADS'] - a['TOT_DOWNLOADS'];
        });

        //Calculate TotalDownloads for Others
        if (tmpData.length > (TOP_X_DATA_FOR_CHART + 1)) {

          for (var i = TOP_X_DATA_FOR_CHART; i < tmpData.length; i++) {
            tmpDataItem = tmpData[i];
            _totalDownloads += tmpDataItem.TOT_DOWNLOADS;
            _totalViwed += tmpDataItem.TOT_VIEWED;
          }

          //Truncate First X+1 Item
          newChartData = tmpData.slice(0, (TOP_X_DATA_FOR_CHART + 1));

          //Get Last data
          var lastDataItem = newChartData[TOP_X_DATA_FOR_CHART];

          //Update last data
          for (var key in lastDataItem) {
            if (key !== "key") {
              lastDataItem[key] = "Others";
            }
          }
          //Update Total Downloads;
          lastDataItem.TOT_DOWNLOADS = _totalDownloads;
          lastDataItem.TOT_VIEWED = _totalViwed;

        } else {
          newChartData = tmpData;
        }

        return newChartData;
      }



    }
  }


})();