/**
 * @ngdoc directive
 * @name Achive Target Summary Component directive
 *
 * @description
 *  Achive Target Summary Component directive.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('achiveTargetSummaryComponent', achiveTargetSummaryComponent);

  function achiveTargetSummaryComponent($rootScope, appConstants, commonUtils, logger, $filter) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        targetData: "=targetData",
        achiveData: "=achiveData",
        quarter: "@quarter",
        year: "@year",
        noteSymbolAchive: "=noteSymbolAchive",
        noteSymbolTarget: "=noteSymbolTarget",
        isTotalTarget: "@isTotalTarget"
      },
      templateUrl: 'scripts/directives/summary/achiveTargetSummaryComponent.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      var currentYear = new Date().getFullYear();

      scope.getClassArrow = getClassArrow;
      scope.profitLossPercent = getProfitLossPercent;
      scope.getPercentBgClass = getPercentBgClass;
      scope.checkQtrVisibility = checkQtrVisibility;
      scope.showNoteSymbol = showNoteSymbol;
      scope.getTargetRemaining = getTargetRemaining;
      scope.getTargetExceed = getTargetExceed;


      function getTargetRemaining() {
        var targetRemaining = parseInt(scope.targetData - scope.achiveData);
        return targetRemaining;
      }

      function getTargetExceed() {
        var targetExceed = parseInt(scope.achiveData - scope.targetData);
        return targetExceed;
      }

      /**
       *
       * @param {type} target
       * @param {type} achive
       * @returns {String}
       */
      function getClassArrow(target, achive) {
        var returnClassName = "invisible-elem";
        var crntQtr = getCurrentQuarter();
        var targetQtr = getTargetQuarter();

        //if (scope.quarter > crntQtr || parseInt(target) === 0) {
        if (targetQtr > crntQtr || parseInt(target) === 0) {
          returnClassName = "invisible-elem";
        } else if (achive > target) {
          returnClassName = "glyphicon-arrow-up up-arrow";
        } else if (achive < target) {
          returnClassName = "glyphicon-arrow-down down-arrow";
        }
        return returnClassName;
      }


      /**
       *
       * @returns {Number}
       */
      function getProfitLossPercent() {
        var plPercent = 100;
        var fractionSize = 1;

        if (scope.targetData !== 0) {
          var value = (scope.achiveData - scope.targetData) / scope.targetData;
          var tempPlPercent = (value * 100);

          plPercent = $filter('number')(tempPlPercent, fractionSize);
          // plPercent = (Math.round(value * 100));
        }

        return plPercent;
      }


      /**
       *
       * @returns {String}
       */
      function getPercentBgClass() {
        var crntQtr = getCurrentQuarter();
        var returnClassName = "invisible-elem";
        var profitLossPercent = getProfitLossPercent();
        var targetQtr = getTargetQuarter();

        //if (scope.quarter > crntQtr || scope.targetData === 0) {
        if (targetQtr > crntQtr || scope.targetData === 0) {
          returnClassName = "percent-blank-box";
        } else if (scope.targetData === 0 && scope.achiveData === 0) {
          returnClassName = "percent-blank-box";
        } else if (profitLossPercent > 0) {
          returnClassName = "percent-up-box";
        } else if (profitLossPercent < 0) {
          returnClassName = "percent-down-box";
        } else {
          returnClassName = "percent-blank-box";
        }
        return returnClassName;
      }


      /**
       *
       * @returns {String}
       */
      function checkQtrVisibility() {

        var crntQtr = getCurrentQuarter();
        var returnClassName = "invisible-elem";

        var targetQtr = getTargetQuarter();

        //if (scope.quarter > crntQtr || scope.targetData === 0) {
        if (targetQtr > crntQtr || scope.targetData === 0) {
          returnClassName = "invisible-elem";
        } else if (scope.targetData === 0 && scope.achiveData === 0) {
          returnClassName = "invisible-elem";
        } else {
          returnClassName = "";
        }

        return returnClassName;
      }

      /**
       *
       * @param {type} symbol
       * @returns {Boolean}
       */
      function showNoteSymbol(symbol) {
        if (symbol !== undefined && symbol !== null && symbol !== "") {
          return true;
        }
      }

      /**
       *
       * @returns {Number}
       */
      function getCurrentQuarter() {

        var month = (new Date().getMonth() + 1);
        var crntQtr = 1;

        if (month >= 1 && month <= 3) {
          crntQtr = 1;
        } else if (month >= 4 && month <= 6) {
          crntQtr = 2;
        } else if (month >= 6 && month <= 9) {
          crntQtr = 3;
        } else if (month >= 9 && month <= 12) {
          crntQtr = 4;
        }

        return crntQtr;
      }

      /**
       *
       * @returns {undefined}
       */
      function getTargetQuarter() {
        var targetQtr = -1;
        if (scope.year === currentYear && scope.quarter !== undefined) {
          targetQtr = scope.quarter;
        }

        return targetQtr;
      }

    } // #End Link
  }
})();