/**
 * @ngdoc directive
 * @name Achive Target Summary Component directive
 *
 * @description
 *  Achive Target Summary Component directive.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('achiveSummaryComponent', achiveSummaryComponent);

  function achiveSummaryComponent($rootScope, appConstants, commonUtils, logger, $filter) {
    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        data: '='
      },
      templateUrl: 'scripts/directives/summary/achiveSummaryComponent.html',
      link: function (scope, element, attrs) {

        scope.getchart = function (item) {

//                    alert(item.appName);
          var chart = {
            type: "ColumnChart",
            data: {
              cols: [
                {label: "Quarters", type: "string"},
                {label: "Regd users (T)", type: "number"},
                {label: "Regd users (A)", type: "number"},
              ],
              rows: []
            }
          };
//                    console.log('*** item: ',item);
          chart.data.rows.push(
                  {c: [
                      {v: 'Q1'},
                      {v: item.Q1Target},
                      {v: item.Q1Achive}
                    ]});
          chart.data.rows.push(
                  {c: [
                      {v: 'Q2'},
                      {v: item.Q2Target},
                      {v: item.Q2Achive}
                    ]}
          );
          chart.data.rows.push(
                  {c: [
                      {v: 'Q3'},
                      {v: item.Q3Target},
                      {v: item.Q3Achive}
                    ]}
          );
          chart.data.rows.push(
                  {c: [
                      {v: 'Q4'},
                      {v: item.Q4Target},
                      {v: item.Q4Achive}
                    ]}
          );
          return chart;
        };

        console.log('data: ', scope.data);

        scope.getTotalTarget = function (target) {
          return target.Q1Target + target.Q2Target + target.Q3Target + target.Q4Target;
        };
        scope.getTotalTargetAchive = function (target) {
          return target.Q1Achive + target.Q2Achive + target.Q3Achive + target.Q4Achive;
        };

        scope.getProfitLossPercent = function (achive, target) {
          var percent = 100;
          console.log('achive: ', achive, ' target: ', target);
          if (target > 0) {
            percent = ((achive - target) / target) * 100;
          }
          return percent.toFixed(1);
        };

        scope.getClassArrow = function (achive, target) {
          var returnClassName = "invisible-elem";
          if (achive > target) {
            returnClassName = "glyphicon-arrow-up up-arrow";
          } else if (achive < target) {
            returnClassName = "glyphicon-arrow-down down-arrow";
          }
          return returnClassName;
        }
      }
    };

    return directive;
  }
})();