/**
 * @ngdoc directive
 * @name MyNovaBuddy directive
 *
 * @description
 *  MyNovaBuddy....
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('myNovaBuddy', myNovaBuddy);

  function myNovaBuddy($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, MyNovaBuddyCountryVO, currencyConverterService, paceService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/mynovaBuddy/mynovaBuddy.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = [
        'France',
        'Spain'
      ];
      var _myNovaBuddyReportData = null;
      var _myNovaBuddyChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var timer = null;

//			console.log('==> myNovaBuddy :: paceDataRegUserOverPeriod :: ', scope.paceDataRegUserOverPeriod);

      //------------------------------------------------------------------
      //--------------------------- Global Properties --------------------
      //------------------------------------------------------------------

      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];
//			scope.myNovaBuddyFranceLaunchDate = appConstants.DATES.MyNovaBuddy_FRANCE_LAUNCHED_DATE;
      scope.mynovaBuddyLaunchDates = appConstants.DATES.MY_NOVA_BUDDY_LAUNCHED_DATES;
      scope.MyNovaBuddy_BOM_FOOTER_NOTE = appConstants.NOTE.MyNovaBuddy_BOM_FOOTER_NOTE;
      scope.uniqueActiveUsers = 0;
      scope.myNovaBuddyTotalUniqueActiveUsers = 0;
      scope.myNovaBuddyLaunchDays = 0;
      scope.myNovaBuddyRegisteredUsers = 0;
      scope.loadDefaultChart = false;

      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;


      //------------------------------------------------------------------
      //--------------- MY PACT CHART INIT SHOW HIDE PROPERTIES ----------
      //------------------------------------------------------------------

      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_REGISTERED_USERS = "No. of registered users";
      var DEFAULT_CHART_NAME = "RegisteredUsers";
      scope.onCountryDropDownChange = onCountryDropDownChange;
      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------------------
      //----------------------  For Chart Options ------------------------
      //------------------------------------------------------------------

      scope.selectedChartTab = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.myNovaBuddyChartTypeSelected = scope.chartOptions[scope.selectedChartTab].toLowerCase();
      scope.myNovaBuddyChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.myNovaBuddyChartName = DEFAULT_CHART_NAME;
      scope.myNovaBuddyChartLabel = NO_OF_REGISTERED_USERS;//NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.disableUniqueActiveUsers = false;
      scope.onMyNovaBuddyChartOptionChange = onMyNovaBuddyChartOptionChange;
      scope.onMyNovaBuddyChartLinkChange = onMyNovaBuddyChartLinkChange;
      scope.chartReady = chartReady;


      init();


      /**
       *
       * @returns {undefined}
       */
      function init() {
//				console.log('==> myNovaBuddy ==> paceDataRegUser: ', scope.paceDataRegUser);
//				console.log('##=> myNovaBuddy ==> paceDataRegUserOverPeriod: ', scope.paceDataRegUserOverPeriod);
        // Init Countries data
        initCountries();
        // Get Report Data
        getMyNovaBuddyReportData();
        // Get Chart Data
        getMyNovaBuddyChartData();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);
        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }

      /**
       * Get MyNovaBuddy report data from NODE
       *
       * @returns {undefined}
       */
      function getMyNovaBuddyReportData() {
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getMyNovaBuddyAppReportData().then(
                function (result) {
                  logger.log("MyNovaBuddy::getMyNovaBuddyReportData::Result::Success");
                  _myNovaBuddyReportData = result;
                  console.log('@127 _myNovaBuddyReportData: ', _myNovaBuddyReportData);
                  parseReportData(_myNovaBuddyReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _myNovaBuddyReportData = null;
                  logger.log("MyNovaBuddy::getMyNovaBuddyReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       * Get MyNovaBuddy CHART data from NODE
       *
       * @returns {undefined}
       */
      function getMyNovaBuddyChartData() {
        loadDefaultChart();

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getMyNovaBuddyAppChartData().then(
                function (result) {
                  logger.log("MyNovaBuddy::getMyNovaBuddyChartData::Result::Success");
                  _myNovaBuddyChartData = result;
                  parseChartData(_myNovaBuddyChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _myNovaBuddyChartData = null;
                  logger.log("MyNovaBuddy::getMyNovaBuddyChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          console.log("mynovaBuddy.js :: loadDefaultChart :: Timer :: Start ");
          onMyNovaBuddyChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("mynovaBuddy.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }


      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       *
       * @returns {undefined}
       */
      function initCountries() {
        var mynovaBuddy = {};
        //--
        for (var i = 0; i < countryNameList.length; i++) {

          mynovaBuddy.countryName = countryNameList[i];
          var countryName = mynovaBuddy.countryName;
//					if (mynovaBuddy.countryName.toUpperCase() === "FRANCE") {
//						mynovaBuddy.launchDate = scope.mynovaBuddyFranceLaunchDate;
//					}
          mynovaBuddy.launchDate = scope.mynovaBuddyLaunchDates[countryName];

          mynovaBuddy.mynovaBuddyLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, mynovaBuddy.launchDate);
          scope.mynovaBuddyLaunchDays = mynovaBuddy.mynovaBuddyLaunchDays;
//					console.log('>>> mynovaBuddy.mynovaBuddyLaunchDays: ', mynovaBuddy.mynovaBuddyLaunchDays);
          mynovaBuddy.uniqueActiveUsers = 0;
          scope.uniqueActiveUsers = mynovaBuddy.uniqueActiveUsers;

          //--- Push country to Countries Array
          scope.countries.push(new MyNovaBuddyCountryVO(mynovaBuddy));
        }
        //Set default Country to update data binding
        setDefaultCountry();
      }


      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
        onMyNovaBuddyChartLinkChange(DEFAULT_CHART_NAME);
      }


      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;
        logger.log(selectedCountryName);
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.launchDate = scope.countries[index].launchDate;
//				scope.mynovaBuddyLaunchDays = scope.countries[index].mynovaBuddyLaunchDays;
        setTableDataAndLink('Active Users', scope.countries[index].uniqueActiveUsers);

        onMyNovaBuddyChartLinkChange(DEFAULT_CHART_NAME);
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload MyNovaBuddy Chart.." + scope.selectedChartTab);
        //->
        onMyNovaBuddyChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.totalBomPrice = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          scope.mynovaBuddyTotalUniqueActiveUsers = scope.mynovaBuddyTotalUniqueActiveUsers + scope.countries[i].uniqueActiveUsers;
        }
        //----------------- SET Default Value ---------------------
        scope.launchDate = scope.countries[0].launchDate;
//                scope.mynovaBuddyLaunchDays = scope.countries[0].mynovaBuddyLaunchDays;
        setTableDataAndLink('Active Users', scope.countries[0].uniqueActiveUsers);
      }


      /**
       *
       * @param {type} fieldName
       * @param {type} value
       * @returns {undefined}
       */
      function setTableDataAndLink(fieldName, value) {
        if (fieldName === "Active Users") {
          if (value === 0) {
            scope.disableUniqueActiveUsers = true;
            angular.element("#uniqueActiveUsers").removeClass("link");
          } else {
            scope.disableUniqueActiveUsers = false;
            angular.element("#uniqueActiveUsers").addClass("link");
          }
          scope.uniqueActiveUsers = value;
        }
        console.log('>>>> fieldName: ', fieldName, ' -- setTableDataAndLink :: scope.uniqueActiveUsers: ', scope.uniqueActiveUsers);
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        }
        console.log("Chart Data.. Prepare...");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onMyNovaBuddyChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        var chartTab = scope.selectedChartTab;
        scope.myNovaBuddyChartTypeSelected = scope.chartOptions[chartTab].toLowerCase();
        scope.myNovaBuddyChartName = chartName;
        onMyNovaBuddyChartOptionChange(null, chartTab);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onMyNovaBuddyChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }
        scope.isfootNoteSymbolVisible = false;
        scope.selectedChartTab = selTab;
        console.log("==>> selectedCountryName: ", selectedCountryName);
        var chartType = scope.chartOptions[scope.selectedChartTab].toLowerCase();
        scope.myNovaBuddyChartToolTip = scope.chartToolTips[scope.selectedChartTab];
        var chartName = scope.myNovaBuddyChartName;
//				var countryPrefix = selectedCountryName + " : ";
        var countryPrefix = selectedCountryName + " : ";
//				var keyIndex = 0;
//				for (var i = 0; i < countryNameList.length; i++) {
//					if (countryNameList[i] === selectedCountryName) {
//						keyIndex = i;
//					}
//				}

        if (isInitChartData === false) {
          initChartData();
        }
        switch (chartName) {
          case "RegisteredUsers":
          {
            countryPrefix = "Total - Worldwide : ";
            scope.isfootNoteSymbolVisible = true;
            scope.myNovaBuddyChartLabel = countryPrefix + NO_OF_REGISTERED_USERS;
            //Add New Chart
            chartObj.data = scope.paceDataRegUserOverPeriod.myNovaBuddyUsersReg[chartType];
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.myNovaBuddyChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }
    }//END Link..
  }
})();