/**
 * @ngdoc directive
 * @name MyNovaBuddy App directive
 *
 * @description
 *  MyNovaBuddy App directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('myNovaBuddy', myNovaBuddy);

  myNovaBuddy.$inject = ['$rootScope', '$timeout', 'appConstants', 'commonUtils', 'logger',
    'gaChartUtil', 'nodeGAService', 'MyNovaBuddyCountryVO', 'mynovabuddyService', 'paceService'];

  function myNovaBuddy($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, MyNovaBuddyCountryVO, mynovabuddyService, paceService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/myNovaBuddy/myNovaBuddy.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

//			var countryNameList = [
//				'France',
//				'Spain'
//			];
      var countryNameList = [];
//			var countryNameList = [
//				'France',
//				'Spain',
//				'Belgium',
//				'Netherlands',
//				'United Kingdom',
//				'Ireland',
//				'Germany'
//			];


      var _myNovaBuddyReportData = null;
      var _myNovaBuddyChartData = null;

      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];

      scope.uniqueActvUsers = 0;
      // scope.configLaunched = 0;
      scope.userSessions = 0;
      scope.myNovaBuddyUsersReg = 0;
      scope.launchedDate = appConstants.DATES.MY_NOVA_BUDDY_LAUNCHED_DATES[countryNameList[0]];
      scope.myNovaBuddyLaunchDays = 0;

      scope.disableUniqueActiveUsers = false;
      scope.disableConfLaunched = false;

      //------------------------------------------------------------------
      //-------------- MyNovaBuddy APP CHART INIT SHOW HIDE PROPERTIES -----------
      //------------------------------------------------------------------
      scope.isGAChartVisible = true;
      scope.isPaceChartVisible = false;

      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      //var NO_OF_CONFIGURATION_LAUNCHED = "No. of Configuration launched";
      var NO_OF_USER_SESSIONS = "No. of user sessions";
      var NO_OF_REGISTERED_USERS = "No. of Registered Users";

      scope.onCountryDropDownChange = onCountryDropDownChange;

      scope.paceChartStyle = commonUtils.getPaceChartStyle();
      scope.paceChartSource = {};
      scope.showPaceChart = showPaceChart;
      var isInitPaceChartData = false;
      var paceChartObj = {};

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};
      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.myNovaBuddyChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.myNovaBuddyChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      //            scope.myNovaBuddyChartName = "ChartActiveUser";
      scope.myNovaBuddyChartLabel = NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;
      scope.noteUserSessions = appConstants.NOTE.USER_SESSIONS;

      scope.onMyNovaBuddyAppChartOptionChange = onMyNovaBuddyAppChartOptionChange;
      scope.onMyNovaBuddyAppChartLinkChange = onMyNovaBuddyAppChartLinkChange;

      scope.chartReady = chartReady;
      scope.device = 'android';

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        //--- Init Countries data
        console.log("my-nova-buddy :: INIT CALLED.......");

//				initCountries();
        // Get Report Data
        getMyNovaBuddyAppReportData();
        // Get Chart Data
        getMyNovaBuddyAppChartData();

        //Add Event Listener  When page resize
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        /*
         scope.$watch('paceDataRegUser.myNovaBuddyUsersReg', function (newValue, oldValue) {
         //console.log("newValue:::::: ***** "+newValue);
         if (newValue !== undefined && newValue !== null) {
         initPaceChartData();
         }
         });
         */
        paceService.getRegisteredUsersForProject('my-nova-buddy').then(function (data) {
          scope.myNovaBuddyUsersReg = data.users;
        });
      }

      /**
       * Get Quick Ref report data from NODE
       *
       * @returns {undefined}
       */
      function getMyNovaBuddyAppReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getMyNovaBuddyAppReportData(scope.device).then(
                function (result) {
                  logger.log("MyNovaBuddy App::getMyNovaBuddyAppReportData::Result::Success");
                  _myNovaBuddyReportData = result;
                  parseReportData(_myNovaBuddyReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _myNovaBuddyReportData = null;
                  logger.log("MyNovaBuddy APP::getMyNovaBuddyAppReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get MyNovaBuddy APP CHART data from NODE
       *
       * @returns {undefined}
       */
      function getMyNovaBuddyAppChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getMyNovaBuddyAppChartData(scope.device).then(
                function (result) {
                  logger.log("MyNovaBuddy App::getMyNovaBuddyAppChartData::Result::Success");
                  _myNovaBuddyChartData = result;
                  parseChartData(_myNovaBuddyChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _myNovaBuddyChartData = null;
                  logger.log("MyNovaBuddy App::getMyNovaBuddyAppChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      scope.setDevice = function (device) {
        scope.device = device;
        getMyNovaBuddyAppReportData();
        getMyNovaBuddyAppChartData();
      };

      scope.isDevice = function (device) {
        return scope.device === device;
      };

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("myNovaBuddy.js :: loadDefaultChart :: Timer :: Start ");
//					onMyNovaBuddyAppChartLinkChange('ChartActiveUser');
          onMyNovaBuddyAppChartLinkChange('UniqueActiveUsers');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("myNovaBuddy.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initPaceChartData() {
        paceChartObj = {};
        paceChartObj.type = "AreaChart";
        paceChartObj.displayed = false;
        paceChartObj.options = commonUtils.getPaceChartOptions();

        isInitPaceChartData = true;
      }

      /***
       *  Onclick Registererd users link click..
       *  Show PACE Chart Hide GA Chart
       *
       * @returns {undefined}
       *
       */
      function showPaceChart() {
        scope.isGAChartVisible = false;
        scope.isPaceChartVisible = true;

        //Show Hide Chart
        onMyNovaBuddyAppChartOptionChange(null, scope.selectedChartTab);
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }

      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }

      /**
       *
       * @param {type} name
       * @returns {unresolved}
       */
      function constructName(name) {
        var result = name.split(' ').join('_');
        return result;
      }


      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;
        //logger.log(selectedCountryName);
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.launchedDate = scope.countries[index].launchedDate;
        scope.myNovaBuddyLaunchDays = scope.countries[index].myNovaBuddyLaunchDays;

        setTableDataAndLink('UNIQUE_ACTIVE_USERS', scope.countries[index].uniqueActiveUsers);
        setTableDataAndLink('USER_SESSIONS', scope.countries[index].userSessions);

//				onMyNovaBuddyAppChartLinkChange('ChartActiveUser');
        onMyNovaBuddyAppChartLinkChange('UniqueActiveUsers');
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        //logger.log("Reload MyNovaBuddyApp Chart.." + scope.selectedChartTab);
        onMyNovaBuddyAppChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        countryNameList = [];
        scope.countries = [];

        for (var country in reportData) {
          countryNameList.push(country);
          var myNovaBuddy = {};
          var countryName = country;

          myNovaBuddy.countryName = country;
          myNovaBuddy.launchDate = appConstants.DATES.MY_NOVA_BUDDY_LAUNCHED_DATES[countryName];
          myNovaBuddy.myNovaBuddyLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, myNovaBuddy.launchDate);

          myNovaBuddy.uniqueActiveUsers = 0;
          myNovaBuddy.userSessions = 0;
          myNovaBuddy.launchedDate = null;
          //--- Push country to Countries Array
          scope.countries.push(new MyNovaBuddyCountryVO(myNovaBuddy));
        }
        // get data
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers);
          scope.countries[i].userSessions = parseFloat(reportData[keyName].sessions);
          scope.countries[i].launchedDate = reportData[keyName].launchedDate;
        }
        //----------------- SET Default Value ---------------------
        setDefaultCountry();

        setTableDataAndLink('UNIQUE_ACTIVE_USERS', scope.countries[0].uniqueActiveUsers);
        setTableDataAndLink('USER_SESSIONS', scope.countries[0].userSessions);
        scope.launchedDate = scope.countries[0].launchedDate;
        selectedCountryName = scope.countries[0].countryName;

      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          scope.countries[i].userSessionsChart = gaChartUtil.populateGAChartData(chartData[keyName].sessions, "dateFormatted", "sessions", "Date", "User Sessions");

        }
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onMyNovaBuddyAppChartLinkChange(chartName) {
        scope.isGAChartVisible = true;
        scope.isPaceChartVisible = false;

        //When you click on a link Chart will reset to Month Chart.
        scope.myNovaBuddyChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.myNovaBuddyChartName = chartName;
        onMyNovaBuddyAppChartOptionChange(null, scope.selectedChartTab);
      }

      /**
       *
       * @param string chartName
       * @returns {Boolean}
       */
      function isChartName(chartName) {
        return chartName === scope.myNovaBuddyChartName;
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onMyNovaBuddyAppChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.myNovaBuddyChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.myNovaBuddyChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.myNovaBuddyChartTypeSelected;
        var chartName = scope.myNovaBuddyChartName;

        var countryPrefix = selectedCountryName + " : ";
        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        if (isInitChartData === false) {
          initChartData();
        }

        //check for PACE Chart
        if (scope.isPaceChartVisible) {
          scope.quickQuotationChartLabel = NO_OF_REG_USERS;

          loadPaceChart(scope.selectedChartTab);
          return;
        }

        switch (chartName) {
//					case "ChartActiveUser":
          case "UniqueActiveUsers":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.myNovaBuddyChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            //console.dir(chartObj.data);
            break;
          }
          case "RegisteredUsers":
          {
            console.log("inside RegisteredUsers.......");
            scope.myNovaBuddyChartLabel = NO_OF_REGISTERED_USERS;
            //Add New Chart
            chartObj.data = scope.paceDataRegUserOverPeriod.myNovaBuddyUsersReg[chartType.toLowerCase()];
            //console.dir(chartObj.data);
            break;
          }
          case "ChartUserSessions":
          {
            console.log("inside ChartUserSessions.......");
            scope.myNovaBuddyChartLabel = countryPrefix + NO_OF_USER_SESSIONS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].userSessionsChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].userSessionsChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].userSessionsChart.month;
            }
            //console.dir(chartObj.data);
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




      /**
       * Load PACE Chart as per selected tab (Day | Week | Month)
       *
       * @param {type} selTab
       * @returns {undefined}
       */
      function loadPaceChart(selTab) {
        var chartTypeSelected = scope.chartOptions[scope.selectedChartTab];

        //TODO :: Also Need To Check "scope.paceDataRegUserOverPeriod.myPactBasicUsersReg" val exist or not..
        if (isInitPaceChartData === false) {
          initPaceChartData();
        }

        if (chartTypeSelected.toLowerCase() === 'day') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.myNovaBuddyUsersReg.day;
        } else if (chartTypeSelected.toLowerCase() === 'week') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.myNovaBuddyUsersReg.week;
        } else if (chartTypeSelected.toLowerCase() === 'month') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.myNovaBuddyUsersReg.month;
        }

        //Set value to Scope
        scope.paceChartSource = paceChartObj;
      }


      /**
       *
       * @param {type} filedName
       * @param {type} value
       * @returns {undefined}
       */
      function setTableDataAndLink(filedName, value) {
        if (filedName === "UNIQUE_ACTIVE_USERS") {
          if (value === 0) {
            scope.disableUniqueActiveUsers = true;
            angular.element("#uniqueActiveUsers").removeClass("link");
          } else {
            scope.disableUniqueActiveUsers = false;
            angular.element("#uniqueActiveUsers").addClass("link");
          }
          scope.uniqueActiveUsers = value;

        } else if (filedName === "USER_SESSIONS") {
          if (value === 0) {
            scope.disableConfLaunched = true;
            angular.element("#userSessions").removeClass("link");
          } else {
            scope.disableConfLaunched = false;
            angular.element("#userSessions").addClass("link");
          }
          scope.userSessions = value;
        }
      }

      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.myNovaBuddyChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }

    }//END Link..

  }
  ;
})();