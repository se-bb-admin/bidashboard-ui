/**
 * @ngdoc directive
 * @name MEA directive
 *
 * @description
 *  MEA....
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('mea', mea);

  function mea($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, MEACountryVO, currencyConverterService, paceService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        usersReg: "=usersReg"
      },
      templateUrl: 'scripts/directives/mea/mea.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      var countryNameList = ['France', 'Spain'];
      var _meaReportData = null;
      var _meaChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var timer = null;

      //------------------------------------------------------------------
      //--------------------------- Global Properties --------------------
      //------------------------------------------------------------------

      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];
//            scope.meaFranceLaunchDate = appConstants.DATES.MEA_FRANCE_LAUNCHED_DATE;
      scope.meaFranceLaunchDate = appConstants.DATES.MEA_LAUNCH_DATES[selectedCountryName];
      scope.MEA_BOM_FOOTER_NOTE = appConstants.NOTE.MEA_BOM_FOOTER_NOTE;
      scope.uniqueActiveUsers = 0;
      scope.meaTotalUniqueActiveUsers = 0;
      scope.meaLaunchDays = 0;

      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;


      //------------------------------------------------------------------
      //--------------- MY PACT CHART INIT SHOW HIDE PROPERTIES ----------
      //------------------------------------------------------------------

      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var DEFAULT_CHART_NAME = "UniqueActiveUsers";
      scope.onCountryDropDownChange = onCountryDropDownChange;
      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------------------
      //----------------------  For Chart Options ------------------------
      //------------------------------------------------------------------

      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.meaChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.meaChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.meaChartName = DEFAULT_CHART_NAME;
      scope.meaChartLabel = NO_OF_ACTIVE_USERS;//NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.disableUniqueActiveUsers = false;
      scope.onMEAChartOptionChange = onMEAChartOptionChange;
      scope.onMEAChartLinkChange = onMEAChartLinkChange;
      scope.chartReady = chartReady;


      init();


      /**
       *
       * @returns {undefined}
       */
      function init() {
        // Init Countries data
        initCountries();

        // paceService
        scope.meaRegisteredUsers = 0;
        paceService.getRegisteredUsersForProject('mea').then(function (data) {
          scope.meaRegisteredUsers = data.users;
        });

        // Get Report Data
        getMEAReportData();
        // Get Chart Data
        getMEAChartData();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);
        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }


      /**
       * Get MEA report data from NODE
       *
       * @returns {undefined}
       */
      function getMEAReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getMEAReportData().then(
                function (result) {
                  logger.log("MEA::getMEAReportData::Result::Success");
                  _meaReportData = result;
//							console.log('@127 _meaReportData: ', _meaReportData);
                  parseReportData(_meaReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _meaReportData = null;
                  logger.log("MEA::getMEAReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       * Get MEA CHART data from NODE
       *
       * @returns {undefined}
       */
      function getMEAChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getMEAChartData().then(
                function (result) {
                  logger.log("MEA::getMEAChartData::Result::Success");
                  _meaChartData = result;
                  parseChartData(_meaChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _meaChartData = null;
                  logger.log("MEA::getMEAChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          console.log("mea.js :: loadDefaultChart :: Timer :: Start ");
          onMEAChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("mea.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }


      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       *
       * @returns {undefined}
       */
      function initCountries() {
        var mea = {};
        //--
        for (var i = 0; i < countryNameList.length; i++) {

          mea.countryName = countryNameList[i];
          mea.launchDate = appConstants.DATES.MEA_LAUNCH_DATES[mea.countryName];
          mea.meaLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, mea.launchDate);

          scope.meaLaunchDays = mea.meaLaunchDays;
          scope.uniqueActiveUsers = mea.uniqueActiveUsers;

          //--- Push country to Countries Array
          scope.countries.push(new MEACountryVO(mea));
        }
        //Set default Country to update data binding
        setDefaultCountry();
      }


      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }


      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;
        logger.log(selectedCountryName);
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.launchDate = scope.countries[index].launchDate || 'Not launched';
        scope.meaLaunchDays = scope.countries[index].meaLaunchDays;
        setTableDataAndLink('Active Users', scope.countries[index].uniqueActiveUsers);

        onMEAChartLinkChange(DEFAULT_CHART_NAME);
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload MEA Chart.." + scope.selectedChartTab);
        //->
        onMEAChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.totalBomPrice = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          if (reportData[keyName]) {
            scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          } else {
            scope.countries[i].uniqueActiveUsers = 0;
          }
          scope.meaTotalUniqueActiveUsers = scope.meaTotalUniqueActiveUsers + scope.countries[i].uniqueActiveUsers;
        }

//                scope.totalBomPrice = ((scope.countries[0].bomPrice) * oneRealToEuroMultiplier) + ((scope.countries[1].bomPrice) * oneRubleToEuroMultiplier) +
//                        (scope.countries[2].bomPrice) + ((scope.countries[2].bomPrice) * oneRSARandToEuroMultiplier);

        //----------------- SET Default Value ---------------------

        scope.launchDate = scope.countries[0].launchDate;
//                scope.meaLaunchDays = scope.countries[0].meaLaunchDays;

        setTableDataAndLink('Active Users', scope.countries[0].uniqueActiveUsers);
      }


      /**
       *
       * @param {type} filedName
       * @param {type} value
       * @returns {undefined}
       */
      function setTableDataAndLink(filedName, value) {
        if (filedName === "Active Users") {
          if (value === 0) {
            scope.disableUniqueActiveUsers = true;
            angular.element("#uniqueActiveUsers").removeClass("link");
          } else {
            scope.disableUniqueActiveUsers = false;
            angular.element("#uniqueActiveUsers").addClass("link");
          }
          scope.uniqueActiveUsers = value;
        }
//				console.log('>>>> filedName: ', filedName, ' -- setTableDataAndLink :: scope.uniqueActiveUsers: ', scope.uniqueActiveUsers);
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          if (chartData[keyName]) {
            scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          } else {
            scope.countries[i].uniqueActiveUsersChart = 0;
          }
        }
        console.log("Chart Data.. Prepare...");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onMEAChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.meaChartTypeSelected = scope.chartOptions[2];
        scope.meaChartName = chartName;
        onMEAChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onMEAChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }
        scope.isfootNoteSymbolVisible = false;
        scope.selectedChartTab = selTab;
        scope.meaChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.meaChartToolTip = scope.chartToolTips[scope.selectedChartTab];
        var chartType = scope.meaChartTypeSelected;
        var chartName = scope.meaChartName;
        var countryPrefix = selectedCountryName + " : ";
        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "UniqueActiveUsers":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.meaChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.meaChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }
    }//END Link..
  }
})();