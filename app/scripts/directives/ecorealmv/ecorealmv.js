/**
 * @ngdoc directive
 * @name Ecorealmv directive
 *
 * @description
 *  Ecorealmv....
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
    .directive('ecorealmv', ecorealmv);

  function ecorealmv($rootScope, $timeout, appConstants, commonUtils, logger,
                     gaChartUtil, nodeGAService, EcorealmvCountryVO, currencyConverterService, paceService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/ecorealmv/ecorealmv.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var _ecorealmvReportData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var timer = null;

      //------------------------------------------------------------------
      //--------------------------- Global Properties --------------------
      //------------------------------------------------------------------

      scope.loadDefaultChart = false;
      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_REGISTERED_USERS = "No. of registered users";
      var DEFAULT_CHART_NAME = "RegisteredUsers";

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      scope.selectedChartTab = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.ecorealmvChartTypeSelected = scope.chartOptions[scope.selectedChartTab].toLowerCase();
      scope.ecorealmvChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.ecorealmvChartName = DEFAULT_CHART_NAME;
      scope.ecorealmvChartLabel = NO_OF_REGISTERED_USERS;//NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.disableUniqueActiveUsers = false;
      scope.onEcorealMVChartOptionChange = onEcorealMVChartOptionChange;

      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        // Get Chart Data
        getEcorealmvChartData();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);
        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }

      /**
       * Get Ecorealmv CHART data from NODE
       *
       * @returns {undefined}
       */
      function getEcorealmvChartData() {
        initChartData();
        //Start Timer
        timer = $timeout(function () {
          console.log("ecorealmv.js :: loadDefaultChart :: Timer :: Start ");
          var chartTab = scope.selectedChartTab || 2;
          scope.ecorealmvChartTypeSelected = scope.chartOptions[chartTab].toLowerCase();
          scope.ecorealmvChartName = chartName;
          scope.loadDefaultChart = true;
          alert('~~~~ getEcorealmvChartData ~~~~');
          onEcorealMVChartOptionChange(null, chartTab);
        }, CHART_LOAD_DELAY);
        //Destroy Timer
        scope.$on("$destroy",
          function (event) {
            console.log("ecorealmv.js :: loadDefaultChart :: Timer :: Stop ");
            $timeout.cancel(timer);
          }
        );
      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onEcorealMVChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }
        scope.isfootNoteSymbolVisible = false;
        scope.selectedChartTab = selTab;
        var chartType = scope.chartOptions[scope.selectedChartTab].toLowerCase();
        scope.ecorealmvChartToolTip = scope.chartToolTips[scope.selectedChartTab];
        var chartName = scope.ecorealmvChartName;

        if (isInitChartData === false) {
          initChartData();
        }

        scope.ecorealmvChartLabel = "Total - Worldwide : " + NO_OF_REGISTERED_USERS;
        //Add New Chart
        chartObj.data = scope.paceDataRegUserOverPeriod.ecorealMvUsersReg[chartType];

        //Set value to Scope
        scope.chartSource = chartObj;
      }

      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.ecorealmvChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }
    }//END Link..
  }
})();
