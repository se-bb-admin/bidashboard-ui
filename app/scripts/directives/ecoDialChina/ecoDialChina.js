/**
 * @ngdoc directive
 * @name EcoDialChina directive
 *
 * @description
 *  EcoDialChina directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('ecoDialChina', ecoDialChina);

  function ecoDialChina($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, EcoDialChinaCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/ecoDialChina/ecoDialChina.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Country1'];

      //var _EcoDialChinaReportData = null;
      //var _EcoDialChinaChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      //var selectedCountryGaId = appConstants.SITE_GA_IDS.ECO_DIAL_CHINA_ID;
      //scope.uniqueActiveUsers = 0;
      //scope.ecoDialChinaLaunchDays = 0;
      //scope.ecoDialChinaLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, appConstants.DATES.ECO_DIAL_CHINA_LAUNCHED_DATE);
      scope.launchDate = appConstants.DATES.ECO_DIAL_CHINA_LAUNCHED_DATE;

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------

      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.ecoDialChinaTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.ecoDialChinaChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.ecoDialChinaChartName = "ChartRegisteredUsers";
      scope.ecoDialChinaChartLabel = NO_OF_REG_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = false;
      scope.loadDefaultChart = true;

      scope.onEcoDialChinaChartOptionChange = onEcoDialChinaChartOptionChange;
      scope.onEcoDialChinaChartLinkChange = onEcoDialChinaChartLinkChange;


      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
//                            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        // Init Countries data
        initCountries();
        // Get Report Data
        //getEcoDialChinaReportData();
        // Get Chart Data
        //getEcoDialChinaChartData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.ecoDialChinaUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initChartData();
            //Load Default Chart
            loadDefaultChart();
          }
        });
      }

      /**
       * Get EcoDialChina report data from NODE
       *
       * @returns {undefined}

       function getEcoDialChinaReportData() {
       $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
       nodeGAService.getEcoDialChinaReportData().then(
       function (result) {
       logger.log("EcoDialChina::getEcoDialChinaReportData::Result::Success");
       _EcoDialChinaReportData = result;
       parseReportData(_EcoDialChinaReportData);
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
       },
       function (error) {
       _EcoDialChinaReportData = null;
       logger.log("EcoDialChina::getEcoDialChinaReportData::Result::FAIL");
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
       }
       );
       }
       */

      /**
       * Get EcoDialChina CHART data from NODE
       *
       * @returns {undefined}

       function getEcoDialChinaChartData() {

       $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

       nodeGAService.getEcoDialChinaChartData().then(
       function (result) {
       logger.log("EcoDialChina::getEcoDialChinaChartData::Result::Success");
       _EcoDialChinaChartData = result;
       parseChartData(_EcoDialChinaChartData);
       //Load Default Chart
       loadDefaultChart();
       //Hide Loader
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
       },
       function (error) {
       _EcoDialChinaChartData = null;
       logger.log("EcoDialChina::getEcoDialChinaChartData::Result::FAIL");
       //Hide Loader
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
       }
       );

       }
       */


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER
        onEcoDialChinaChartLinkChange('ChartRegisteredUsers');
        scope.loadDefaultChart = false;
        //Start Timer
       /* timer = $timeout(function () {
          console.log("ecoDialChina.js :: loadDefaultChart :: Timer :: Start ");
          onEcoDialChinaChartLinkChange('ChartRegisteredUsers');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);*/

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoDialChina.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );


      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var ecoDialChina = {};
        //--- EcoDialChina
        for (var i = 0; i < countryNameList.length; i++) {
          ecoDialChina.countryName = countryNameList[i];
          ecoDialChina.countryGaId = appConstants.SITE_GA_IDS.ECO_DIAL_CHINA_ID;
          ecoDialChina.launchDate = appConstants.DATES.ECO_DIAL_CHINA_LAUNCHED_DATE;
          ecoDialChina.launchDateStr = appConstants.DATES.ECO_DIAL_CHINA_LAUNCHED_DATE_STR;
          ecoDialChina.regUsers = 0;
          ecoDialChina.uniqueActiveUsers = 0;
          ecoDialChina.projectsCreated = 0;
          ecoDialChina.uniqueActiveUsersChart = 0;
          ecoDialChina.projectsCreatedChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new EcoDialChinaCountryVO(ecoDialChina));
        }
      }

      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload EcoDialChina Chart.." + scope.selectedChartTab);
        onEcoDialChinaChartOptionChange(null, scope.selectedChartTab);

      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}

       function parseReportData(reportData) {
       scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
       scope.countries[0].projectsCreated = parseFloat(reportData.projectList.totalsForAllResults['ga:totalEvents']);
       //----------------- SET Default Value ---------------------
       scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
       scope.projectsCreated = scope.countries[0].projectsCreated;
       } */




      /**
       *
       * @param {type} chartData
       * @returns {undefined}

       function parseChartData(chartData) {
       scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
       scope.countries[0].projectsCreatedChart = gaChartUtil.populateGAChartData(chartData.projectList, "dateFormatted", "projectList", "Date", "Projects Created");

       console.log("Chart Data.. Prepare...");
       }*/




      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onEcoDialChinaChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.ecoDialChinaChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.ecoDialChinaChartName = chartName;
        onEcoDialChinaChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onEcoDialChinaChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.ecoDialChinaChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.ecoDialChinaChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.ecoDialChinaChartTypeSelected;
        var chartName = scope.ecoDialChinaChartName;

        if (isInitChartData === false) {
          initChartData();
        }
        console.log("===> onEcoDialChinaChartOptionChange: selTab :: ", selTab);
        console.dir(scope.paceDataRegUserOverPeriod);
        switch (chartName) {
          case "ChartRegisteredUsers":
          {
            scope.ecoDialChinaChartLabel = NO_OF_REG_USERS;

            if (chartType === 'Day') {
              chartObj.data = scope.paceDataRegUserOverPeriod.ecoDialChinaUsersReg.day;
            } else if (chartType === 'Week') {
              chartObj.data = scope.paceDataRegUserOverPeriod.ecoDialChinaUsersReg.week;
            } else if (chartType === 'Month') {
              chartObj.data = scope.paceDataRegUserOverPeriod.ecoDialChinaUsersReg.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




    }//END Link..

  }
  ;
})();
