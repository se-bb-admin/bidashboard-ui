/**
 * @ngdoc directive
 * @name CBT Report directive
 *
 * @description
 *  CBT Report directive displays all list of information based on counties selection.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding')
          .directive('cbt', cbt);

  function cbt($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, CBTCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/cbt/cbt.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Global Website', 'Australia', 'Belgium', 'Brazil', 'Denmark',
        'France', 'Germany', 'India', 'Netherlands', 'Poland',
        'Russia', 'Saudi Arabia', 'Slovakia', 'Spain', 'Switzerland',
        'Turkey', 'Vietnam'];

      var _cbtReportData = null;
      var _cbtChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------------------
      //---------------------------- Global Properties -------------------
      //------------------------------------------------------------------
      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];

      scope.cbtUsersUnique = 0;
      scope.cbtNoOfReports = 0;
      scope.cbtNoOfChecksCal = 0;
      scope.cbtAveCalPerSess = 0;
      scope.cbtLaunchedDate = appConstants.DATES.CBT_GLOBAL_WEBSITE_LAUNCHED_DATE;

      scope.cbtTotalUserUnique = 0;
      scope.cbtTotalNoOfReports = 0;
      scope.cbtTotalNoOfChecksCal = 0;


      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      //------------------------------------------------------------------
      //--------------- CBT CHART INIT SHOW HIDE PROPERTIES --------------
      //------------------------------------------------------------------

      scope.isGAChartVisible = true;
      scope.isPaceChartVisible = false;

      var NO_OF_ACTIVE_USERS = 'No. of unique active users';
      var NO_OF_PROJECTS_CREATED = 'No. of projects created';
      var NO_OF_CHECKS_CAL = 'No. of checks, cal, serch discrim';
      var AVG_CALC_PER_SESSION = 'AVG. calculations per session per user';
      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";

      var DEFAULT_CHART_NAME = "ChartPaceChart";


      scope.onCountryDropDownChange = onCountryDropDownChange;

      scope.paceChartStyle = commonUtils.getPaceChartStyle();
      scope.paceChartSource = {};
      var isInitPaceChartData = false;
      var paceChartObj = {};

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};


      //------------------------------------------------------------------
      //-------------------  For Chart TYPE Options  ---------------------
      //------------------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX; // 2
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES; // ['Day', 'Week', 'Month']
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS; // ['Data for last 30 days', 'Data for last 6 month', 'Data for last one year']
      scope.selectedChartTab = chartOptionDfltSelInx; //2
      scope.cbtChartTypeSelected = scope.chartOptions[scope.selectedChartTab]; // Day / Week / Month
      scope.cbtChartToolTip = scope.chartToolTips[scope.selectedChartTab]; // Tooltip for Day / Week / Month
      scope.cbtChartName = DEFAULT_CHART_NAME; // ChartActiveUser, ChartNoOfProjCreated, ChartNoOfChecksCal etc..
      scope.cbtChartLabel = selectedCountryName + " : " + NO_OF_ACTIVE_USERS;

      scope.onCbtChartLinkChange = onCbtChartLinkChange;
      scope.onCbtChartOptionChange = onCbtChartOptionChange;
      scope.showCountryDialog = showCountryDialog;

      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;

      scope.paceChartStyle = commonUtils.getPaceChartStyle;
      scope.chartReady = chartReady;

      init();



      /**
       * Function to initialized the repors
       *
       * @returns {undefined}
       */
      function init() {
        // Init Countries data
        initCountries();
        // Get Report Data
        getCbtReportData();
        // Get Chart Data
        getCbtChartData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.cbtUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initPaceChartData();
          }
        });
      }

      /**
       * Get CBT report data from NODE
       *
       * @returns {undefined}
       */
      function getCbtReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getCbtReportData().then(
                function (result) {
                  logger.log("CBT::getCbtReportData::Result::Success");
                  _cbtReportData = result;
                  parseReportData(_cbtReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _cbtReportData = null;
                  logger.log("CBT::getCbtReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get CBT CHART data from NODE
       *
       * @returns {undefined}
       */
      function getCbtChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getCbtChartData().then(
                function (result) {
                  logger.log("CBT::getCbtChartData::Result::Success");
                  _cbtChartData = result;
                  parseChartData(_cbtChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _cbtChartData = null;
                  logger.log("CBT::getCbtChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          console.log("cbt.js :: loadDefaultChart :: Timer :: Start ");
          onCbtChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("cbt.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initPaceChartData() {
        paceChartObj = {};
        paceChartObj.type = "AreaChart";
        paceChartObj.displayed = false;
        paceChartObj.options = commonUtils.getPaceChartOptions();

        isInitPaceChartData = true;
      }


      /***
       *  Onclick Registererd users link click..
       *  Show PACE Chart Hide GA Chart
       *
       * @returns {undefined}
       *
       function showPaceChart() {
       scope.isGAChartVisible = false;
       scope.isPaceChartVisible = true;

       //Show Hide Chart
       onCbtChartOptionChange(null, chartOptionDfltSelInx);
       }
       */
      /**
       * Load PACE Chart as per selected tab (Day | Week | Month)
       *
       * @param {type} selTab
       * @returns {undefined}
       */
      function loadPaceChart(selTab) {

        if (isInitPaceChartData === false) {
          initPaceChartData();
        }

        var chartTypeSelected = scope.chartOptions[scope.selectedChartTab];

        if (chartTypeSelected.toLowerCase() === 'day') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.cbtUsersReg.day;
        }
        if (chartTypeSelected.toLowerCase() === 'week') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.cbtUsersReg.week;
        }
        if (chartTypeSelected.toLowerCase() === 'month') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.cbtUsersReg.month;
        }

        //Set value to Scope
        scope.paceChartSource = paceChartObj;
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }

      /**
       *
       * @returns {undefined}
       */
      function initCountries() {
        var country = {};
        //--
        for (var i = 0; i < countryNameList.length; i++) {
          var capsCountryName = constructName(countryNameList[i].toUpperCase());
          var gaId = 'CBT_' + capsCountryName + '_ID';
          var launchDate = 'CBT_' + capsCountryName + '_LAUNCHED_DATE';
          var launchDateStr = 'CBT_' + capsCountryName + '_LAUNCHED_DATE_STR';
          country.countryName = countryNameList[i];
          country.countryGaId = appConstants.SITE_GA_IDS[gaId];
          country.launchDate = appConstants.DATES[launchDate];
          country.launchDateStr = appConstants.DATES[launchDateStr];
          country.regUsers = 'NA';
          country.uniqueActiveUsers = 0;
          country.noOfProjCreated = 0;
          country.noOfChecksCal = 0;
          country.aveCalPerSess = 0;
          country.uniqueActiveUsersChart = 0;
          country.noOfProjCreatedChart = 0;
          country.noOfChecksCalChart = 0;
          country.aveCalPerSessChart = 0;
          scope.countries.push(new CBTCountryVO(country));
        }
        //Set default Country to update data binding
        setDefaultCountry();
      }

      function constructName(name) {
        var result = name.split(' ').join('_');
        return result;
      }

      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }

      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        //Set the values
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;

//                logger.log(selectedCountryName);

        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.cbtUsersUnique = scope.countries[index].uniqueActiveUsers;
        scope.cbtNoOfReports = scope.countries[index].noOfProjCreated;
        scope.cbtNoOfChecksCal = scope.countries[index].noOfChecksCal;
        scope.cbtAveCalPerSess = scope.countries[index].aveCalPerSess;
        scope.cbtLaunchedDate = scope.countries[index].launchDate;

        //Load Default Chart
        onCbtChartLinkChange(DEFAULT_CHART_NAME);

      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
//                logger.log("Reload CBT Chart.." + scope.selectedChartTab);
        //->
        onCbtChartOptionChange(null, scope.selectedChartTab);
      }

      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          scope.countries[i].noOfProjCreated = parseFloat(reportData[keyName].noOfProjCreated.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].noOfChecksCal = parseFloat(reportData[keyName].noOfChecksCal.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].aveCalPerSess = parseFloat(reportData[keyName].aveCalPerSess.totalsForAllResults['ga:eventsPerSessionWithEvent']);

          scope.cbtTotalUserUnique = scope.cbtTotalUserUnique + scope.countries[i].uniqueActiveUsers;
          scope.cbtTotalNoOfReports = scope.cbtTotalNoOfReports + scope.countries[i].noOfProjCreated;
          scope.cbtTotalNoOfChecksCal = scope.cbtTotalNoOfChecksCal + scope.countries[i].noOfChecksCal;
        }

        //----------------- SET Default Value ---------------------
        scope.cbtUsersUnique = scope.countries[0].uniqueActiveUsers;
        scope.cbtNoOfReports = scope.countries[0].noOfProjCreated;
        scope.cbtNoOfChecksCal = scope.countries[0].noOfChecksCal;
        scope.cbtAveCalPerSess = scope.countries[0].aveCalPerSess;
      }

      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          scope.countries[i].noOfProjCreatedChart = gaChartUtil.populateGAChartData(chartData[keyName].noOfProjCreated, "dateFormatted", "noOfProjCreated", "Date", "Projects Created");
          scope.countries[i].noOfChecksCalChart = gaChartUtil.populateGAChartData(chartData[keyName].noOfChecksCal, "dateFormatted", "noOfChecksCal", "Date", "No. Of Checks, Cal, Serch");
          scope.countries[i].aveCalPerSessChart = gaChartUtil.populateGAChartData(chartData[keyName].aveCalPerSess, "dateFormatted", "aveCalPerSess", "Date", "AVG. Calculations Per Session");
        }
        console.log("Chart Data.. Prepare...");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onCbtChartLinkChange(chartName) {

        if (chartName === 'ChartPaceChart') {
          scope.isGAChartVisible = false;
          scope.isPaceChartVisible = true;
        } else {
          scope.isGAChartVisible = true;
          scope.isPaceChartVisible = false;
        }

        //When you click on a link Chart will reset to Month Chart.
        scope.cbtChartTypeSelected = scope.chartOptions[2]; // Day / Week / Month
        //Set Chart Name ..ChartActiveUser, ChartNoOfProjCreated, ChartNoOfChecksCal etc..
        scope.cbtChartName = chartName;

        onCbtChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onCbtChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }
        //Set Country Name & gaId
        scope.isfootNoteSymbolVisible = false;
        selectedCountryName = scope.selectedCountry.countryName;

        //Set the selected Tab val again..
        scope.selectedChartTab = selTab;
        scope.cbtChartTypeSelected = scope.chartOptions[scope.selectedChartTab]; // Day / Week / Month
        scope.cbtChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.cbtChartTypeSelected;
        var chartName = scope.cbtChartName;
        var countryPrefix = selectedCountryName + " : ";

        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }


        //check for PACE Chart
        if (scope.isPaceChartVisible) {
          scope.cbtChartLabel = NO_OF_REG_USERS;

          loadPaceChart(scope.selectedChartTab);
          return;
        }

        if (isInitChartData === false) {
          initChartData();
        }

        // ADD Chart
        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.cbtChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartNoOfProjCreated":
          {
            scope.cbtChartLabel = countryPrefix + NO_OF_PROJECTS_CREATED;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].noOfProjCreatedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].noOfProjCreatedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].noOfProjCreatedChart.month;
            }
            break;
          }
          case "ChartNoOfChecksCal":
          {
            scope.cbtChartLabel = countryPrefix + NO_OF_CHECKS_CAL;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].noOfChecksCalChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].noOfChecksCalChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].noOfChecksCalChart.month;
            }
            break;
          }
          case "ChartAveCalPerSess":
          {
            scope.cbtChartLabel = countryPrefix + AVG_CALC_PER_SESSION;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].aveCalPerSessChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].aveCalPerSessChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].aveCalPerSessChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }

      function showCountryDialog() {
        var countryData = {};
        //var totalRegisteredUser = scope.paceData.cbtUsersReg;
        countryData.containerWidth = 80;
        countryData.tableHeader = "CBT";
        countryData.note1 = "* Individual data is not available for countries.";
        countryData.gridOptions = {
          enableVerticalScrollbar: 0,
          enableHorizontalScrollbar: 2
        };
        countryData.gridOptions.rowHeight = 30;
        countryData.gridOptions.columnDefs = [
          {field: "country_0", name: "Global Website", width: 110, enableColumnMenu: false, enablePinning: false, enableSorting: false},
          {field: "label", name: "Country", width: 250, enableColumnMenu: false, pinnedLeft: true, enableSorting: false},
          {field: "total", name: "Total", width: 110, enableColumnMenu: false, pinnedRight: true, enableSorting: false},
          {field: "country_1", name: "Australia", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_2", name: "Belgium", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_3", name: "Brazil", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_4", name: "Denmark", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_5", name: "France", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_6", name: "Germany", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_7", name: "India", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_8", name: "Netherlands", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_9", name: "Poland", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_10", name: "Russia", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_11", name: "Saudi Arabia", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_12", name: "Slovakia", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_13", name: "Spain", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_14", name: "Switzerland", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_15", name: "Turkey", width: 110, enableColumnMenu: false, enableSorting: false},
          {field: "country_16", name: "Vietnam", width: 110, enableColumnMenu: false, enableSorting: false}
        ];


        countryData.gridOptions.data = [
          {
            "label": "Launch Date",
            "country_0": commonUtils.getLaunchDateFormat(scope.countries[0].launchDate),
            "country_1": commonUtils.getLaunchDateFormat(scope.countries[1].launchDate),
            "country_2": commonUtils.getLaunchDateFormat(scope.countries[2].launchDate),
            "country_3": commonUtils.getLaunchDateFormat(scope.countries[3].launchDate),
            "country_4": commonUtils.getLaunchDateFormat(scope.countries[4].launchDate),
            "country_5": commonUtils.getLaunchDateFormat(scope.countries[5].launchDate),
            "country_6": commonUtils.getLaunchDateFormat(scope.countries[6].launchDate),
            "country_7": commonUtils.getLaunchDateFormat(scope.countries[7].launchDate),
            "country_8": commonUtils.getLaunchDateFormat(scope.countries[8].launchDate),
            "country_9": commonUtils.getLaunchDateFormat(scope.countries[9].launchDate),
            "country_10": commonUtils.getLaunchDateFormat(scope.countries[10].launchDate),
            "country_11": commonUtils.getLaunchDateFormat(scope.countries[11].launchDate),
            "country_12": commonUtils.getLaunchDateFormat(scope.countries[12].launchDate),
            "country_13": commonUtils.getLaunchDateFormat(scope.countries[13].launchDate),
            "country_14": commonUtils.getLaunchDateFormat(scope.countries[14].launchDate),
            "country_15": commonUtils.getLaunchDateFormat(scope.countries[15].launchDate),
            "country_16": commonUtils.getLaunchDateFormat(scope.countries[16].launchDate),
            "total": ""
          }, {
            "label": "No. of registered users",
            "country_0": scope.countries[0].regUsers,
            "country_1": scope.countries[1].regUsers,
            "country_2": scope.countries[2].regUsers,
            "country_3": scope.countries[3].regUsers,
            "country_4": scope.countries[4].regUsers,
            "country_5": scope.countries[5].regUsers,
            "country_6": scope.countries[6].regUsers,
            "country_7": scope.countries[7].regUsers,
            "country_8": scope.countries[8].regUsers,
            "country_9": scope.countries[9].regUsers,
            "country_10": scope.countries[10].regUsers,
            "country_11": scope.countries[11].regUsers,
            "country_12": scope.countries[12].regUsers,
            "country_13": scope.countries[13].regUsers,
            "country_14": scope.countries[14].regUsers,
            "country_15": scope.countries[15].regUsers,
            "country_16": scope.countries[16].regUsers,
            "total": commonUtils.getNumberFormat(scope.paceDataRegUser.cbtUsersReg, 0) + "*"
          }, {
            "label": "No. of unique active users for last 12 months",
            "country_0": commonUtils.getNumberFormat(scope.countries[0].uniqueActiveUsers, 0),
            "country_1": commonUtils.getNumberFormat(scope.countries[1].uniqueActiveUsers, 0),
            "country_2": commonUtils.getNumberFormat(scope.countries[2].uniqueActiveUsers, 0),
            "country_3": commonUtils.getNumberFormat(scope.countries[3].uniqueActiveUsers, 0),
            "country_4": commonUtils.getNumberFormat(scope.countries[4].uniqueActiveUsers, 0),
            "country_5": commonUtils.getNumberFormat(scope.countries[5].uniqueActiveUsers, 0),
            "country_6": commonUtils.getNumberFormat(scope.countries[6].uniqueActiveUsers, 0),
            "country_7": commonUtils.getNumberFormat(scope.countries[7].uniqueActiveUsers, 0),
            "country_8": commonUtils.getNumberFormat(scope.countries[8].uniqueActiveUsers, 0),
            "country_9": commonUtils.getNumberFormat(scope.countries[9].uniqueActiveUsers, 0),
            "country_10": commonUtils.getNumberFormat(scope.countries[10].uniqueActiveUsers, 0),
            "country_11": commonUtils.getNumberFormat(scope.countries[11].uniqueActiveUsers, 0),
            "country_12": commonUtils.getNumberFormat(scope.countries[12].uniqueActiveUsers, 0),
            "country_13": commonUtils.getNumberFormat(scope.countries[13].uniqueActiveUsers, 0),
            "country_14": commonUtils.getNumberFormat(scope.countries[14].uniqueActiveUsers, 0),
            "country_15": commonUtils.getNumberFormat(scope.countries[15].uniqueActiveUsers, 0), "country_16": commonUtils.getNumberFormat(scope.countries[16].uniqueActiveUsers, 0),
            "total": commonUtils.getNumberFormat(scope.cbtTotalUserUnique, 0)
          }, {
            "label": "No. of projects created",
            "country_0": commonUtils.getNumberFormat(scope.countries[0].noOfProjCreated, 0),
            "country_1": commonUtils.getNumberFormat(scope.countries[1].noOfProjCreated, 0),
            "country_2": commonUtils.getNumberFormat(scope.countries[2].noOfProjCreated, 0),
            "country_3": commonUtils.getNumberFormat(scope.countries[3].noOfProjCreated, 0),
            "country_4": commonUtils.getNumberFormat(scope.countries[4].noOfProjCreated, 0),
            "country_5": commonUtils.getNumberFormat(scope.countries[5].noOfProjCreated, 0),
            "country_6": commonUtils.getNumberFormat(scope.countries[6].noOfProjCreated, 0),
            "country_7": commonUtils.getNumberFormat(scope.countries[7].noOfProjCreated, 0),
            "country_8": commonUtils.getNumberFormat(scope.countries[8].noOfProjCreated, 0),
            "country_9": commonUtils.getNumberFormat(scope.countries[9].noOfProjCreated, 0),
            "country_10": commonUtils.getNumberFormat(scope.countries[10].noOfProjCreated, 0),
            "country_11": commonUtils.getNumberFormat(scope.countries[11].noOfProjCreated, 0),
            "country_12": commonUtils.getNumberFormat(scope.countries[12].noOfProjCreated, 0),
            "country_13": commonUtils.getNumberFormat(scope.countries[13].noOfProjCreated, 0),
            "country_14": commonUtils.getNumberFormat(scope.countries[14].noOfProjCreated, 0),
            "country_15": commonUtils.getNumberFormat(scope.countries[15].noOfProjCreated, 0),
            "country_16": commonUtils.getNumberFormat(scope.countries[16].noOfProjCreated, 0),
            "total": commonUtils.getNumberFormat(scope.cbtTotalNoOfReports, 0)
          }, {
            "label": "No. of checks, cal, serch discrim.",
            "country_0": commonUtils.getNumberFormat(scope.countries[0].noOfChecksCal, 0),
            "country_1": commonUtils.getNumberFormat(scope.countries[1].noOfChecksCal, 0),
            "country_2": commonUtils.getNumberFormat(scope.countries[2].noOfChecksCal, 0),
            "country_3": commonUtils.getNumberFormat(scope.countries[3].noOfChecksCal, 0),
            "country_4": commonUtils.getNumberFormat(scope.countries[4].noOfChecksCal, 0),
            "country_5": commonUtils.getNumberFormat(scope.countries[5].noOfChecksCal, 0),
            "country_6": commonUtils.getNumberFormat(scope.countries[6].noOfChecksCal, 0),
            "country_7": commonUtils.getNumberFormat(scope.countries[7].noOfChecksCal, 0),
            "country_8": commonUtils.getNumberFormat(scope.countries[8].noOfChecksCal, 0),
            "country_9": commonUtils.getNumberFormat(scope.countries[9].noOfChecksCal, 0),
            "country_10": commonUtils.getNumberFormat(scope.countries[10].noOfChecksCal, 0),
            "country_11": commonUtils.getNumberFormat(scope.countries[11].noOfChecksCal, 0),
            "country_12": commonUtils.getNumberFormat(scope.countries[12].noOfChecksCal, 0),
            "country_13": commonUtils.getNumberFormat(scope.countries[13].noOfChecksCal, 0),
            "country_14": commonUtils.getNumberFormat(scope.countries[14].noOfChecksCal, 0),
            "country_15": commonUtils.getNumberFormat(scope.countries[15].noOfChecksCal, 0),
            "country_16": commonUtils.getNumberFormat(scope.countries[16].noOfChecksCal, 0),
            "total": commonUtils.getNumberFormat(scope.cbtTotalNoOfChecksCal, 0)
          }, {
            "label": "Ave. calculations per session per user",
            "country_0": commonUtils.getNumberFormat(scope.countries[0].aveCalPerSess),
            "country_1": commonUtils.getNumberFormat(scope.countries[1].aveCalPerSess),
            "country_2": commonUtils.getNumberFormat(scope.countries[2].aveCalPerSess),
            "country_3": commonUtils.getNumberFormat(scope.countries[3].aveCalPerSess),
            "country_4": commonUtils.getNumberFormat(scope.countries[4].aveCalPerSess),
            "country_5": commonUtils.getNumberFormat(scope.countries[5].aveCalPerSess),
            "country_6": commonUtils.getNumberFormat(scope.countries[6].aveCalPerSess),
            "country_7": commonUtils.getNumberFormat(scope.countries[7].aveCalPerSess),
            "country_8": commonUtils.getNumberFormat(scope.countries[8].aveCalPerSess),
            "country_9": commonUtils.getNumberFormat(scope.countries[9].aveCalPerSess),
            "country_10": commonUtils.getNumberFormat(scope.countries[10].aveCalPerSess),
            "country_11": commonUtils.getNumberFormat(scope.countries[11].aveCalPerSess),
            "country_12": commonUtils.getNumberFormat(scope.countries[12].aveCalPerSess),
            "country_13": commonUtils.getNumberFormat(scope.countries[13].aveCalPerSess),
            "country_14": commonUtils.getNumberFormat(scope.countries[14].aveCalPerSess),
            "country_15": commonUtils.getNumberFormat(scope.countries[15].aveCalPerSess),
            "country_16": commonUtils.getNumberFormat(scope.countries[16].aveCalPerSess),
            "total": "NA"
          }
        ];

        $rootScope.$broadcast(appConstants.EVENTS.COUNTRY_DIALOG_BOX, countryData);
      }



      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.cbtChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }



    }
  }


})();