/**
 * @ngdoc directive
 * @name MyPact directive
 *
 * @description
 *  MyPact directive displays all list of information based on counties selection.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').directive('myPact', myPact);

  function myPact($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, MyPactCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/myPact/myPact.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var myPactList = ['Basic', 'Integrated'];

      var _myPactReportData = null;
      var _myPactChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;


      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];

      var newList = [
        ["Belarus", "Russia", "Kazakhstan", "Argentina", "Belgium", "Bolivia", "Peru", "Brazil", "Chile", "Colombia", "Denmark", "Germany", "India", "Indonesia", "United Kingdom", "Ireland", "Japan", "Mexico", "Netherlands", "Philippines", "Portugal", "Romania", "Saudi Arabia", "Singapore", "South Africa", "South Korea", "Spain", "Sweden", "Switzerland", "Turkey", "Vietnam", "United Arab Emirates", "Pakistan", "Yemen", "Kuwait", "Bahrain", "Oman", "Qatar", "Malaysia", "Thailand", "Ireland", "Finland", "Norway"],
        ["Russia", "Kazakhstan", "Argentina", "India", "Romania"]
      ];

      scope.subCountries = null;
      scope.selectedSubCountry;
      scope.selectedCountry = {};
      var selectedCountryName = myPactList[0];
      var selectedSubCountry;

      scope.countriesDeployedCount = 0;

      scope.myPactActiveUsersUnique = 0;
      scope.myPactNoSelectionLaunched = 0;
      scope.myPactNoSelectionSaved = 0;

      scope.myPactTotalActiveUsersUnique = 0;
      scope.myPactTotalNoSelectionLaunched = 0;
      scope.myPactTotalNoSelectionSaved = 0;

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------

      scope.isGAChartVisible = false;
      scope.isPaceChartVisible = false;

      var NO_OF_ACTIVE_USERS = 'No. of unique active users';
      var SELECTION_LAUNCHED = 'No. of Selection Launched';
      var SELECTION_SAVED = 'No. of Selection Saved';
      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";
      var DEFAULT_CHART_NAME = "ChartPaceChart";


      scope.onCountryDropDownChange = onCountryDropDownChange;
      scope.onSubCountryDropDownChange = onSubCountryDropDownChange;

      scope.paceChartStyle = commonUtils.getPaceChartStyle();
      scope.paceChartSource = {};
      var isInitPaceChartData = false;
      var paceChartObj = {};


      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.myPactChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.myPactChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.myPactChartName = DEFAULT_CHART_NAME;
      scope.myPactChartLabel = NO_OF_REG_USERS;//selectedCountryName + " : " + NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.zoomLabel = appConstants.NOTE.ZOOM_LABEL;
      scope.disableActiveUser = false;
      scope.disableSelectionLauched = false;
      scope.disableSelectionSave = false;
      scope.loadDefaultChart = false;
      scope.paceDataReady = false;


      scope.onMyPactChartLinkChange = onMyPactChartLinkChange;
      scope.onMyPactChartOptionChange = onMyPactChartOptionChange;
      scope.showCountryDialog = showCountryDialog;


      scope.paceChartStyle = commonUtils.getPaceChartStyle;
      scope.chartReady = chartReady;

      //
      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        // Init Countries data
        initCountries();
        // Get Report Data
        getMyPactReportData();
        // Get Chart Data
        getMyPactChartData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);


        //Init Chart Data When Data is Ready..
        /*scope.$watch('paceDataRegUserOverPeriod.myPactBasicUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initPaceChartData();
          }
        });*/
      }

      /**
       * Get My Pact report data from NODE
       *
       * @returns {undefined}
       */
      function getMyPactReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getMyPactReportData().then(
                function (result) {
                  logger.log("My Pact::getMyPactReportData::Result::Success");
                  _myPactReportData = result;

                  parseReportData(_myPactReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _myPactReportData = null;
                  logger.log("My Pact::getMyPactReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get My Pact CHART data from NODE
       *
       * @returns {undefined}
       */
      function getMyPactChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getMyPactChartData().then(
                function (result) {
                  logger.log("My Pact::getMyPactChartData::Result::Success");
                  _myPactChartData = result;
                  parseChartData(_myPactChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _myPactChartData = null;
                  logger.log("My Pact::getMyPactChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        onMyPactChartLinkChange(DEFAULT_CHART_NAME);
        scope.loadDefaultChart = true;
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("myPact.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initPaceChartData() {
        paceChartObj = {};
        paceChartObj.type = "AreaChart";
        paceChartObj.displayed = false;
        paceChartObj.options = commonUtils.getPaceChartOptions();

        isInitPaceChartData = true;
      }


      /**
       * Load PACE Chart as per selected tab (Day | Week | Month)
       *
       * @param {type} selTab
       * @returns {undefined}
       */
      function loadPaceChart(selTab) {

        //TODO :: Also Need To Check "scope.paceDataRegUserOverPeriod.myPactBasicUsersReg" val exist or not..
        if (isInitPaceChartData === false) {
          initPaceChartData();
        }

        var chartTypeSelected = scope.chartOptions[scope.selectedChartTab];

        if (chartTypeSelected.toLowerCase() === 'day') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.myPactBasicUsersReg.day;
        }
        if (chartTypeSelected.toLowerCase() === 'month') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.myPactBasicUsersReg.month;
        }

        if (chartTypeSelected.toLowerCase() === 'week') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.myPactBasicUsersReg.week;
        }

        //Set value to Scope
        scope.paceChartSource = paceChartObj;
        scope.myPactChartLabel = NO_OF_REG_USERS;
        scope.isPaceChartVisible = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       *
       * @returns {undefined}
       */
      function initCountries() {
        var myPact = {};
        //--
        for (var i = 0; i < myPactList.length; i++) {
          var capsName = myPactList[i].toUpperCase();
          myPact.countryName = myPactList[i];
          myPact.countryGaId = eval('appConstants.SITE_GA_IDS.MY_PACT_' + capsName + '_ID');
          myPact.launchDate = eval('appConstants.DATES.MY_PACT_' + capsName + '_LAUNCHED_DATE');
          myPact.launchDateStr = eval('appConstants.DATES.MY_PACT_' + capsName + '_LAUNCHED_DATE_STR');
          myPact.regUsers = 'NA';
          myPact.uniqueActiveUsers = 0;
          myPact.uniqueCountryWiseActvUsers = 0;
          myPact.countrySubName = 0;
          myPact.noSelectionLaunched = 0;
          myPact.noCountryWiseSelectionLaunched = 0;
          myPact.noSelectionSaved = 0;
          myPact.noCountryWiseSelectionSaved = 0;
          myPact.uniqueActiveUsersChart = {};
          myPact.noSelectionLaunchedChart = {};
          myPact.noSelectionSavedChart = {};

          scope.countries.push(new MyPactCountryVO(myPact));
        }
        //Set default Country to update data binding
        setDefaultCountry();
      }

      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }

      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;

        for (var i = 0; i < myPactList.length; i++) {
          if (myPactList[i] === selectedCountryName) {
            index = i;
          }
        }
//                scope.subCountries = scope.countries[index].countrySubName;
        scope.subCountries = getRefinedCountries(newList[index]);
        scope.countriesDeployedCount = scope.subCountries.length;
        scope.selectedSubCountry = scope.subCountries[0];
        selectedSubCountry = scope.selectedSubCountry.countryFilterName;

        setMyPactVlaue('Active User', getValue(scope.countries[index].uniqueCountryWiseActvUsers, selectedSubCountry));
        setMyPactVlaue('Selection Launched', getValue(scope.countries[index].noCountryWiseSelectionLaunched, selectedSubCountry));
        setMyPactVlaue('Selection Save', getValue(scope.countries[index].noCountryWiseSelectionSaved, selectedSubCountry));

        var key = myPactList[index];

        scope.countries[index].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(newChartData[key][selectedSubCountry].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        scope.countries[index].noSelectionLaunchedChart = gaChartUtil.populateGAChartData(newChartData[key][selectedSubCountry].noSelectionLaunched, "dateFormatted", "noSelectionLaunched", "Date", "Selection Launched");
        scope.countries[index].noSelectionSavedChart = gaChartUtil.populateGAChartData(newChartData[key][selectedSubCountry].noSelectionSaved, "dateFormatted", "noSelectionSaved", "Date", "Selection Saved");

        onMyPactChartLinkChange(DEFAULT_CHART_NAME);
      }

      function getRefinedCountries(newList) {
        //countryFilterName
        //countryName
        newList.sort();
        var arr = [];
        for (var i in newList) {
          var newCountry = newList[i];
          arr.push({countryFilterName: newCountry, countryName: newCountry});
        }

        return arr;
      }

      /**
       *
       * @returns {undefined}
       */
      function onSubCountryDropDownChange() {
        var countryIndex = 0;

        selectedCountryName = scope.selectedCountry.countryName;
        selectedSubCountry = scope.selectedSubCountry.countryFilterName;

        for (var i = 0; i < myPactList.length; i++) {
          if (myPactList[i] === selectedCountryName) {
            countryIndex = i;
          }
        }

        setMyPactVlaue('Active User', getValue(scope.countries[countryIndex].uniqueCountryWiseActvUsers, selectedSubCountry));
        setMyPactVlaue('Selection Launched', getValue(scope.countries[countryIndex].noCountryWiseSelectionLaunched, selectedSubCountry));
        setMyPactVlaue('Selection Save', getValue(scope.countries[countryIndex].noCountryWiseSelectionSaved, selectedSubCountry));

        var key = myPactList[countryIndex];

        scope.countries[countryIndex].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(newChartData[key][selectedSubCountry].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        scope.countries[countryIndex].noSelectionLaunchedChart = gaChartUtil.populateGAChartData(newChartData[key][selectedSubCountry].noSelectionLaunched, "dateFormatted", "noSelectionLaunched", "Date", "Selection Launched");
        scope.countries[countryIndex].noSelectionSavedChart = gaChartUtil.populateGAChartData(newChartData[key][selectedSubCountry].noSelectionSaved, "dateFormatted", "noSelectionSaved", "Date", "Selection Saved");

        onMyPactChartLinkChange(DEFAULT_CHART_NAME);
      }

      /**
       *
       * @param {type} dataArray
       * @param {type} country
       * @returns {Number}
       */
      function getValue(dataArray, country) {
        var dataValuePresent = getCountryDataPresentOrNot(dataArray, country);

        if (dataValuePresent === true) {
          for (var i = 0; i < dataArray.length; i++) {
            if (dataArray[i].countryName === country) {
              return dataArray[i].totalEvents;
            }
          }
        }
        return 0;
      }

      /**
       *
       * @param {type} dataArray
       * @param {type} country
       * @returns {Boolean}
       */
      function getCountryDataPresentOrNot(dataArray, country) {
        for (var i = 0; i < dataArray.length; i++) {
          if (dataArray[i].countryName === country) {
            return true;
          }
        }
        return false;
      }

      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload myPact Chart.." + scope.selectedChartTab);
        //->
        onMyPactChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.countries[0].uniqueActiveUsers = parseFloat(reportData.Basic.uniqueActiveUsers.totalsForAllResults['ga:users']);
        scope.countries[0].uniqueCountryWiseActvUsers = getCountryWisedata(reportData.Basic.uniqueActiveUsers.rows, 1);
        scope.countries[0].noSelectionLaunched = parseFloat(reportData.Basic.noSelectionLaunched.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].noCountryWiseSelectionLaunched = getCountryWisedata(reportData.Basic.noSelectionLaunched.rows, 2);
        scope.countries[0].noSelectionSaved = parseFloat(reportData.Basic.noSelectionSaved.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].noCountryWiseSelectionSaved = getCountryWisedata(reportData.Basic.noSelectionSaved.rows, 2);
        scope.countries[0].countrySubName = getRefinedCountries(newList[0]); //getSubCountry(reportData.Basic.countryDeployed.rows);

        scope.countries[1].uniqueActiveUsers = parseFloat(reportData.Integrated.uniqueActiveUsers.totalsForAllResults['ga:users']);
        scope.countries[1].uniqueCountryWiseActvUsers = getCountryWisedata(reportData.Integrated.uniqueActiveUsers.rows, 1);
        scope.countries[1].noSelectionLaunched = parseFloat(reportData.Integrated.noSelectionLaunched.totalsForAllResults['ga:totalEvents']);
        scope.countries[1].noCountryWiseSelectionLaunched = getCountryWisedata(reportData.Integrated.noSelectionLaunched.rows, 2);
        scope.countries[1].noSelectionSaved = parseFloat(reportData.Integrated.noSelectionSaved.totalsForAllResults['ga:totalEvents']);
        scope.countries[1].noCountryWiseSelectionSaved = getCountryWisedata(reportData.Integrated.noSelectionSaved.rows, 2);
        scope.countries[1].countrySubName = getRefinedCountries(newList[1]); //getSubCountry(reportData.Integrated.countryDeployed.rows);


        //----------------- SET Default Value ---------------------
        scope.subCountries = scope.countries[0].countrySubName;
        scope.selectedSubCountry = scope.subCountries[0];
        scope.countriesDeployedCount = scope.subCountries.length;
        selectedSubCountry = scope.selectedSubCountry.countryFilterName;

        setMyPactVlaue('Active User', getValue(scope.countries[0].uniqueCountryWiseActvUsers, selectedSubCountry));
        setMyPactVlaue('Selection Launched', getValue(scope.countries[0].noCountryWiseSelectionLaunched, selectedSubCountry));
        setMyPactVlaue('Selection Save', getValue(scope.countries[0].noCountryWiseSelectionSaved, selectedSubCountry));

        for (var i = 0; i < myPactList.length; i++) {
          scope.myPactTotalActiveUsersUnique = scope.myPactTotalActiveUsersUnique + scope.countries[i].uniqueActiveUsers;
          scope.myPactTotalNoSelectionLaunched = scope.myPactTotalNoSelectionLaunched + scope.countries[i].noSelectionLaunched;
          scope.myPactTotalNoSelectionSaved = scope.myPactTotalNoSelectionSaved + scope.countries[i].noSelectionSaved;
        }
      }

      var newChartData = {};
      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < myPactList.length; i++) {
          var countryNameArray = scope.countries[i].countrySubName;
          var key = myPactList[i];
          newChartData[key] = {};
          for (var l = 0; l < countryNameArray.length; l++) {
            newChartData[key][countryNameArray[l].countryFilterName] = {
              "uniqueActiveUsers": {
                "day": [],
                "week": [],
                "month": []
              },
              "noSelectionLaunched": {
                "day": [],
                "week": [],
                "month": []
              },
              "noSelectionSaved": {
                "day": [],
                "week": [],
                "month": []
              }
            };
          }
        }

        insertFormatedData("uniqueActiveUsers", "day", chartData);
        insertFormatedData("uniqueActiveUsers", "week", chartData);
        insertFormatedData("uniqueActiveUsers", "month", chartData);
        insertFormatedData("noSelectionLaunched", "day", chartData);
        insertFormatedData("noSelectionLaunched", "week", chartData);
        insertFormatedData("noSelectionLaunched", "month", chartData);
        insertFormatedData("noSelectionSaved", "day", chartData);
        insertFormatedData("noSelectionSaved", "week", chartData);
        insertFormatedData("noSelectionSaved", "month", chartData);

        fromatDataByDayWekMonForChart(newChartData);

        selectedSubCountry = selectedSubCountry || scope.countries[0]['countrySubName'][0]['countryName'];
        scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(newChartData["Basic"][selectedSubCountry].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        scope.countries[0].noSelectionLaunchedChart = gaChartUtil.populateGAChartData(newChartData["Basic"][selectedSubCountry].noSelectionLaunched, "dateFormatted", "noSelectionLaunched", "Date", "Selection Launched");
        scope.countries[0].noSelectionSavedChart = gaChartUtil.populateGAChartData(newChartData["Basic"][selectedSubCountry].noSelectionSaved, "dateFormatted", "noSelectionSaved", "Date", "Selection Saved");
      }


      /**
       *
       * @param {type} dataTypeKey
       * @param {type} chartType
       * @param {type} chartData
       * @returns {undefined}
       */
      function insertFormatedData(dataTypeKey, chartType, chartData) {
        for (var i = 0; i < myPactList.length; i++) {
          var key = myPactList[i];
          for (var l = 0; l < chartData[key][dataTypeKey][chartType].length; l++) {
            var data = chartData[key][dataTypeKey][chartType][l];
            var countryName = data[0];
            var dateString = dateFormate(data[1], chartType);
            var value = data[2];
            if (newChartData[key][countryName] !== undefined) {
              var tempObj = {};
              tempObj["dateFormatted"] = dateString;
              tempObj[dataTypeKey] = value;
              newChartData[key][countryName][dataTypeKey][chartType].push(tempObj);
            }
          }
        }
      }

      /**
       * Format the newChartData data for Day, Week and Month chart.
       * @param {type} data
       * @returns {undefined}
       */
      function fromatDataByDayWekMonForChart(data) {
        for (var i = 0; i < myPactList.length; i++) {
          var countryNameArray = scope.countries[i].countrySubName;
          var key = myPactList[i];
          for (var l = 0; l < countryNameArray.length; l++) {
            data[key][countryNameArray[l].countryFilterName]["uniqueActiveUsers"] = getFinalDataForChart(data[key][countryNameArray[l].countryFilterName]["uniqueActiveUsers"], "uniqueActiveUsers");
            data[key][countryNameArray[l].countryFilterName]["noSelectionLaunched"] = getFinalDataForChart(data[key][countryNameArray[l].countryFilterName]["noSelectionLaunched"], "noSelectionLaunched");
            data[key][countryNameArray[l].countryFilterName]["noSelectionSaved"] = getFinalDataForChart(data[key][countryNameArray[l].countryFilterName]["noSelectionSaved"], "noSelectionSaved");
          }
        }
      }

      /**
       *
       *
       * @param {type} data
       * @param {type} dataType
       * @returns {unresolved}
       */
      function getFinalDataForChart(data, dataType) {
        data.day = getFormatDayChart(data.day, dataType);
        data.week = getFormatWeekChart(data.week, dataType);
        data.month = getFormatMonthChart(data.month, dataType);
        return data;
      }

      /**
       *
       * @param {type} dayData
       * @param {type} dataType
       * @returns {Array}
       */
      function getFormatDayChart(dayData, dataType) {
        var day = getBlankDayData(dataType);
        day = processData(dayData, day, dataType);
        return day;
      }

      /**
       *
       * @param {type} weekData
       * @param {type} dataType
       * @returns {Array}
       */
      function getFormatWeekChart(weekData, dataType) {
        var week = getBlankWeekData(dataType);
        week = processData(weekData, week, dataType);
        return week;
      }

      /**
       *
       * @param {type} monthData
       * @param {type} dataType
       * @returns {Array}
       */
      function getFormatMonthChart(monthData, dataType) {
        var month = getBlankMonthData(dataType);
        month = processData(monthData, month, dataType);
        return month;
      }

      /**
       *
       * @param {type} orgData
       * @param {type} tempData
       * @param {type} dataType
       * @returns {unresolved}
       */
      function processData(orgData, tempData, dataType) {
        for (var i = 0; i < orgData.length; i++) {
          for (var l = 0; l < tempData.length; l++) {
            if (tempData[l].dateFormatted === orgData[i].dateFormatted) {
              tempData[l][dataType] = orgData[i][dataType];
            }
          }
        }
        return tempData;
      }

      /**
       * Return Last 30 Days Data.
       * @param {type} dataType
       * @returns {Array}
       */
      function getBlankDayData(dataType) {
        var blankDayArray = [];
        var blankDayObj = {};
        var tmpDate = null;

        for (var i = 30; i >= 1; i--) {
          tmpDate = getPreviousDateByXDaysBack(i);
          blankDayObj = {};

          //blankDayObj.date = tmpDate;
          blankDayObj.dateFormatted = getDateStr(tmpDate);
          blankDayObj[dataType] = 0;

          //Push into
          blankDayArray.push(blankDayObj);
        }

        return blankDayArray;
      }

      /**
       * Return Last 6 Month Data Week wise
       * @param {type} dataType
       * @returns {Array}
       */
      function getBlankWeekData(dataType) {
        var blankWeekArray = [];
        var blankWeekObj = {};
        var currentDate = getCurrentDate();

        var tmpDate = null;
        var tempWeek = null;


        for (var i = 26; i >= 0; i--) {
          tmpDate = getStrartDateOfWeekByXWeekBack(i);
          tempWeek = getWeekFromDate(tmpDate);
          blankWeekObj = {};
          if (tmpDate < currentDate) {
            //blankWeekObj.date = tmpDate;
            if (tempWeek <= 9) {
              tempWeek = "0" + tempWeek.toString();
            }
            blankWeekObj.dateFormatted = getWeekStr(tmpDate, tempWeek);
            blankWeekObj[dataType] = 0;
            //blankWeekObj.week = tempWeek;
            //blankWeekObj.users = 0;
            //Push into
            blankWeekArray.push(blankWeekObj);
          }
        }


        return blankWeekArray;
      }

      /**
       * Return Last One Year Data Month wise
       * @param {type} dataType
       * @returns {Array}
       */
      function getBlankMonthData(dataType) {
        var blankDayArray = [];
        var blankDayObj = {};
        var tmpDate = null;

        for (var i = 11; i >= 0; i--) {
          tmpDate = getSrartDateOfMonthByXMonthsBack(i);
          blankDayObj = {};

          //blankDayObj.date = tmpDate;
          blankDayObj.dateFormatted = getDateStr(tmpDate);
          blankDayObj[dataType] = 0;

          //Push into
          blankDayArray.push(blankDayObj);
        }

        return blankDayArray;
      }


      /**
       *
       * @param {type} xDaysBack
       * @param {type} dt
       * @returns {Date}
       */
      function getPreviousDateByXDaysBack(xDaysBack, dt) {
        if (dt === undefined || dt === null) {
          dt = new Date();
        }

        var year = dt.getFullYear();
        var month = dt.getMonth();
        var day = dt.getDate();

        var newDate = new Date(year, month, (day - xDaysBack));

        return newDate;
      }

      /**
       *
       * @param {type} xWeekBack
       * @returns {Date}
       */
      function getStrartDateOfWeekByXWeekBack(xWeekBack) {
        var prevDate = getPreviousDateByXDaysBack(xWeekBack * 7);
        var startDateOfWeek = getStartDateOfWeekOfGivenDate(prevDate);

        //Rerurn startDateOfWeek
        return startDateOfWeek;
      }

      /**
       *
       * @param {type} xMonthsBack
       * @returns {Date}
       */
      function getSrartDateOfMonthByXMonthsBack(xMonthsBack) {
        var dt = new Date();
        var year = dt.getFullYear();
        var month = dt.getMonth();
        var day = dt.getDate();

        var newDate = new Date(year, (month - xMonthsBack), 1);

        return newDate;
      }

      /**
       * Return Week of the year from a Date
       *
       * @param {type} dt
       * @param {type} dowOffset
       * @returns {Number}
       */
      function getWeekFromDate(dt, dowOffset) {
        var nYear;
        var nday;
        dowOffset = typeof (dowOffset) === 'int' ? dowOffset : 0; //default dowOffset to zero
        var newYear = new Date(dt.getFullYear(), 0, 1);
        var day = newYear.getDay() - dowOffset; //the day of week the year begins on
        day = (day >= 0 ? day : day + 7);
        var daynum = Math.floor((dt.getTime() - newYear.getTime() -
                (dt.getTimezoneOffset() - newYear.getTimezoneOffset()) * 60000) / 86400000) + 1;
        var weeknum;
        //if the year starts before the middle of a week
        if (day < 4) {
          weeknum = Math.floor((daynum + day - 1) / 7) + 1;
          if (weeknum > 52) {
            nYear = new Date(dt.getFullYear() + 1, 0, 1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
             the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
          }
        } else {
          weeknum = Math.floor((daynum + day - 1) / 7);
        }
        return weeknum;
      }

      /**
       *
       * @param {type} tmpDate
       * @param {type} tempWeek
       * @returns {String}
       */
      function getWeekStr(tmpDate, tempWeek) {
        var year = tmpDate.getFullYear();
        var yearStr = year.toString();
        var dtStr = yearStr + "-" + tempWeek;
        return dtStr;
      }

      /**
       *
       * @param {type} dt
       * @returns {String}
       */
      function getDateStr(dt) {
        var year = dt.getFullYear();
        var month = dt.getMonth() + 1;
        var day = dt.getDate();

        var yearStr = year.toString();
        var monthStr = "";
        var dayStr = "";

        if (month <= 9) {
          monthStr = "0" + month.toString();
        } else {
          monthStr = month.toString();
        }

        if (day <= 9) {
          dayStr = "0" + day.toString();
        } else {
          dayStr = day.toString();
        }

        var dtStr = yearStr + "-" + monthStr + "-" + dayStr;
        return dtStr;
      }

      /**
       *
       * @returns {Date}
       */
      function getCurrentDate() {
        var dt = new Date();

        var year = dt.getFullYear();
        var month = dt.getMonth();
        var day = dt.getDate();

        var newDate = new Date(year, month, day);

        return newDate;
      }

      /**
       *
       * @param {type} dt
       * @returns {undefined}
       */
      function getStartDateOfWeekOfGivenDate(dt) {
        var dow = dt.getDay();
        var startDateOfWeek = null;

        if (dow !== 0) {
          startDateOfWeek = getPreviousDateByXDaysBack(dow, dt);
        } else {
          startDateOfWeek = dt;
        }

        //Rerurn startDateOfWeek
        return startDateOfWeek;
      }


      /**
       *
       * @param {type} dateString
       * @param {type} chartType
       * @returns {unresolved}
       */
      function dateFormate(dateString, chartType) {
        if (chartType === 'day') {
          return commonUtils.getFormattedDayString(dateString);
        } else if (chartType === "week") {
          return commonUtils.getFormattedWeekString(dateString);
        } else if (chartType === "month") {
          return commonUtils.getFormattedMonthString(dateString);
        }
      }

      /**
       *
       * @param {type} filedName
       * @param {type} value
       * @returns {undefined}
       */
      function setMyPactVlaue(filedName, value) {
        if (filedName === "Active User") {
          if (value === 0) {
            scope.disableActiveUser = true;
            angular.element("#activeUser").removeClass("link");
          } else {
            scope.disableActiveUser = false;
            angular.element("#activeUser").addClass("link");
          }
          scope.myPactActiveUsersUnique = value;
        } else if (filedName === "Selection Launched") {
          if (value === 0) {
            scope.disableSelectionLauched = true;
            angular.element("#noSelectionLauched").removeClass("link");
          } else {
            scope.disableSelectionLauched = false;
            angular.element("#noSelectionLauched").addClass("link");
          }
          scope.myPactNoSelectionLaunched = value;
        } else if (filedName === "Selection Save") {
          if (value === 0) {
            scope.disableSelectionSave = true;
            angular.element("#noSelectionSave").removeClass("link");
          } else {
            scope.disableSelectionSave = false;
            angular.element("#noSelectionSave").addClass("link");
          }
          scope.myPactNoSelectionSaved = value;
        }
      }

      /**
       *
       * @param {type} data
       * @param {type} dimensionsLength
       * @returns {Array}
       */
      function getCountryWisedata(data, dimensionsLength) {
        var newData = [];
        if (data !== undefined && data !== null) {
          if (dimensionsLength === 1) {
            for (var i = 0; i < data.length; i++) {
              var temp = data[i];
              newData.push({
                'countryName': temp[0],
                'totalEvents': temp[1]
              });
            }
          } else if (dimensionsLength === 2) {
            for (var i = 0; i < data.length; i++) {
              var temp = data[i];
              newData.push({
                'countryName': temp[0],
                'totalEvents': temp[2]
              });
            }
          }
        }
        return newData;

      }

      /**
       *
       * @param {type} data
       * @returns {Array}
       */
      function getSubCountry(data) {
        var newData = [];

        if (data !== undefined && data !== null) {
          for (var i = 0; i < data.length; i++) {
            var temp = data[i];
            var name = temp[0] === "(not set)" ? "Others" : temp[0]
            newData.push({
              'countryName': name,
              'countryFilterName': temp[0]
            });
          }
        }

        newData.push(newData.shift());

        return newData;
      }

      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onMyPactChartLinkChange(chartName) {
        if (chartName === 'ChartPaceChart') {
          scope.isGAChartVisible = false;
          //scope.isPaceChartVisible = true;
        } else {
          scope.isPaceChartVisible = false;
        }
        //When you click on a link Chart will reset to Month Chart.
        scope.myPactChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.myPactChartName = chartName;
        onMyPactChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onMyPactChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;
        selectedCountryName = scope.selectedCountry.countryName;
        //selectedCountryGaId = scope.selectedCountry.countryGaId;
        selectedSubCountry = scope.selectedSubCountry.countryFilterName;

        scope.selectedChartTab = selTab;
        scope.myPactChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.myPactChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.myPactChartTypeSelected;
        var chartName = scope.myPactChartName;
        var selectedSubCountryName = scope.selectedSubCountry.countryName;
        var countryPrefix = selectedCountryName + " - " + selectedSubCountryName + " : ";
        var keyIndex = 0;
        for (var i = 0; i < myPactList.length; i++) {
          if (myPactList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        //check for PACE Chart
        if (chartName === 'ChartPaceChart') {
          if (scope.paceDataRegUserOverPeriod !== null && scope.paceDataRegUserOverPeriod.myPactBasicUsersReg !== undefined) {
            scope.paceDataReady = true;
            loadPaceChart(scope.selectedChartTab);
          }else {
            $rootScope.$on('RegUserOverPeriodDataReady', function (event, data) {
              scope.paceDataRegUserOverPeriod = data;
              scope.paceDataReady = true;
              loadPaceChart(scope.selectedChartTab);
            });
          }
          return;
        }

        if (isInitChartData === false) {
          initChartData();
        }


        // ADD Chart
        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.myPactChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartSelectionLaunched":
          {
            scope.myPactChartLabel = countryPrefix + SELECTION_LAUNCHED;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].noSelectionLaunchedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].noSelectionLaunchedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].noSelectionLaunchedChart.month;
            }
            break;
          }
          case "ChartSelectionSaved":
          {
            scope.myPactChartLabel = countryPrefix + SELECTION_SAVED;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].noSelectionSavedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].noSelectionSavedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].noSelectionSavedChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
        scope.isGAChartVisible = true;
      }

      function showCountryDialog() {
        var countryData = {};
        var totalRegisteredUser = scope.paceDataRegUser === null ? 0 : scope.paceDataRegUser.myPactBasicUsersReg;
        countryData.containerWidth = 610;
        countryData.tableHeader = "MyPact";
        countryData.note1 = "* Individual data is not available for countries.";
        countryData.gridOptions = {
          enableVerticalScrollbar: 0,
          enableHorizontalScrollbar: 0
        };
        countryData.gridOptions.rowHeight = 30;
        countryData.gridOptions.columnDefs = [
          {field: "label", name: "Country", width: 250, enableColumnMenu: false, enableSorting: false}
        ];

        for (var i = 0; i < myPactList.length; i++) {
          countryData.gridOptions.columnDefs.push({
            field: "country_" + i,
            name: myPactList[i],
            width: 110,
            enableColumnMenu: false,
            enableSorting: false
          });
        }

        countryData.gridOptions.columnDefs.push({field: "total", name: "Total - Worldwide", width: 120, enableColumnMenu: false, enableSorting: false});

        countryData.gridOptions.data = [{
            "label": "Global figures",
            "total": "",
            "country_0": "-",
            "country_1": "-"
          }, {
            "label": "No. of registered users",
            "total": commonUtils.getNumberFormat(totalRegisteredUser, 0) + " *",
            "country_0": "-",
            "country_1": "-"
          }, {
            "label": "No. of unique active users for last 12 months",
            "total": commonUtils.getNumberFormat(scope.myPactTotalActiveUsersUnique, 0),
            "country_0": commonUtils.getNumberFormat(scope.countries[0].uniqueActiveUsers, 0),
            "country_1": commonUtils.getNumberFormat(scope.countries[1].uniqueActiveUsers, 0)
          }, {
            "label": "No. of selection launched",
            "total": commonUtils.getNumberFormat(scope.myPactTotalNoSelectionLaunched, 0),
            "country_0": commonUtils.getNumberFormat(scope.countries[0].noSelectionLaunched, 0),
            "country_1": commonUtils.getNumberFormat(scope.countries[1].noSelectionLaunched, 0)
          }, {
            "label": "No. of selection saved",
            "total": commonUtils.getNumberFormat(scope.myPactTotalNoSelectionSaved, 0),
            "country_0": commonUtils.getNumberFormat(scope.countries[0].noSelectionSaved, 0),
            "country_1": commonUtils.getNumberFormat(scope.countries[1].noSelectionSaved, 0)
          }, {
            "label": "No. of order placed (SAP)",
            "total": "NA",
            "country_0": "NA",
            "country_1": "NA"
          }, {
            "label": "Value of order placed (SAP)",
            "total": "NA",
            "country_0": "NA",
            "country_1": "NA"
          }];

        $rootScope.$broadcast(appConstants.EVENTS.COUNTRY_DIALOG_BOX, countryData);
      }


      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.myPactChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }


    } //END Link..
  }


})();
