/**
 * @ngdoc directive
 * @name BLM directive
 *
 * @description
 *  BLM directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('blm', blm);

  function blm($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, BLMCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/blm/blm.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Country1'];

      var _BLMReportData = null;
      var _BLMChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      //var selectedCountryGaId = appConstants.SITE_GA_IDS.BLM_ID;

      scope.uniqueActiveUsers = 0;
      scope.projectsCreated = 0;
      scope.launchDate = appConstants.DATES.BLM_LAUNCHED_DATE;

      scope.blmLaunchDays = 0;
      scope.blmLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, appConstants.DATES.BLM_LAUNCHED_DATE);

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------

      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_PROJ_CREATED = "No. of projects created";
      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.blmTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.blmChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.bmlChartName = "ChartActiveUser";
      scope.blmChartLabel = NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;

      scope.onBlmChartOptionChange = onBlmChartOptionChange;
      scope.onBlmChartLinkChange = onBlmChartLinkChange;


      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
//                            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        // Init Countries data
        initCountries();
        // Get Report Data
        getBLMReportData();
        // Get Chart Data
        getBLMChartData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.blmUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initChartData();
          }
        });
      }

      /**
       * Get BLM report data from NODE
       *
       * @returns {undefined}
       */
      function getBLMReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getBLMReportData().then(
                function (result) {
                  logger.log("BLM::getBLMReportData::Result::Success");
                  _BLMReportData = result;
                  parseReportData(_BLMReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _BLMReportData = null;
                  logger.log("BLM::getBLMReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       * Get BLM CHART data from NODE
       *
       * @returns {undefined}
       */
      function getBLMChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getBLMChartData().then(
                function (result) {
                  logger.log("BLM::getBLMChartData::Result::Success");
                  _BLMChartData = result;
                  parseChartData(_BLMChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _BLMChartData = null;
                  logger.log("BLM::getBLMChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          logger.log("blm.js :: loadDefaultChart :: Timer :: Start ");
          onBlmChartLinkChange('ChartActiveUser');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  logger.log("blm.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );


      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var blm = {};
        //----------- BLM ------------
        for (var i = 0; i < countryNameList.length; i++) {
          blm.countryName = countryNameList[i];
          blm.countryGaId = appConstants.SITE_GA_IDS.BLM_ID;
          blm.launchDate = appConstants.DATES.BLM_LAUNCHED_DATE;
          blm.launchDateStr = appConstants.DATES.BLM_LAUNCHED_DATE_STR;
          blm.regUsers = 0;
          blm.uniqueActiveUsers = 0;
          blm.projectsCreated = 0;
          blm.uniqueActiveUsersChart = 0;
          blm.projectsCreatedChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new BLMCountryVO(blm));
        }
      }

      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload BLM Chart.." + scope.selectedChartTab);
        onBlmChartOptionChange(null, scope.selectedChartTab);

      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
        scope.countries[0].projectsCreated = parseFloat(reportData.projectList.totalsForAllResults['ga:totalEvents']);
        //----------------- SET Default Value ---------------------
        scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
        scope.projectsCreated = scope.countries[0].projectsCreated;
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        scope.countries[0].projectsCreatedChart = gaChartUtil.populateGAChartData(chartData.projectList, "dateFormatted", "projectList", "Date", "Projects Created");

        console.log("Chart Data.. Prepare...");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onBlmChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.blmChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.bmlChartName = chartName;
        onBlmChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onBlmChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.blmChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.blmChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.blmChartTypeSelected;
        var chartName = scope.bmlChartName;

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartRegisteredUsers":
          {
            scope.blmChartLabel = NO_OF_REG_USERS;

            if (chartType === 'Day') {
              chartObj.data = scope.paceDataRegUserOverPeriod.blmUsersReg.day;
            } else if (chartType === 'Week') {
              chartObj.data = scope.paceDataRegUserOverPeriod.blmUsersReg.week;
            } else if (chartType === 'Month') {
              chartObj.data = scope.paceDataRegUserOverPeriod.blmUsersReg.month;
            }
            break;
          }
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.blmChartLabel = NO_OF_ACTIVE_USERS;

            if (chartType === 'Day') {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.day;
            } else if (chartType === 'Week') {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.week;
            } else if (chartType === 'Month') {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.month;
            }
            break;
          }

          case "ChartProjectCreated":
          {
            scope.blmChartLabel = NO_OF_PROJ_CREATED;

            if (chartType === 'Day') {
              chartObj.data = scope.countries[0].projectsCreatedChart.day;
            } else if (chartType === 'Week') {
              chartObj.data = scope.countries[0].projectsCreatedChart.week;
            } else if (chartType === 'Month') {
              chartObj.data = scope.countries[0].projectsCreatedChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




    }//END Link..
  }


})();