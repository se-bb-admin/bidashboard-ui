/**
 * @ngdoc directive
 * @name MES directive
 *
 * @description
 *  MES....
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('mes', mes);

  function mes($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, MESCountryVO, currencyConverterService, paceService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        usersReg: "=usersReg"
      },
      templateUrl: 'scripts/directives/mes/mes.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      var countryNameList = ['France'];
      var _mesReportData = null;
      var _mesChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var timer = null;


      //------------------------------------------------------------------
      //--------------------------- Global Properties --------------------
      //------------------------------------------------------------------

      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];
      scope.mesFranceLaunchDate = appConstants.DATES.MES_FRANCE_LAUNCHED_DATE;
      scope.MES_BOM_FOOTER_NOTE = appConstants.NOTE.MES_BOM_FOOTER_NOTE;
      scope.uniqueActiveUsers = 0;
      scope.mesTotalUniqueActiveUsers = 0;
      scope.mesLaunchDays = 0;

      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;


      //------------------------------------------------------------------
      //--------------- MY PACT CHART INIT SHOW HIDE PROPERTIES ----------
      //------------------------------------------------------------------

      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var DEFAULT_CHART_NAME = "UniqueActiveUsers";
      scope.onCountryDropDownChange = onCountryDropDownChange;
      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------------------
      //----------------------  For Chart Options ------------------------
      //------------------------------------------------------------------

      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.mesChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.mesChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.mesChartName = DEFAULT_CHART_NAME;
      scope.mesChartLabel = NO_OF_ACTIVE_USERS;//NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.disableUniqueActiveUsers = false;
      scope.onMESChartOptionChange = onMESChartOptionChange;
      scope.onMESChartLinkChange = onMESChartLinkChange;
      scope.chartReady = chartReady;


      init();


      /**
       *
       * @returns {undefined}
       */
      function init() {
        // Init Countries data
        initCountries();

        // paceService
        scope.mesRegisteredUsers = 0;
        paceService.getRegisteredUsersForProject('mes-estudio').then(function (data) {
          scope.mesRegisteredUsers = data.users;
        });
        // Get Report Data
        getMESReportData();
        // Get Chart Data
        getMESChartData();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);
        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }


      /**
       * Get MES report data from NODE
       *
       * @returns {undefined}
       */
      function getMESReportData() {
//                $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
//                nodeGAService.getMESReportData().then(
//                        function (result) {
//                            logger.log("MES::getMESReportData::Result::Success");
//                            _mesReportData = result;
//                        console.log('@127 _mesReportData: ', _mesReportData);
//                            parseReportData(_mesReportData);
//                            $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
//                        },
//                        function (error) {
//                            _mesReportData = null;
//                            logger.log("MES::getMESReportData::Result::FAIL");
//                            $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
//                        }
//                );
      }


      /**
       * Get MES CHART data from NODE
       *
       * @returns {undefined}
       */
      function getMESChartData() {

//                $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
//                nodeGAService.getMESChartData().then(
//                        function (result) {
//                            logger.log("MES::getMESChartData::Result::Success");
//                            _mesChartData = result;
//                            parseChartData(_mesChartData);
//                            //Load Default Chart
//                            loadDefaultChart();
//                            //Hide Loader
//                            $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
//                        },
//                        function (error) {
//                            _mesChartData = null;
//                            logger.log("MES::getMESChartData::Result::FAIL");
//                            //Hide Loader
//                            $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
//                        }
//                );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          console.log("mes.js :: loadDefaultChart :: Timer :: Start ");
          onMESChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("mes.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }


      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       *
       * @returns {undefined}
       */
      function initCountries() {
        var mes = {};
        //--
        for (var i = 0; i < countryNameList.length; i++) {

          mes.countryName = countryNameList[i];

          if (mes.countryName.toUpperCase() === "FRANCE") {
            mes.launchDate = scope.mesFranceLaunchDate;
          }

          mes.mesLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, mes.launchDate);
          scope.mesLaunchDays = mes.mesLaunchDays;
//					console.log('>>> mes.mesLaunchDays: ', mes.mesLaunchDays);
//                    mes.uniqueActiveUsers = 0;
          scope.uniqueActiveUsers = mes.uniqueActiveUsers;

          //--- Push country to Countries Array
          scope.countries.push(new MESCountryVO(mes));
        }
        //Set default Country to update data binding
        setDefaultCountry();
      }


      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }


      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;
        logger.log(selectedCountryName);
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.launchDate = scope.countries[index].launchDate;
        scope.mesLaunchDays = scope.countries[index].mesLaunchDays;
        setTableDataAndLink('Active Users', scope.countries[index].uniqueActiveUsers);

        onMESChartLinkChange(DEFAULT_CHART_NAME);
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload MES Chart.." + scope.selectedChartTab);
        //->
        onMESChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.totalBomPrice = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          scope.mesTotalUniqueActiveUsers = scope.mesTotalUniqueActiveUsers + scope.countries[i].uniqueActiveUsers;
        }

//                scope.totalBomPrice = ((scope.countries[0].bomPrice) * oneRealToEuroMultiplier) + ((scope.countries[1].bomPrice) * oneRubleToEuroMultiplier) +
//                        (scope.countries[2].bomPrice) + ((scope.countries[2].bomPrice) * oneRSARandToEuroMultiplier);

        //----------------- SET Default Value ---------------------

        scope.launchDate = scope.countries[0].launchDate;
//                scope.mesLaunchDays = scope.countries[0].mesLaunchDays;

        setTableDataAndLink('Active Users', scope.countries[0].uniqueActiveUsers);
      }


      /**
       *
       * @param {type} filedName
       * @param {type} value
       * @returns {undefined}
       */
      function setTableDataAndLink(filedName, value) {
        if (filedName === "Active Users") {
          if (value === 0) {
            scope.disableUniqueActiveUsers = true;
            angular.element("#uniqueActiveUsers").removeClass("link");
          } else {
            scope.disableUniqueActiveUsers = false;
            angular.element("#uniqueActiveUsers").addClass("link");
          }
          scope.uniqueActiveUsers = value;
        }
//				console.log('>>>> filedName: ', filedName, ' -- setTableDataAndLink :: scope.uniqueActiveUsers: ', scope.uniqueActiveUsers);
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        }
        console.log("Chart Data.. Prepare...");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onMESChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.mesChartTypeSelected = scope.chartOptions[2];
        scope.mesChartName = chartName;
        onMESChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onMESChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }
        scope.isfootNoteSymbolVisible = false;
        scope.selectedChartTab = selTab;
        scope.mesChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.mesChartToolTip = scope.chartToolTips[scope.selectedChartTab];
        var chartType = scope.mesChartTypeSelected;
        var chartName = scope.mesChartName;
        var countryPrefix = selectedCountryName + " : ";
        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "UniqueActiveUsers":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.mesChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.mesChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }
    }//END Link..
  }
})();