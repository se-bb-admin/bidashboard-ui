/**
 * @ngdoc directive
 * @name ECO_REACH Report directive
 *
 * @description
 *  ECO_REACH Report directive displays all list of information based on counties selection.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding')
          .directive('ecoReach', ecoreach);

  function ecoreach($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, EcoReachCountryVO, paceService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/ecoReach/ecoReach.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      var _ecoreachReportData = null;
      var _ecoreachChartData = null;
      var _ecoreachGAReportData = null;
      var _ecoreachGAChartData = null;

      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------------------
      //---------------------------- Global Properties -------------------
      //------------------------------------------------------------------
      var countryNameList = [];
      var countryNameListRegd = [];
      var countryNameListActive = [];

      scope.countries = [];
      scope.countriesRegd = [];
      scope.countriesActive = [];

      var selectedCountryName = "";

      scope.ecoreachLaunchedDate = appConstants.DATES.ECO_REACH_1_LAUNCHED_DATE;
      scope.ecoreach2LaunchedDate = appConstants.DATES.ECO_REACH_2_LAUNCHED_DATE;
      scope.ecoreachUsersReg = 0;
      scope.ecoreach2UsersReg = 0;
      scope.ecoreachTotalUsersReg = 0;
      scope.ecoreachUniqueActiveUsers = 0;
      scope.totalUniqueActiveUsers = 0;
      scope.dataVersion = '1';

      scope.pageLoading = false;

      scope.isChartData = {
        day: false,
        week: false,
        month: false
      };

      scope.isGAData = {
        day: false,
        week: false,
        month: false
      };

      scope.showVersion = function (dataVersion) {
        scope.dataVersion = dataVersion;
        switch (dataVersion) {
          case '1':
            countryNameList = countryNameListRegd;
            scope.countries = scope.countriesRegd;
            scope.selectedCountry = scope.countries[0];
            scope.ecoreachLaunchedDate = appConstants.DATES.ECO_REACH_1_LAUNCHED_DATE;
            onEcoReachChartLinkChange('ChartRegisteredUsers');
            onCountryDropDownChange();
            break;
          case '2':
            countryNameList = countryNameListActive;
            scope.countries = scope.countriesActive;
            scope.selectedCountry = scope.countries[0];
            scope.ecoreachLaunchedDate = appConstants.DATES.ECO_REACH_2_LAUNCHED_DATE;
            onEcoReachChartLinkChange('ChartRegisteredUsers2');
            onCountryDropDownChange();
            break;
        }
      };

      scope.isVersion = function (version) {
        return scope.dataVersion === version;
      };


      //------------------------------------------------------------------
      //--------------- ECO_REACH CHART INIT SHOW HIDE PROPERTIES --------------
      //------------------------------------------------------------------

      scope.isGAChartVisible = true;
      scope.isPaceChartVisible = false;

      var NO_OF_REG_USERS = "No. of registered users";
      var NO_OF_ACTIVE_USERS = "No. of active users";

      scope.onCountryDropDownChange = onCountryDropDownChange;
      scope.showCountryDialog = showCountryDialog;

      scope.paceChartStyle = commonUtils.getPaceChartStyle();
      scope.paceChartSource = {};
      scope.showPaceChart = showPaceChart;
      scope.chartSource = {};

      var paceChartObj = {};
      var isInitChartData = false;
      var chartObj = {};


      //------------------------------------------------------------------
      //-------------------  For Chart TYPE Options  ---------------------
      //------------------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX; // 2
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES; // ['Day', 'Week', 'Month']
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS; // ['Data for last 30 days', 'Data for last 6 month', 'Data for last one year']
      scope.selectedChartTab = chartOptionDfltSelInx; //2
      scope.ecoreachChartTypeSelected = scope.chartOptions[scope.selectedChartTab]; // Day / Week / Month
      scope.ecoreachChartToolTip = scope.chartToolTips[scope.selectedChartTab]; // Tooltip for Day / Week / Month
      scope.ecoreachChartName = "ChartRegisteredUsers"; // ChartActiveUser, ChartNoOfProjCreated, ChartNoOfChecksCal etc..
      scope.ecoreachChartLabel = selectedCountryName + " : " + NO_OF_REG_USERS;
      scope.onEcoReachChartLinkChange = onEcoReachChartLinkChange;
      scope.onEcoReach2ChartLinkChange = onEcoReach2ChartLinkChange;
      scope.onEcoReachChartOptionChange = onEcoReachChartOptionChange;
      scope.loadDefaultChart = false;
      scope.paceChartStyle = commonUtils.getPaceChartStyle;
      scope.chartReady = chartReady;

      init();



      /**
       * Function to initialized the repors
       *
       * @returns {undefined}
       */
      function init() {
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        scope.pageLoading = true;

        nodeGAService.getEcoReachCountryList().then(
                function (data) {

                  countryNameList = data.countries;
                  countryNameListRegd = countryNameList;

                  // Init Countries data
                  initCountries();

                  getPaceRegisteredUsers();

                  // Get Report Data
                  getEcoReachReportData();

                  // Get Chart Data
                  getEcoReachChartData();

                  // Get GA Report Data
                  getEcoReachGAReportData();

                  // Get GA Chart Data
                  getEcoReachGAChartData();

                },
                function () {
                  logger.log("ECO_REACH::getEcoReachCountryList::Result::FAIL");
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);


        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }

      /**
       * Get ECO_REACH report data from NODE
       *
       * @returns {undefined}
       */
      function getEcoReachReportData() {

//				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getEcoReachReportData().then(
                function (result) {
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  logger.log("ECO_REACH::getEcoReachReportData::Result::Success");

                  _ecoreachReportData = result;

                  parseReportData(_ecoreachReportData);
                },
                function (error) {
                  _ecoreachReportData = null;
                  logger.log("ECO_REACH::getEcoReachReportData::Result::FAIL");
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get ECO_REACH report data from NODE
       *
       * @returns {undefined}
       */
      function getEcoReachGAReportData() {
//				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getEcoReachGAReportData().then(
                function (result) {
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  logger.log("ECO_REACH::getEcoReachGAReportData::Result::Success");
                  _ecoreachGAReportData = result;
                  parseGAReportData(_ecoreachGAReportData);
                },
                function (error) {
                  _ecoreachReportData = null;
                  logger.log("ECO_REACH::getEcoReachGAReportData::Result::FAIL");
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get ECO_REACH CHART data from NODE
       *
       * @returns {undefined}
       */
      function getEcoReachChartData() {

//				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getEcoReachChartData().then(
                function (result) {
                  //Hide Loader
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  logger.log("ECO_REACH::getEcoReachChartData::Result::Success");

                  _ecoreachChartData = result;
                  parseChartData(_ecoreachChartData);

                  //Init Chart Data
                  initChartData();

                  //Load Default Chart
                  loadDefaultChart();
                },
                function (error) {
                  _ecoreachChartData = null;
                  logger.log("ECO_REACH::getEcoReachChartData::Result::FAIL");
                  //Hide Loader
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      /**
       * Get ECO_REACH CHART data from GA
       *
       * @returns {undefined}
       */
      function getEcoReachGAChartData() {

//				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getEcoReachGAChartData().then(
                function (result) {
                  //Hide Loader
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  logger.log("ECO_REACH::getEcoReachChartData::Result::Success");

                  _ecoreachChartData = result;
                  parseGAChartData(_ecoreachChartData);

                  //Init Chart Data
                  initChartData();

                  //Load Default Chart
                  loadDefaultChart();
                  scope.pageLoading = false;
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  scope.showVersion('1');
                },
                function (error) {
                  _ecoreachChartData = null;
                  logger.log("ECO_REACH::getEcoReachChartData::Result::FAIL");
                  //Hide Loader
//							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      function getPaceRegisteredUsers() {
        paceService.getRegisteredUsersForProject('ecoreach').then(function (data) {
          scope.ecoreach2UsersReg = data.users;
        });
      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();
        onEcoReachChartLinkChange('ChartRegisteredUsers');
        scope.loadDefaultChart = true;
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoreach.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initPaceChartData() {
        paceChartObj = {};
        paceChartObj.type = "AreaChart";
        paceChartObj.displayed = false;
        paceChartObj.options = commonUtils.getPaceChartOptions();

        isInitPaceChartData = true;
      }


      /***
       *  Onclick Registererd users link click..
       *  Show PACE Chart Hide GA Chart
       *
       * @returns {undefined}
       *
       */
      function showPaceChart() {
        scope.isGAChartVisible = false;
        scope.isPaceChartVisible = true;

        //Show Hide Chart
        onEcoReachChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }

      /**
       *
       * @returns {undefined}
       */
      function initCountries() {
        var country = {};
        //--
        countryNameListRegd = [];
        for (var i = 0; i < countryNameList.length; i++) {
          var gaId = "XXXXXXXXX";//'ECO_REACH_' + capsCountryName + '_ID';
          var launchDate = "";//'ECO_REACH_' + capsCountryName + '_LAUNCHED_DATE';
          var launchDateStr = scope.ecoreachLaunchedDate;//'ECO_REACH_' + capsCountryName + '_LAUNCHED_DATE_STR';
          country.countryName = countryNameList[i];
          country.countryGaId = appConstants.SITE_GA_IDS[gaId];
          country.launchDate = appConstants.DATES[launchDate];
          country.launchDateStr = appConstants.DATES[launchDateStr];
          country.regUsers = '0';
          country.uniqueActiveUsers = 0;
          country.noOfProjCreated = 0;
          country.noOfChecksCal = 0;
          country.aveCalPerSess = 0;
          country.uniqueActiveUsersChart = 0;
          country.noOfProjCreatedChart = 0;
          country.noOfChecksCalChart = 0;
          country.aveCalPerSessChart = 0;
          scope.countries.push(new EcoReachCountryVO(country));
          countryNameListRegd.push(countryNameList[i]);
        }
        scope.countriesRegd = scope.countries;
        //Set default Country to update data binding
        setDefaultCountry();
      }


      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
        onEcoReachChartOptionChange(null, scope.chartOptions[2]);
      }

      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        //Set the values
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;

        logger.log(selectedCountryName);

        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.ecoreachUsersReg = scope.countries[index].ecoreachUsersReg;
        scope.ecoreachUniqueActiveUsers = scope.countries[index].ecoreachUniqueActiveUsers;


        //Load Default Chart
        onEcoReachChartLinkChange(scope.ecoreachChartName || 'ChartRegisteredUsers');
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload ECO_REACH Chart.." + scope.selectedChartTab);
        //->
        onEcoReachChartOptionChange(null, scope.selectedChartTab);
      }

      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.ecoreachTotalUsersReg = 0;

        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].ecoreachUsersReg = parseFloat(reportData[keyName].totRegUsers);

          scope.ecoreachTotalUsersReg += scope.countries[i].ecoreachUsersReg;
        }

        //----------------- SET Default Value ---------------------
        scope.ecoreachUsersReg = scope.countries[0].ecoreachUsersReg;
      }

      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseGAReportData(reportData) {
        scope.totalUniqueActiveUsers = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].ecoreachUniqueActiveUsers = 0;
          if (reportData[keyName]) {
            scope.countries[i].ecoreachUniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
            scope.totalUniqueActiveUsers += scope.countries[i].ecoreachUniqueActiveUsers;
          }
        }
        countryNameListActive = [];
        scope.countriesActive = scope.countries.filter(function (obj, indx) {
          if (obj.ecoreachUniqueActiveUsers) {
            countryNameListActive.push(obj.countryName);
            return true;
          }
        });

        //----------------- SET Default Value ---------------------
        scope.ecoreachUniqueActiveUsers = scope.countries[0].ecoreachUniqueActiveUsers;
      }

      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].ecoreachUsersRegChart = {};
          if (chartData[keyName] !== undefined || chartData[keyName] !== null) {
            scope.countries[i].ecoreachUsersRegChart = gaChartUtil.populateGAChartData(chartData[keyName].totRegUsers, "dateFormatted", "users", "Date", "Registered Users");
          }
        }
        console.log("Chart Data.. Prepare...");
      }

      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseGAChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = {};
          if (chartData[keyName]) {
            scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          }
        }
        console.log("Chart Data.. Prepare...");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onEcoReachChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        //Set Chart Name ..ChartActiveUser, ChartNoOfProjCreated, ChartNoOfChecksCal etc..
        scope.ecoreachChartName = chartName;
        var tab = 2;
        if (scope.dataVersion === '2') {
          tab = scope.selectedChartTab;
        }
        scope.ecoreachChartTypeSelected = scope.chartOptions[tab]; // Day / Week / Month
        onEcoReachChartOptionChange(null, tab);
      }

      function onEcoReach2ChartLinkChange() {
        scope.ecoreachChartName = 'ChartRegisteredUsers2';
        onEcoReachChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onEcoReachChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }
        //Set Country Name & gaId
        scope.isfootNoteSymbolVisible = false;
        selectedCountryName = scope.selectedCountry.countryName;

        //Set the selected Tab val again..
        scope.selectedChartTab = selTab;

        scope.ecoreachChartTypeSelected = scope.chartOptions[scope.selectedChartTab]; // Day / Week / Month
        scope.ecoreachChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.ecoreachChartTypeSelected;
        var chartName = scope.ecoreachChartName;
        var countryPrefix = selectedCountryName + " : ";

        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        if (isInitChartData === false) {
          initChartData();
        }

        // ADD Chart
        switch (chartName) {
          case "ChartRegisteredUsers":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.ecoreachChartLabel = countryPrefix + NO_OF_REG_USERS;

            var chart = scope.countries[keyIndex].ecoreachUsersRegChart;

            scope.isChartData.day = chart && chart.day.rows ? true : false;
            scope.isChartData.week = chart && chart.week.rows ? true : false;
            scope.isChartData.month = chart && chart.month.rows ? true : false;

            if (chartType === "Day" && scope.isChartData.day) {
              chartObj.data = scope.countries[keyIndex].ecoreachUsersRegChart.day;
            } else if (chartType === "Week" && scope.isChartData.week) {
              chartObj.data = scope.countries[keyIndex].ecoreachUsersRegChart.week;
            } else if (chartType === "Month" && scope.isChartData.month) {
              chartObj.data = scope.countries[keyIndex].ecoreachUsersRegChart.month;
            }
            break;
          }
          case "ChartActiveUsers":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.ecoreachChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;

            var chart = scope.countries[keyIndex].uniqueActiveUsersChart;

            scope.isChartData.day = chart.day && chart.day.rows ? true : false;
            scope.isChartData.week = chart.week && chart.week.rows ? true : false;
            scope.isChartData.month = chart.month && chart.month.rows ? true : false;


            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
//						chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart[chartType.toLowerCase()];
//						scope.isGAData[chartType.toLowerCase()] = chartObj.data ? true : false;
            break;
          }
          case "ChartRegisteredUsers2":
          {
            scope.ecoreachChartLabel = NO_OF_REG_USERS;
            scope.isChartData.day = true;
            scope.isChartData.week = true;
            scope.isChartData.month = true;
            chartObj.data = scope.paceDataRegUserOverPeriod.ecoreach2UsersReg[chartType.toLowerCase()];
            break;
          }
        }

        //Set value to Scope
        scope.chartSource = chartObj;
      }

      scope.isSelectedChart = function (chartname) {
        return (chartname === scope.ecoreachChartName);
      };


      function showCountryDialog() {
        var countryData = {};
        //var totalRegisteredUser = scope.paceData.cbtUsersReg;
        countryData.containerWidth = 80;
        countryData.tableHeader = "EcoReach";
        countryData.gridOptions = {
          enableVerticalScrollbar: 0,
          enableHorizontalScrollbar: 2
        };
        countryData.gridOptions.rowHeight = 30;
        countryData.gridOptions.columnDefs = getColumnDefs();

        countryData.gridOptions.data = getColumnData();

        $rootScope.$broadcast(appConstants.EVENTS.COUNTRY_DIALOG_BOX, countryData);
      }

      /**
       *
       * @returns {undefined}
       */
      function getColumnDefs() {
        var colArr = [];
        var tmpCol = {};

        //Add Label Col..
        tmpCol = {field: "label", name: "Country", width: 250, enableColumnMenu: false, pinnedLeft: true, enableSorting: false};
        colArr.push(tmpCol);

        //Add Country Col..
        for (var i = 0; i < scope.countries.length; i++) {
          tmpCol = {};
          tmpCol.field = 'country_' + i;
          tmpCol.name = scope.countries[i].countryName;
          tmpCol.width = 110;
          tmpCol.enableColumnMenu = false;
          tmpCol.enablePinning = false;
          tmpCol.enableSorting = false;
          colArr.push(tmpCol);
        }

        //Add Total Col..
        tmpCol = {};
        tmpCol = {field: "total", name: "Total", width: 110, enableColumnMenu: false, pinnedRight: true, enableSorting: false};
        colArr.push(tmpCol);

        return colArr;
      }

      /**
       *
       * @returns {undefined}
       */
      function getColumnData() {

        var dtaArr = [];
        var row_1 = {};
        var colName = "";


        row_1['label'] = "No. of registered users";

        //Add Country Col..
        for (var i = 0; i < scope.countries.length; i++) {
          colName = 'country_' + i;
          row_1[colName] = scope.countries[i].ecoreachUsersReg;
        }

        row_1['total'] = scope.ecoreachTotalUsersReg;


        dtaArr.push(row_1);

        return dtaArr;

      }
    }
  }


})();