/**
 * @ngdoc directive
 * @name tracePartDownload Report directive
 *
 * @description
 *  tracePartDownload Report directive displays all list of information based on counties selection.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding')
          .directive('tracePartDownload', tracePartDownload);

  function tracePartDownload($rootScope, $timeout, appConstants, gaChartUtil, businessReportService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
      },
      templateUrl: 'scripts/directives/tracePartOnline/tracePartDownload.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      scope.countries = [];
      scope.userlist = [];


      scope.businessUnits = appConstants.UPLOAD_FILE.TRACE_PART_BUSINESS_UNIT_DROP_DOWN;
      scope.totalDownloads = 0;
      scope.totalDistinctUsers = 0;
      scope.newUsers = 0;
      scope.isChartReady = true;

      scope.currentYear = (new Date).getFullYear();
      scope.selectedYear = (new Date).getFullYear();
      scope.years = [];

      scope.selectedCountry = '';
      scope.selectedBusinessUnit = '2';
      scope.countryMap = {};

      scope.downloadsByCountries = [];
      scope.topTenProducts = [];
      scope.topTenCountries = [];
      scope.worldWide = true;

      scope.pageLoading = false;

      scope.chartDownloadByCountries = {
        type: 'TreeMap',
        displayed: true,
        data: {
          cols: [{
              id: 'child',
              label: 'Country',
              type: 'string'
            }, {
              id: 'parent',
              label: 'Year',
              type: 'string'
            }, {
              id: 'count',
              label: 'Count',
              type: 'number'
            }],
          rows: []
        },
        options: {
          backgroundColor: '#999',
          minColor: '#f00',
          midColor: '#ddd',
          maxColor: '#0d0',
          fontColor: 'black',
          showScale: true,
          showTooltips: false,
          highlightOnMouseOver: true,
          height: 300,
//          generateTooltip: showChartDownloadTooltip
        }
      };

      function showChartDownloadTooltip(row, size, value) {
        var d = scope.chartDownloadByCountries.data.rows[row];
        return "<div class='treemap-tooltip'>" + d.c[0].v + " : " + d.c[2].v + "</div>";
      }

      scope.chartTopProductDownloads = {
        type: 'BarChart',
        data: {
          cols: [{
              id: 'product',
              label: 'Products',
              type: 'string'
            }, {
              id: 'count',
              label: 'Download count',
              type: 'number'
            }],
          rows: []
        },
        options: {
          height: 450,
          legend: {
            position: 'top'
          },
          vAxis: {
            title: 'Part numbers'
          },
          hAxis: {
            title: 'Download count'
          }
        }
      };

      scope.chartTrendTopProductDownloads = {
        type: 'BarChart',
        data: {
          cols: [{
              id: 'product',
              label: 'Products',
              type: 'string'
            }, {
              id: 'count',
              label: 'Download count',
              type: 'number'
            }],
          rows: []
        },
        options: {
          height: 450,
          legend: {
            position: 'top'
          },
          vAxis: {
            title: 'Part numbers'
          },
          hAxis: {
            position: 'top',
            title: 'Download count'
          }
        }
      };

      scope.chartTrendDownloads = {
        type: 'ColumnChart',
        data: {
          cols: [{
              id: 'date',
              label: 'Download date',
              type: 'string'
            }, {
              id: 'count',
              label: 'Download count',
              type: 'number'
            }],
          rows: []
        },
        options: {
          height: 450,
          legend: {
            position: 'top'
          },
          vAxis: {
            title: 'Download count'
          }
        }
      };

      scope.chartDistinctUsersCad = {
        type: 'ColumnChart',
        data: {
          cols: [{
              id: 'date',
              label: 'Date',
              type: 'string'
            }, {
              id: 'dlcount',
              label: 'All Users',
              type: 'number'
            }, {
              id: 'usercount',
              label: 'New Users',
              type: 'number'
            }],
          rows: []
        },
        options: {
          height: 450,
          legend: {
            position: 'top'
          },
          vAxis: {
            title: 'Download count'
          }
        }
      };

      function init() {
        // init years
        scope.pageLoading = true;
        initYears();
        // get countries
        initTrendCountries();
        // get user stats
        initTotals();

        // create total graphs
        createMainGraphs();

        //create trend grap
        createTrendGraphs();
        scope.pageLoading = false;
      }

      function initYears() {
        for (var y = appConstants.DATES.APP_START_YEAR; y <= scope.currentYear; y++) {
          scope.years.push(y);
        }
      }

      function initTrendCountries() {
        scope.countryMap = {};
        businessReportService.getTracepartsTrendCountries(scope.selectedBusinessUnit).then(function (data) {
          scope.countries = data.filter(function (obj, indx) {
            if (obj.country) {
              return true;
            }
          });
          scope.countries.forEach(function (obj, indx) {
            scope.countryMap[obj.countrycode] = obj.country;
          });
        });
      }

      function initTotals() {
        businessReportService.getTracepartsTotals(scope.selectedBusinessUnit, scope.selectedYear).then(function (data) {
          scope.totalDownloads = data.dlcount;
          scope.totalDistinctUsers = data.distinctusercount;
          scope.newUsers = data.newusers;
        });
      }

      scope.getSelectedCountryName = function () {
        return scope.selectedCountry ? scope.countryMap[scope.selectedCountry] : 'World wide';
      };

      function createDownloadsTreeMap() {
        var dlTreeMap = scope.chartDownloadByCountries;
        dlTreeMap.data.rows = [{
            c: [
              {v: scope.selectedYear.toString()},
              {v: ''},
              {v: 0}
            ]
          }];
        dlTreeMap.data.rows.slice(1);
        scope.downloadsByCountries.forEach(function (obj, indx) {
          var row = {c: []};
          row.c.push({v: obj.country + " - " + obj.percent + ' %'});
          row.c.push({v: scope.selectedYear.toString()});
          row.c.push({v: obj.dlcount});
          dlTreeMap.data.rows.push(row);
        });
      }

      function createTopTenProductsChart() {
        var topTenProdChart = scope.chartTopProductDownloads;
        topTenProdChart.data.rows = [];
        scope.topTenProducts.forEach(function (product, indx) {
          var row = {c: []};
          row.c.push({v: product.partnumber});
          row.c.push({v: product.dlcount});
          topTenProdChart.data.rows.push(row);
        });
      }

      scope.setSelectedYear = function (year) {
        scope.selectedYear = year;
        scope.pageLoading = true;
        initTotals();
        createMainGraphs();
        scope.pageLoading = false;
      };

      scope.setSelectedCountry = function (country) {
        scope.selectedCountry = country.countrycode;
        scope.worldWide = false;
        createTrendGraphs();
      };

      scope.isSelectedCountry = function (country) {
        return scope.selectedCountry === country.countrycode;
      };

      scope.setWorldwide = function () {
        scope.selectedCountry = '';
        scope.worldWide = true;
        createTrendGraphs();
      };

      scope.isSelectedYear = function (year) {
        return scope.selectedYear === year;
      };

      scope.setBusinessUnit = function (businessUnit) {
        scope.selectedBusinessUnit = businessUnit;
        scope.pageLoading = true;
        createMainGraphs();
        createTrendGraphs();
        scope.pageLoading = false;
      };

      scope.isBusinessUnit = function (businessUnit) {
        return scope.selectedBusinessUnit === businessUnit;
      };

      scope.onChartOptionChange = function () {
        scope.worldWide = false;
        createTrendGraphs();
      };

      function createMainGraphs() {
        scope.downloadsByCountries = [];
        scope.topTenProducts = [];
        // get country-wise downloads - totals
        businessReportService.getTracepartsDownloadList(scope.selectedBusinessUnit, scope.selectedYear)
                .then(function (data) {
                  scope.downloadsByCountries = data;
                  var total = 0;
                  scope.downloadsByCountries.forEach(function (obj, indx) {
                    total += obj.dlcount;
                    obj.percent = 0;
                  });
                  // compute percentage
                  scope.downloadsByCountries.forEach(function (obj, indx) {
                    obj.percent = (obj.dlcount / total * 100).toFixed(2);
                  });

                  for (var i = 0; i < 10; i++) {
                    var country = scope.downloadsByCountries[i];
                    scope.topTenCountries.push(country);
                  }
                  createDownloadsTreeMap();
                });
        // get top 10 product downloads
        businessReportService.getTracepartsTopTenProductDownloads(scope.selectedBusinessUnit, scope.selectedYear)
                .then(function (data) {
                  scope.topTenProducts = data;
                  createTopTenProductsChart();
                });

      }

      function createTrendGraphs() {
        scope.trendTopTenCountries = [];
        scope.trendDownloads = [];
        scope.trendTopTenProducts = [];
        // get top ten countries by downloads
        businessReportService.getTracepartsTrendTopTenCountries(scope.selectedBusinessUnit)
                .then(function (data) {
                  scope.trendTopTenCountries = data;
                });

        // get trend downloads
        businessReportService.getTracepartsTrendDownloads(scope.selectedBusinessUnit, scope.selectedCountry)
                .then(function (data) {
                  scope.trendDownloads = data;
                  createTrendDownloadsChart();
                });

        // get trend users
        businessReportService.getTracepartsTrendDownloadsAndUsers(scope.selectedBusinessUnit, scope.selectedCountry)
                .then(function (data) {
                  scope.trendDownloadAndUsers = data;
                  createTrendDownloadsUsersChart();
                });
        // get trend top ten products
        businessReportService.getTracepartsTrendTopTenProducts(scope.selectedBusinessUnit, scope.selectedCountry)
                .then(function (data) {
                  scope.trendTopTenProducts = data;
                  createTrendTopProductsChart();
                });
      }

      function createTrendDownloadsChart() {
        var trendChart = scope.chartTrendDownloads;
        var trendData = scope.trendDownloads;

        trendChart.data.rows = [];
        trendData.forEach(function (download, indx) {
          var row = {c: []};
          row.c.push({v: download.dldate});
          row.c.push({v: download.dlcount});
          trendChart.data.rows.push(row);
        });
      }

      function createTrendDownloadsUsersChart() {
        var trendChart = scope.chartDistinctUsersCad;

        var downloadAndUsersData = scope.trendDownloadAndUsers;

        trendChart.data.rows = [];
        downloadAndUsersData.forEach(function (download, indx) {
          var row = {c: []};
          row.c.push({v: download.chartdate});
          row.c.push({v: download.dlcount});
          row.c.push({v: download.usercount});
          trendChart.data.rows.push(row);
        });
      }

      function createTrendTopProductsChart() {
        var topTenProdChart = scope.chartTrendTopProductDownloads;
        topTenProdChart.data.rows = [];
        scope.trendTopTenProducts.forEach(function (product, indx) {
          var row = {c: []};
          row.c.push({v: product.partnumber});
          row.c.push({v: product.dlcount});
          topTenProdChart.data.rows.push(row);
        });
      }

      $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER);

      init();

      $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
    } //


  } //End Function tracePartDownload

})();