/**
 * @ngdoc directive
 * @name E_PLAN Report directive
 *
 * @description
 *  E_PLAN Report directive displays all list of information based on counties selection.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding')
          .directive('wiseup', wiseup);

  function wiseup($rootScope, $timeout, appConstants, commonUtils, gaChartUtil, wiseupService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
      },
      templateUrl: 'scripts/directives/wiseup/wiseup.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var DEFAULT_CHART_NAME = "ChartRegisteredUsers";
      var CHART_LABELS = {
        REG_USERS: 'Registered Users',
        VISITORS: 'Visitors',
        TARGET_VISITORS: 'Target Visitors',
        PROD_ADDED: 'Products Added To Basket'
      };

      var timer = null;

      //------------------------------------------------------------------
      //---------------------------- Global Properties -------------------
      //------------------------------------------------------------------

      scope.months = appConstants.DATES.MONTH_ARR_LONG;
      scope.selectedMonth = "";

      scope.years = [];
      scope.selectedYear = '';

      var baseReportData = {orders: 0, ordersTarget: 0, invoicedAmount: 0};

      scope.reportData = baseReportData;
      scope.reportData.chartData = {};

      scope.mnmChartName = DEFAULT_CHART_NAME;
      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      scope.chartSource = {};
      scope.mnmChartLabel = "";

      scope.isChartReady = false;
      scope.hasChartData = false;

      var chartObj = {};

      var reportData = {};
      var reportTotals = {};

      var chartOptions = {
        fill: 20,
        areaOpacity: 0.1,
        legend: {
          position: 'top'
        },
        lineWidth: 4,
        pointShape: "circle",
        pointSize: 6,
        colors: ['#058DC7'],
        chartArea: {
          left: 20,
          top: 50,
          width: '93%',
          height: '75%'
        },
        vAxis: {
          textPosition: 'in'
        },
        hAxis: {
          textPosition: 'out',
          showTextEvery: 3
        }
      };

      //------------------------------------------------------------------
      //--------- E_PLAN CHART INIT SHOW HIDE PROPERTIES --------------
      //------------------------------------------------------------------




      //------------------------------------------------------------------
      //-------------------  For Chart TYPE Options  ---------------------
      //------------------------------------------------------------------


      init();



      /**
       * Function to initialized the repors
       *
       * @returns {undefined}
       */
      function init() {

        initValues();
        // get ad-hoc data
        getData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChartDataAndSource);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }

      /**
       *
       * @returns {undefined}
       */
      function initValues() {
        addAllParamsInDropDowns();
        setDefaultDropDownValues();
      }

      /**
       *
       * @returns {undefined}
       */
      function getData() {
        wiseupService.getData().then(function (data) {
          reportData = data;
          parseData();
        });
      }

      function parseData() {
        for (var year in reportData) {
          var yeardata = reportData[year];
          for (var m in yeardata) {
            var data = yeardata[m];
            reportTotals[year] = reportTotals[year] || baseReportData;
            reportTotals[year].orders += data.orders;
            reportTotals[year].ordersTarget += data.ordersTarget;
            reportTotals[year].invoicedAmount += data.invoicedAmount;
          }
        }
        scope.showOrdersChart();
      }

      /**
       *
       * @returns {undefined}
       */
      function addAllParamsInDropDowns() {
        scope.years = angular.copy(appConstants.DATES.WISEUP_YEARS);
        scope.selectedYear = scope.years[0];
        scope.years.sort(function (a, b) {
          return a < b;
        });
      }

      /**
       *
       * @returns {undefined}
       */
      function setDefaultDropDownValues() {
        scope.selectedMonth = scope.months[0];
        scope.selectedYear = scope.years[0];
      }

      scope.showOrdersChart = function () {
        scope.chartName = 'ChartOrders';
        scope.isChartReady = false;
        var year = scope.selectedYear;
        chartObj.type = 'ComboChart';
        chartObj.data = {
          "cols": [
            {id: 'months', label: 'Months', type: 'string'},
            {id: 'count', label: 'Orders', type: 'number'},
            {id: 'target', label: 'Target', type: 'number'}
          ],
          "rows": []
        };
        chartObj.options = angular.copy(chartOptions);
        chartObj.options.series = {0: {type: 'bars'}, 1: {type: 'line'}};
        chartObj.options.colors = ['#337AB7', '#999'];
        for (var month in reportData[year]) {
          var data = reportData[year][month];
          var row = {c: []};
          var tm = {v: data.month + " '" + year.toString().substr(-2)};
          var tv = {v: data.orders};
          var tvtar = {v: data.ordersTarget};
          row.c.push(tm, tv, tvtar);
          chartObj.data.rows.push(row);
        }
        scope.chartSource = chartObj;
        scope.isChartReady = true;
      }
      ;

      scope.showInvoicedAmountChart = function () {
        scope.chartName = 'ChartInvoicedAmount';
        scope.isChartReady = false;
        var year = scope.selectedYear;
        chartObj.type = 'AreaChart';
        chartObj.data = {
          "cols": [
            {id: 'months', label: 'Months', type: 'string'},
            {id: 'amount', label: 'Invoiced Amount', type: 'number'}
          ],
          "rows": []
        };
        chartObj.options = angular.copy(chartOptions);
        chartObj.options.colors = ['#058DC7'];
        for (var month in reportData[year]) {
          var data = reportData[year][month];
          var row = {c: []};
          var tm = {v: data.month + " '" + year.toString().substr(-2)};
          var ta = {v: data.invoicedAmount};
          row.c.push(tm, ta);
          chartObj.data.rows.push(row);
        }
        scope.chartSource = chartObj;
        scope.isChartReady = true;

      };

      scope.ifChartReady = function () {
        return scope.isChartReady;
      };

      /**
       *  Init COLUMN  Chart Options..
       *
       * @returns {undefined}
       */
      function initColumnChartOption() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        scope.isChartReady = false;
        chartObj.data = {};

        //Start Timer
        timer = $timeout(function () {
          console.log("ecoReach.js :: loadDefaultChart :: Timer :: Start ");

          reloadChartDataAndSource();

          scope.isChartReady = true;

        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoReach.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChartDataAndSource() {
        //Set Chart Data
        chartObj.data = scope.reportData.chartData;
        //Set value to Scope
        scope.chartSource = chartObj;
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }

      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.chartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }
    }
  }


})();