/**
 * @ngdoc directive
 * @name Quick Ref directive
 *
 * @description
 *  Quick Ref directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('quickRef', quickRef);

  function quickRef($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, QuickRefCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        vm: "=vm"
      },
      templateUrl: 'scripts/directives/quickRef/quickRef.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Country1'];


      var _quickRefReportData = null;
      var _quickRefChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var DEFAULT_CHART_NAME = "ChartActiveUser";

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];

      scope.uniqueActvUsers = 0;
      scope.configLaunched = 0;
      scope.regUsers = "NA";
      scope.launchDate = appConstants.DATES.QUICK_REF_LAUNCHED_DATE;

      scope.quickRefLaunchDays = 0;
      scope.quickRefLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, appConstants.DATES.QUICK_REF_LAUNCHED_DATE);

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_CONFIGURATION_LAUNCHED = "No. of Configuration launched";


      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};


      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.quickRefChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.quickRefChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.quickRefChartName = DEFAULT_CHART_NAME;
      scope.quickRefChartLabel = NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;

      scope.onQuickRefChartOptionChange = onQuickRefChartOptionChange;
      scope.onQuickRefChartLinkChange = onQuickRefChartLinkChange;

      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        //--- Init Countries data
        initCountries();
        // Get Report Data
        getQuickRefReportData();
        // Get Chart Data
        getQuickRefChartData();

        //Add Event Listener  When page resize
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }

      /**
       * Get Quick Ref report data from NODE
       *
       * @returns {undefined}
       */
      function getQuickRefReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getQuickRefReportData().then(
                function (result) {
                  logger.log("Quick Ref::getQuickRefReportData::Result::Success");
                  _quickRefReportData = result;
                  parseReportData(_quickRefReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _quickRefReportData = null;
                  logger.log("Quick Ref::getQuickRefReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get Quick Ref CHART data from NODE
       *
       * @returns {undefined}
       */
      function getQuickRefChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getQuickRefChartData().then(
                function (result) {
                  logger.log("Quick Ref::getQuickRefChartData::Result::Success");
                  _quickRefChartData = result;
                  parseChartData(_quickRefChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _quickRefChartData = null;
                  logger.log("Quick Ref::getQuickRefChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("quickRef.js :: loadDefaultChart :: Timer :: Start ");
          onQuickRefChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("quickRef.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }



      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var quickRef = {};
        //--- 1) Country 1
        for (var i = 0; i < countryNameList.length; i++) {
          quickRef.countryName = countryNameList[i];
          quickRef.countryGaId = appConstants.SITE_GA_IDS.QUICK_REF_ID;
          quickRef.launchDate = appConstants.DATES.QUICK_REF_LAUNCHED_DATE;
          quickRef.launchDateStr = appConstants.DATES.QUICK_REF_LAUNCHED_DATE_STR;
          quickRef.regUsers = 0;
          quickRef.uniqueActiveUsers = 0;
          quickRef.configLaunched = 0;
          quickRef.uniqueActiveUsersChart = 0;
          quickRef.configLaunchedChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new QuickRefCountryVO(quickRef));
        }
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload QuickRef Chart.." + scope.selectedChartTab);
        onQuickRefChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
        scope.countries[0].configLaunched = parseFloat(reportData.configLaunched.totalsForAllResults['ga:totalEvents']);

        //----------------- SET Default Value ---------------------
        scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
        scope.configLaunched = scope.countries[0].configLaunched;
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        scope.countries[0].configLaunchedChart = gaChartUtil.populateGAChartData(chartData.configLaunched, "dateFormatted", "configLaunched", "Date", "Configuration Launched");
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onQuickRefChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.quickRefChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.quickRefChartName = chartName;
        onQuickRefChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onQuickRefChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.quickRefChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.quickRefChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.quickRefChartTypeSelected;
        var chartName = scope.quickRefChartName;

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.quickRefChartLabel = NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartConfigLaunched":
          {
            scope.quickRefChartLabel = NO_OF_CONFIGURATION_LAUNCHED;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].configLaunchedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].configLaunchedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].configLaunchedChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.quickRefChartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }

    }//END Link..

  }
  ;
})();