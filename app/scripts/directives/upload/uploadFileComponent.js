/**
 * @ngdoc directive
 * @name Upload File Component directive
 *
 * @description
 *  Upload File Component directive.
 *
 */

(function () {
  'use strict';
  angular.module('se-branding').controller('confirmBox', confirmBox);
  confirmBox.$inject = ['$rootScope'];
  function confirmBox($rootScope) {
    $rootScope.cancel = function (param) {
      this.closeThisDialog(0);
    }
    $rootScope.confirm = function (param) {
      this.closeThisDialog(1);
    }
  }

  angular.module('se-branding').directive('uploadFileComponent', [
    '$rootScope', 'appConstants', 'logger', '$timeout', 'authUtil', '$window',
    'summaryDataService', 'nodeGAService', '$q', 'commonUtils', 'fileUploadService',
    'businessReportService', 'ngDialog', '$mdDialog',
    function ($rootScope, appConstants, logger, $timeout, authUtil,
              $window, summaryDataService, nodeGAService, $q,
              commonUtils, fileUploadService, businessReportService, ngDialog, $mdDialog) {
      return {
        restrict: 'E',
        replace: true,
        scope: {},
        templateUrl: 'scripts/directives/upload/uploadFileComponent.html',
        link: function (scope, elem, attrs) {

          var TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE = appConstants.NOTE.TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE;
          var TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG = appConstants.MESSAGE.TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG;
          var TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG = appConstants.MESSAGE.TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG;
          var TRACE_PART_UPLOAD_FAIL_PARSE_FAIL_MSG = appConstants.MESSAGE.TRACE_PART_UPLOAD_FAIL_PARSE_FAIL_MSG;
          var MY_PACT_FILE_UP_LOAD_NOTE = "Please download the sample file, update data and then upload it.";
          var MONTH_ARR_LONG = appConstants.DATES.MONTH_ARR_LONG;
          var DATA_YEARS = appConstants.DATES.DATA_YEARS_FILE_UPLOAD;
          var PROJECT_EPLAN = 2;
          var EPLAN_REPORT_TYPES = {
            1: 'company',
            3: 'product'
          };
          var dataExists = false;
          var selectedEplan = false;
          var rewriteData = 0;
          var loggedInUser = {};

          scope.projectListBase = angular.copy(appConstants.UPLOAD_FILE.PROJECT_LIST_DROP_DOWN);
          scope.projectList = angular.copy(appConstants.UPLOAD_FILE.PROJECT_LIST_DROP_DOWN);
          scope.selectedProject = scope.projectListBase[0];
          scope.onProjectListChange = onProjectListChange;
          scope.isDisableProjListDropDn = true;
          scope.tracePartBusinessUnit = appConstants.UPLOAD_FILE.TRACE_PART_BUSINESS_UNIT_DROP_DOWN;
          scope.tracePartSelectedBusinessUnit = "";
          scope.eplanReportType = appConstants.UPLOAD_FILE.EPLAN_REPORT_TYPE_DROP_DOWN;
          scope.eplanSelectedReportType = "";
          scope.onEplanReportTypeChange = onEplanReportTypeChange;
          scope.eplanProdGrpReportType = [];
          scope.ePlanDataSummary = {};
          scope.eplanSelectedProdGrpReportType = "";
          scope.isVisibleEplanProductGroupReport = false;
          scope.isVisibleEplanProductGroupFootNote = false;
          scope.onEplanProdGrpReportTypeChange = onEplanProdGrpReportTypeChange;
          scope.mnmReportType = appConstants.UPLOAD_FILE.MNM_REPORT_TYPE_DROP_DOWN;
          scope.mnmSelectedReportType = "";
          scope.onMnmReportTypeChange = onMnmReportTypeChange;
          scope.isVisibleSampleFileDiv = false;
          scope.isVisibleMonthDiv = false;
          scope.isVisibleYearDiv = false;
          scope.isVisibleBusinessUnit = false;
          scope.isVisibleEplanReportDropDown = false;
          scope.isVisibleMnmReportDropDown = false;
          scope.fileUploadNote = TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE;
          scope.uploadedFile = null;
          scope.monthArr = MONTH_ARR_LONG;
          scope.selectedMonth = "";
          scope.yearArr = DATA_YEARS;
          scope.selectedYear = "";
          scope.fileNamePath = "";
          scope.submitForm = submitForm;
          scope.resetForm = resetForm;
          //FOR FILE UPLOADING
          scope.percentUploaded = 0;
          scope.isUploading = false;
          scope.uploadComplete = false;
          scope.uploadMessage = "";
          scope.uploadMessageHeaderLabel = "";
          scope.downloadSampleFile = downloadSampleFile;
          init();
          /**
           *
           * @returns {undefined}
           */
          function init() {
            //Show Loader
            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
            fileUploadService.getEplanDataSummary().then(function (data) {
              if (data) {
                scope.ePlanDataSummary = data;
              }
            });
            //Get Data for getEplanProductGroupList
            fileUploadService.getEplanProductGroupList()
              .then(
                function (data) {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  if (data.productGroups !== undefined || data.productGroups !== null) {
                    if (data.productGroups.length > 0) {
                      scope.isVisibleEplanProductGroupFootNote = false;
                      scope.eplanProdGrpReportType = data.productGroups;
                    } else {
                      scope.isVisibleEplanProductGroupFootNote = true;
                      scope.eplanProdGrpReportType = [];
                    }
                  } else {
                    scope.isVisibleEplanProductGroupFootNote = true;
                    scope.eplanProdGrpReportType = [];
                  }

                  //Get Data for
                  getSelectedProjectByUser();
                  //Show Hide Div as per selected Project
                  showHideProjectDiv();
                },
                function () {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
              );
          }

          /**
           *`
           * @returns {undefined}
           */
          function getSelectedProjectByUser() {
//						var loggedInUser = authUtil.getUserDetails();
            loggedInUser = $rootScope.loginUser;
            scope.loggedInUser = loggedInUser;
            var adminUsers = appConstants.ADMIN_ID;
            var tmpAdmin = null;
            var loggedInUserSESA_Id = loggedInUser ? loggedInUser.id : null;
            var loggedInUserEmailId = loggedInUser ? loggedInUser.email : null;
            var adminAccess = "-1";
            for (var i = 0; i < adminUsers.length; i++) {
              tmpAdmin = adminUsers[i];
              if (tmpAdmin.id === loggedInUserSESA_Id || tmpAdmin.email === loggedInUserEmailId) {
                adminAccess = tmpAdmin.access;
                break;
              }
            }
            //Get the drop down list item on the basis of access righths
            getProjectListDropDown(adminAccess);
            //Show - Hide Divs on the basis of selected items
            onProjectListChange();
          }


          /**
           *
           *
           */
          angular.element("#uploadBtn").on('change', function () {
            var target = this.value;
            target = target.split("\\");
            var fName = target.pop();
            logger.log("Uploaded  File :: " + fName);
            if (!/(\.xlsx|\.xls)$/i.test(fName)) {
              //scope.fileNamePath = "";
              fName = "";
              this.value = null;
              scope.uploadedFile = null;
              //Show Error POPUP
              $rootScope.$broadcast(appConstants.EVENTS.SHOW_ERROR,
                {title: "Error", message: TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE});
            } else {
              scope.uploadedFile = this.files[0];
            }
            scope.$apply(function () {
              scope.fileNamePath = fName;
            });
          });
          function uploadAfterConfirm() {
            if (scope.uploadForm.$valid) {
              //Hide upload complete message
              scope.uploadComplete = false;
              scope.uploadMessage = "";
              //Set uploading -- for HIDE spinner
              scope.isUploading = true;
              //Send Data
              var dataToBeUpload = new FormData();
              dataToBeUpload.append('file', scope.uploadedFile);
              var uploadURL = getFileUploadURL();
              //fileUploadService.uploadTracePartFile(dataToBeUpload, qryString).then(
              uploadTracePartFile(dataToBeUpload, uploadURL).then(
                function (data) {
                  //Re-Load Data from server
                  resetAndReloadData();
                  //Set uploading -- for HIDE spinner
                  scope.isUploading = false;
                  //Show upload complete message
                  scope.uploadComplete = true;
                  //Parse and display Uploaded message
                  parseAndDisplayUploadedMessage(data);
                  //Update EplanSummary
                  if (!dataExists && selectedEplan) {
                    updateEplanDataSummary();
                  }
                  //Print output
                  logger.log(data);
                  //Reset Form..
                  resetForm();
                },
                function (err) {
                  scope.isUploading = false;
                  scope.uploadComplete = true;
                  scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_FAIL_PARSE_FAIL_MSG;
                  console.log('----> @236: err: ', err);
                  scope.uploadMessage = parseTracePartUploadErrorMessage(err);
                  //Reset Form..
                  //resetForm();
                  logger.log(err);
                }
              );
            } else {
              console.log('\n::: error in form :::\n');
            }
          }

          /**
           *
           * @returns {undefined}
           */
          function submitForm() {
            dataExists = false;
            selectedEplan = false;

            if (scope.selectedProject.value === PROJECT_EPLAN) {
              selectedEplan = true;
              var reportType = EPLAN_REPORT_TYPES[scope.eplanSelectedReportType.value];
              var dataForSelectedYear = scope.ePlanDataSummary[reportType][scope.selectedYear];
              if (dataForSelectedYear !== undefined) {
                var dataForSelectedMonth = dataForSelectedYear[scope.selectedMonth.value];
                if ((dataForSelectedMonth !== undefined)) {
                  if (reportType === 'company' || dataForSelectedMonth[scope.eplanSelectedProdGrpReportType] !== undefined) {
                    dataExists = true;
                    scope.uploadForm.$valid = false;
                  }
                }

                if (dataExists) {
                  // ngDialog.open({
                  //   template: 'scripts/directives/upload/uploadConfirm.html',
                  //   showClose: false,
                  //   closeByDocument: false,
                  //   controller: ['$rootScope', confirmBox]
                  // }).closePromise.then(function (param) {
                  //   if (param.value === 1) {
                  //     scope.uploadForm.$valid = true;
                  //     rewriteData = 1;
                  //     uploadAfterConfirm();
                  //   }
                  // });
                  $mdDialog.show({
                    templateUrl: 'scripts/directives/upload/uploadConfirm.html',
                    controller: function ($scope, $mdDialog) {
                      $scope.cancel = function () {
                        $mdDialog.hide();
                      };

                      $scope.continue = function () {
                        $mdDialog.hide();
                        scope.uploadForm.$valid = true;
                        rewriteData = 1;
                        uploadAfterConfirm();
                      }
                    }
                  });
                }
              }
            }
            if (!dataExists) {
              rewriteData = 0;
              uploadAfterConfirm();
            }
          }


          /**
           *
           * @returns {undefined}
           */
          function resetForm() {
            //$("#uploadForm")[0].reset();
            document.getElementById("uploadForm").reset();
            //Reset Values
            scope.uploadedFile = null;
            angular.element("#uploadBtn").value = null;
            scope.fileNamePath = "";
            scope.tracePartSelectedBusinessUnit = "";
            scope.selectedMonth = "";
            scope.selectedYear = "";
            //Call Init
            init();
          }


          /**
           *
           * @returns {undefined}
           */
          function onProjectListChange() {
            logger.log(scope.selectedProject);
            // alert(JSON.stringify(scope.selectedProject));
            showHideProjectDiv();
            updateFileUploadNote();
            initDropdownSelectedValues();
          }


          /**
           *
           * @returns {undefined}
           */
          function showHideProjectDiv() {
            scope.isVisibleSampleFileDiv = false;
            scope.isVisibleMonthDiv = false;
            scope.isVisibleYearDiv = false;
            scope.isVisibleBusinessUnit = false;
            scope.isVisibleEplanReportDropDown = false;
            scope.isVisibleMnmReportDropDown = false;
            scope.isRegisteredUsersUpload = false;
            // TODO:: NEED TO OPTIMIZED THIS IF ELSE change to SWITH CASE
            if (scope.selectedProject.value === scope.projectListBase[0].value) { //"Trace Part Online"
              scope.isVisibleMonthDiv = true;
              scope.isVisibleYearDiv = true;
              scope.isVisibleBusinessUnit = true;
            } else if (scope.selectedProject.value === scope.projectListBase[1].value) { //"EcoReach"
              scope.isVisibleMonthDiv = true;
              scope.isVisibleYearDiv = true;
              scope.isVisibleSampleFileDiv = true;
            } else if (scope.selectedProject.value === scope.projectListBase[2].value) { //"EPlan"
              scope.isVisibleEplanReportDropDown = true;
              scope.isVisibleMonthDiv = true;
              scope.isVisibleYearDiv = true;
            } else if (scope.selectedProject.value === scope.projectListBase[3].value) { //"BIM"
              scope.isVisibleMonthDiv = true;
              scope.isVisibleYearDiv = true;
            } else if (scope.selectedProject.value === scope.projectListBase[4].value) { //"MnM"
              scope.isVisibleMnmReportDropDown = true;
              scope.isVisibleMonthDiv = true;
              scope.isVisibleYearDiv = true;
            } else if (scope.selectedProject.value === scope.projectListBase[5].value) { //"Registered Users"
              scope.isRegisteredUsersUpload = true;
              scope.isVisibleMnmReportDropDown = false;
              scope.isVisibleMonthDiv = false;
              scope.isVisibleYearDiv = false;
            }

          }


          /**
           *  TODO:: Optimized Later
           * @returns {undefined}
           */
          function updateFileUploadNote() {

            // TODO:: NEED TO OPTIMIZED THIS IF ELSE change to SWITH CASE
            if (scope.selectedProject.value === scope.projectListBase[0].value) { //"Trace Part Online"
              scope.fileUploadNote = TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE;
            } else if (scope.selectedProject.value === scope.projectListBase[1].value) { //"EcoReach"
              scope.fileUploadNote = MY_PACT_FILE_UP_LOAD_NOTE;
            } else if (scope.selectedProject.value === scope.projectListBase[2].value) { //"EPlan"
              scope.fileUploadNote = TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE;
            } else if (scope.selectedProject.value === scope.projectListBase[3].value) { //"BIM"
              scope.fileUploadNote = TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE;
            } else if (scope.selectedProject.value === scope.projectListBase[4].value) { //"BIM"
              scope.fileUploadNote = TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE;
            }
          }


          /**
           *
           * @returns {undefined}
           */
          function initDropdownSelectedValues() {
            scope.isVisibleEplanProductGroupReport = false;
            scope.eplanSelectedReportType = "";
          }


          /**
           *
           * @param {type} data
           * @returns {undefined}
           */
          function parseAndDisplayUploadedMessage(data) {
            var statusObj = "";
            var statusCode = "";
            var statusMessage = "";
            // TODO:: NEED TO OPTIMIZED THIS IF ELSE change to SWITH CASE
            if (scope.selectedProject.value === scope.projectListBase[0].value) { //"Trace Part Online"
              statusObj = parseTracePartUploadSuccessMessage(data);
              statusCode = statusObj.statusCode;
              statusMessage = statusObj.statusMessage;
              if (parseInt(statusCode) === 200) {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG;
                scope.uploadMessage = statusMessage;
              } else {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG;
                scope.uploadMessage = statusMessage;
              }

            } else if (scope.selectedProject.value === scope.projectListBase[1].value) { //"EcoReach"

              if (typeof (data) === 'string') {
                data = JSON.parse(data);
              }
              statusObj = data;
              statusCode = statusObj.statusCode;
              statusMessage = statusObj.statusMessage;
              if (parseInt(statusCode) === 200) {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG;
                //scope.uploadMessage = statusMessage;
                //scope.uploadMessage = "No Error.";
              } else {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG;
                scope.uploadMessage = statusMessage;
              }

            } else if (scope.selectedProject.value === scope.projectListBase[2].value) { //"EPlan"

              if (typeof (data) === 'string') {
                data = JSON.parse(data);
              }
              statusObj = data;
              statusCode = statusObj.statusCode;
              statusMessage = statusObj.statusMessage;
              if (parseInt(statusCode) === 200) {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG;
                //scope.uploadMessage = statusMessage;
                //scope.uploadMessage = "No Error.";
              } else {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG;
                scope.uploadMessage = statusMessage;
              }

            } else if (scope.selectedProject.value === scope.projectListBase[3].value) { //"BIM"

              if (typeof (data) === 'string') {
                data = JSON.parse(data);
              }
              statusObj = data;
              statusCode = statusObj.statusCode;
              statusMessage = statusObj.statusMessage;
              if (parseInt(statusCode) === 200) {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG;
                //scope.uploadMessage = statusMessage;
                //scope.uploadMessage = "No Error.";
              } else {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG;
                scope.uploadMessage = statusMessage;
              }
            } else if (scope.selectedProject.value === scope.projectListBase[4].value) { //"MnM"

              if (typeof (data) === 'string') {
                data = JSON.parse(data);
              }
              statusObj = data;
              statusCode = statusObj.statusCode;
              statusMessage = statusObj.statusMessage;
              if (parseInt(statusCode) === 200) {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG;
                //scope.uploadMessage = statusMessage;
                //scope.uploadMessage = "No Error.";
              } else {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG;
                scope.uploadMessage = statusMessage;
              }
            } else if (scope.selectedProject.value === scope.projectListBase[5].value) { //"Registered Users"

              if (typeof (data) === 'string') {
                data = JSON.parse(data);
              }
              statusObj = data;
              statusCode = statusObj.statusCode;
              statusMessage = statusObj.statusMessage;
              if (parseInt(statusCode) === 200) {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG;
                //scope.uploadMessage = statusMessage;
                //scope.uploadMessage = "No Error.";
              } else {
                scope.uploadMessageHeaderLabel = TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG;
                scope.uploadMessage = statusMessage;
              }
            }

          }


          /**
           *
           * @returns {undefined}
           */
          function getFileUploadURL() {
            var URL = "";
            var qryStr = "";
            var finalURL = "";
            // var uploadedByUser = authUtil.getUserDetails();
            var uploadedByUser = loggedInUser;
            var uploadedByEmail = uploadedByUser.email;
            // TODO:: NEED TO OPTIMIZED THIS IF ELSE change to SWITH CASE
            if (scope.selectedProject.value === scope.projectListBase[0].value) { //"Trace Part Online"
              URL = commonUtils.getNodeRESTFulURL('TRACE_PART_FILE_UPLOAD_URL');
              qryStr = "?month=" + scope.selectedMonth.value + "&year=" + scope.selectedYear
//                                        + "&businessUnit=" + scope.tracePartSelectedBusinessUnit.value + "&uploadedBy=" + uploadedByEmail;
                + "&businessUnit=" + scope.tracePartSelectedBusinessUnit + "&uploadedBy=" + uploadedByEmail;
              // console.log('\n==> TRACE_PART_FILE_UPLOAD_URL: ', qryStr, '\ntracePartSelectedBusinessUnit: ', scope.tracePartSelectedBusinessUnit);
            } else if (scope.selectedProject.value === scope.projectListBase[1].value) { //"EcoReach"
              URL = commonUtils.getNodeRESTFulURL('ECO_REACH_FILE_UPLOAD_URL');
              qryStr = "?month=" + scope.selectedMonth.value + "&year=" + scope.selectedYear
                + "&uploadedBy=" + uploadedByEmail;
            } else if (scope.selectedProject.value === scope.projectListBase[2].value) { //"EPlan"
              URL = commonUtils.getNodeRESTFulURL('EPLAN_FILE_UPLOAD_URL');
              qryStr = "?month=" + scope.selectedMonth.value + "&year=" + scope.selectedYear
                + "&reportType=" + scope.eplanSelectedReportType.value
                + "&productGroup=" + scope.eplanSelectedProdGrpReportType
                + "&rewriteData=" + rewriteData
                + "&uploadedBy=" + uploadedByEmail;
            } else if (scope.selectedProject.value === scope.projectListBase[3].value) { //"BIM"
              URL = commonUtils.getNodeRESTFulURL('BIM_FILE_UPLOAD_URL');
              qryStr = "?month=" + scope.selectedMonth.value + "&year=" + scope.selectedYear
                + "&uploadedBy=" + uploadedByEmail;
            } else if (scope.selectedProject.value === scope.projectListBase[4].value) { //"MnM"
              URL = commonUtils.getNodeRESTFulURL('MNM_FILE_UPLOAD_URL');
              qryStr = "?month=" + scope.selectedMonth.value + "&year=" + scope.selectedYear
                + "&reportType=" + scope.mnmSelectedReportType.value
                + "&uploadedBy=" + uploadedByEmail;
            } else if (scope.selectedProject.value === scope.projectListBase[5].value) { //"Registered users"
              URL = commonUtils.getNodeRESTFulURL('REGISTERED_USERS_UPLOAD_URL');
              qryStr = "?uploadedBy=" + uploadedByEmail;
            }


            finalURL = URL + qryStr;
            // console.log("File Upload URL :: " + finalURL);
            return finalURL;
          }

          /**
           *
           * @param {type} errObj
           * @returns {errObj.errMsg}
           */
          function parseTracePartUploadErrorMessage(errObj) {
            var finalErrMsg = "";
            if (errObj) {
              if (typeof (errObj) === 'string') {
                errObj = JSON.parse(errObj);
              }
              var finalErrMsg = "";
              var errMsg = errObj.errMsg;
              var sheet = errObj.sheet;
              finalErrMsg = errMsg + " on Sheet " + sheet;
              return errMsg;
            }
//                            return errMsg;
            return finalErrMsg;
          }


          /**
           *
           * @param {type} dataObj
           * @returns {uploadFileComponent_L14.uploadFileComponentAnonym$0.link.parseTracePartUploadSuccessMessage.retuunrObj}
           */
          function parseTracePartUploadSuccessMessage(dataObj) {
            var successMsg = "";
            if (typeof (dataObj) === 'string') {
              dataObj = JSON.parse(dataObj);
            }

            var msgObj = dataObj[8][0];
            var statusCode = msgObj["@statusCode"];
            var statusMessage = msgObj["@statusMsg"];
            statusMessage = statusMessage.replace("TPUM", " New users");
            statusMessage = statusMessage.replace("TPUVS", "\n User Visits");
            statusMessage = statusMessage.replace("TPUDS", "\n CAD Downloads ");
            var retuunrObj = {"statusCode": statusCode, "statusMessage": statusMessage};
            return retuunrObj;
          }

          /**
           *
           * @param {type} proj
           * @returns {undefined}
           */
          function downloadSampleFile(proj) {
            // alert(proj);
            // console.log(proj);
            var fileName = "";
            if (proj === scope.projectListBase[1]) {
              fileName = "EchoReachUsers.xlsx";
            }
            var url = appConstants.PACE_LOGIN_REDIRECT_URL[$rootScope.env].APP_URL + "/upload-sample-files/" + fileName;
            var myWindow = $window.open(url, "_blank");
            //myWindow.close();
          }

          /**
           *
           * @param {type} adminAccess
           * @returns {undefined}
           */
          function getProjectListDropDown(adminAccess) {
            var projList = angular.copy(scope.projectListBase);
            var tId = "";
            scope.projectList = [];
            for (var i = 0; i < adminAccess.length; i++) {
              tId = adminAccess[i];
              scope.projectList.push(projList[tId]);
            }
            //Set First Item Selected
            scope.selectedProject = scope.projectList[0];
            if (scope.projectList.length > 1) {
              scope.isDisableProjListDropDn = false;
            } else {
              scope.isDisableProjListDropDn = true;
            }
          }


          /**
           *
           * @returns {undefined}
           */
          function onEplanReportTypeChange() {
            scope.eplanSelectedProdGrpReportType = "";
            // Object {value: 3, label: "Product Group Details Report"}
            if (scope.eplanSelectedReportType.value === 3) {
              scope.isVisibleEplanProductGroupReport = true;
            } else {
              scope.isVisibleEplanProductGroupReport = false;
            }
          }

          /**
           *
           * @returns {undefined}
           */
          function onEplanProdGrpReportTypeChange() {
            console.dir(scope.eplanSelectedProdGrpReportType);
          }


          /**
           *
           * @returns {undefined}
           */
          function onMnmReportTypeChange() {
            console.dir(scope.mnmSelectedReportType);
          }

          //----------------------------------------------------------------------
          //----------------------------------------------------------------------
          //----------------------------------------------------------------------
          //----------------------------------------------------------------------

          /**
           *
           * @param {type} dataToBePost
           * @param {type} url
           * @returns {$q@call;defer.promise}
           */
          function uploadTracePartFile(dataToBePost, url) {
            var deferred = $q.defer();
            scope.percentUploaded = 0;
            var request = new XMLHttpRequest();
            //-- On Ready State Change
            request.onreadystatechange = function () {
              if (request.readyState === 4) {
                try {
                  var resp = request.response;
                  if (request.status === 200) {
                    deferred.resolve(resp);
                  } else {
                    deferred.reject(resp);
                  }
                } catch (e) {
                  deferred.reject(e);
                }
              }
            };
            // -- For Upload  Progress
            request.upload.addEventListener('progress', function (e) {
              var loaded = parseFloat(e.loaded);
              var total = parseFloat(e.total);
              var progressVal = (commonUtils.roundToTwoDecimal(loaded / total) * 100);
              $timeout(function () {
                logger.log("File Uploaded .." + progressVal + " %");
                scope.percentUploaded = Math.ceil(progressVal);
              }, 500);
            }, false);
            request.open('POST', url);
            request.send(dataToBePost);
            return deferred.promise;
          }


          //----------------------------------------------------
          //----------------------------------------------------
          //----------------------------------------------------


          function resetAndReloadData() {
            //Reset KPI Summary Page Data
            resetKpiSummaryData();
            //Reset Details page Data
            if (scope.selectedProject.value === scope.projectListBase[0].value) { //"Trace Part Online"
              //
            } else if (scope.selectedProject.value === scope.projectListBase[1].value) { //"EcoReach"
              reloadEcoReachData();
            } else if (scope.selectedProject.value === scope.projectListBase[2].value) { //"EPlan"
              reloadEPlanData();
            } else if (scope.selectedProject.value === scope.projectListBase[3].value) { //"BIM"
              reloadBIMData();
            } else if (scope.selectedProject.value === scope.projectListBase[4].value) { //"MnM"
              reloadMnMData();
            }
          }


          /**
           * Reset KPI Summary Data
           *
           * @returns {undefined}
           */
          function resetKpiSummaryData() {
            summaryDataService.resetKPI_SummaryData();
          }


          /**
           * reLoad EcoReach Data
           *
           * @returns {undefined}
           */
          function reloadEcoReachData() {
            // Get Report Data
            getEcoReachReportData();
            // Get Chart Data
            getEcoReachChartData();
          }


          /**
           * Get EcoReach report data from NODE
           *
           * @returns {undefined}
           */
          function getEcoReachReportData() {
            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
            nodeGAService.getEcoReachReportData(true).then(
              function (result) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                logger.log("uploadFileComponent::getEcoReachReportData::Result::Success");
              },
              function (error) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                logger.log("uploadFileComponent::getEcoReachReportData::Result::FAIL");
              }
            );
          }

          /**
           * Get EcoReach CHART data from NODE
           *
           * @returns {undefined}
           */
          function getEcoReachChartData() {
            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
            nodeGAService.getEcoReachChartData(true).then(
              function (result) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                logger.log("uploadFileComponent::getEcoReachChartData::Result::Success");
              },
              function (error) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                logger.log("uploadFileComponent::getEcoReachChartData::Result::Success");
              }
            );
          }


          /**
           * reLoad ePlan Data
           *
           * @returns {undefined}
           */
          function reloadEPlanData() {
            reloadEplanProductGroupData();
            getEplanCountryList();
          }


          /**
           *
           * @returns {undefined}
           */
          function reloadEplanProductGroupData() {

            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
            fileUploadService.getEplanProductGroupList()
              .then(
                function (data) {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                  if (data.productGroups !== undefined || data.productGroups !== null) {
                    if (data.productGroups.length > 0) {
                      scope.isVisibleEplanProductGroupFootNote = false;
                      scope.eplanProdGrpReportType = data.productGroups;
                    } else {
                      scope.isVisibleEplanProductGroupFootNote = true;
                      scope.eplanProdGrpReportType = [];
                    }
                  } else {
                    scope.isVisibleEplanProductGroupFootNote = true;
                    scope.eplanProdGrpReportType = [];
                  }
                },
                function () {
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
              );
          }


          /**
           *
           * @returns {undefined}
           */
          function getEplanCountryList() {

            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
            businessReportService.getEplanCountryList(true).then(
              function (data) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              },
              function (Err) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              }
            );
          }


          /**
           *
           * @returns {undefined}
           */
          function reloadBIMData() {

            reloadBIMObjectProductData();
            getBIMObjectCountryList();
          }

          function reloadBIMObjectProductData() {

            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
            businessReportService.getBIMProductList(true).then(
              function (data) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              },
              function (Err) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              }
            );
          }

          function getBIMObjectCountryList() {
            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
            businessReportService.getBIMCountryList(true).then(
              function (data) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              },
              function (Err) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              }
            );
          }

          /**
           *
           * @returns {undefined}
           */
          function reloadMnMData() {

            getMnMCountryList();
          }

          function getMnMCountryList() {

            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
            businessReportService.getMnMCountryList(true).then(
              function (data) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              },
              function (Err) {
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              }
            );
          }

          function updateEplanDataSummary() {
            var reportType = EPLAN_REPORT_TYPES[scope.eplanSelectedReportType.value];
            var dataForSelectedYear = scope.ePlanDataSummary[reportType][scope.selectedYear];
            if (dataForSelectedYear === undefined) {
              scope.ePlanDataSummary[reportType][scope.selectedYear] = {};
            }
            if (reportType === 'company') {
              scope.ePlanDataSummary[reportType][scope.selectedYear][scope.selectedMonth.value] = 1;
            } else {
              scope.ePlanDataSummary[reportType][scope.selectedYear][scope.selectedMonth.value] = {};
              scope.ePlanDataSummary[reportType][scope.selectedYear][scope.selectedMonth.value][scope.eplanSelectedProdGrpReportType] = 1;
            }
          }
        }
      };
    }]);
})();
