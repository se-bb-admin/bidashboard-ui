/**
 * @ngdoc directive
 * @name Global Meter  directive
 *
 * @description
 *  Global Meter  directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('closedLoop', closedLoop);

  function closedLoop($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, ClosedLoopCountryVO, closedLoopService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
      },
      templateUrl: 'scripts/directives/closedLoop/closedLoop.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Country1'];
      var _globalMeterReportData = null;
      var _globalMeterChartData = null;
      var registeredOrdersData = {};
      var registeredOrdersDataTotal = {};
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var timer = null;
      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      scope.uniqueActiveUsers = 0;
      scope.installationsNumber = 0;
      scope.opportunitiesCreated = 0;
      scope.opportunitiesAcceptedByElectrician = 0;
      scope.opportunitiesRejectedByElectrician = 0;
      scope.opportunitiesWon = 0;
      scope.opportunitiesWonVal = 0;

      scope.totalRegisteredOrders = 0;

      scope.launchDate = appConstants.DATES.GLOBAL_METER_APP_LAUNCHED_DATE;

      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_INSTALLATION = "No of installations";
      var NO_OF_OPPT_WON = "No of opportunities won";
      var NO_OF_OPPT_INST = "No of opportunities installed";
      var DEFAULT_CHART_NAME = "ChartNoOfInstallation";
      var NO_OF_OPPORTUNITIES_ACCEPTED_BY_ELECTRICIAN = "No of opportunities accepted by electrician";
      var NO_OF_OPPORTUNITIES_CREATED = "No of opportunities created";
      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};
      var selectedYear = (new Date).getFullYear();

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.globalMeterTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.closedLoopChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.chartName = DEFAULT_CHART_NAME;
      scope.closedLoopChartLabel = NO_OF_INSTALLATION;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;
      scope.onClosedLoopChartOptionChange = onClosedLoopChartOptionChange;
      scope.onClosedLoopChartLinkChange = onClosedLoopChartLinkChange;
      scope.chartReady = chartReady;
      init();
      /**
       *
       * @returns {undefined}
       */
      function init() {
        // Init Countries data
        initCountries();
        // Get Report Data
        getClosedLoopReportData();
        // Get Chart Data
        getClosedLoopChartData();
        // get registered orders data
        getRegisteredOrdersData();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);
        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }

      /**
       * Get Global Meter report data from NODE
       *
       * @returns {undefined}
       */
      function getClosedLoopReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getClosedLoopReportData().then(
                function (result) {
                  logger.log("Global Meter::getClosedLoopReportData::Result::Success");
                  _globalMeterReportData = result;
                  parseReportData(_globalMeterReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _globalMeterReportData = null;
                  logger.log("Global Meter::getClosedLoopReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get Global Meter CHART data from NODE
       *
       * @returns {undefined}
       */
      function getClosedLoopChartData() {
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        nodeGAService.getClosedLoopChartData().then(
                function (result) {
                  logger.log("Global Meter::getClosedLoopChartData::Result::Success");
                  _globalMeterChartData = result;
                  parseChartData(_globalMeterChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _globalMeterChartData = null;
                  logger.log("Global Meter::getClosedLoopChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function getRegisteredOrdersData() {
        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
        closedLoopService.getRegisteredOrdersData().then(function (data) {
          for (var year in data) {
            registeredOrdersData[year] = data[year].map(function (obj, indx) {
              obj.month = obj.month + " '" + selectedYear.toString().substr(-2);
              return obj;
            });
            for (var d in data[year]) {
              var dObj = data[year][d];
              registeredOrdersDataTotal.registeredOrders = registeredOrdersDataTotal.registeredOrders || 0;
              registeredOrdersDataTotal.registeredOrders += dObj.registeredOrders;
            }
          }
          scope.totalRegisteredOrders = registeredOrdersDataTotal.registeredOrders;
          $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
        });
      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();
        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("globalMeter.js :: loadDefaultChart :: Timer :: Start ");
          onClosedLoopChartLinkChange(DEFAULT_CHART_NAME);
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("globalMeter.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }

      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var closedLoop = {};
        //--- clipsalWishlist
        for (var i = 0; i < countryNameList.length; i++) {
          closedLoop.countryName = countryNameList[i];
          closedLoop.countryGaId = appConstants.SITE_GA_IDS.GLOBAL_METER_APP_ID;
          closedLoop.launchDate = appConstants.DATES.GLOBAL_METER_APP_LAUNCHED_DATE;
          closedLoop.launchDateStr = appConstants.DATES.GLOBAL_METER_APP_LAUNCHED_DATE_STR;
          closedLoop.regUsers = 0;
          scope.uniqueActiveUsers = 0;
          scope.installationsNumber = 0;
          scope.opportunitiesCreated = 0;
          scope.opportunitiesAcceptedByElectrician = 0;
          scope.opportunitiesRejectedByElectrician = 0;
          scope.opportunitiesWon = 0;
          scope.opportunitiesWonVal = 0;
          scope.uniqueActiveUsersChart = 0;
          scope.installationsNumberChart = 0;
          scope.opportunitiesCreatedChart = 0;
          scope.opportunitiesAcceptedByElectricianChart = 0;
          scope.opportunitiesRejectedByElectricianChart = 0;
          scope.opportunitiesWonChart = 0;
          scope.opportunitiesWonValChart = 0;



          //--- Push country to Countries Array
          scope.countries.push(new ClosedLoopCountryVO(closedLoop));
        }
      }

      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload Global Meter  Chart.." + scope.selectedChartTab);
        onClosedLoopChartOptionChange(null, scope.selectedChartTab);
      }

      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
        scope.countries[0].installationsNumber = parseFloat(reportData.installationsNumber.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].opportunitiesCreated = parseFloat(reportData.opportunitiesCreated.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].opportunitiesAcceptedByElectrician = parseFloat(reportData.opportunitiesAcceptedByElectrician.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].opportunitiesRejectedByElectrician = parseFloat(reportData.opportunitiesRejectedByElectrician.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].opportunitiesWon = parseFloat(reportData.opportunitiesWon.totalsForAllResults['ga:totalEvents']);
        scope.countries[0].opportunitiesWonVal = 0;
        //scope.countries[0].opportunitiesWonVal = parseFloat(reportData.opportunitiesWonVal.totalsForAllResults['ga:totalEvents']);
        //----------------- SET Default Value ---------------------

        scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
        scope.installationsNumber = scope.countries[0].installationsNumber;
        scope.opportunitiesCreated = scope.countries[0].opportunitiesCreated;
        scope.opportunitiesAcceptedByElectrician = scope.countries[0].opportunitiesAcceptedByElectrician;
        scope.opportunitiesRejectedByElectrician = scope.countries[0].opportunitiesRejectedByElectrician;
        scope.opportunitiesWon = scope.countries[0].opportunitiesWon;
        scope.opportunitiesWonVal = scope.countries[0].opportunitiesWonVal;
      }

      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
        scope.countries[0].installationsNumberChart = gaChartUtil.populateGAChartData(chartData.installationsNumber, "dateFormatted", "installationsNumber", "Date", "No of installations");
        scope.countries[0].opportunitiesCreatedChart = gaChartUtil.populateGAChartData(chartData.opportunitiesCreated, "dateFormatted", "opportunitiesCreated", "Date", "No of opportunities created");
        scope.countries[0].opportunitiesAcceptedByElectricianChart = gaChartUtil.populateGAChartData(chartData.opportunitiesAcceptedByElectrician, "dateFormatted", "opportunitiesAcceptedByElectrician", "Date", "No of opportunities accepted by electrician");
        scope.countries[0].opportunitiesRejectedByElectricianChart = gaChartUtil.populateGAChartData(chartData.opportunitiesRejectedByElectrician, "dateFormatted", "opportunitiesRejectedByElectrician", "Date", "No of opportunities rejected by electrician");
        scope.countries[0].opportunitiesWonChart = gaChartUtil.populateGAChartData(chartData.opportunitiesWon, "dateFormatted", "opportunitiesWon", "Date", "No of opportunities won");
        scope.countries[0].opportunitiesWonValChart = null;
        //scope.countries[0].opportunitiesWonValChart = gaChartUtil.populateGAChartData(chartData.opportunitiesWonVal, "dateFormatted", "opportunitiesWonVal", "Date", "Value of opportunities won");

        console.log("Chart Data.. Prepare...");
      }

      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onClosedLoopChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.closedLoopChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.chartName = chartName;
        onClosedLoopChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onClosedLoopChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;
        scope.selectedChartTab = selTab;
        scope.closedLoopChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.closedLoopChartToolTip = scope.chartToolTips[scope.selectedChartTab];
        var chartType = scope.closedLoopChartTypeSelected;
        var chartName = scope.chartName;
        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.closedLoopChartLabel = NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartNoOfInstallation":
          {
            scope.closedLoopChartLabel = NO_OF_INSTALLATION;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].installationsNumberChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].installationsNumberChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].installationsNumberChart.month;
            }
            break;
          }
          case "ChartNoOfOpportunitiesCreated":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.closedLoopChartLabel = "";//NO_OF_OPPORTUNITIES_CREATE;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].opportunitiesCreatedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].opportunitiesCreatedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].opportunitiesCreatedChart.month;
            }
            break;
          }
          case "ChartNoOfOpportunitiesAcceptedByElectrician":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.closedLoopChartLabel = NO_OF_OPPORTUNITIES_ACCEPTED_BY_ELECTRICIAN;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].opportunitiesAcceptedByElectricianChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].opportunitiesAcceptedByElectricianChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].opportunitiesAcceptedByElectricianChart.month;
            }
            break;
          }

          case "ChartNoOfOpportunitiesRejectedByElectrician":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.closedLoopChartLabel = "";//NO_OF_OPPORTUNITIES_REJECTED_BY_ELECTRICIAN;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].opportunitiesRejectedByElectricianChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].opportunitiesRejectedByElectricianChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].opportunitiesRejectedByElectricianChart.month;
            }
            break;
          }
          case "ChartNoOfOpportunitiesWon":
          {
            scope.closedLoopChartLabel = NO_OF_OPPT_WON;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].opportunitiesWonChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].opportunitiesWonChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].opportunitiesWonChart.month;
            }
            break;
          }
          case "ChartValueOfOpportunitiesWon":
          {
            scope.closedLoopChartLabel = "";//VALUE_OF_OPPT_WON;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[0].opportunitiesWonValChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[0].opportunitiesWonValChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[0].opportunitiesWonValChart.month;
            }
            break;
          }
          case "ChartRegisteredOrders":
          {
            scope.closedLoopChartLabel = NO_OF_OPPORTUNITIES_CREATED;
            var chartData = angular.copy(registeredOrdersData[selectedYear]);
            chartObj.data = gaChartUtil.populateTwoFieldChartData(chartData, 'month', 'registeredOrders', 'Month', 'Registered Orders');
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }



      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.chartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }


    }//END Link..
  }


})();