/**
 * @ngdoc directive
 * @name Quick Ref directive
 *
 * @description
 *  Quick Ref directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('smartSelectors', smartSelectors);

  function smartSelectors($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, SmartSelectorsCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/smartSelectors/smartSelectors.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      var countryNameList = ['Belgium', 'Brazil', 'France', 'Germany',
        'Indonesia', 'Italy', 'Netherlands', 'Russia', 'South Korea',
        'Spain', 'Switzerland', 'Turkey', 'United Kingdom', 'United States',
        'Canada', 'India', 'Australia', 'China', 'Japan'];


      var _smartSelectorsReportData = null;
      var _smartSelectorsChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];

      scope.uniqueActvUsers = 0;
      scope.configLaunched = 0;
      scope.regUsers = 0;
      scope.launchDate = appConstants.DATES.SMART_SELECTORS_LAUNCHED_DATE;
      scope.smartSelectorsLaunchDays = 0;

      scope.disableUniqueActiveUsers = false;
      scope.disableConfLaunched = false;

      //------------------------------------------------------------------
      //-------------- MY PACT CHART INIT SHOW HIDE PROPERTIES -----------
      //------------------------------------------------------------------
      scope.isGAChartVisible = true;
      scope.isPaceChartVisible = false;

      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";
      var NO_OF_ACTIVE_USERS = "No. of unique active users";
      var NO_OF_CONFIGURATION_LAUNCHED = "No. of Configuration launched";

      scope.onCountryDropDownChange = onCountryDropDownChange;

      scope.paceChartStyle = commonUtils.getPaceChartStyle();
      scope.paceChartSource = {};
      scope.showPaceChart = showPaceChart;
      var isInitPaceChartData = false;
      var paceChartObj = {};

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};


      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.smartSelectorsChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.smartSelectorsChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.smartSelectorsChartName = "ChartActiveUser";
      scope.smartSelectorsChartLabel = NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.loadDefaultChart = false;

      scope.onSmartSelectorsChartOptionChange = onSmartSelectorsChartOptionChange;
      scope.onSmartSelectorsChartLinkChange = onSmartSelectorsChartLinkChange;

      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        //--- Init Countries data
        initCountries();
        // Get Report Data
        getSmartSelectorsReportData();
        // Get Chart Data
        getSmartSelectorsChartData();

        //Add Event Listener  When page resize
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.smartSelectorsUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initPaceChartData();
          }
        });

      }

      /**
       * Get Quick Ref report data from NODE
       *
       * @returns {undefined}
       */
      function getSmartSelectorsReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getSmartSelectorsReportData().then(
                function (result) {
                  logger.log("Quick Ref::getSmartSelectorsReportData::Result::Success");
                  _smartSelectorsReportData = result;
                  parseReportData(_smartSelectorsReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _smartSelectorsReportData = null;
                  logger.log("Quick Ref::getSmartSelectorsReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get Quick Ref CHART data from NODE
       *
       * @returns {undefined}
       */
      function getSmartSelectorsChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getSmartSelectorsChartData().then(
                function (result) {
                  logger.log("Quick Ref::getSmartSelectorsChartData::Result::Success");
                  _smartSelectorsChartData = result;
                  parseChartData(_smartSelectorsChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _smartSelectorsChartData = null;
                  logger.log("Quick Ref::getSmartSelectorsChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("smartSelectors.js :: loadDefaultChart :: Timer :: Start ");
          onSmartSelectorsChartLinkChange('ChartActiveUser');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("smartSelectors.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initPaceChartData() {
        paceChartObj = {};
        paceChartObj.type = "AreaChart";
        paceChartObj.displayed = false;
        paceChartObj.options = commonUtils.getPaceChartOptions();

        isInitPaceChartData = true;
      }

      /***
       *  Onclick Registererd users link click..
       *  Show PACE Chart Hide GA Chart
       *
       * @returns {undefined}
       *
       */
      function showPaceChart() {
        scope.isGAChartVisible = false;
        scope.isPaceChartVisible = true;

        //Show Hide Chart
        onSmartSelectorsChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }



      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var smartSelectors = {};
        for (var i = 0; i < countryNameList.length; i++) {
          smartSelectors.countryName = countryNameList[i];

          var capsCountryName = constructName(countryNameList[i].toUpperCase());
          var launchDate = 'SMART_SELECTORS_' + capsCountryName + '_LAUNCHED_DATE';

          smartSelectors.launchDate = appConstants.DATES[launchDate];//appConstants.DATES.SMART_SELECTORS_LAUNCHED_DATE;
          smartSelectors.smartSelectorsLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, smartSelectors.launchDate);

          smartSelectors.regUsers = 0;
          smartSelectors.uniqueActiveUsers = 0;
          smartSelectors.configLaunched = 0;
          smartSelectors.uniqueActiveUsersChart = 0;
          smartSelectors.configLaunchedChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new SmartSelectorsCountryVO(smartSelectors));
        }

        //Set default Country to update data binding
        setDefaultCountry();
      }


      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }

      /**
       *
       * @param {type} name
       * @returns {unresolved}
       */
      function constructName(name) {
        var result = name.split(' ').join('_');
        return result;
      }


      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;
        logger.log(selectedCountryName);
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.launchDate = scope.countries[index].launchDate;
        scope.smartSelectorsLaunchDays = scope.countries[index].smartSelectorsLaunchDays;

        setTableDataAndLink('UNIQUE_ACTIVE_USERS', scope.countries[index].uniqueActiveUsers);
        setTableDataAndLink('CONFIG_LAUNCHED', scope.countries[index].configLaunched);

        onSmartSelectorsChartLinkChange('ChartActiveUser');
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload SmartSelectors Chart.." + scope.selectedChartTab);
        onSmartSelectorsChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {

        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];

          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          scope.countries[i].configLaunched = parseFloat(reportData[keyName].configLaunched.totalsForAllResults['ga:totalEvents']);
        }
        //----------------- SET Default Value ---------------------
        scope.smartSelectorsLaunchDays = scope.countries[0].smartSelectorsLaunchDays;
        setTableDataAndLink('UNIQUE_ACTIVE_USERS', scope.countries[0].uniqueActiveUsers);
        setTableDataAndLink('CONFIG_LAUNCHED', scope.countries[0].configLaunched);
      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          scope.countries[i].configLaunchedChart = gaChartUtil.populateGAChartData(chartData[keyName].configLaunched, "dateFormatted", "configLaunched", "Date", "Configuration Launched");
        }
      }


      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onSmartSelectorsChartLinkChange(chartName) {
        scope.isGAChartVisible = true;
        scope.isPaceChartVisible = false;

        //When you click on a link Chart will reset to Month Chart.
        scope.smartSelectorsChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.smartSelectorsChartName = chartName;
        onSmartSelectorsChartOptionChange(null, chartOptionDfltSelInx);
      }


      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onSmartSelectorsChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.smartSelectorsChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.smartSelectorsChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.smartSelectorsChartTypeSelected;
        var chartName = scope.smartSelectorsChartName;

        var countryPrefix = selectedCountryName + " : ";
        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        if (isInitChartData === false) {
          initChartData();
        }

        //check for PACE Chart
        if (scope.isPaceChartVisible) {
          scope.quickQuotationChartLabel = NO_OF_REG_USERS;

          loadPaceChart(scope.selectedChartTab);
          return;
        }


        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.smartSelectorsChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartConfigLaunched":
          {
            scope.smartSelectorsChartLabel = countryPrefix + NO_OF_CONFIGURATION_LAUNCHED;
            //Add New Chart
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].configLaunchedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].configLaunchedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].configLaunchedChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




      /**
       * Load PACE Chart as per selected tab (Day | Week | Month)
       *
       * @param {type} selTab
       * @returns {undefined}
       */
      function loadPaceChart(selTab) {
        var chartTypeSelected = scope.chartOptions[scope.selectedChartTab];

        //TODO :: Also Need To Check "scope.paceDataRegUserOverPeriod.myPactBasicUsersReg" val exist or not..
        if (isInitPaceChartData === false) {
          initPaceChartData();
        }

        if (chartTypeSelected.toLowerCase() === 'day') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.smartSelectorsUsersReg.day;
        } else if (chartTypeSelected.toLowerCase() === 'week') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.smartSelectorsUsersReg.week;
        } else if (chartTypeSelected.toLowerCase() === 'month') {
          paceChartObj.data = scope.paceDataRegUserOverPeriod.smartSelectorsUsersReg.month;
        }

        //Set value to Scope
        scope.paceChartSource = paceChartObj;
      }


      /**
       *
       * @param {type} filedName
       * @param {type} value
       * @returns {undefined}
       */
      function setTableDataAndLink(filedName, value) {
        if (filedName === "UNIQUE_ACTIVE_USERS") {
          if (value === 0) {
            scope.disableUniqueActiveUsers = true;
            angular.element("#uniqueActiveUsers").removeClass("link");
          } else {
            scope.disableUniqueActiveUsers = false;
            angular.element("#uniqueActiveUsers").addClass("link");
          }
          scope.uniqueActiveUsers = value;

        } else if (filedName === "CONFIG_LAUNCHED") {
          if (value === 0) {
            scope.disableConfLaunched = true;
            angular.element("#configLaunched").removeClass("link");
          } else {
            scope.disableConfLaunched = false;
            angular.element("#configLaunched").addClass("link");
          }
          scope.configLaunched = value;
        }
      }




    }//END Link..

  }
  ;
})();