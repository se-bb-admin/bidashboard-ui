/**
 * @ngdoc directive
 * @name SafeRepository directive
 *
 * @description
 *  SafeRepository....
 *
 */

(function () {
	'use strict';

	angular.module('se-branding')
			.directive('saferepository', safeRepository);

	function safeRepository($rootScope, $timeout, appConstants, commonUtils, logger,
			gaChartUtil, nodeGAService, currencyConverterService, paceService) {

		var directive = {
			restrict: 'E',
			replace: true,
			scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
			templateUrl: 'scripts/directives/safeRepository/safeRepository.html',
			link: link
		};
		return directive;


		/**
		 *
		 * @param {type} scope
		 * @param {type} elem
		 * @param {type} attrs
		 * @returns {undefined}
		 */
		function link(scope, elem, attrs) {
			var countryNameList = ['France'];
			var _safeRepositoryReportData = null;
			var _safeRepositoryChartData = null;
			var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
			var timer = null;
			// alert("\nlink::: paceDataRegUserOverPeriod :: " + JSON.stringify(scope.paceDataRegUserOverPeriod));

			//------------------------------------------------------------------
			//--------------------------- Global Properties --------------------
			//------------------------------------------------------------------

			scope.countries = [];
			scope.selectedCountry = {};
			var selectedCountryName = countryNameList[0];
			scope.safeRepositoryRegisteredUsers = 0;
			scope.loadDefaultChart = false;

			scope.getSelectedRowChartCSS = getSelectedRowChartCSS;


			//------------------------------------------------------------------
			//--------------- MY PACT CHART INIT SHOW HIDE PROPERTIES ----------
			//------------------------------------------------------------------

			var NO_OF_REGISTERED_USERS = "No. of registered users";
			var DEFAULT_CHART_NAME = "RegisteredUsers";
			scope.chartSource = {};
			var isInitChartData = false;
			var chartObj = {};

			//------------------------------------------------------------------
			//----------------------  For Chart Options ------------------------
			//------------------------------------------------------------------

			scope.selectedChartTab = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
			scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
			scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
			scope.safeRepositoryChartTypeSelected = scope.chartOptions[scope.selectedChartTab].toLowerCase();
			scope.safeRepositoryChartToolTip = scope.chartToolTips[scope.selectedChartTab];
			scope.safeRepositoryChartName = DEFAULT_CHART_NAME;
			scope.safeRepositoryChartLabel = NO_OF_REGISTERED_USERS;//NO_OF_ACTIVE_USERS;
			scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
			scope.isfootNoteSymbolVisible = true;
			scope.disableUniqueActiveUsers = false;
			scope.onSafeRepositoryChartOptionChange = onSafeRepositoryChartOptionChange;
			scope.onSafeRepositoryChartLinkChange = onSafeRepositoryChartLinkChange;
			scope.chartReady = chartReady;


			init();


			/**
			 *
			 * @returns {undefined}
			 */
			function init() {
				// Init Countries data
				initCountries();
				// Get Chart Data
        initChartData();
				getSafeRepositoryChartData();
				//Add Event Listener
				var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);
				//Remove event Listener on Destroy
				scope.$on('$destroy', listenerReloadChart);
			}

			/**
			 * Get SafeRepository CHART data from NODE
			 *
			 * @returns {undefined}
			 */
			function getSafeRepositoryChartData() {
        initChartData();
				// loadDefaultChart();
			}


			/**
			 *
			 *  Init PACE Default Chart Options..
			 *
			 *
			 * @returns {undefined}
			 */
			function initChartData() {
				chartObj = {};
				chartObj.type = "AreaChart";
				chartObj.displayed = false;
				chartObj.options = commonUtils.getPaceChartOptions();
				// isInitChartData = true;
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function loadDefaultChart() {
				initChartData();

				//Start Timer
				timer = $timeout(function () {
					console.log("safeRepository.js :: loadDefaultChart :: Timer :: Start ");
					// onSafeRepositoryChartLinkChange(DEFAULT_CHART_NAME);
          onSafeRepositoryChartOptionChange(null, scope.selectedChartTab);
					scope.loadDefaultChart = true;
				}, CHART_LOAD_DELAY);
				//Destroy Timer
				scope.$on("$destroy",
						function (event) {
							console.log("safeRepository.js :: loadDefaultChart :: Timer :: Stop ");
              scope.loadDefaultChart = false;
							$timeout.cancel(timer);
						}
				);
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function chartReady() {
				fixGoogleChartsBarsBootstrap();
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function fixGoogleChartsBarsBootstrap() {
				$(".google-visualization-table-table img[width]").each(function (index, img) {
					$(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
				});
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function initCountries() {
				var safeRepository = {};
			}


			/**
			 * Set default Country
			 * For Updating data binding.
			 * @returns {undefined}
			 */
			function setDefaultCountry() {
				scope.selectedCountry = scope.countries[0];
				onSafeRepositoryChartLinkChange(DEFAULT_CHART_NAME);
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function reloadChart() {
				logger.log("Reload SafeRepository Chart.." + scope.safeRepositoryChartTypeSelected);
				//->
				onSafeRepositoryChartOptionChange(null, scope.selectedChartTab);
			}
			/**
			 *
			 * @param {type} chartData
			 * @returns {undefined}
			 */
			function parseChartData(chartData) {
				for (var i = 0; i < countryNameList.length; i++) {
					var keyName = countryNameList[i];
					scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
				}
				console.log("Chart Data.. Prepare...");
			}


			/**
			 *
			 * @param {type} chartName
			 * @returns {undefined}
			 */
			function onSafeRepositoryChartLinkChange(chartName) {
				//When you click on a link Chart will reset to Month Chart.
				var chartTab = scope.selectedChartTab;
				scope.safeRepositoryChartTypeSelected = scope.chartOptions[chartTab].toLowerCase();
				scope.safeRepositoryChartName = chartName;
				onSafeRepositoryChartOptionChange(null, chartTab);
			}


			/**
			 *
			 * @param {type} evnt
			 * @param {type} selTab
			 * @returns {undefined}
			 */
			function onSafeRepositoryChartOptionChange(evnt, selTab) {
				if (evnt !== undefined && evnt !== null) {
					evnt.stopImmediatePropagation();
				}
				scope.isfootNoteSymbolVisible = false;
				scope.selectedChartTab = selTab;
				var chartType = scope.chartOptions[scope.selectedChartTab].toLowerCase();
				scope.safeRepositoryChartToolTip = scope.chartToolTips[scope.selectedChartTab];
				var chartName = scope.safeRepositoryChartName;

				// if (isInitChartData === false) {
					initChartData();
				// }
				switch (chartName) {
					case "RegisteredUsers":
					{
						var countryPrefix = "Total - Worldwide : ";
						scope.isfootNoteSymbolVisible = true;
						scope.safeRepositoryChartLabel = countryPrefix + NO_OF_REGISTERED_USERS;
						//Add New Chart
						chartObj.data = scope.paceDataRegUserOverPeriod.safeRepositoryUsersReg[chartType];
						break;
					}
				}
				//Set value to Scope
				scope.chartSource = chartObj;
			}


			/**
			 *
			 * @param {type} cName
			 * @returns {String}
			 */
			function getSelectedRowChartCSS(cName) {
				var chartName = scope.safeRepositoryChartName;
				var styleName = "";
				if (cName === chartName) {
					styleName = "selected-row-chart ";
				}
				return styleName;
			}
		}//END Link..
	}
})();
