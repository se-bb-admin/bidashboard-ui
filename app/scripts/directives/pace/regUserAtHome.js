/**
 * @ngdoc directive
 * @name BLM directive
 *
 * @description
 *  BLM directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('regUserAtHome', paceRegUser);

  function paceRegUser($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, paceService, summaryDataService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/pace/regUserAtHome.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      scope.CHART_TYPE_YEAR = 'YEAR';
      scope.CHART_TYPE_MONTH = 'MONTH';
      scope.REG_USR_HOME_FOOTER_NOTE = appConstants.NOTE.REG_USR_HOME_FOOTER_NOTE;

      scope.registeredUsersByYear = null;
      scope.registeredUsersByYearChartDataAllYears = null;
      scope.registeredUsersByYearChartDataByYearByQtr = null;
      scope.chartSource = {};
      scope.chartReady = chartReady;
      scope.loadDefaultChart = false;
      scope.getCumulative = getCumulative;

      scope.chartYear = undefined;
      scope.currentChartType = scope.CHART_TYPE_YEAR;
      scope.changeChartType = changeChartType;

      var isInitChartData = false;
      var _tempSummaryObj = null;
      var chartObj = {};

      var timer = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      init();

      function init() {

        _tempSummaryObj = angular.copy(summaryDataService.getSummaryBasicData());

        //Get data from server.
        getRegUserByYear();
      }




      /**
       *
       * @returns {undefined}
       */
      function getRegUserByYear() {
        paceService.getRegUserByYear().then(
                function (result) {
                  logger.log("Summary::PACE:: getRegUserByYear ::Result::Success");
                  var tempRegisteredUsersByYear = angular.copy(result.home.registeredUsersByYear);

                  //-------
                  scope.registeredUsersByYear = margeCurrentYearTargetForHome(tempRegisteredUsersByYear);

                  //Populate Chart
                  scope.registeredUsersByYearChartDataAllYears = gaChartUtil.populateLinearChartData(scope.registeredUsersByYear, "totalRegUsersAchieve", 'year', "Year", "Registered Users");

                  //Load Default Chart
                  loadDefaultChart();
                },
                function (error) {
                  logger.log("Summary::PACE:: getRegUserByYear ::Result::FAIL");
                  //$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          showChartForAllYears();
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  logger.log("blm.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );


      }



      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getGoogleChartOptionsTwoCol();
        //Update showTextEvery property in chart options
        chartObj.options.hAxis.showTextEvery = 1;

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }


      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }



      /**
       *
       * @param {type} idx
       * @returns {unresolved}
       */
      function getCumulative(idx) {
        var total = 0;
        var tmpData = null;

        for (var i = 0; i <= idx; i++) {
          tmpData = scope.registeredUsersByYear[i].totalRegUsersAchieve;
          total += tmpData;
        }

        return total;
      }


      /**
       *
       * @param {type} type
       * @param {type} year
       * @returns {undefined}
       */
      function changeChartType(type, year) {
        scope.chartYear = year;
        scope.currentChartType = type.toUpperCase();

        if (scope.currentChartType === scope.CHART_TYPE_YEAR) {
          showChartForAllYears();
        } else if (scope.currentChartType === scope.CHART_TYPE_MONTH) {
          showChartForYearByQuarter(year);
        }
      }



      function showChartForYearByQuarter(year) {
        var chartYearData = null;
        var tempData = null;
        var rawYearData = null;

        for (var i = 0; i <= scope.registeredUsersByYear.length; i++) {
          tempData = scope.registeredUsersByYear[i];

          if (tempData.year === year) {
            rawYearData = tempData;
            break;
          }
        }


        //Populate Chart
        scope.registeredUsersByYearChartDataByYearByQtr = gaChartUtil.populateGAChartDataTwoCol(rawYearData.users, 'quarter', "regUsersAchieve", "regUsersTarget", "Quarter", "Registered Users Achive", "Registered Users Target");

        chartObj.type = "ColumnChart";

        //Check Options
        //chartObj.options.annotations = {};
        //chartObj.options.annotations.alwaysOutside = true;

        chartObj.options.axisTitlesPosition = "out";

        //Set Chart Data
        chartObj.data = scope.registeredUsersByYearChartDataByYearByQtr;
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      function showChartForAllYears() {
        chartObj.type = "AreaChart";
        //Set Chart Data
        chartObj.data = scope.registeredUsersByYearChartDataAllYears;
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      /**
       *
       * @param {type} regUsersByYear
       * @returns {unresolved}
       */
      function margeCurrentYearTargetForHome(regUsersByYear) {


        var EASY_QUOTE = _tempSummaryObj.EASY_QUOTE;
        var CLIPSAL_WISH_LIST = _tempSummaryObj.CLIPSAL_WISH_LIST;
        var GLOBAL_METER_APP = _tempSummaryObj.GLOBAL_METER_APP;

        var totalTargetQ1 = EASY_QUOTE.Q1Target + CLIPSAL_WISH_LIST.Q1Target + GLOBAL_METER_APP.Q1Target;

        var totalTargetQ2 = EASY_QUOTE.Q2Target + CLIPSAL_WISH_LIST.Q2Target + GLOBAL_METER_APP.Q2Target;

        var totalTargetQ3 = EASY_QUOTE.Q3Target + CLIPSAL_WISH_LIST.Q3Target + GLOBAL_METER_APP.Q3Target;

        var totalTargetQ4 = EASY_QUOTE.Q4Target + CLIPSAL_WISH_LIST.Q4Target + GLOBAL_METER_APP.Q4Target;

        console.log(totalTargetQ1 + " - " + totalTargetQ2 + " - " + totalTargetQ3 + " - " + totalTargetQ4);
        var tempData = null;
        var tempUser = null;
        for (var i = 0; i < regUsersByYear.length; i++) {
          tempData = regUsersByYear[i];

          if (tempData.year !== null && tempData.year !== undefined && tempData.year === 2015) {
            for (var j = 0; j < tempData.users.length; j++) {
              var tempUser = tempData.users[j];

              if (tempUser.quarter === "Q1") {
                tempUser.regUsersTarget = totalTargetQ1;
              } else if (tempUser.quarter === "Q2") {
                tempUser.regUsersTarget = totalTargetQ2;
              } else if (tempUser.quarter === "Q3") {
                tempUser.regUsersTarget = totalTargetQ3;
              } else if (tempUser.quarter === "Q4") {
                tempUser.regUsersTarget = totalTargetQ4;
              }
            }
          } else {
            console.log("Other Than current Year");
          }
        }

        return regUsersByYear;
      }

    }//END Link..
  }


})();