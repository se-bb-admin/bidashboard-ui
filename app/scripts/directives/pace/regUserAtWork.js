/**
 * @ngdoc directive
 * @name BLM directive
 *
 * @description
 *  BLM directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding')
          .directive('regUserAtWork', paceRegUser);

  function paceRegUser($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, paceService, summaryDataService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/pace/regUserAtWork.html',
      link: link
    };
    return directive;



    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {

      scope.CHART_TYPE_YEAR = 'YEAR';
      scope.CHART_TYPE_MONTH = 'MONTH';

      scope.registeredUsersByYear = null;
      scope.registeredUsersByYearChartDataAllYears = null;
      scope.registeredUsersByYearChartDataByYearByQtr = null;
      scope.chartSource = {};
      scope.chartReady = chartReady;
      scope.loadDefaultChart = false;
      scope.getCumulative = getCumulative;

      scope.chartYear = undefined;
      scope.currentChartType = scope.CHART_TYPE_YEAR;
      scope.changeChartType = changeChartType;

      var isInitChartData = false;
      var _tempSummaryObj = null;
      var chartObj = {};

      var timer = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      init();

      function init() {

        _tempSummaryObj = angular.copy(summaryDataService.getSummaryBasicData());

        //Get data from server.
        getRegUserByYear();
      }




      /**
       *
       * @returns {undefined}
       */
      function getRegUserByYear() {
        paceService.getRegUserByYear().then(
                function (result) {
                  logger.log("Summary::PACE:: getRegUserByYear ::Result::Success");
                  var tempRegisteredUsersByYear = angular.copy(result.work.registeredUsersByYear);

                  //-------
                  scope.registeredUsersByYear = margeCurrentYearTargetForWork(tempRegisteredUsersByYear);

                  //Populate Chart
                  scope.registeredUsersByYearChartDataAllYears = gaChartUtil.populateLinearChartData(scope.registeredUsersByYear, "totalRegUsersAchieve", 'year', "Year", "Registered Users");

                  //Load Default Chart
                  loadDefaultChart();
                },
                function (error) {
                  logger.log("Summary::PACE:: getRegUserByYear ::Result::FAIL");
                  //$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //Start Timer
        timer = $timeout(function () {
          showChartForAllYears();
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  logger.log("blm.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );


      }



      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getGoogleChartOptionsTwoCol();
        //Update showTextEvery property in chart options
        chartObj.options.hAxis.showTextEvery = 1;

        isInitChartData = true;
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }


      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }



      /**
       *
       * @param {type} idx
       * @returns {unresolved}
       */
      function getCumulative(idx) {
        var total = 0;
        var tmpData = null;

        for (var i = 0; i <= idx; i++) {
          tmpData = scope.registeredUsersByYear[i].totalRegUsersAchieve;
          total += tmpData;
        }

        return total;
      }


      /**
       *
       * @param {type} type
       * @param {type} year
       * @returns {undefined}
       */
      function changeChartType(type, year) {
        scope.chartYear = year;
        scope.currentChartType = type.toUpperCase();

        if (scope.currentChartType === scope.CHART_TYPE_YEAR) {
          showChartForAllYears();
        } else if (scope.currentChartType === scope.CHART_TYPE_MONTH) {
          showChartForYearByQuarter(year);
        }
      }



      function showChartForYearByQuarter(year) {
        var chartYearData = null;
        var tempData = null;
        var rawYearData = null;

        for (var i = 0; i <= scope.registeredUsersByYear.length; i++) {
          tempData = scope.registeredUsersByYear[i];

          if (tempData.year === year) {
            rawYearData = tempData;
            break;
          }
        }


        //Populate Chart
        scope.registeredUsersByYearChartDataByYearByQtr = gaChartUtil.populateGAChartDataTwoCol(rawYearData.users, 'quarter', "regUsersAchieve", "regUsersTarget", "Quarter", "Registered Users Achive", "Registered Users Target");

        chartObj.type = "ColumnChart";

        //Check Options
        //chartObj.options.annotations = {};
        //chartObj.options.annotations.alwaysOutside = true;

        chartObj.options.axisTitlesPosition = "out";

        //Set Chart Data
        chartObj.data = scope.registeredUsersByYearChartDataByYearByQtr;
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      function showChartForAllYears() {
        chartObj.type = "AreaChart";
        //Set Chart Data
        chartObj.data = scope.registeredUsersByYearChartDataAllYears;
        //Set value to Scope
        scope.chartSource = chartObj;
      }


      /**
       *
       * @param {type} regUsersByYear
       * @returns {unresolved}
       */
      function margeCurrentYearTargetForWork(regUsersByYear) {

        var QUICK_REF = _tempSummaryObj.QUICK_REF;
        var MY_PACT = _tempSummaryObj.MY_PACT;
        var QUICK_QUOTATION_630 = _tempSummaryObj.QUICK_QUOTATION_630;
        var CBT = _tempSummaryObj.CBT;
        var BLM = _tempSummaryObj.BLM;
        var CAD_LIBRARY = _tempSummaryObj.CAD_LIBRARY;
        var RAPSODY = _tempSummaryObj.RAPSODY;
        var SMART_SELECTORS = _tempSummaryObj.SMART_SELECTORS;

        var totalTargetQ1 = QUICK_REF.Q1Target + MY_PACT.Q1Target + QUICK_QUOTATION_630.Q1Target +
                CBT.Q1Target + BLM.Q1Target + CAD_LIBRARY.Q1Target +
                RAPSODY.Q1Target + SMART_SELECTORS.Q1Target;

        var totalTargetQ2 = QUICK_REF.Q2Target + MY_PACT.Q2Target + QUICK_QUOTATION_630.Q2Target +
                CBT.Q2Target + BLM.Q2Target + CAD_LIBRARY.Q2Target +
                RAPSODY.Q2Target + SMART_SELECTORS.Q2Target;

        var totalTargetQ3 = QUICK_REF.Q3Target + MY_PACT.Q3Target + QUICK_QUOTATION_630.Q3Target +
                CBT.Q3Target + BLM.Q3Target + CAD_LIBRARY.Q3Target +
                RAPSODY.Q3Target + SMART_SELECTORS.Q3Target;

        var totalTargetQ4 = QUICK_REF.Q4Target + MY_PACT.Q4Target + QUICK_QUOTATION_630.Q4Target +
                CBT.Q4Target + BLM.Q4Target + CAD_LIBRARY.Q4Target +
                RAPSODY.Q4Target + SMART_SELECTORS.Q4Target;

        console.log(totalTargetQ1 + " - " + totalTargetQ2 + " - " + totalTargetQ3 + " - " + totalTargetQ4);
        var tempData = null;
        var tempUser = null;
        for (var i = 0; i < regUsersByYear.length; i++) {
          tempData = regUsersByYear[i];

          if (tempData.year !== null && tempData.year !== undefined && tempData.year === 2015) {
            for (var j = 0; j < tempData.users.length; j++) {
              var tempUser = tempData.users[j];

              if (tempUser.quarter === "Q1") {
                tempUser.regUsersTarget = totalTargetQ1;
              } else if (tempUser.quarter === "Q2") {
                tempUser.regUsersTarget = totalTargetQ2;
              } else if (tempUser.quarter === "Q3") {
                tempUser.regUsersTarget = totalTargetQ3;
              } else if (tempUser.quarter === "Q4") {
                tempUser.regUsersTarget = totalTargetQ4;
              }
            }
          } else {
            console.log("Other Than current Year");
          }
        }

        return regUsersByYear;
      }

    }//END Link..
  }


})();