/**
 * @ngdoc directive
 * @name CadLibrary directive
 *
 * @description
 *  CadLibrary directive displays all list of information based on counties selection.
 *
 */

(function () {
	'use strict';

	angular.module('se-branding')
			.directive('cadLibrary', cadLibrary);

	function cadLibrary($rootScope, $timeout, appConstants, commonUtils, logger,
			gaChartUtil, nodeGAService, CadLibraryCountryVO) {

		var directive = {
			restrict: 'E',
			replace: true,
			scope: {
				paceDataRegUser: "=paceDataRegUser",
				paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
			},
			templateUrl: 'scripts/directives/cadLibrary/cadLibrary.html',
			link: link
		};
		return directive;


		/**
		 *
		 * @param {type} scope
		 * @param {type} elem
		 * @param {type} attrs
		 * @returns {undefined}
		 */
		function link(scope, elem, attrs) {
			var countryNameList = ['France'];

			var _cadLibraryReportData = null;
			var _cadLibraryChartData = null;
			var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

			var timer = null;

			//------------------------------------------------------
			//--------------------- Global Properties --------------
			//------------------------------------------------------
			scope.countries = {};
			scope.selectedCountry = {};
			scope.appList = {};
			var selectedCountryName = countryNameList[0];

			scope.uniqueActiveUsers = 0;
			scope.productDownloads = 0;
			scope.returningUsers = 0;
			scope.visits = 0;
			scope.pageViews = 0;
			scope.launchDate = appConstants.DATES.CAD_LIBRARY_FRANCE_LAUNCHED_DATE;

			scope.cadLibraryLaunchDays = 0;
			scope.cadLibraryLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, appConstants.DATES.CAD_LIBRARY_FRANCE_LAUNCHED_DATE);


			//------------------------------------------------------
			//------ CAD LIBRARY CHART INIT SHOW HIDE PROPERTIES ---
			//------------------------------------------------------

			scope.isGAChartVisible = true;
			scope.isPaceChartVisible = false;

			var NO_OF_ACTIVE_USERS = "No. of unique active users";
			var NO_OF_PRODUCT_DOWNLOADS = "No. of product downloads";
			var NO_OF_RETURNING_USERS = "No. of returning users";
			var NO_OF_VISITS = "No. of new visits";
			var NO_OF_PAGE_VIEWS = "No. of page views";

			var NO_OF_REG_USERS = "No. of registered users";

			scope.paceChartStyle = commonUtils.getPaceChartStyle();

			scope.paceChartSource = {};
			scope.showPaceChart = showPaceChart;
			var isInitPaceChartData = false;
			var paceChartObj = {};


			scope.chartSource = {};
			var isInitChartData = false;
			var chartObj = {};


			//------------------------------------------------------------------
			//------------------  For Chart Options ----------------------------
			//------------------------------------------------------------------

			var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
			scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
			scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
			var chartTypes = appConstants.CHART_OPTIONS.CHART_TYPE_VALUES;
			scope.chartTypeSel = chartTypes[0];
			var chartTypeSelected = scope.chartTypeSel.toUpperCase();
			scope.selectedChartTab = chartOptionDfltSelInx;
			scope.cadLibraryChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
			scope.cadLibraryChartToolTip = scope.chartToolTips[scope.selectedChartTab];
			scope.cadLibraryChartName = "ChartActiveUser";
			scope.cadLibraryChartLabel = selectedCountryName + " : " + NO_OF_ACTIVE_USERS;
			scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
			scope.isfootNoteSymbolVisible = true;
			scope.loadDefaultChart = false;
			scope.onCadLibraryChartLinkChange = onCadLibraryChartLinkChange;
			scope.onCadLibraryChartOptionAndType = onCadLibraryChartOptionAndType;
			scope.onCountryDropDownChange = onCountryDropDownChange;
			scope.isSelectedChart = isSelectedChart;
			scope.chartReady = chartReady;


			init();


			/**
			 *
			 * @returns {undefined}
			 */
			function init() {
				// Init Countries data
				initCountries();
				// Get Report Data
				getCadLibraryReportData();
				// Get Chart Data
				getCadLibraryChartData();

				//Add Event Listener  When page resize
				var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

				//Remove event Listener on Destroy
				scope.$on('$destroy', listenerReloadChart);

				//Init Chart Data When Data is Ready..
				scope.$watch('paceDataRegUserOverPeriod.cadLibraryUsersReg', function (newValue, oldValue) {
					if (newValue !== undefined && newValue !== null) {
						initPaceChartData();
					}
				});
			}

			/**
			 * Get Cad Library report data from NODE
			 *
			 * @returns {undefined}
			 */
			function getCadLibraryReportData() {

				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

				nodeGAService.getCadLibraryReportData().then(
						function (result) {
							logger.log("Cad Library::getCadLibraryReportData::Result::Success", result);
							_cadLibraryReportData = result;
							parseReportData(_cadLibraryReportData);
							setDefaultCountry();
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
						},
						function (error) {
							_cadLibraryReportData = null;
							logger.log("Cad Library::getCadLibraryReportData::Result::FAIL");
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
						}
				);
			}

			/**
			 * Get Cad Library CHART data from NODE
			 *
			 * @returns {undefined}
			 */
			function getCadLibraryChartData() {

				$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

				nodeGAService.getCadLibraryChartData().then(
						function (result) {
							logger.log("Cad Library::getCadLibraryChartData::Result::Success");
							_cadLibraryChartData = result;
							parseChartData(_cadLibraryChartData);
							//Load Default Chart
							loadDefaultChart();
							//Hide Loader
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
						},
						function (error) {
							_cadLibraryChartData = null;
							logger.log("Cad Library::getCadLibraryChartData::Result::FAIL");
							//Hide Loader
							$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
						}
				);
			}


			/**
			 *  Init PACE Default Chart Options..
			 *
			 * @returns {undefined}
			 */
			function initChartData() {
				chartObj = {};
				chartObj.type = "AreaChart";
				chartObj.displayed = false;
				chartObj.options = commonUtils.getPaceChartOptions();

				isInitChartData = true;
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function loadDefaultChart() {
				initChartData();

				//TODO :: IMPORTENT CHECK LATER

				//Start Timer
				timer = $timeout(function () {
					console.log("cadLibrary.js :: loadDefaultChart :: Timer :: Start ");
					onCadLibraryChartLinkChange('ChartActiveUser');
					scope.loadDefaultChart = true;
				}, CHART_LOAD_DELAY);

				//Destroy Timer
				scope.$on("$destroy",
						function (event) {
							console.log("cadLibrary.js :: loadDefaultChart :: Timer :: Stop ");
							$timeout.cancel(timer);
						}
				);
			}


			/**
			 *
			 *  Init PACE Default Chart Options..
			 *
			 *
			 * @returns {undefined}
			 */
			function initPaceChartData() {
				paceChartObj = {};
				paceChartObj.type = "AreaChart";
				paceChartObj.displayed = false;
				paceChartObj.options = commonUtils.getPaceChartOptions();

				isInitPaceChartData = true;
			}


			/***
			 *  Onclick Registererd users link click..
			 *  Show PACE Chart Hide GA Chart
			 *
			 * @returns {undefined}
			 *
			 */
			function showPaceChart() {
				scope.isGAChartVisible = false;
				scope.isPaceChartVisible = true;

				//Show Hide Chart
				onCadLibraryChartOptionChange(scope.selectedChartTab);
			}



			/**
			 * Load PACE Chart as per selected tab (Day | Week | Month)
			 *
			 * @param {type} selTab
			 * @returns {undefined}
			 */
			function loadPaceChart(selTab) {

				//TODO :: Also Need To Check "scope.paceDataRegUserOverPeriod.cadLibraryUsersReg" val exist or not..
				if (isInitPaceChartData === false) {
					initPaceChartData();
				}

				var chartTypeSelected = scope.chartOptions[scope.selectedChartTab];

				if (chartTypeSelected.toLowerCase() === 'day') {
					paceChartObj.data = scope.paceDataRegUserOverPeriod.cadLibraryUsersReg.day;
				}
				if (chartTypeSelected.toLowerCase() === 'month') {
					paceChartObj.data = scope.paceDataRegUserOverPeriod.cadLibraryUsersReg.month;
				}

				if (chartTypeSelected.toLowerCase() === 'week') {
					paceChartObj.data = scope.paceDataRegUserOverPeriod.cadLibraryUsersReg.week;
				}

				//Set value to Scope
				scope.paceChartSource = paceChartObj;
			}


			/**
			 *
			 * @returns {undefined}
			 */
			function chartReady() {
				fixGoogleChartsBarsBootstrap();
			}

			/**
			 *
			 * @returns {undefined}
			 */
			function fixGoogleChartsBarsBootstrap() {
				$(".google-visualization-table-table img[width]").each(function (index, img) {
					$(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
				});
			}



			/**
			 * Set default values of all countries
			 *
			 * @returns {undefined}
			 */
			function initCountries() {
				var cadLibrary = {};
				//--- cadLibrary
				for (var i = 0; i < countryNameList.length; i++) {
					var capsName = countryNameList[i].toUpperCase();
					cadLibrary.countryName = countryNameList[i];
					cadLibrary.countryGaId = eval('appConstants.SITE_GA_IDS.CAD_LIBRARY_' + capsName + '_ID');
					cadLibrary.launchDate = eval('appConstants.DATES.CAD_LIBRARY_' + capsName + '_LAUNCHED_DATE');
					cadLibrary.launchDateStr = eval('appConstants.DATES.CAD_LIBRARY_' + capsName + '_LAUNCHED_DATE_STR');
					cadLibrary.regUsers = "NA";
					cadLibrary.uniqueActiveUsers = 0;
					cadLibrary.productDownloads = 0;
					cadLibrary.returningUsers = 0;
					cadLibrary.visits = 0;
					cadLibrary.pageViews = 0;
					cadLibrary.uniqueActiveUsersChart = 0;
					cadLibrary.productDownloadsChart = 0;
					cadLibrary.returningUsersChart = 0;
					cadLibrary.visitsChart = 0;
					cadLibrary.pageViewsChart = 0;
					//--- Push country to Countries Array
					//scope.countries.push(new CadLibraryCountryVO(cadLibrary));
				}
				//Set default Country to update data binding
				//setDefaultCountry();
			}

			/**
			 * Set default Country
			 * For Updating data binding.
			 * @returns {undefined}
			 */
			function setDefaultCountry() {
				scope.selectedCountry = scope.appList[0];
			}

			/**
			 *
			 * @returns {undefined}
			 */
			function reloadChart() {
				logger.log("Reload CadLibrary Chart.." + scope.selectedChartTab);
				onCadLibraryChartOptionChange(scope.selectedChartTab);
			}


			/**
			 *
			 * @param {type} reportData
			 * @returns {undefined}
			 */
			function parseReportData(reportData) {
				//console.log('\n\n==> cadLibrary: reportData: ', JSON.stringify(reportData), "\n\n");
				var appList = Object.keys(reportData);
				scope.appList = appList;

				for (var country in reportData) {
					scope.countries[country] = scope.countries[country] || {};
					scope.countries[country].uniqueActiveUsers = parseFloat(reportData[country].uniqueActiveUsers);
					scope.countries[country].productDownloads = parseFloat(reportData[country].productDownloads);
					scope.countries[country].returningUsers = parseFloat(reportData[country].returningUsers);
					scope.countries[country].visits = parseFloat(reportData[country].visits);
					scope.countries[country].pageViews = parseFloat(reportData[country].pageViews);
					scope.countries[country].launchedDate = reportData[country].launchedDate;
				}

				//----------------- SET Default Value ---------------------
				scope.uniqueActiveUsers = scope.countries[appList[0]].uniqueActiveUsers;
				scope.productDownloads = scope.countries[appList[0]].productDownloads;
				scope.returningUsers = scope.countries[appList[0]].returningUsers;
				scope.visits = scope.countries[appList[0]].visits;
				scope.pageViews = scope.countries[appList[0]].pageViews;
				scope.launchDate = scope.countries[appList[0]].launchedDate;
			}

			/**
			 *
			 * @param {type} chartData
			 * @returns {undefined}
			 */
			function parseChartData(chartData) {
//				console.log('\n\n==> cadLibrary: chartData: ', JSON.stringify(chartData), "\n\n");
				for (var country in chartData) {
					scope.countries[country] = scope.countries[country] || {};
					scope.countries[country].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[country].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
					scope.countries[country].productDownloadsChart = gaChartUtil.populateGAChartData(chartData[country].productDownloads, "dateFormatted", "productDownloads", "Date", "Product Downloads");
					scope.countries[country].returningUsersChart = gaChartUtil.populateGAChartData(chartData[country].returningUsers, "dateFormatted", "returningUsers", "Date", "Returning Users");
					scope.countries[country].visitsChart = gaChartUtil.populateGAChartData(chartData[country].visits, "dateFormatted", "visits", "Date", "New Visits");
					scope.countries[country].pageViewsChart = gaChartUtil.populateGAChartData(chartData[country].pageViews, "dateFormatted", "pageViews", "Date", "Page Views");
				}
				console.log("Chart Data.. Prepare...");
			}


			/**
			 *
			 * @param {type} chartName
			 * @returns {undefined}
			 */
			function onCadLibraryChartLinkChange(chartName) {
				scope.isGAChartVisible = true;
				scope.isPaceChartVisible = false;
				chartTypeSelected = chartTypes[0].toUpperCase();
				scope.chartTypeSel = chartTypes[0];
				scope.cadLibraryChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
				scope.cadLibraryChartName = chartName;
				onCadLibraryChartOptionChange(scope.selectedChartTab);
			}

			function onCadLibraryChartOptionAndType(evnt, selTab) {
				if (evnt !== undefined && evnt !== null) {
					evnt.stopImmediatePropagation();
				}
				chartTypeSelected = chartTypes[0].toUpperCase();
				scope.chartTypeSel = chartTypes[0];
				onCadLibraryChartOptionChange(selTab);
			}


			/**
			 *
			 * @param {type} selTab
			 * @returns {undefined}
			 */
			function onCadLibraryChartOptionChange(selTab) {
				//Set Country Name & gaId
				scope.isfootNoteSymbolVisible = false;
				//scope.showPie = false;
				selectedCountryName = scope.selectedCountry;//.countryName;


				//Set the selected Tab val again..
				scope.selectedChartTab = selTab;
				scope.cadLibraryChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
				scope.cadLibraryChartToolTip = scope.chartToolTips[scope.selectedChartTab];

				var chartType = scope.cadLibraryChartTypeSelected;
				var chartName = scope.cadLibraryChartName;
				var countryPrefix = selectedCountryName + " : ";

				if (isInitChartData === false) {
					initChartData();
				}

				//check for PACE Chart
				if (scope.isPaceChartVisible) {
					scope.cadLibraryChartLabel = NO_OF_REG_USERS;

					loadPaceChart(scope.selectedChartTab);
					return;
				}
				// ADD Chart
				switch (chartName) {
					case "ChartActiveUser":
					{
						scope.isfootNoteSymbolVisible = true;
						scope.cadLibraryChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
						//Add New Chart
						if (chartType === "Day") {
							chartObj.data = scope.countries[scope.selectedCountry].uniqueActiveUsersChart.day;
						} else if (chartType === "Week") {
							chartObj.data = scope.countries[scope.selectedCountry].uniqueActiveUsersChart.week;
						} else if (chartType === "Month") {
							chartObj.data = scope.countries[scope.selectedCountry].uniqueActiveUsersChart.month;
						}
						break;
					}
					case "ChartProductDownloads":
					{
						scope.cadLibraryChartLabel = countryPrefix + NO_OF_PRODUCT_DOWNLOADS;
						if (chartType === "Day") {
							chartObj.data = scope.countries[scope.selectedCountry].productDownloadsChart.day;
						} else if (chartType === "Week") {
							chartObj.data = scope.countries[scope.selectedCountry].productDownloadsChart.week;
						} else if (chartType === "Month") {
							chartObj.data = scope.countries[scope.selectedCountry].productDownloadsChart.month;
						}
						break;
					}
					case "ChartReturningUsers":
					{
						scope.cadLibraryChartLabel = countryPrefix + NO_OF_RETURNING_USERS;
						if (chartType === "Day") {
							chartObj.data = scope.countries[scope.selectedCountry].returningUsersChart.day;
						} else if (chartType === "Week") {
							chartObj.data = scope.countries[scope.selectedCountry].returningUsersChart.week;
						} else if (chartType === "Month") {
							chartObj.data = scope.countries[scope.selectedCountry].returningUsersChart.month;
						}
						break;
					}
					case "ChartNewVisits":
					{
						scope.cadLibraryChartLabel = countryPrefix + NO_OF_VISITS;
						if (chartType === "Day") {
							chartObj.data = scope.countries[scope.selectedCountry].visitsChart.day;
						} else if (chartType === "Week") {
							chartObj.data = scope.countries[scope.selectedCountry].visitsChart.week;
						} else if (chartType === "Month") {
							chartObj.data = scope.countries[scope.selectedCountry].visitsChart.month;
						}
						break;
					}
					case "ChartPageviews":
					{
						scope.cadLibraryChartLabel = countryPrefix + NO_OF_PAGE_VIEWS;
						if (chartType === "Day") {
							chartObj.data = scope.countries[scope.selectedCountry].pageViewsChart.day;
						} else if (chartType === "Week") {
							chartObj.data = scope.countries[scope.selectedCountry].pageViewsChart.week;
						} else if (chartType === "Month") {
							chartObj.data = scope.countries[scope.selectedCountry].pageViewsChart.month;
						}
						break;
					}
				}
				//Set value to Scope
				scope.chartSource = chartObj;
			}

			function onCountryDropDownChange(country) {
				scope.selectedCountry = country;
				scope.uniqueActiveUsers = scope.countries[country].uniqueActiveUsers;
				scope.productDownloads = scope.countries[country].productDownloads;
				scope.returningUsers = scope.countries[country].returningUsers;
				scope.visits = scope.countries[country].visits;
				scope.pageViews = scope.countries[country].pageViews;
				scope.launchDate = scope.countries[country].launchedDate;

				if (!scope.isPaceChartVisible) {
					onCadLibraryChartLinkChange(scope.cadLibraryChartName);
				}
			}

			function isSelectedChart(chart) {
				return scope.isGAChartVisible && (scope.cadLibraryChartName === chart);
			}

		}//END Link..

	}

})();