/**
 * @ngdoc directive
 * @name E_PLAN Report directive
 *
 * @description
 *  E_PLAN Report directive displays all list of information based on counties selection.
 *
 */
(function () {
  'use strict';
  angular.module('se-branding')
          .directive('mnmReport', mnmReport);
  function mnmReport($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, businessReportService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
      },
      templateUrl: 'scripts/directives/mnm/mnmReport.html',
      link: link
    };
    return directive;
    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;
      var DEFAULT_CHART_NAME = "ChartRegisteredUsers";
      var CHART_LABELS = {
        REG_USERS: 'Registered Users',
        VISITORS: 'Visitors',
        TARGET_VISITORS: 'Target Visitors',
        PROD_ADDED: 'Products Added To Basket'
      };
      var timer = null;
      //------------------------------------------------------------------
      //---------------------------- Global Properties -------------------
      //------------------------------------------------------------------

      scope.months = appConstants.DATES.MONTH_ARR_LONG;
      scope.selectedMonth = "";
      scope.years = [];
      scope.selectedYear = '';
      var baseReportData = {launchDate: '', visitors: 0, targetVisitors: 0, registeredUsers: 0, productsAddedToBasket: 0};
      scope.reportData = baseReportData;
      scope.reportData.chartData = {};
      scope.onChartOptionChange = onChartOptionChange;
      scope.onChartLinkChange = onChartLinkChange;
      scope.mnmChartName = DEFAULT_CHART_NAME;
      scope.getSelectedRowChartCSS = getSelectedRowChartCSS;
      scope.chartSource = {};
      scope.mnmChartLabel = "";
      scope.isChartReady = false;
      scope.hasChartData = false;
      var chartObj = {};
      var adhocData = {};
      var adhocTotals = {};
      var worldwideTotals = {};
      //------------------------------------------------------------------
      //--------- E_PLAN CHART INIT SHOW HIDE PROPERTIES --------------
      //------------------------------------------------------------------




      //------------------------------------------------------------------
      //-------------------  For Chart TYPE Options  ---------------------
      //------------------------------------------------------------------


      init();
      /**
       * Function to initialized the repors
       *
       * @returns {undefined}
       */
      function init() {

        initValues();
        // get ad-hoc data
        getAdhocData();
        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChartDataAndSource);
        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);
      }

      /**
       *
       * @returns {undefined}
       */
      function initValues() {
        addAllParamsInDropDowns();
        setDefaultDropDownValues();
      }

      /**
       *
       * @returns {undefined}
       */
      function getAdhocData() {
        businessReportService.getMnMAdhocData().then(function (data) {
          adhocData = data;
          parseAdhocData();
        });
      }

      function parseAdhocData() {
        worldwideTotals = angular.copy(baseReportData);
        for (var year in adhocData) {
          var data = adhocData[year];
          if (Number(year) < 2016) {
            worldwideTotals.registeredUsers += data['total'].registeredUsers;
            worldwideTotals.visitors += data['total'].visitors;
            worldwideTotals.targetVisitors += data['total'].targetVisitors;
            worldwideTotals.productsAddedToBasket += data['total'].productsAddedToBasket;
          } else {
            for (var d in data) {
              var obj = data[d];
              adhocTotals[year] = adhocTotals[year] || baseReportData;
              adhocTotals[year].registeredUsers += obj.registeredUsers;
              adhocTotals[year].visitors += obj.visitors;
              adhocTotals[year].targetVisitors += obj.targetVisitors;
              adhocTotals[year].productsAddedToBasket += obj.productsAddedToBasket;
              //worldwideTotals
              worldwideTotals.registeredUsers += obj.registeredUsers;
              worldwideTotals.visitors += obj.visitors;
              worldwideTotals.targetVisitors += obj.targetVisitors;
              worldwideTotals.productsAddedToBasket += obj.productsAddedToBasket;
            }
          }
        }
        scope.reportTotal = worldwideTotals;
        onChartOptionChange();
      }

      /**
       *
       * @returns {undefined}
       */
      function addAllParamsInDropDowns() {
        var months = angular.copy(appConstants.DATES.MONTH_ARR_LONG);
        scope.months = months.map(function (obj, indx) {
          return obj.label;
        });
        scope.months.unshift("All");
        scope.years = angular.copy(appConstants.DATES.MNM_YEARS);
        scope.selectedYear = scope.years[0];
        scope.years.sort(function (a, b) {
          return a < b;
        });
      }

      /**
       *
       * @returns {undefined}
       */
      function setDefaultDropDownValues() {
        scope.selectedMonth = scope.months[0];
        scope.selectedYear = scope.years[0];
      }

      /**
       *
       * @returns {undefined}
       */
      function onChartOptionChange() {
        var year = scope.selectedYear;
        var month = scope.selectedMonth ? scope.selectedMonth.substring(0, 3) : 'All';
        switch (month) {
          case 'All':
            scope.reportData = adhocTotals[year];
            scope.hasChartData = true;
            onChartLinkChange('ChartRegisteredUsers');
            break;
          default:
            scope.reportData = adhocData[year][month];
            scope.hasChartData = false;
        }
      }

      scope.ifChartReady = function () {
        return scope.isChartReady;
      };
      function makeChart(dataitem, datalabel) {
        initColumnChartOption();
        scope.isChartReady = false;
        var year = scope.selectedYear;
        var chartData = [];
        for (var month in adhocData[year]) {
          var data = adhocData[year][month];
          var t = {};
          t.month = month;
          t[dataitem] = data[dataitem];
          chartData.push(t);
        }
        chartObj.colors = "['#058DC7']";
        chartObj.data = {
          "cols": [
            {id: 'months', label: 'Months', type: 'string'},
            {id: 'count', label: datalabel, type: 'number'}
          ],
          "rows": []
        };
        for (var i in chartData) {
          var obj = chartData[i];
          var row = {c: []};
          var tm = {v: obj.month + " '" + year.toString().substr(-2)};
          var td = {v: obj[dataitem]};
          row.c.push(tm, td);
          chartObj.data.rows.push(row);
        }
        scope.chartSource = chartObj;
        scope.isChartReady = true;
      }

      function makeVisitorChart() {
        var year = scope.selectedYear;
        chartObj.type = 'ComboChart';
        chartObj.data = {
          "cols": [
            {id: 'months', label: 'Months', type: 'string'},
            {id: 'count', label: 'Visitors', type: 'number'},
            {id: 'target', label: 'Target', type: 'number'}
          ],
          "rows": []
        };
        chartObj.options = {
          legend: {position: 'top'},
          lineWidth: 4,
          pointShape: "circle",
          pointSize: 6,
//					colors: ['#009', '#76e676'],
          colors: ['#337AB7', '#999'],
          series: {0: {type: 'bars'}, 1: {type: 'line'}},
          chartArea: {
            left: 20,
            top: 50,
            width: '93%',
            height: '75%'
          },
          vAxis: {
            textPosition: 'in'
          },
          hAxis: {
            textPosition: 'out',
            showTextEvery: 3
          }
        };
        for (var month in adhocData[year]) {
          var data = adhocData[year][month];
          var row = {c: []};
          var tm = {v: month + " '" + year.toString().substr(-2)};
          var tv = {v: data.visitors};
          var tvtar = {v: data.targetVisitors};
          row.c.push(tm, tv, tvtar);
          chartObj.data.rows.push(row);
        }
      }

      function onChartLinkChange(chartName) {
        var dataitem = '';
        var datalabel = '';
        scope.chartName = chartName;
        if (chartName === 'ChartVisitors') {
          makeVisitorChart();
        } else {
          switch (chartName) {
            case 'ChartRegisteredUsers':
              dataitem = 'registeredUsers';
              datalabel = CHART_LABELS.REG_USERS;
              break;
            case 'ChartVisitors':
              break;
            case 'ChartProdAdded':
              dataitem = 'productsAddedToBasket';
              datalabel = CHART_LABELS.PROD_ADDED;
              break;
          }
          makeChart(dataitem, datalabel);
        }
      }

      /**
       *  Init COLUMN  Chart Options..
       *
       * @returns {undefined}
       */
      function initColumnChartOption() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();
      }


      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        scope.isChartReady = false;
        chartObj.data = {};
        //Start Timer
        timer = $timeout(function () {
          console.log("ecoReach.js :: loadDefaultChart :: Timer :: Start ");
          reloadChartDataAndSource();
          scope.isChartReady = true;
        }, CHART_LOAD_DELAY);
        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoReach.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChartDataAndSource() {
        //Set Chart Data
        chartObj.data = scope.reportData.chartData;
        //Set value to Scope
        scope.chartSource = chartObj;
      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }

      /**
       *
       * @param {type} cName
       * @returns {String}
       */
      function getSelectedRowChartCSS(cName) {
        var chartName = scope.chartName;
        var styleName = "";
        if (cName === chartName) {
          styleName = "selected-row-chart ";
        }
        return styleName;
      }
    }
  }


}
)();