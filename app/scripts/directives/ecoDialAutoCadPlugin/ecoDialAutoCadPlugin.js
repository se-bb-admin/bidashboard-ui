/**
 * @ngdoc directive
 * @name EcoDialAutoCadPlugin directive
 *
 * @description
 *  EcoDialAutoCadPlugin directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('ecoDialAutoCadPlugin', ecoDialAutoCadPlugin);

  function ecoDialAutoCadPlugin($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, EcoDialAutoCadPluginCountryVO) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/ecoDialAutoCadPlugin/ecoDialAutoCadPlugin.html',
      link: link
    };

    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['Country1'];

      //var _EcoDialAutoCadPluginReportData = null;
      //var _EcoDialAutoCadPluginChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      //var selectedCountryGaId = appConstants.SITE_GA_IDS.ECO_DIAL_AUTOCAD_PLUGIN_ID;
      //scope.uniqueActiveUsers = 0;
      //scope.ecoDialAutoCadPluginLaunchDays = 0;
      //scope.ecoDialAutoCadPluginLaunchDays = commonUtils.getDateDiff(appConstants.DATES.TODAY, appConstants.DATES.ECO_DIAL_AUTOCAD_PLUGIN_LAUNCHED_DATE);
      scope.launchDate = appConstants.DATES.ECO_DIAL_AUTOCAD_PLUGIN_LAUNCHED_DATE;

      //------------------------------------------------------
      //------ MY PACT CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------

      var NO_OF_REG_USERS = "Total - Worldwide : No. of registered users";

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.ecoDialAutoCadPluginTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.ecoDialAutoCadPluginChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.ecoDialAutoCadPluginChartName = "ChartRegisteredUsers";
      scope.ecoDialAutoCadPluginChartLabel = NO_OF_REG_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = false;
      scope.loadDefaultChart = false;

      scope.onEcoDialAutoCadPluginChartOptionChange = onEcoDialAutoCadPluginChartOptionChange;
      scope.onEcoDialAutoCadPluginChartLinkChange = onEcoDialAutoCadPluginChartLinkChange;


      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
//                            $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        // Init Countries data
        initCountries();
        // Get Report Data
        //getEcoDialAutoCadPluginReportData();
        // Get Chart Data
        //getEcoDialAutoCadPluginChartData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.ecoDialAutoCadPluginUsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initChartData();
            //Load Default Chart
            loadDefaultChart();
          }
        });
      }

      /**
       * Get EcoDialAutoCadPlugin report data from NODE
       *
       * @returns {undefined}

       function getEcoDialAutoCadPluginReportData() {
       $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
       nodeGAService.getEcoDialAutoCadPluginReportData().then(
       function (result) {
       logger.log("EcoDialAutoCadPlugin::getEcoDialAutoCadPluginReportData::Result::Success");
       _EcoDialAutoCadPluginReportData = result;
       parseReportData(_EcoDialAutoCadPluginReportData);
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
       },
       function (error) {
       _EcoDialAutoCadPluginReportData = null;
       logger.log("EcoDialAutoCadPlugin::getEcoDialAutoCadPluginReportData::Result::FAIL");
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
       }
       );
       }
       */

      /**
       * Get EcoDialAutoCadPlugin CHART data from NODE
       *
       * @returns {undefined}

       function getEcoDialAutoCadPluginChartData() {

       $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

       nodeGAService.getEcoDialAutoCadPluginChartData().then(
       function (result) {
       logger.log("EcoDialAutoCadPlugin::getEcoDialAutoCadPluginChartData::Result::Success");
       _EcoDialAutoCadPluginChartData = result;
       parseChartData(_EcoDialAutoCadPluginChartData);
       //Load Default Chart
       loadDefaultChart();
       //Hide Loader
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
       },
       function (error) {
       _EcoDialAutoCadPluginChartData = null;
       logger.log("EcoDialAutoCadPlugin::getEcoDialAutoCadPluginChartData::Result::FAIL");
       //Hide Loader
       $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
       }
       );

       }
       */


      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("ecoDialAutoCadPlugin.js :: loadDefaultChart :: Timer :: Start ");
          onEcoDialAutoCadPluginChartLinkChange('ChartRegisteredUsers');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoDialAutoCadPlugin.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );


      }

      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }


      /**
       * Set default values of all countries
       *
       * @returns {undefined}
       */
      function initCountries() {
        var ecoDialAutoCadPlugin = {};
        //--- EcoDialAutoCadPlugin
        for (var i = 0; i < countryNameList.length; i++) {
          ecoDialAutoCadPlugin.countryName = countryNameList[i];
          ecoDialAutoCadPlugin.countryGaId = appConstants.SITE_GA_IDS.ECO_DIAL_AUTOCAD_PLUGIN_ID;
          ecoDialAutoCadPlugin.launchDate = appConstants.DATES.ECO_DIAL_AUTOCAD_PLUGIN_LAUNCHED_DATE;
          ecoDialAutoCadPlugin.launchDateStr = appConstants.DATES.ECO_DIAL_AUTOCAD_PLUGIN_LAUNCHED_DATE_STR;
          ecoDialAutoCadPlugin.regUsers = 0;
          ecoDialAutoCadPlugin.uniqueActiveUsers = 0;
          ecoDialAutoCadPlugin.projectsCreated = 0;
          ecoDialAutoCadPlugin.uniqueActiveUsersChart = 0;
          ecoDialAutoCadPlugin.projectsCreatedChart = 0;
          //--- Push country to Countries Array
          scope.countries.push(new EcoDialAutoCadPluginCountryVO(ecoDialAutoCadPlugin));
        }
      }

      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload EcoDialAutoCadPlugin Chart.." + scope.selectedChartTab);
        onEcoDialAutoCadPluginChartOptionChange(null, scope.selectedChartTab);

      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}

       function parseReportData(reportData) {
       scope.countries[0].uniqueActiveUsers = parseFloat(reportData.uniqueActiveUsers.totalsForAllResults['ga:users']);
       scope.countries[0].projectsCreated = parseFloat(reportData.projectList.totalsForAllResults['ga:totalEvents']);
       //----------------- SET Default Value ---------------------
       scope.uniqueActiveUsers = scope.countries[0].uniqueActiveUsers;
       scope.projectsCreated = scope.countries[0].projectsCreated;
       } */




      /**
       *
       * @param {type} chartData
       * @returns {undefined}

       function parseChartData(chartData) {
       scope.countries[0].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData.uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
       scope.countries[0].projectsCreatedChart = gaChartUtil.populateGAChartData(chartData.projectList, "dateFormatted", "projectList", "Date", "Projects Created");

       console.log("Chart Data.. Prepare...");
       }*/




      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function onEcoDialAutoCadPluginChartLinkChange(chartName) {
        //When you click on a link Chart will reset to Month Chart.
        scope.ecoDialAutoCadPluginChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.ecoDialAutoCadPluginChartName = chartName;
        onEcoDialAutoCadPluginChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function onEcoDialAutoCadPluginChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;

        scope.selectedChartTab = selTab;
        scope.ecoDialAutoCadPluginChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.ecoDialAutoCadPluginChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.ecoDialAutoCadPluginChartTypeSelected;
        var chartName = scope.ecoDialAutoCadPluginChartName;

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartRegisteredUsers":
          {
            scope.ecoDialAutoCadPluginChartLabel = NO_OF_REG_USERS;

            if (chartType === 'Day') {
              chartObj.data = scope.paceDataRegUserOverPeriod.ecoDialAutoCadPluginUsersReg.day;
            } else if (chartType === 'Week') {
              chartObj.data = scope.paceDataRegUserOverPeriod.ecoDialAutoCadPluginUsersReg.week;
            } else if (chartType === 'Month') {
              chartObj.data = scope.paceDataRegUserOverPeriod.ecoDialAutoCadPluginUsersReg.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }




    }//END Link..

  }
  ;
})();