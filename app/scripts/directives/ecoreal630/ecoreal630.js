/**
 * @ngdoc directive
 * @name Ecoreal 630A directive
 *
 * @description
 *  Ecoreal Quick Quotation 630A directive displays all list of information based on counties selection.
 *
 */

(function () {
  'use strict';

  angular.module('se-branding').directive('ecoreal630', ecoreal630);

  function ecoreal630($rootScope, $timeout, appConstants, commonUtils, logger,
          gaChartUtil, nodeGAService, EcorealQQ630CountryVO, currencyConverterService) {

    var directive = {
      restrict: 'E',
      replace: true,
      scope: {
        paceDataRegUser: "=paceDataRegUser",
        paceDataRegUserOverPeriod: "=paceDataRegUserOverPeriod"
      },
      templateUrl: 'scripts/directives/ecoreal630/ecoreal630.html',
      link: link
    };
    return directive;


    /**
     *
     * @param {type} scope
     * @param {type} elem
     * @param {type} attrs
     * @returns {undefined}
     */
    function link(scope, elem, attrs) {
      var countryNameList = ['France'];

      var _ecoreal630ReportData = null;
      var _ecoreal630ChartData = null;
      var CHART_LOAD_DELAY = appConstants.TIMER.CHART_LOAD_DELAY;

      var timer = null;

      //------------------------------------------------------
      //--------------------- Global Properties --------------
      //------------------------------------------------------
      scope.countries = [];
      scope.selectedCountry = {};
      var selectedCountryName = countryNameList[0];

      scope.quickQuotationLaunchedDate = appConstants.DATES.ECOREAL_630_FRANCE_LAUNCHED_DATE;
      scope.quickQuotationUsersUnique = 0;
      scope.quickQuotationProductsAdded = 0;
      scope.quickQuotationProjectCreated = 0;
      scope.quickQuotationBomExportedNo = 0;
      scope.quickQuotationBomExportedVal = 0;

      scope.quickQuotTotalUsersUnique = 0;
      scope.quickQuotTotalProductsAdded = 0;
      scope.quickQuotTotalProjectCreated = 0;
      scope.quickQuotTotalBomExportedNo = 0;
      scope.quickQuotTotalBomExportedVal = 0;

      //------------------------------------------------------
      //------ Ecoreal QQ 630 CHART INIT SHOW HIDE PROPERTIES -------
      //------------------------------------------------------
      scope.isGAChartVisible = true;
      scope.isPaceChartVisible = false;

      var NO_OF_ACTIVE_USERS = 'No. of unique active users';
      var PRODUCT_ADDED = 'Products Added';
      var PROJECT_CREATED = 'Projects Created';
      var BOM_EXPORTED = 'BOM Exported';
      var VAL_BOM_EXPORTED = 'Value of BOM Exported';
      var NO_OF_REG_USERS = "No. of registered users";

      scope.onCountryDropDownChange = onCountryDropDownChange;

      scope.paceChartStyle = commonUtils.getPaceChartStyle();
      scope.paceChartSource = {};
      scope.showPaceChart = showPaceChart;
      var isInitPaceChartData = false;
      var paceChartObj = {};

      scope.chartSource = {};
      var isInitChartData = false;
      var chartObj = {};

      //------------------------------------------------------
      //-------------  For Chart Options ---------------------
      //------------------------------------------------------
      var chartOptionDfltSelInx = appConstants.CHART_OPTIONS.CHART_OPTIONS_DFLT_SEL_IDX;
      scope.chartOptions = appConstants.CHART_OPTIONS.CHART_OPTIONS_VALUES;
      scope.chartToolTips = appConstants.CHART_OPTIONS.CHART_OPTIONS_TOOL_TIPS;
      scope.selectedChartTab = chartOptionDfltSelInx;
      scope.quickQuotationChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
      scope.quickQuotationChartToolTip = scope.chartToolTips[scope.selectedChartTab];
      scope.quickQuotationChartName = "SpainChartActiveUser";
      scope.quickQuotationChartLabel = selectedCountryName + " : " + NO_OF_ACTIVE_USERS;
      scope.noteActiveUser = appConstants.NOTE.UNIQUE_ACTIVE_USER;
      scope.isfootNoteSymbolVisible = true;
      scope.zoomLabel = appConstants.NOTE.ZOOM_LABEL;
      scope.loadDefaultChart = false;

      scope.on630ChartLinkChange = on630ChartLinkChange;
      scope.on630ChartOptionChange = on630ChartOptionChange;

      scope.paceChartStyle = commonUtils.getPaceChartStyle;
      scope.chartReady = chartReady;

      init();

      /**
       *
       * @returns {undefined}
       */
      function init() {
        // Init Countries data
        initCountries();
        // Get Report Data
        getEcorealReportData();
        // Get Chart Data
        getEcorealChartData();

        //Add Event Listener
        var listenerReloadChart = $rootScope.$on(appConstants.EVENTS.RELOAD_CHART, reloadChart);

        //Remove event Listener on Destroy
        scope.$on('$destroy', listenerReloadChart);

        //Init Chart Data When Data is Ready..
        scope.$watch('paceDataRegUserOverPeriod.ecoreal630UsersReg', function (newValue, oldValue) {
          if (newValue !== undefined && newValue !== null) {
            initPaceChartData();
          }
        });
      }


      /**
       * Get Ecoreal Quick Quotation 630 report data from NODE
       *
       * @returns {undefined}
       */
      function getEcorealReportData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getEcoreal630ReportData().then(
                function (result) {
                  logger.log("Ecoreal Quick Quotation 630::getEcorealReportData::Result::Success");
                  _ecoreal630ReportData = result;
                  parseReportData(_ecoreal630ReportData);
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _ecoreal630ReportData = null;
                  logger.log("Ecoreal Quick Quotation 630::getEcorealReportData::Result::FAIL");
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );
      }

      /**
       * Get Ecoreal Quick Quotation 630 CHART data from NODE
       *
       * @returns {undefined}
       */
      function getEcorealChartData() {

        $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

        nodeGAService.getEcoreal630ChartData().then(
                function (result) {
                  logger.log("Ecoreal Quick Quotation 630::getEcorealChartData::Result::Success");
                  _ecoreal630ChartData = result;
                  parseChartData(_ecoreal630ChartData);
                  //Load Default Chart
                  loadDefaultChart();
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                },
                function (error) {
                  _ecoreal630ChartData = null;
                  logger.log("Ecoreal Quick Quotation 630::getEcorealChartData::Result::FAIL");
                  //Hide Loader
                  $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, {isForceHide: true});
                }
        );

      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initChartData() {
        chartObj = {};
        chartObj.type = "AreaChart";
        chartObj.displayed = false;
        chartObj.options = commonUtils.getPaceChartOptions();

        isInitChartData = true;
      }

      /**
       *
       * @returns {undefined}
       */
      function loadDefaultChart() {
        initChartData();

        //TODO :: IMPORTENT CHECK LATER

        //Start Timer
        timer = $timeout(function () {
          console.log("ecoreal63.js :: loadDefaultChart :: Timer :: Start ");
          on630ChartLinkChange('ChartActiveUser');
          scope.loadDefaultChart = true;
        }, CHART_LOAD_DELAY);

        //Destroy Timer
        scope.$on("$destroy",
                function (event) {
                  console.log("ecoreal63.js :: loadDefaultChart :: Timer :: Stop ");
                  $timeout.cancel(timer);
                }
        );
      }

      /**
       *
       *  Init PACE Default Chart Options..
       *
       *
       * @returns {undefined}
       */
      function initPaceChartData() {
        paceChartObj = {};
        paceChartObj.type = "AreaChart";
        paceChartObj.displayed = false;
        paceChartObj.options = commonUtils.getPaceChartOptions();

        isInitPaceChartData = true;
      }

      /***
       *  Onclick Registererd users link click..
       *  Show PACE Chart Hide GA Chart
       *
       * @returns {undefined}
       *
       */
      function showPaceChart() {
        scope.isGAChartVisible = false;
        scope.isPaceChartVisible = true;

        //Show Hide Chart
        on630ChartOptionChange(null, chartOptionDfltSelInx);
      }

      /**
       * Load PACE Chart as per selected tab (Day | Week | Month)
       *
       * @param {type} selTab
       * @returns {undefined}
       */
      function loadPaceChart(selTab) {

        //TODO :: Also Need To Check "scope.paceDataRegUserOverPeriod.myPactBasicUsersReg" val exist or not..
        if (isInitPaceChartData === false) {
          initPaceChartData();
        }

        var countryName = scope.selectedCountry.countryName;
        var chartTypeSelected = scope.chartOptions[scope.selectedChartTab];

        switch (countryName) {
          case "France" :
          {
            if (chartTypeSelected.toLowerCase() === 'day') {
              paceChartObj.data = scope.paceDataRegUserOverPeriod.ecoreal630UsersReg.day;
            }
            if (chartTypeSelected.toLowerCase() === 'week') {
              paceChartObj.data = scope.paceDataRegUserOverPeriod.ecoreal630UsersReg.week;
            }
            if (chartTypeSelected.toLowerCase() === 'month') {
              paceChartObj.data = scope.paceDataRegUserOverPeriod.ecoreal630UsersReg.month;
            }
            break;
          }
        }

        //Set value to Scope
        scope.paceChartSource = paceChartObj;
      }


      /**
       *
       * @returns {undefined}
       */
      function chartReady() {
        fixGoogleChartsBarsBootstrap();
      }

      /**
       *
       * @returns {undefined}
       */
      function fixGoogleChartsBarsBootstrap() {
        $(".google-visualization-table-table img[width]").each(function (index, img) {
          $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
      }



      /**
       *
       * @returns {undefined}
       */
      function initCountries() {
        var ecoreal = {};
        //--
        for (var i = 0; i < countryNameList.length; i++) {
          var capsName = countryNameList[i].toUpperCase();
          var gaId = 'QUICK_QUOTATION_630_' + capsName + '_ID';
          var launchDate = 'QUICK_QUOTATION_630_' + capsName + '_LAUNCHED_DATE';
          var launchDateStr = 'QUICK_QUOTATION_630_' + capsName + '_LAUNCHED_DATE_STR';
          ecoreal.countryName = countryNameList[i];
          ecoreal.countryGaId = appConstants.SITE_GA_IDS[gaId];
          ecoreal.launchDate = appConstants.DATES[launchDate];
          ecoreal.launchDateStr = appConstants.DATES[launchDateStr];
          ecoreal.regUsers = 'NA';
          ecoreal.uniqueActiveUsers = 0;
          ecoreal.productsAdded = 0;
          ecoreal.projectCreated = 0;
          ecoreal.bomExportedNo = 0;
          ecoreal.bomExportedVal = 0;
          ecoreal.uniqueActiveUsersChart = 0;
          ecoreal.productsAddedChart = 0;
          ecoreal.projectCreatedChart = 0;
          ecoreal.bomExportedNoChart = 0;
          ecoreal.bomExportedValChart = 0;
          scope.countries.push(new EcorealQQ630CountryVO(ecoreal));
        }
        //Set default Country to update data binding
        setDefaultCountry();
      }


      /**
       * Set default Country
       * For Updating data binding.
       * @returns {undefined}
       */
      function setDefaultCountry() {
        scope.selectedCountry = scope.countries[0];
      }

      /**
       * On Change of Country Drop Down
       *
       * @returns {undefined}
       */
      function onCountryDropDownChange() {
        var index = 0;
        selectedCountryName = scope.selectedCountry.countryName;

        logger.log(selectedCountryName);

        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            index = i;
          }
        }

        scope.quickQuotationLaunchedDate = scope.countries[index].launchDate;
        scope.quickQuotationUsersUnique = scope.countries[index].uniqueActiveUsers;
        scope.quickQuotationProductsAdded = scope.countries[index].productsAdded;
        scope.quickQuotationProjectCreated = scope.countries[index].projectCreated;
        scope.quickQuotationBomExportedNo = scope.countries[index].bomExportedNo;
        scope.quickQuotationBomExportedVal = scope.countries[index].bomExportedVal;

        on630ChartLinkChange('ChartActiveUser');
      }


      /**
       *
       * @returns {undefined}
       */
      function reloadChart() {
        logger.log("Reload 630 Chart.." + scope.selectedChartTab);
        //->
        on630ChartOptionChange(null, scope.selectedChartTab);
      }


      /**
       *
       * @param {type} reportData
       * @returns {undefined}
       */
      function parseReportData(reportData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsers = parseFloat(reportData[keyName].uniqueActiveUsers.totalsForAllResults['ga:users']);
          scope.countries[i].productsAdded = parseFloat(reportData[keyName].productsAdded.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].projectCreated = parseFloat(reportData[keyName].projectCreated.totalsForAllResults['ga:totalEvents']);
          scope.countries[i].bomExportedNo = parseFloat(reportData[keyName].bomExported.totalsForAllResults['ga:totalEvents']);
          //scope.countries[i].bomExportedVal = parseFloat(reportData[keyName].bomExported.totalsForAllResults['ga:eventValue']);

          scope.quickQuotTotalUsersUnique = scope.quickQuotTotalUsersUnique + scope.countries[i].uniqueActiveUsers;
          scope.quickQuotTotalProductsAdded = scope.quickQuotTotalProductsAdded + scope.countries[i].productsAdded;
          scope.quickQuotTotalProjectCreated = scope.quickQuotTotalProjectCreated + scope.countries[i].projectCreated;
          scope.quickQuotTotalBomExportedNo = scope.quickQuotTotalBomExportedNo + scope.countries[i].bomExportedNo;
        }

        //----------------- SET Default Value ---------------------
        scope.quickQuotationUsersUnique = scope.countries[0].uniqueActiveUsers;
        scope.quickQuotationProductsAdded = scope.countries[0].productsAdded;
        scope.quickQuotationProjectCreated = scope.countries[0].projectCreated;
        scope.quickQuotationBomExportedNo = scope.countries[0].bomExportedNo;
        //scope.quickQuotationBomExportedVal = scope.countries[0].bomExportedVal;

      }


      /**
       *
       * @param {type} chartData
       * @returns {undefined}
       */
      function parseChartData(chartData) {
        for (var i = 0; i < countryNameList.length; i++) {
          var keyName = countryNameList[i];
          scope.countries[i].uniqueActiveUsersChart = gaChartUtil.populateGAChartData(chartData[keyName].uniqueActiveUsers, "dateFormatted", "uniqueActiveUsers", "Date", "Active Users");
          scope.countries[i].productsAddedChart = gaChartUtil.populateGAChartData(chartData[keyName].productsAdded, "dateFormatted", "productsAdded", "Date", "Products Added");
          scope.countries[i].projectCreatedChart = gaChartUtil.populateGAChartData(chartData[keyName].projectCreated, "dateFormatted", "projectCreated", "Date", "Projects Created");
          scope.countries[i].bomExportedNoChart = gaChartUtil.populateGAChartData(chartData[keyName].bomExportedNo, "dateFormatted", "bomExportedNo", "Date", "No. of BOM Exported");
          //scope.countries[i].bomExportedValChart = gaChartUtil.populateGAChartData(chartData[keyName].bomExportedVal, "dateFormatted", "bomExportedVal", "Date", "Value of BOM Exported");
        }
        console.log("Chart Data.. Prepare...");
      }

      /**
       *
       * @param {type} chartName
       * @returns {undefined}
       */
      function on630ChartLinkChange(chartName) {
        scope.isGAChartVisible = true;
        scope.isPaceChartVisible = false;
        //When you click on a link Chart will reset to Month Chart.
        scope.quickQuotationChartTypeSelected = scope.chartOptions[2];
        scope.quickQuotationChartName = chartName;
        on630ChartOptionChange(null, chartOptionDfltSelInx);
      }



      /**
       *
       * @param {type} evnt
       * @param {type} selTab
       * @returns {undefined}
       */
      function  on630ChartOptionChange(evnt, selTab) {
        if (evnt !== undefined && evnt !== null) {
          evnt.stopImmediatePropagation();
        }

        scope.isfootNoteSymbolVisible = false;
        selectedCountryName = scope.selectedCountry.countryName;

        scope.selectedChartTab = selTab;
        scope.quickQuotationChartTypeSelected = scope.chartOptions[scope.selectedChartTab];
        scope.quickQuotationChartToolTip = scope.chartToolTips[scope.selectedChartTab];

        var chartType = scope.quickQuotationChartTypeSelected;
        var chartName = scope.quickQuotationChartName;
        var countryPrefix = selectedCountryName + " : ";

        var keyIndex = 0;
        for (var i = 0; i < countryNameList.length; i++) {
          if (countryNameList[i] === selectedCountryName) {
            keyIndex = i;
          }
        }

        //check for PACE Chart
        if (scope.isPaceChartVisible) {
          scope.quickQuotationChartLabel = countryPrefix + NO_OF_REG_USERS;

          loadPaceChart(scope.selectedChartTab);
          return;
        }

        if (isInitChartData === false) {
          initChartData();
        }

        switch (chartName) {
          case "ChartActiveUser":
          {
            scope.isfootNoteSymbolVisible = true;
            scope.quickQuotationChartLabel = countryPrefix + NO_OF_ACTIVE_USERS;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].uniqueActiveUsersChart.month;
            }
            break;
          }
          case "ChartProdAdded":
          {
            scope.quickQuotationChartLabel = countryPrefix + PRODUCT_ADDED;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].productsAddedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].productsAddedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].productsAddedChart.month;
            }
            break;
          }
          case "ChartProjCreated":
          {
            scope.quickQuotationChartLabel = countryPrefix + PROJECT_CREATED;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].projectCreatedChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].projectCreatedChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].projectCreatedChart.month;
            }
            break;
          }
          case "ChartBomExported":
          {
            scope.quickQuotationChartLabel = countryPrefix + BOM_EXPORTED;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].bomExportedNoChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].bomExportedNoChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].bomExportedNoChart.month;
            }
            break;
          }
          case "ChartBomExportedVal":
          {
            scope.quickQuotationChartLabel = countryPrefix + VAL_BOM_EXPORTED;
            if (chartType === "Day") {
              chartObj.data = scope.countries[keyIndex].bomExportedValChart.day;
            } else if (chartType === "Week") {
              chartObj.data = scope.countries[keyIndex].bomExportedValChart.week;
            } else if (chartType === "Month") {
              chartObj.data = scope.countries[keyIndex].bomExportedValChart.month;
            }
            break;
          }
        }
        //Set value to Scope
        scope.chartSource = chartObj;
      }

      /**
       *
       * @returns {undefined}

       function showCountryDialog() {
       var countryData = {};
       var totalRegisteredUser = scope.paceDataRegUser.ecoreal630UsersReg;
       countryData.containerWidth = 610;
       countryData.tableHeader = "Ecoreal Quick Quotation 630A";
       countryData.note1 = "* The Total user has been added with figure of " + scope.paceDataRegUser.unAssignedUser + " unaccountable registered users in PACE.";
       countryData.gridOptions = {
       enableVerticalScrollbar: 0,
       enableHorizontalScrollbar: 0
       };
       countryData.gridOptions.columnDefs = [
       {field: "label", name: "Country", width: 250, enableColumnMenu: false, enableSorting: false}
       ];

       for (var i = 0; i < countryNameList.length; i++) {
       countryData.gridOptions.columnDefs.push({
       field: "country_" + i,
       name: countryNameList[i],
       width: 110,
       enableColumnMenu: false,
       enableSorting: false
       });
       }

       countryData.gridOptions.columnDefs.push({field: "total", name: "Total - Worldwide", width: 120, enableColumnMenu: false, enableSorting: false});


       countryData.gridOptions.data = [{
       "label": "Launch Date",
       "total": "",
       "country_0": commonUtils.getLaunchDateFormat(scope.countries[0].launchDate),
       "country_1": commonUtils.getLaunchDateFormat(scope.countries[1].launchDate)
       }, {
       "label": "Value of BOM exported",
       "total": commonUtils.getMoneyFormat(scope.quickQuotTotalBomExportedVal, 0, " k€"),
       "country_0": commonUtils.getMoneyFormat(scope.countries[0].bomExportedVal, 0, " k€"),
       "country_1": commonUtils.getMoneyFormat(scope.countries[1].bomExportedVal, 0, " kRUB")
       }, {
       "label": "No. of BOM exported",
       "total": commonUtils.getNumberFormat(scope.quickQuotTotalBomExportedNo, 0),
       "country_0": commonUtils.getNumberFormat(scope.countries[0].bomExportedNo, 0),
       "country_1": commonUtils.getNumberFormat(scope.countries[1].bomExportedNo, 0)
       }, {
       "label": "No. of registered users",
       "total": commonUtils.getNumberFormat(totalRegisteredUser, 0) + " *",
       "country_0": commonUtils.getNumberFormat(scope.paceDataRegUser.ecoreal630SpainUsersReg, 0),
       "country_1": commonUtils.getNumberFormat(scope.paceDataRegUser.ecoreal630RussiaUsersReg, 0)
       }, {
       "label": "No. of unique active users for last 12 months",
       "total": commonUtils.getNumberFormat(scope.quickQuotTotalUsersUnique, 0),
       "country_0": commonUtils.getNumberFormat(scope.countries[0].uniqueActiveUsers, 0),
       "country_1": commonUtils.getNumberFormat(scope.countries[1].uniqueActiveUsers, 0)
       }, {
       "label": "No. of products added",
       "total": commonUtils.getNumberFormat(scope.quickQuotTotalProductsAdded, 0),
       "country_0": commonUtils.getNumberFormat(scope.countries[0].productsAdded, 0),
       "country_1": commonUtils.getNumberFormat(scope.countries[1].productsAdded, 0)
       }, {
       "label": "No. of projects created",
       "total": commonUtils.getNumberFormat(scope.quickQuotTotalProjectCreated, 0),
       "country_0": commonUtils.getNumberFormat(scope.countries[0].projectCreated, 0),
       "country_1": commonUtils.getNumberFormat(scope.countries[1].projectCreated, 0)
       }];

       $rootScope.$broadcast(appConstants.EVENTS.COUNTRY_DIALOG_BOX, countryData);
       }
       */
    } //link End
  }


})();