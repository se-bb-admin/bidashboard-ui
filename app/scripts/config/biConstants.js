/**
 * @description  Store All Constant for Project in a center Location
 *
 */
(function () {
  'use strict';
  var biConstants = {
    GLOBAL: '/registrations/global',
  };

  angular.module('se-branding').constant('biConstants', biConstants);
})();