/**
 * @description  Store All Constant for Project in a center Location
 *
 */
(function () {
  'use strict';
  /**
   * Question Screen
   * @type Object
   */
  var EVENTS = {
    'SHOW_LOADER': "show_loader",
    'HIDE_LOADER': "hide_loader",
    'RELOAD_CHART': "reload_chart",
    'SHOW_ERROR': "show_Error",
    'COUNTRY_DIALOG_BOX': "show_country_detail",
    'INVALID_LOGIN': "invalid_login"
  };
  var SITE_NAME = {
    'QUICK_QUOTATION_630_SPAIN': "QUICK_QUOTATION_630_SPAIN",
    'QUICK_QUOTATION_630_RUSSIA': "QUICK_QUOTATION_630_RUSSIA",
    'MY_PACT': "MY_PACT",
    'CBT': "CBT",
    'BLM': "BLM",
    'CAD_LIBRARY': "CAD_LIBRARY",
    'EASY_QUOTE': "EASY_QUOTE", // Done
    'RAPSODY': "RAPSODY",
    'SMART_SELECTORS': "SMART_SELECTORS",
    'BERRI_SOFT': "BERRI_SOFT",
    'ECOREAL_630': "ECOREAL_630",
    'ECO_DIAL_DESKTOP': "ECO_DIAL_DESKTOP",
    'ECO_DIAL_CHINA': "ECO_DIAL_CHINA",
    'ECO_DIAL_AUTOCAD_PLUGIN': "ECO_DIAL_AUTOCAD_PLUGIN",
    'ECO_REACH': "ECO_REACH",
    'CLIPSAL_WISHLIST': "CLIPSAL_WISHLIST",
    'NOT_LISTED': 'NOT_LISTED',
    'CCC_APP': "customer-care",
    'CCC_APP_ANDROID': "customer-care-android",
    'CCC_APP_IOS': "customer-care-ios",
    'MEA': 'mea',
    'MES': 'mes-estudio',
    'ECOREALMV': 'dynamo',
    'MY_NOVA_BUDDY': 'MY_NOVA_BUDDY',
    'ECO_REACH_2': 'ECO_REACH_2',
    'SAFE_REPOSITORY': 'SAFE_REPOSITORY',
    'SAFE_REPOSITORY_CLIENT_ID': 'SR'
  };
  var SITE_DETAILS_FOR_SUMMARY_PAGE = {
    'EASY_QUOTE': {
      'APP_NAME': "Easy Mobile Quote",
      'CLIENT_ID': "emq",
      'SITE_NAME': "EASY_QUOTE",
      'PATH': "/home/1"
    },
    'CLIPSAL_WISH_LIST': {
      'APP_NAME': "Clipsal Wishlist",
      'CLIENT_ID': "wishlist",
      'SITE_NAME': "CLIPSAL_WISH_LIST",
      'PATH': "/home/1"
    },
    'GLOBAL_METER_APP': {
      'APP_NAME': "Global Meter App",
      'CLIENT_ID': "gma",
      'SITE_NAME': "GLOBAL_METER_APP",
      'PATH': "/home/2"
    },
    'CLOSED_LOOP': {
      'APP_NAME': "Closed Loop",
      'CLIENT_ID': "closed-loop",
      'SITE_NAME': "CLOSED_LOOP",
      'PATH': "/home/2"
    },
    'MIX_N_MATCH': {
      'APP_NAME': "Mix n Match",
      'CLIENT_ID': "mixnmatch",
      'SITE_NAME': "MIX_N_MATCH",
      'PATH': "/home/3"
    },
    'WISE_UP': {
      'APP_NAME': "WiseUp",
      'CLIENT_ID': "wiseup",
      'SITE_NAME': "WISE_UP",
      'PATH': "/home/X" // NOT LINKED to details Page
    },
    'WISER_LINK': {
      'APP_NAME': "WiserLink",
      'CLIENT_ID': "wiserlink",
      'SITE_NAME': "WISER_LINK",
      'PATH': "/home/X" // NOT LINKED to details Page
    },
    'NULL_HOME': {
      'APP_NAME': "Null",
      'CLIENT_ID': "null_home",
      'SITE_NAME': "NULL_HOME",
      'PATH': "/home/X" // NOT LINKED to details Page
    },
    'QUICK_REF': {
      'APP_NAME': "Quick Ref",
      'CLIENT_ID': "quickref",
      'SITE_NAME': "QUICK_REF",
      'PATH': "/work/1"
    },
    'MY_PACT': {
      'APP_NAME': "MyPact",
      'CLIENT_ID': "mypact",
      'SITE_NAME': "MY_PACT",
      'PATH': "/work/1"
    },
    'QUICK_QUOTATION_630': {
      'APP_NAME': "Ecoreal Quick Quotation 630A",
      'CLIENT_ID': "quick-quotation",
      'SITE_NAME': "QUICK_QUOTATION_630",
      'PATH': "/work/2"
    },
    'CBT': {
      'APP_NAME': "CBT",
      'CLIENT_ID': "cbt",
      'SITE_NAME': "CBT",
      'PATH': "/work/2"
    },
    'BLM': {
      'APP_NAME': "BLM",
      'CLIENT_ID': "blm",
      'SITE_NAME': "BLM",
      'PATH': "/work/3"
    },
    'CAD_LIBRARY': {
      'APP_NAME': "CAD Library",
      'CLIENT_ID': "clo",
      'SITE_NAME': "CAD_LIBRARY",
      'PATH': "/work/3"
    },
    'SMART_SELECTORS': {
      'APP_NAME': "Smart Selector",
      'CLIENT_ID': "oemsmartselector",
      'SITE_NAME': "SMART_SELECTORS",
      'PATH': "/work/4"
    },
    'BERRI_SOFT': {
      'APP_NAME': "Berrisoft",
      'CLIENT_ID': "berrisoft",
      'SITE_NAME': "BERRI_SOFT",
      'PATH': "/work/4"
    },
    'ECOREAL_630': {
      'APP_NAME': "Ecoreal 630",
      'CLIENT_ID': "ecoreal630",
      'SITE_NAME': "ECOREAL_630",
      'PATH': "/work/5" //Not Linked to page
    },
    'NULL_WORK': {
      'APP_NAME': "Null",
      'CLIENT_ID': "null_work",
      'SITE_NAME': "NULL_WORK",
      'PATH': "/work/X" //Not Linked to page
    },
    'RAPSODY': {
      'APP_NAME': "Rapsody",
      'CLIENT_ID': "rapsody",
      'SITE_NAME': "RAPSODY",
      'PATH': "/work/5"
    },
    'ECO_DIAL_DESKTOP': {
      'APP_NAME': "Ecodial Desktop",
      'CLIENT_ID': "ecodial",
      'SITE_NAME': "ECO_DIAL_DESKTOP",
      'PATH': "/work/6"
    },
    'ECO_DIAL_CHINA': {
      'APP_NAME': "Ecodial AutoCAD Plugin China",
      'CLIENT_ID': "ecodial-autocad-plugin-china",
      'SITE_NAME': "ECO_DIAL_CHINA",
      'PATH': "/work/6"
    },
    'ECO_DIAL_AUTOCAD_PLUGIN': {
      'APP_NAME': "Ecodial AutoCAD Plugin",
      'CLIENT_ID': "ecodial-autocad-plugin",
      'SITE_NAME': "ECO_DIAL_AUTOCAD_PLUGIN",
      'PATH': "/work/7"
    },
    'ECO_REACH': {
      'APP_NAME': "Ecoreach",
      'CLIENT_ID': "ecoreach",
      'SITE_NAME': "ECO_REACH",
      'PATH': "/work/7"
    },
    'CCC_APP': {
      'APP_NAME': "CCC App",
      'CLIENT_ID': "cccApp",
      'SITE_NAME': "CCC_APP",
      'PATH': "/work/8"
    },
    'MEA_APP': {
      'APP_NAME': "My Electric App",
      'CLIENT_ID': "mea",
      'SITE_NAME': "MEA_APP",
      'PATH': "/home/3"
    },
    'ECO_REACH_2': {
      'APP_NAME': "Ecoreach 2.x",
      'CLIENT_ID': "ecoreach2",
      'SITE_NAME': "ECO_REACH_2",
      'PATH': "/work/7"
    },
    'MY_NOVA_BUDDY': {
      'APP_NAME': "Masterpact MTZ App",
      'CLIENT_ID': "my-nova-buddy",
      'SITE_NAME': "MY_NOVA_BUDDY",
      'PATH': "/work/8"
    }
  };
  var PAGE_TITLE = {
    WORK: {
      PAGE_1: "QuickRef | MyPact",
      PAGE_2: "Ecoreal QQ 630A | CBT",
      PAGE_3: "BLM | CAD Library",
      PAGE_4: "Smart Selectors | Berrisoft ",
      PAGE_5: "Ecoreal 630 | Rapsody ",
      PAGE_6: "Ecodial Desktop | Ecodial China ",
      PAGE_7: "Ecodial AutoCAD Plugin | Ecoreach",
      PAGE_8: "Masterpact MTZ"
    },
    REPORT: {
      PAGE_1: "ePlan",
      PAGE_2: "BIM",
      PAGE_3: "TracePart"
    },
    OTHERS: {
      PAGE_1: 'Ecoreal MV'
    }

  };
  var SITE_GA_IDS = {
    QUICK_REF_ID: "ga:93886648", //PROD env(UA-52016136-2) -> No GOMEZ TRAFFIC
    BLM_ID: "ga:98057158", //Building lifecycle management (Prod)(UA-59819663-2) -> All Web Site Data
    MY_PACT_BASIC_ID: "ga:91188238", // PROD env(UA-52015049-2) -> Real Users (no Gomez)
    MY_PACT_INTEGRATED_ID: "ga:95888221", // MyPact INTEGRATED (Prod)(UA-52595516-2) -> Real Users (Not Gomez)
    QUICK_QUOTATION_630_SPAIN_ID: "ga:84293563",
    QUICK_QUOTATION_630_RUSSIA_ID: "ga:89037778",
    QUICK_QUOTATION_630_BACKUP_SPAIN_ID: "ga:95187588",
    QUICK_QUOTATION_630_BACKUP_RUSSIA_ID: "ga:95214102",
    CBT_AUSTRALIA_ID: "ga:98504757",
    CBT_BELGIUM_ID: "ga:98458799",
    CBT_BRAZIL_ID: "ga:98469200",
    CBT_DENMARK_ID: "ga:98493474",
    CBT_FRANCE_ID: "ga:98469501",
    CBT_GERMANY_ID: "ga:98466613",
    CBT_GLOBAL_WEBSITE_ID: "ga:62141561",
    CBT_INDIA_ID: "ga:98382578",
    CBT_NETHERLANDS_ID: "ga:98472232",
    CBT_POLAND_ID: "ga:98473737",
    CBT_RUSSIA_ID: "ga:64863001",
    CBT_SAUDI_ARABIA_ID: "ga:98502154",
    CBT_SLOVAKIA_ID: "ga:98499981",
    CBT_SPAIN_ID: "ga:98467608",
    CBT_SWITZERLAND_ID: "ga:98471352",
    CBT_TURKEY_ID: "ga:98470746",
    CBT_VIETNAM_ID: "ga:98442057",
    EASY_QUOTE_ID: "ga:90618898", //Easy mobile quote(UA-54375474-1)
    CLIPSAL_WISH_LIST_AUSTRALIA_ID: "ga:98376898", //ClipsalWishlist-Prod(UA-60049394-1)
    GLOBAL_METER_APP_ID: "ga:94055678",
    CAD_LIBRARY_FRANCE_ID: "ga:98868918",
    SMART_SELECTORS_ID: "ga:110283331",
    BERRI_SOFT_ID: "ga:106564840",
    CLOSED_LOOP_ID: "ga:97060502",
    RAPSODY_ID: "ga:XXXXXXXX",
    ECO_DIAL_DESKTOP_ID: "ga:XXXXXXXX",
    ECO_DIAL_CHINA_ID: "ga:XXXXXXXX",
    ECO_DIAL_AUTOCAD_PLUGIN_ID: "ga:XXXXXXXX",
    ECO_REACH: "ga:128161386",
    CCC_APP: "ga:95054041",
    MY_NOVA_BUDDY_ID: "ga:95054041"
  };
  var DATES = {
    MONTH_ARR_LONG: [
      {value: 1, label: 'January (01)'},
      {value: 2, label: 'February (02)'},
      {value: 3, label: 'March (03)'},
      {value: 4, label: 'April (04)'},
      {value: 5, label: 'May (05)'},
      {value: 6, label: 'June (06)'},
      {value: 7, label: 'July (07)'},
      {value: 8, label: 'August (08)'},
      {value: 9, label: 'September (09)'},
      {value: 10, label: 'October (10)'},
      {value: 11, label: 'November (11)'},
      {value: 12, label: 'December (12)'}],
    MONTH_ARR_SHORT: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    DATA_YEARS: [2014, 2015, 2016, 2017],
    MNM_YEARS: [2016],
    WISEUP_YEARS: [2016],
    APP_START_YEAR: 2014,
    DATA_YEARS_FILE_UPLOAD: [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017],
    TODAY: new Date(new Date().getFullYear(), (new Date().getMonth() + 1), new Date().getDate()),
    YESTERDAY: "yesterday",
    START_DATE_DAY: "30daysAgo",
    START_DATE_WEEK: "182daysAgo",
    START_DATE_MONTH: "365daysAgo",
    START_DATE_ONE_YEAR_AGO: "365daysAgo",
    END_DATE: "yesterday",
    QUICK_REF_LAUNCHED_DATE: new Date("2014", "06", "01"), // YYYY MM-1 DD
    QUICK_REF_LAUNCHED_DATE_STR: "2014-07-01", // YYYY MM DD
    MY_PACT_BASIC_LAUNCHED_DATE: new Date("2014", "08", "15"), // YYYY MM-1 DD
    MY_PACT_BASIC_LAUNCHED_DATE_STR: "2014-09-15", // YYYY MM DD
    MY_PACT_INTEGRATED_LAUNCHED_DATE: new Date("2014", "08", "15"), // YYYY MM-1 DD
    MY_PACT_INTEGRATED_LAUNCHED_DATE_STR: "2014-09-15", // YYYY MM DD
    QUICK_QUOTATION_630_SPAIN_LAUNCHED_DATE: new Date("2014", "08", "23"), // YYYY MM-1 DD
    QUICK_QUOTATION_630_SPAIN_LAUNCHED_DATE_STR: "2014-09-23", // YYYY MM DD
    QUICK_QUOTATION_630_RUSSIA_LAUNCHED_DATE: new Date("2014", "09", "09"), // YYYY MM-1 DD
    QUICK_QUOTATION_630_RUSSIA_LAUNCHED_DATE_STR: "2014-10-09", // YYYY MM DD
    QUICK_QUOTATION_630_BACKUP_SPAIN_LAUNCHED_DATE: new Date("2014", "11", "17"), // YYYY MM-1 DD
    QUICK_QUOTATION_630_BACKUP_SPAIN_LAUNCHED_DATE_STR: "2014-12-17", // YYYY MM DD
    QUICK_QUOTATION_630_BACKUP_RUSSIA_LAUNCHED_DATE: new Date("2014", "11", "17"), // YYYY MM-1 DD
    QUICK_QUOTATION_630_BACKUP_RUSSIA_LAUNCHED_DATE_STR: "2014-12-17", // YYYY MM DD
    ECOREAL_630_FRANCE_LAUNCHED_DATE: new Date("2016", "04", "01"), // YYYY MM-1 DD
    ECOREAL_630_FRANCE_LAUNCHED_DATE_STR: "2016-04-01",
    CBT_AUSTRALIA_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_AUSTRALIA_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_BELGIUM_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_BELGIUM_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_BRAZIL_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_BRAZIL_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_DENMARK_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_DENMARK_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_FRANCE_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_FRANCE_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_GERMANY_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_GERMANY_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_GLOBAL_WEBSITE_LAUNCHED_DATE: new Date("2012", "06", "19"), // YYYY MM-1 DD
    CBT_GLOBAL_WEBSITE_LAUNCHED_DATE_STR: "2012-07-19", // YYYY MM DD
    CBT_INDIA_LAUNCHED_DATE: new Date("2015", "01", "24"), // YYYY MM-1 DD
    CBT_INDIA_LAUNCHED_DATE_STR: "2015-02-24", // YYYY MM DD
    CBT_NETHERLANDS_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_NETHERLANDS_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_POLAND_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_POLAND_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_RUSSIA_LAUNCHED_DATE: new Date("2012", "09", "09"), // YYYY MM-1 DD 09-Oct-12
    CBT_RUSSIA_LAUNCHED_DATE_STR: "2012-10-09", // YYYY MM DD
    CBT_SAUDI_ARABIA_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_SAUDI_ARABIA_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_SLOVAKIA_LAUNCHED_DATE: new Date("2015", "01", "26"), // YYYY MM-1 DD
    CBT_SLOVAKIA_LAUNCHED_DATE_STR: "2015-02-26", // YYYY MM DD
    CBT_SPAIN_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_SPAIN_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_SWITZERLAND_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_SWITZERLAND_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_TURKEY_LAUNCHED_DATE: new Date("2015", "01", "25"), // YYYY MM-1 DD
    CBT_TURKEY_LAUNCHED_DATE_STR: "2015-02-25", // YYYY MM DD
    CBT_VIETNAM_LAUNCHED_DATE: new Date("2015", "01", "24"), // YYYY MM-1 DD
    CBT_VIETNAM_LAUNCHED_DATE_STR: "2015-02-24", // YYYY MM DD
    BLM_LAUNCHED_DATE: new Date("2014", "11", "19"), // YYYY MM-1 DD
    BLM_LAUNCHED_DATE_STR: "2014-12-19", // YYYY MM DD
    RAPSODY_LAUNCHED_DATE: new Date("2015", "02", "01"), // YYYY MM-1 DD
    RAPSODY_LAUNCHED_DATE_STR: "2015-03-01", // YYYY MM DD
    SMART_SELECTORS_LAUNCHED_DATE: new Date("2015", "10", "04"), // YYYY MM-1 DD
    SMART_SELECTORS_FRANCE_LAUNCHED_DATE: new Date("2016", "02", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_ITALY_LAUNCHED_DATE: new Date("2015", "12", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_SPAIN_LAUNCHED_DATE: new Date("2016", "02", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_NETHERLANDS_LAUNCHED_DATE: new Date("2015", "12", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_BRAZIL_LAUNCHED_DATE: new Date("2015", "12", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_SOUTH_KOREA_LAUNCHED_DATE: new Date("2016", "01", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_INDONESIA_LAUNCHED_DATE: new Date("2016", "01", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_GERMANY_LAUNCHED_DATE: new Date("2016", "03", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_UNITED_KINGDOM_LAUNCHED_DATE: new Date("2016", "03", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_SWITZERLAND_LAUNCHED_DATE: new Date("2016", "03", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_BELGIUM_LAUNCHED_DATE: new Date("2016", "03", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_RUSSIA_LAUNCHED_DATE: new Date("2016", "02", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_TURKEY_LAUNCHED_DATE: new Date("2016", "03", "01"), // YYYY MM-1 DD
    SMART_SELECTORS_LAUNCHED_DATE_STR: "2015-11-04", // YYYY MM DD
    BERRI_SOFT_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRI_SOFT_LAUNCHED_DATE_STR: "2015-09-24", // YYYY MM DD
    /* ECO DIAL DESKTOP LAUNCH DATES */
    ECO_DIAL_DESKTOP_LAUNCHED_DATE: new Date("2015", "04", "25"), // YYYY MM-1 DD
//        ECO_DIAL_DESKTOP_BULGARIA_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_CZECKREPUBLIC_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_DENMARK_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_GERMANY_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_INDONESIA_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_SPAIN_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_AUSTRALIA_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_COLOMBIA_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_INDIA_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_VIETNAM_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_BELGIUM_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_BRAZIL_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_NETHERLANDS_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_POLAND_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_SAUDI_ARABIA_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_TURKEY_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_FRANCE_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_HUNGARY_LAUNCHED_DATE: new Date("2016","06","01"),
//        ECO_DIAL_DESKTOP_SOUTH_AFRICA_LAUNCHED_DATE: new Date("2016","06","01"),

    ECODIAL_DESKTOP_LAUNCH_DATES: {
      "BULGARIA": new Date("2016", "06", "01"),
      "CZECH REPUBLIC": new Date("2016", "06", "02"),
      "DENMARK": new Date("2016", "06", "03"),
      "GERMANY": new Date("2016", "06", "01"),
      "INDONESIA": new Date("2016", "06", "01"),
      "SPAIN": new Date("2016", "06", "01"),
      "AUSTRALIA": new Date("2016", "06", "01"),
      "COLOMBIA": new Date("2016", "06", "01"),
      "INDIA": new Date("2016", "06", "01"),
      "VIETNAM": new Date("2016", "06", "01"),
      "BELGIUM": new Date("2016", "06", "01"),
      "BRAZIL": new Date("2016", "06", "01"),
      "NETHERLANDS": new Date("2016", "06", "01"),
      "poland": new Date("2016", "06", "01"),
      "SAUDI ARABIA": new Date("2016", "06", "01"),
      "TURKEY": new Date("2016", "06", "01"),
      "FRANCE": new Date("2016", "06", "01"),
      "HUNGARY": new Date("2016", "06", "01"),
      "SOUTH AFRICA": new Date("2016", "06", "01")
    },
    /* END: ECO DIAL DESKTOP LAUNCH DATES */
    ECO_DIAL_DESKTOP_LAUNCHED_DATE_STR: "2015-05-25", // YYYY MM DD
    ECO_DIAL_CHINA_LAUNCHED_DATE: new Date("2015", "09", "05"), // YYYY MM-1 DD
    ECO_DIAL_CHINA_LAUNCHED_DATE_STR: "2015-10-05", // YYYY MM DD
    ECO_DIAL_AUTOCAD_PLUGIN_LAUNCHED_DATE: new Date("2015", "09", "05"), // YYYY MM-1 DD
    ECO_DIAL_AUTOCAD_PLUGIN_LAUNCHED_DATE_STR: "2015-10-05", // YYYY MM DD
    ECO_REACH_1_LAUNCHED_DATE: new Date("2015", "07", "01"), // YYYY MM-1 DD
    ECO_REACH_2_LAUNCHED_DATE: new Date("2016", "05", "30"), // YYYY MM-1 DD
    ECO_REACH_LAUNCHED_DATE_STR: "2015-08-01", // YYYY MM DD
    ECO_REACH_2_LAUNCHED_DATE_STR: "2016-06-30", // YYYY MM DD
    EASY_QUOTE_BRAZIL_LAUNCHED_DATE: new Date("2015", "04", "04"), // YYYY MM-1 DD
    EASY_QUOTE_BRAZIL_LAUNCHED_DATE_STR: "2015-05-04", // YYYY MM DD
    EASY_QUOTE_RUSSIA_LAUNCHED_DATE: new Date("2015", "04", "04"), // YYYY MM-1 DD
    EASY_QUOTE_RUSSIA_LAUNCHED_DATE_STR: "2015-05-04", // YYYY MM DD
    EASY_QUOTE_SPAIN_LAUNCHED_DATE: new Date("2015", "10", "27"), // YYYY MM-1 DD
    EASY_QUOTE_SPAIN_LAUNCHED_DATE_STR: "2015-11-27", // YYYY MM DD
    EASY_QUOTE_SOUTH_AFRICA_LAUNCHED_DATE: new Date("2016", "02", "03"), // YYYY MM-1 DD
    EASY_QUOTE_SOUTH_AFRICA_LAUNCHED_DATE_STR: "2016-03-03", // YYYY MM DD
    CLIPSAL_WISH_LIST_AUSTRALIA_LAUNCHED_DATE: new Date("2015", "01", "24"), // YYYY MM-1 DD
    CLIPSAL_WISH_LIST_AUSTRALIA_LAUNCHED_DATE_STR: "2015-02-24", // YYYY MM DD
    GLOBAL_METER_APP_LAUNCHED_DATE: new Date("2014", "11", "19"), // YYYY MM-1 DD
    GLOBAL_METER_APP_LAUNCHED_DATE_STR: "2014-12-19", // YYYY MM DD
    CAD_LIBRARY_FRANCE_LAUNCHED_DATE: new Date("2015", "02", "13"), // YYYY MM-1 DD
    CAD_LIBRARY_FRANCE_LAUNCHED_DATE_STR: "2015-03-13", // YYYY MM DD
    CLOSED_LOOP_ID_LAUNCHED_DATE: new Date("2015", "03", "27"), // YYYY MM-1 DD
    CLOSED_LOOP_ID_LAUNCHED_DATE_STR: "2015-04-27", // YYYY MM DD

    CCC_APP_GLOBAL_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_CHINA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_UNITED_STATES_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_FRANCE_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_INDIA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_INDONESIA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_SPAIN_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_CANADA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_BRAZIL_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_GERMANY_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_TAIWAN_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_TURKEY_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_RUSSIA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_THAILAND_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_ITALY_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_MOROCCO_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_CHILE_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_AUSTRALIA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_UNITED_KINGDOM_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_NEW_ZEALAND_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_SOUTH_KOREA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_PERU_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_SWEDEN_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_MALAYSIA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_ARGENTINA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD
    CCC_APP_COLOMBIA_LAUNCHED_DATE: new Date("2015", "01", "01"), // YYYY MM-1 DD

    BERRISOFT_FRANCE_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_ITALY_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_SPAIN_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_NETHERLANDS_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_BRAZIL_LAUNCHED_DATE: new Date("2016", "03", "02"), // YYYY MM-1 DD
    BERRISOFT_INDONESIA_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_GERMANY_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_UNITED_KINGDOM_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_BELGIUM_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_TURKEY_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_RUSSIA_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_UNITED_STATES_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_INDIA_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_AUSTRALIA_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_MEXICO_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_POLAND_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_CHINA_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_SOUTH_AFRICA_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_SAUDI_ARABIA_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD
    BERRISOFT_THAILAND_LAUNCHED_DATE: new Date("2016", "04", "03"), // YYYY MM-1 DD

    MEA_FRANCE_LAUNCHED_DATE: new Date("2016", "06", "10"), //YYYY MM-1 DD
    MEA_LAUNCH_DATES: {
//			"France": new Date("2016", "06", "10")
      "France": null,
      "Spain": null
    },
    MY_NOVA_BUDDY_LAUNCHED_DATES: {
      "France": new Date("2016-10-17"),
      "Spain": new Date("2016-10-17")
    },
    ECOREACH_LAUNCHED_DATES: {
      "France": new Date("2016-10-17"),
      "Spain": new Date("2016-10-17")
    }
  };
  //
  var MESSAGE = {
    SERVICE_FAIL_ERROR_MSG: "Unable to load data from server, Please refresh the page after a few second.",
    AUTH_FAIL_ERROR_MSG: "Please login with valid google analytic user.",
    TRACE_PART_UPLOAD_SUCCESS_PARSE_SUCCESS_MSG: "File has been uploaded & parsed successfully",
    TRACE_PART_UPLOAD_SUCCESS_SAVE_FAIL_MSG: "File has been uploaded & but unable to save data.",
    TRACE_PART_UPLOAD_FAIL_PARSE_FAIL_MSG: "Unable to upload or parse uploaded file."
  };
  var NOTE = {
    UNIQUE_ACTIVE_USER: "Users are unique only for the specific day/week/month chosen.",
    ZOOM_LABEL: "All countries data",
    SUMMARY_FOOTER_QUICK_REF: "QuickRef currently tracks Active Users via GA and is not currently linked to PACE for Registration.",
    SUMMARY_FOOTER_QUICK_QUOTATION_630: " After third quarter this project will be closed.",
    EASY_QUOTE_BOM_FOOTER_NOTE: "We are capturing BOM value in GA from dated : 25 Aug 2015.",
    GLOBAL_METER_FOOTER_NOTE: "Fetching Register Users data from GA.",
    REG_USR_HOME_FOOTER_NOTE: "The Register User statistics for @HOME is shown only for Clipsal Wishlist.",
    TOTAL_WORK_FOOTER_NOTE: "<b><i>Ecoreal Quick Quotation 630A</i></b> is not included in Total KPI data.",
    NULL_PROJECT_FOOTER_NOTE: "These user are not linked to any particular application.",
    WORK_GRAND_TOTAL_FOOTER_NOTE: "The Grand Total KPI Number includes QuickRef, MyPact, BLM, CBT, Smart Selectors, Berrisoft, Ecoreal 630, CAD Library and Null.",
    YEAR_2017_NOTE: "This includes data for 2016 and 2017",
    TRACE_PART_ONLINE_FILE_UP_LOAD_NOTE: "Please upload valid Excel file (.XLS or .XLSX).",
    USER_SESSIONS: "Total number of Sessions within the date range. A session is the period time a user is actively engaged with your website, app, etc. All usage data (Screen Views, Events, Ecommerce, etc.) is associated with a session."
  };
  var TIMER = {
    CHART_LOAD_DELAY: 3000
  };
  var ERROR = {
    TYPE: {
      ERROR: 'error',
      WARNING: 'warning',
      INFO: 'info'
    },
    ERROR_CODE_VAL: {
      E001: 'You are not authorized to access this site.',
      E002: 'You are not authorized to access this site.',
      E003: 'You are not authorized to access this Page.',
      E010: 'PACE server is Down.'
    }
  };
  var CHART_OPTIONS = {
    CHART_OPTIONS_VALUES: ['Day', 'Week', 'Month'],
    CHART_OPTIONS_TOOL_TIPS: ['Data for last 30 days', 'Data for last 6 month', 'Data for last one year'],
    CHART_OPTIONS_DFLT_SEL_IDX: 2,
    CHART_TYPE_VALUES: ['Line', 'Column', 'Pie'],
    MONTH_ARR: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  };
  var NODE_REST_URL = {
    PACE_REG_USER_URL: 'pace/regUserSummary',
    PACE_REG_USER_OVER_PERIOD_URL: 'pace/regUserLastOneYear',
    PACE_REG_USER_ON_CURRENT_YEAR_URL: 'pace/regUserOnCurrentYear',
    PACE_REG_USER_BY_YEAR_BY_QTR: 'pace/regUsersByYearByQuarter',
    CURRENCY_COVERTER_RUB_EUR: 'currencyConverter/convertCurrency/RUB_EUR',
    CURRENCY_COVERTER_BRL_EUR: 'currencyConverter/convertCurrency/BRL_EUR',
    BLM_REPORT_DATA_URL: 'ga/BLM/report',
    BLM_CHART_DATA_URL: 'ga/BLM/chart',
    CBT_REPORT_DATA_URL: 'ga/CBT/report',
    CBT_CHART_DATA_URL: 'ga/CBT/chart',
    SMART_SELECTORS_REPORT_DATA_URL: 'ga/SmartSelectors/report', //'https://localhost:9000/data/SmartSelectors_Report.json',//,
    SMART_SELECTORS_CHART_DATA_URL: 'ga/SmartSelectors/chart', // 'https://localhost:9000/data/SmartSelectors_Chart.json', //,
    CAD_LIBRARY_REPORT_DATA_URL: 'ga/CadLib/report',
    CAD_LIBRARY_CHART_DATA_URL: 'ga/CadLib/chart',
    CLIPSAL_WISHLIST_REPORT_DATA_URL: 'ga/ClipsalWishlist/report',
    CLIPSAL_WISHLIST_CHART_DATA_URL: 'ga/ClipsalWishlist/chart',
    EASY_QUOTE_REPORT_DATA_URL: 'ga/EasyQuote/report',
    EASY_QUOTE_CHART_DATA_URL: 'ga/EasyQuote/chart',
    BERRI_SOFT_REPORT_DATA_URL: 'ga/BerriSoft/report',
    BERRI_SOFT_CHART_DATA_URL: 'ga/BerriSoft/chart',
    ECOREAL_QUICK_QUOTATION_REPORT_DATA_URL: 'ga/EcorealQQ630/report',
    ECOREAL_QUICK_QUOTATION_CHART_DATA_URL: 'ga/EcorealQQ630/chart',
    ECOREAL_630_REPORT_DATA_URL: 'ga/Ecoreal630/report',
    ECOREAL_630_CHART_DATA_URL: 'ga/Ecoreal630/chart',
    GLOBAL_METER_REPORT_DATA_URL: 'ga/GlobalMeter/report',
    GLOBAL_METER_CHART_DATA_URL: 'ga/GlobalMeter/chart',
    MY_PACT_REPORT_DATA_URL: 'ga/MyPact/report',
    MY_PACT_CHART_DATA_URL: 'ga/MyPact/chart',
    QUICK_REF_REPORT_DATA_URL: 'ga/QuickRef/report',
    QUICK_REF_CHART_DATA_URL: 'ga/QuickRef/chart',
    SUMMARY_REPORT_DATA_URL: 'ga/Summary/report',
    GET_TOKEN_URL: 'pace/getToken',
    REFRESH_TOKEN_URL: 'pace/getToken',
    CLOSED_LOOP_REPORT_DATA_URL: 'ga/ClosedLoop/report',
    CLOSED_LOOP_CHART_DATA_URL: 'ga/ClosedLoop/chart',
    ECO_REACH_COUNTRY_LIST_URL: 'offline/ecoreach/countries',
    ECO_REACH_REPORT_URL: 'offline/ecoreach/report',
    ECO_REACH_GA_REPORT_URL: 'ga/ecoreach/report',
    ECO_REACH_CHART_URL: 'offline/ecoreach/chart',
    ECO_REACH_GA_CHART_URL: 'ga/ecoreach/chart',
    SUMMARY_KPI_URL: 'Summary/KPI',
    TRACE_PART_FILE_UPLOAD_URL: 'upload/CADLib',
    ECO_REACH_FILE_UPLOAD_URL: 'upload/EcoReach',
    EPLAN_FILE_UPLOAD_URL: 'upload/EPlan',
    BIM_FILE_UPLOAD_URL: 'upload/BIMObject',
    MNM_FILE_UPLOAD_URL: 'upload/mixnmatch',
    REGISTERED_USERS_UPLOAD_URL: 'upload/registeredUsers', // registered users
    EPLAN_PRODUCT_GROUP_LIST_URL: 'upload/EPlan/productGroup/list',
    EPLAN_PRODUCT_GROUP_TYPES_URL: 'EPlan/productGroupTypes/list',
    EPLAN_DATA_SUMMARY_URL: 'Eplan/summary',
    EPLAN_COUNTRY_LIST_URL: 'businessReport/EPlan/countryList',
    EPLAN_COMPANY_WISE_REPORT_URL: 'businessReport/EPlan/report/companyWise',
    EPLAN_COMPANY_COUNT_URL: 'businessReport/EPlan/report/companyWise/companyCounts',
    EPLAN_COMPANY_WISE_LIFETIMT_DOWNLOADS_URL: 'businessReport/EPlan/report/companyWise/lifeTimeDownload',
    EPLAN_PRODUCT_GROUP_REPORT_URL: 'businessReport/EPlan/report/productGroup',
    EPLAN_PRODUCT_GROUP_LIFETIME_DOWNLOADS_URL: 'businessReport/EPlan/report/productGroup/lifeTimeDownload',
    EPLAN_PRODUCT_GROUP_COUNT_URL: 'businessReport/EPlan/report/productGroup/productGroupCounts',
    BIM_COUNTRY_LIST_URL: "businessReport/BIMObject/countryList",
    BIM_PRODUCT_LIST_URL: "businessReport/BIMObject/productList",
    BIM_GENERAL_DOWNLOAD_REPORT_URL: "businessReport/BIMObject/report/generalDownload",
    BIM_GENERAL_DOWNLOAD_LIFETIME_DOWNLOADS_URL: "businessReport/BIMObject/report/lifeTimeDownload",
    BIM_VIEW_DOWNLOAD_REPORT_URL: "businessReport/BIMObject/report/viewedDownload",
    BIM_USER_COUNTRY_LIST_URL: "businessReport/BIMObject/userCountryList",
    BIM_USER_DOWNLOAD_REPORT_URL: "businessReport/BIMObject/report/users",
    MNM_COUNTRY_LIST_URL: 'businessReport/MixnMatch/userCountryList',
    MNM_REPORT_URL: 'businessReport/MixnMatch/report/getVisitorInformation',
    ECO_REACH_FILE_UPLOAD_SAMPLE_FILE_URL: '/upload-sample-files/EchoReachUers.xlsx',
    CCC_APP_REPORT_DATA_URL: 'ga/cccapp/report',
    CCC_APP_CHART_DATA_URL: 'ga/cccapp/chart',
    MEA_REPORT_DATA_URL: 'ga/meaapp/report',
    MEA_CHART_DATA_URL: 'ga/meaapp/chart',
    REGISTERED_USERS_URL: 'offline/registeredUsers',
    ECODIAL_USERS_URL: 'offline/registeredUsers/ecodial',
    ECODIAL_OFFLINE_CHART_DAY_URL: 'offline/chart/day/ecodial',
    ECODIAL_OFFLINE_CHART_WEEK_URL: 'offline/chart/week/ecodial',
    ECODIAL_OFFLINE_CHART_MONTH_URL: 'offline/chart/month/ecodial',
    ECODIAL_OFFLINE_CHART_URL: 'offline/chart/ecodial',
    TRACEPART_TOTALS: 'traceparts/totalcounts',
    TRACEPART_TOTAL_DOWNLOADS_URL: 'traceparts/totaldownloads',
    TRACEPART_DOWNLOADS_LIST_URL: 'traceparts/downloads',
    TRACEPART_TOP_TEN_PRODUCT_DOWNLOADS_URL: 'traceparts/toptenproductdownloads',
    TRACEPART_TREND_DOWNLOADS_URL: 'traceparts/trend/downloads',
    TRACEPART_TREND_DOWNLOADS_AND_USERS_URL: 'traceparts/trend/downloadsAndUsers',
    TRACEPART_TREND_COUNTRIES_URL: 'traceparts/trend/countries',
    TRACEPART_TREND_TOPTEN_COUNTRIES_URL: 'traceparts/trend/toptencountries',
    TRACEPART_TREND_PRODUCT_DOWNLOADS_URL: 'traceparts/trend/toptenproductdownloads',
    REGISTERED_USERS_MAX_DATE_URL: 'offline/registeredUsers/maxdate',
    ECO_DIAL_DESKTOP_QTR_DATA_URL: 'offline/ecodial/qtr',
    ECO_DIAL_DESKTOP_WORLDWIDE_TOTAL_URL: 'offline/worldwide/ecodial',
    ECO_DIAL_DESKTOP_WORLDWIDE_DAY_URL: 'offline/worldwide/day/ecodial',
    ECO_DIAL_DESKTOP_WORLDWIDE_WEEK_URL: 'offline/worldwide/week/ecodial',
    ECO_DIAL_DESKTOP_WORLDWIDE_MONTH_URL: 'offline/worldwide/month/ecodial',
    MY_NOVA_BUDDY_APP_REPORT_DATA_URL: 'ga/mynovabuddy/report',
    MY_NOVA_BUDDY_APP_CHART_DATA_URL: 'ga/mynovabuddy/chart'
  };


  var NODE_REST_DOMAIN = {
    LOCAL: {
      // BASE_DOMAIN: 'https://localhost:3000/statserver/'
      BASE_DOMAIN: 'https://localhost:3000/statserver/'
    },
    DEBUG: {
      BASE_DOMAIN: 'https://dashboard.schneider-electric.com/statserverdev/'
    },
    SIT: {
      BASE_DOMAIN: 'https://dashboard.schneider-electric.com/statserverdev/'
    },
    PROD: {
      BASE_DOMAIN: 'https://dashboard.schneider-electric.com/statserver/'
    }
  };

  var PACE_LOGIN_REDIRECT_URL = {
    LOCAL: {
//			APP_URL: 'https://localhost:9000'
      APP_URL: 'https://dashboard.schneider-electric.com/ManagementDashboardWork'
    },
    DEBUG: {
      APP_URL: 'https://dashboard.schneider-electric.com/ManagementDashboardDebug'
    },
    SIT: {
      APP_URL: 'https://dashboard.schneider-electric.com/ManagementDashboardWork'
    },
    PROD: {
      APP_URL: 'https://dashboard.schneider-electric.com/ManagementDashboard'
    },
    AUTHENTICATION_SERVER_URL: 'https://login.dces.schneider-electric.com'
  };

  var PACE_CHART = {
    LABEL: {
      REG_USER: "Registered Users"
    }
  };

  var FILTER = {
    SERVICE_PROVIDER: 'ga:networkLocation!~^*zscaler*;ga:networkLocation!~^*mphasis*;ga:networkLocation!~^*schneider*'
  };
  var UPLOAD_FILE = {
    PROJECT_LIST_DROP_DOWN: [
      {value: 0, label: "Trace Part Online"},
      {value: 1, label: "EcoReach"},
      {value: 2, label: "EPlan"},
      {value: 3, label: "BIM"},
      {value: 4, label: "Mix n Match"},
      {value: 5, label: "Registered Users"}
    ],
    TRACE_PART_BUSINESS_UNIT_DROP_DOWN: [
      {value: 1, label: 'Automation and Control'},
      {value: 2, label: 'Electrical Distribution'}
    ],
    EPLAN_REPORT_TYPE_DROP_DOWN: [
      {value: 1, label: 'Company Report'},
      {value: 2, label: 'Product Group Report'},
      {value: 3, label: 'Product Group Details Report'}
    ],
    MNM_REPORT_TYPE_DROP_DOWN: [
      {value: 1, label: 'Visitor Report'},
      {value: 2, label: 'Detail Click Report'}
    ]
  };

  var ADMIN_ID = [
    {id: "SESA000000", email: "schneider.analytic@gmail.com", access: [0, 1, 2, 3, 4, 5]}, //It is Index of PROJECT_LIST_DROP_DOWN not the ID
    // {id: "SESA000000", email: "analytics.mphasis@gmail.com", access: [0, 1, 2, 3, 4]},
    // {id: "SESA341790", email: "kalpataru.roy@mphasis.com", access: [0, 1, 2, 3, 4]},
    //{id: "SESA341774", email: "mithil.shukla@non.schneider-electric.com", access: [0, 1, 2, 3, 4, 5]},
    {id: "SESA336847", email: "christopher.lewis@schneider-electric.com", access: [0, 1, 2, 3, 4, 5]},
    {id: "SESA285847", email: "christopher.david@schneider-electric.com", access: [0, 1, 2, 3, 4, 5]},
    {id: "SESA98103", email: "olga.mareeva@schneider-electric.com", access: [0, 2, 3]}, //   "Trace Part Online", EPlan
    {id: "SESA80182", email: "nicolas.bigot@schneider-electric.com", access: [1]}, // "EcoReach"
    {id: "SESA125068", email: "rajeev.rv@schneider-electric.com", access: [1]}, // "EcoReach"
    {id: "SESA196170", email: "pratyush.sahay@schneider-electric.com", access: [0, 1, 2, 3, 4, 5]}, // "EcoReach"
    //{id: "SESA431390", email: "srikant.gudi@non.schneider-electric.com", access: [0, 1, 2, 3, 4, 5]}, // "EcoReach"
    {id: "SESA445225", email: "praveesh.chaladan@non.schneider-electric.com", access: [0, 1, 2, 3, 4, 5]}
  ];

  var ENVIRONMENT = {
    LOCAL: 'LOCAL',
    DEBUG: 'DEBUG',
    SIT: 'SIT',
    PROD: 'PROD'
  };

  var SUMMARY_NOTES = {
    updatedTill: {
      text: 'Data is updated till',
      value: ''
    },
    updatedIn: {
      text: 'Data is updated in the ',
      value: 'first and third week of every month'
    },
    registrationThru: {
      text: 'Data for registration through',
      value: 'DCES Authentication block'
    }
  };

  var SECTION_TITLES = {
    registrationChart: 'DCES Users - New & Active',
    usersByCountry: 'DCES Users by Country',
    appTable: 'Users by DCES Applications'
  };

  /**
   * Constants
   * @type type
   */
  var appConstants = {
    'EVENTS': EVENTS,
    'SITE_NAME': SITE_NAME,
    'SITE_DETAILS_FOR_SUMMARY_PAGE': SITE_DETAILS_FOR_SUMMARY_PAGE,
    'PAGE_TITLE': PAGE_TITLE,
    'SITE_GA_IDS': SITE_GA_IDS,
    'DATES': DATES,
    'MESSAGE': MESSAGE,
    'ERROR': ERROR,
    'NOTE': NOTE,
    'TIMER': TIMER,
    'CHART_OPTIONS': CHART_OPTIONS,
    'PROJECT_LOAD_DELAY': 6000,
    'NODE_REST_URL': NODE_REST_URL,
    'NODE_REST_DOMAIN': NODE_REST_DOMAIN,
    'PACE_LOGIN_REDIRECT_URL': PACE_LOGIN_REDIRECT_URL,
    'PACE_CHART': PACE_CHART,
    'FILTER': FILTER,
    'ADMIN_ID': ADMIN_ID,
    'UPLOAD_FILE': UPLOAD_FILE,
    'ENVIRONMENT': ENVIRONMENT,
    'SUMMARY_NOTES': SUMMARY_NOTES,
    'SECTION_TITLES': SECTION_TITLES
  };
  //Define Constant..
  angular.module('se-branding').constant('appConstants', appConstants);
})();
