/**
 * Created by SESA373456 on 1/21/2016.
 */
angular.module('se-branding').controller('webUiCtrl', ['$scope', '$log', function ($scope, $log) {
    // Progressive Tab Data
    //$scope.seFirstCheckBox = "Default value";
    $scope.pTabs = [{
        'name': 'Normal 1',
        'templateUrl': 'progtabs.html',
        'state': 'done'
      }, {
        'name': 'Normal 2',
        'templateUrl': 'progtabs.html',
        'state': 'active'
      }, {
        'name': 'Normal 3',
        'templateUrl': 'progtabs.html',
        'state': 'disabled'
      }, {
        'name': 'Normal 4',
        'templateUrl': 'progtabs.html',
        'state': 'enabled'
      }];
    $scope.tabs = [{
        'name': 'Normal 1',
        'active': true,
        'content': 'Tab 1 Content'
      }, {
        'name': 'Normal 2',
        'templateUrl': 'views/test2.html'
      }, {
        'name': 'Normal 3'
      }, {
        'name': 'Normal 4'
      }, {
        'name': 'Normal 5'
      }, {
        'name': 'Normal 6'
      }];
    $scope.tabslightgreen = [{
        'name': 'Normal 1',
        'active': true,
        'content': 'Tab 1 Content'
      }, {
        'name': 'Normal 2'
      }, {
        'name': 'Normal 3'
      }, {
        'name': 'Normal 4'
      }, {
        'name': 'Normal 5'
      }, {
        'name': 'Normal 6',
        'templateUrl': 'views/test6.html'
      }, {
        'name': 'Normal 7'
      }];
    $scope.tabstabunderline = [{
        'name': 'Normal 1',
        'active': true,
        'content': 'Tab 1 Content'
      }, {
        'name': 'Normal 2'
      }, {
        'name': 'Normal 3'
      }, {
        'name': 'Normal 4',
        'templateUrl': 'views/test4.html'
      }, {
        'name': 'Normal 5'
      }, {
        'name': 'Normal 6'
      }];
    $scope.slides1 = [{
        "url": "images/GLOBAL-HOME.jpg",
        "alt": "GLOBAL-HOME",
        "active": true
      }, {
        "url": "images/EU_SE_header001.jpg",
        "alt": "EU_SE_header001"
      }, {
        "url": "images/IOT.jpg",
        "alt": "IOT"
      }, {
        "url": "images/170551533.jpg",
        "alt": "170551533"
      }];
    $scope.slides2 = [{
        "content": "asdfasdfsafsafsafd"
      }, {
        "content": '<h1>Heading</h1><p>subheading</p><img src="images/EU_SE_header001.jpg" alt="imagename1" />'
      }, {
        "content": '<h1>Heading</h1><p>subheading</p><img src="images/GLOBAL-HOME.jpg" alt="imagename1" />'
      }, {
        "content": '<h1>Heading</h1><p>subheading</p><img src="images/IOT.jpg" alt="imagename2" />'
      }];
    $scope.slides = [{
        "content": '<div class="caroussel-object"><img src="images/caroussel.png" /><div class="mainInfo">Recommended</div><h1>Acti 9</h1><p>Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem, Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem,</p><a href="" class="se-link-blue"> > Order Acti 9</a></div>'
      }, {
        "content": '<div class="caroussel-object"><img src="images/caroussel.png" /><div class="mainInfo">Recommended</div><h1>Acti 9</h1><p>New Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem, Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem,</p><a href="" class="se-link-blue"> > Order Acti 9</a></div>'
      }, {
        "content": '<div class="caroussel-object"><img src="images/caroussel.png" /><div class="mainInfo">Recommended</div><h1>Acti 9</h1><p>Commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem, Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem,</p><a href="" class="se-link-blue"> > Order Acti 9</a></div>'
      }, {
        "content": '<div class="caroussel-object"><img src="images/caroussel.png" /><div class="mainInfo">Recommended</div><h1>Acti 9</h1><p>Lorem ipsum Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem, Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem,</p><a href="" class="se-link-blue"> > Order Acti 9</a></div>'
      }, {
        "content": '<div class="caroussel-object"><img src="images/caroussel.png" /><div class="mainInfo">Recommended</div><h1>Acti 9</h1><p>Lorem ipsum Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem, Mauris commodo quam augue, nec  tristique ligula faucibus at. Ut porttitor venenatis lorem,</p><a href="#/homepage" class="se-link-blue"> > Order Acti 9</a></div>'
      }];
    $scope.collapseLinks = [{
        "SocialLinks": [{
            "ClassName": "envelope",
            "Action": "#"
          }, {
            "ClassName": "question-circle",
            "Action": "#"
          }, {
            "ClassName": "comment",
            "Action": "#"
          }, {
            "ClassName": "map-marker",
            "Action": "#"
          }]
      }];
  }]);