'use strict';
angular.module('se-branding')
  .controller('landingPage', ['$rootScope','$scope', 'sdkTemplateServices', '$http', '$window', 'gettextCatalog', 'toasterService', '$log', '$sce', 'authorizationService', '$q','authenticationConstants',
    function ($rootScope, $scope, sdkTemplateServices, $http, $window, gettextCatalog, toasterService, $log, $sce, authorizationService, $q, authenticationConstants) {
    $scope.headerLogo = {
      'link': '/',
      'imageUrl': 'images/bi-logo-bounding.png',
      'altText': 'BI Dashboard'
    };
    // starts tiles page scripts
    $scope.tiles = [];

    // locales
    $scope.locales = [];

    $scope.app = {
      'appTitle': 'DCES BI DASHBOARD',
      'imageUrl': 'images/bi-logo-bounding.png',
      'link': '/',
      'mainMenuItems': []
    };

    authorizationService.setUserHavePermissionCallback(function (permissions) {
      var guestUser = false;
      if (angular.isArray(permissions)) {
        angular.forEach(permissions, function (permission) {
          if (permission === 'GUEST')
            guestUser = true;
        });
      } else if (angular.isString(permissions)) {
        if (permissions === 'GUEST')
          guestUser = true;
      }

      return guestUser;
    });

    sdkTemplateServices.fetch('data/se-main-menuitems.json').then(function (data) {
      $scope.app.mainMenuItems = data;
    });

    $rootScope.$on(authenticationConstants.LOGIN_CONFIRMED_EVENT, function (event, data) {
      var obj =$scope.app.mainMenuItems.filter(function (o) {
        return o.title === 'ADMIN';
      });
      if (!obj.length) {
        $scope.app.mainMenuItems.push({
          "title": "ADMIN",
          "initialVisibility": "false",
          "link": "#/admin",
          "permissions": ["ADMIN", "GUEST"]
        });
      }
    });

    // sdkTemplateServices.fetch('data/se-countries-languages.json').then(function (data) {
    //   // $scope.locales = data;
    // });

    // ends header data source

    // starts footer data source
    $scope.footerLogoLink = function () {
      $window.location.href = $scope.seBrandingConfiguration[0].footerLogoLink;
    };
    $scope.footerLinks = [];
    sdkTemplateServices.fetch('data/se-footer-links.json').then(function (data) {
      $scope.footerLinks = data;
    });

    $scope.mainfooterlinks = [];
    sdkTemplateServices.fetch('data/se-main-footer-links.json').then(function (data) {
      $scope.mainfooterlinks = data;
    });

    $scope.footerSocialLinks = [];
    sdkTemplateServices.fetch('data/se-social-contents.json').then(function (data) {
      $scope.footerSocialLinks = data;
    });
    //Listen Language Change
    $scope.$on('languageChanged', function (event, data) {
      $log.info("Callback event triggered post language changed");
    });

    var position = {
      bottom: true,
      top: false,
      left: false,
      right: true
    };

    /*Configuration object
     * for custom toaster
     */
    var customToaster = {
      controller: "ToastCtrl",
      timeDelay: 400000
    };

    /*Toaster message can be obtained from the service or
     * othersources according to application's source of data
     * Provided APIs are,
     * showSimpleToast(String Message,Object Position)
     * showActionToast(String Message,Object Position)
     */

    var toasterMessage = {
      message: "Hello message"
    };

    /*Configuration object for
     * for HTML toaster
     */

    var toasterHtmlObj = {
      message: "Hello there, we are here",
      header: "This is header",
      timeDelay: 3000
    }

    /* setInterval(function(){*/
//				toasterService.showHtmlToast(toasterHtmlObj, position);
    /*},4000);*/

    //Notification changes

    $scope.$on('performPostToastAction', function () {
      $log.info('Write your code post toast');
    });

    $scope.$on('prerender', function () {
      alert("Trigger preRender click");
    });

    $scope.$on('postrender', function () {
      alert("Trigger postRender click");
    });

    $scope.onbeforeLogout = function () {
      var deferred = $q.defer(),
        confirmation = confirm("Want to really logout?");
      if (confirmation === true) {
        deferred.resolve(confirmation);
      } else {
        deferred.reject(confirmation);
      }
      return deferred.promise;
    };
    ;
    //Alternate Footer configuration settings
    sdkTemplateServices.fetch('data/se-footer-alt-config.json').then(function (data) {
      $scope.footerconfig = data;
    });
  }])
  .controller('ToastCtrl', function ($scope, $mdToast) {
    $scope.toastObj = {
      message: "hello there",
      header: "This is header"
    };
    $scope.closeToast = function () {
      $mdToast.hide();
    };
  })
  .controller('footerDemo', function ($scope, $rootScope) {
    $rootScope.stickyFooterDemo = true;
    $rootScope.altFooterShow = true;
    $scope.footerconfig = {};
    $scope.altFooterShowOptions = [{
      "id": 1,
      "text": "Default Footer"
    }, {
      "id": 2,
      "text": "Collapsible Footer"
    }];
    $scope.altFooterShowSelect = function () {
      $rootScope.altFooterShow = ($scope.altFooterShowOption.id !== 1) ? true : false;
    };
    $scope.stickyFooterDemoFn = function () {
      $rootScope.stickyFooterDemo = !$rootScope.stickyFooterDemo;
    };
  });
