angular.module('FooterExampleModule', ['seWebUI', 'gettext',
  'ui.bootstrap'])
        .controller('FooterController', ['$scope', function ($scope) {
            $scope.footerLinks = [
              {
                "name": "Legal",
                "link": "#"
              },
              {
                "name": "Privacy Policy",
                "link": "#"
              },
              {
                "name": "Copyright",
                "link": "#"
              }
            ];
            $scope.footerSocialLinks = [
              {
                "title": "Google",
                "url": "../images/gg.png",
                "link": "#",
                "alttext": "Google"
              },
              {
                "title": "LinkedIn",
                "url": "../images/in.png",
                "link": "#",
                "alttext": "LinkedIn"
              },
              {
                "title": "Twitter",
                "url": "../images/tw.png",
                "link": "#",
                "alttext": "Twitter"
              },
              {
                "title": "YouTube",
                "url": "../images/utube.png",
                "link": "#",
                "alttext": "YouTube"
              }
            ];

          }]);
