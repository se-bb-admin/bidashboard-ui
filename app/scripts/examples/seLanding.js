angular.module('LandingExampleModule', ['seWebUI', 'gettext',
  'ui.bootstrap'])
        .controller('LandingController', ['$scope', function ($scope) {
            $scope.tiles = [
              {
                "rowid": 0,
                "columns": [
                  {
                    "type": "text",
                    "bgstyle": "darkGreenBG",
                    "header": "Create a New Project",
                    "paragraph": "Project Paragraph",
                    "links": [{"Create Project": "http://www.google.com"}],
                    "linktype": "",
                    "tilenumber": 1,
                    "numofColumn": 1
                  },
                  {
                    "type": "image",
                    "url": "../images/a253.png",
                    "url253px": "../images/a253.png",
                    "url505px": "../images/505.png",
                    "url720px": "../images/720.png",
                    "url1010px": "../images/a1010.png",
                    "alttext": "Home Image",
                    "bgstyle": "img",
                    "tilenumber": 3,
                    "numofColumn": 1
                  }
                ]
              }
            ];
          }]);
