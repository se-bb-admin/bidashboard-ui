angular.module('HeaderExampleModule', ['seWebUI', 'gettext',
  'ui.bootstrap'])
        .run(['authenticationService', function (authenticationService) {
            authenticationService.setLoginApplicationEnvironment('SQE');
            authenticationService.setClientId('demo');
          }])
        .controller('HeaderController', ['$scope', function ($scope) {
            $scope.headerLogo = {'link': '#/homepage', 'imageUrl': '../images/se-logo.png'};
            $scope.menus = [
              {
                //Menu have sub menus
                'title': 'Products',
                childMenuItems: [
                  {
                    title: 'Access to Engery',
                    link: 'http://www2.schneider-electric.com/sites/corporate/en/products-services/access-to-energy/access-to-energy.page'
                  },
                  {
                    title: 'Automation and Control',
                    link: 'http://www2.schneider-electric.com/sites/corporate/en/products-services/automation-control/automation-control.page'
                  }
                ]
              },
              {
                //Attach Link url with menu
                'title': 'Solutions',
                'link': 'http://www.schneider-electric.com/b2b/en/solutions/'
              },
              {
                //Implements On click event on menu
                'title': 'Services',
                'onclick': function () {
                  alert('Services menu click');
                }
              }
            ];

            $scope.autoCompleteData = {searchTexts: ['Dynamo', 'Ecoreal630', 'EcorealAD', 'EcorealXL', 'Berrisoft', 'CAD Library'],
              //Search Click Callback
              onSearchClickCallback: function (searchText, searchCaseItem) { }
            };

            $scope.searchCategories = [{
                "id": 1,
                "text": "Products",
                "value": "Products"
              }, {
                "id": 2,
                "text": "Solutions",
                "value": "Solutions"
              }, {
                "id": 3,
                "text": "Services",
                "value": "Services"
              }];



            $scope.locales = [{
                "region": "America",
                "Countries": [{
                    "name": "USA - English",
                    "value": "en-INT",
                    "default": true
                  }]
              },
              {
                "region": "Europe",
                "Countries": [
                  {
                    "name": "France - français",
                    "value": "fr-FR",
                    "default": true
                  }
                ]
              }];
            //Localization Change Listener
            $scope.$on('languageChanged', function (event, data) {
              console.log(data);
            });

            //
            $

          }]);
