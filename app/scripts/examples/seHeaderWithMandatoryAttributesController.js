angular.module('HeaderExampleModule', ['seWebUI', 'gettext',
  'ui.bootstrap'])
        .run(['authenticationService', function (authenticationService) {
            authenticationService.setLoginApplicationEnvironment('SQE');
            authenticationService.setClientId('demo');
          }])
        .controller('HeaderController', ['$scope', function ($scope) {
            $scope.headerLogo = {'link': '#/homepage', 'imageUrl': '../images/se-logo.png'};
            $scope.menus = [
              {
                'title': 'Products',
                childMenuItems: [
                  {
                    title: 'Access to Engery',
                    link: 'http://www2.schneider-electric.com/sites/corporate/en/products-services/access-to-energy/access-to-energy.page'
                  },
                  {
                    title: 'Automation and Control',
                    link: 'http://www2.schneider-electric.com/sites/corporate/en/products-services/automation-control/automation-control.page'
                  }
                ]
              },
              {
                'title': 'Solutions',
                'link': 'http://www.schneider-electric.com/b2b/en/solutions/'
              }
            ];

            $scope.locales = [{
                "region": "America",
                "Countries": [{
                    "name": "USA - English",
                    "value": "en-INT",
                    "default": true
                  }]
              },
              {
                "region": "Europe",
                "Countries": [
                  {
                    "name": "France - français",
                    "value": "fr-FR",
                    "default": true
                  }
                ]
              }];

            //Localization Change Listener
            $scope.$on('languageChanged', function (event, data) {
              console.log(data);
            });

            //Observe to check user is already logged. If token is undefined then user is not logged.
            $rootScope.$watch('token', function (newToken) {
              if (angular.isDefined(newToken)) {
                console.log("Access Token -->" + newToken);
              }
            });
          }]);
