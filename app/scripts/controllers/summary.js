(function () {
  'use strict';
  angular.module('se-branding')
  //		.controller('summaryCtrl', ['$scope', '$mdToast', 'sdkTemplateServices', '$http', '$window', 'gettextCatalog',
  //			'toasterService', '$log', '$sce', 'authorizationService',
  //			function ($scope, $mdToast, sdkTemplateServices, $http, $window, gettextCatalog, toasterService, $log,
  //					$sce, authorizationService) {
  //				$scope.pageTitle = "Summary";
  //			}]);
    .controller('summaryCtrl', ['$scope', 'appConstants', 'commonUtils', 'moment', 'globalSummaryService', 'gaChartUtil', '$timeout', '$location', '$window', '$document', '$interval', 'googleChartApiPromise',
      function ($scope, appConstants, commonUtils, moment, globalSummaryService, gaChartUtil, $timeout, $location, $window, $document, $interval, googleChartApiPromise) {
        $scope.pageTitle = "Summary";
        $scope.dataNotes = appConstants.SUMMARY_NOTES;
        $scope.dataNotes.updatedTill.value = '';
        $scope.sectionTitles = appConstants.SECTION_TITLES;
        var summary = {};
        var countryData = [];
        var appData = {};
        var bomData = {};
        var appType = 'table';
        var ALL = 'All';
        var BOM_CHART_DELAY = 600;
        var startActiveUsersSummaryFrom = 0;
        $scope.showNotes = true;
        var countryTotalRegistrations = 0;
        // flag to toggle country chart loading..
        $scope.loadingSummary = true;
        $scope.loadingCountryChart = false;
        $scope.loadingAppChart = false;
        $scope.loadingBomSummary = true;
        $scope.years = [ALL];
        $scope.currentYear = moment().format('YYYY');
        $scope.selectedYear = $scope.currentYear;
        $scope.period = {index: 1};
        $scope.bomPeriod = {index: 0};
        $scope.userType = {index: 0};
        $scope.appChartLoading = false;
        $scope.selectedAll = false;
        $scope.showActiveUserByCountry = true;
        $scope.showActiveUsers = true;
        $scope.initCountryData = initCountryData;
        $scope.countryUsersType = 'new';
        $scope.showBomChart = true;
        $scope.totalBomData = {};
       /*$scope.totalUserRegistrations = {
          2017: 24758,
          2016: 55808
        }*/

        var apps = ['mypact'];

        $scope.chartRegistrationByCountries = {
          type: 'TreeMap',
          displayed: true,
          data: {
            cols: [
              {id: 'country', label: 'Country', type: 'string'},
              {id: 'year', label: 'Year', type: 'string'},
              {id: 'count', label: 'Count', type: 'number'}
            ],
            rows: []
          },
          options: {
            theme: 'material',
            backgroundColor: '#999',
            minColor: '#0ff',
            midColor: '#0aa',
            maxColor: '#099',
            fontColor: 'black',
            fontWeight: 'bold',
            headerHeight: 35,
            showScale: false,
            showTooltips: true,
            highlightOnMouseOver: true,
            height: 400,
            generateTooltip: showChartTooltip,
            vAxis: {
              textPosition: 'out',
              textStyle: {
                fontSize: 14
              }
            },
            hAxis: {
              textPosition: 'out',
              textStyle: {
                fontSize: 14
              }
            }
          }
        };
        $scope.chartApps = {
          type: 'BarChart',
          data: {
            cols: [
              {id: 'appname', label: 'Applications', type: 'string'},
              {id: 'target', label: 'Target', type: 'number'},
              {id: 'achived', label: 'Achived', type: 'number'}
            ],
            rows: []
          },
          options: {
            theme: 'material',
            chartArea: {
              left: '25%',
              top: 2,
              height: '90%',
              width: '100vw'
            },
            series: {
              0: {
                type: 'scatter',
                color: 'black',
                pointSize: 10
              }
            },
            width: '100%',
            colors: ['#058DC7'],
            legend: {
              position: 'top'
            },
            hAxis: {
              textPosition: 'out',
              textStyle: {
                fontSize: 14
              }
            },
            vAxis: {
              textPosition: 'out',
              textStyle: {
                fontSize: 14
              }
            }
          }
        };

        function showChartTooltip(row, size, value) {
          var d = $scope.chartRegistrationByCountries.data.rows[row];
//                var perc = (value * 100).toFixed(2);
          var perc = (d.c[2].v / countryTotalRegistrations * 100).toFixed(1);
          return perc > 0 ? "<div style='background:#f9f9f9; padding:10px; box-shadow: 0 0 10px #333;font-weight: 600;text-align: center;border-radius: 4px;'><div>" + d.c[0].v + '</div><div>' + perc + "%</div></div>" : "";
        }

        $scope.registrations = {};
        $scope.activeUsers = {};
        $scope.periods = [
          {type: 'daily', title: 'Daily'},
          {type: 'weekly', title: 'Weekly'},
          {type: 'monthly', title: 'Monthly'}
        ];

        /**
         * page initialization function
         *
         * @returns {undefined}
         */
        function init() {
          $scope.loadingSummary = true;
          $scope.loadingBomSummary = true;
          $scope.userChartReady = false;
          //$scope.loadingCountryChart = true;
          initGlobalData();
          initCountryData();
          initAppData();
          globalSummaryService.getAppGroups().then(function (data) {
            $scope.appGroups = data;
          });
          globalSummaryService.getAppConfig().then(function(config) {
            $scope.totalUserRegistrations = config.data.totalUserRegistrations;
          });
        }

        /**
         * initialize global data
         */
        function initGlobalData() {
          globalSummaryService.getGlobalTotal().then(function (data) {
            $scope.dataNotes.updatedTill.value = data.lastUpdatedDate;
            $scope.totalRegistrations = 0;
            $scope.registrationSummary = data.registrations;
            startActiveUsersSummaryFrom = data.activeUsersSummaryFrom;
            var years = [];
            data.registrations.forEach(function (obj, index) {
              var year = obj.regYear;
              summary[year] = summary[year] || {daily: [], weekly: [], monthly: []};
              years.unshift(year);
              $scope.registrations[year] = $scope.registrations[year] || 0;
              $scope.activeUsers[year] = $scope.activeUsers[year] || 0;
              $scope.registrations[year] = obj.newUsers;
              $scope.activeUsers[year] = obj.totalUsers;
              $scope.totalRegistrations += obj.newUsers;
            });
            years.forEach(function (year, indx) {
              $scope.years.push(year);
            });

            $scope.selectedYear = $scope.currentYear;
            initSummaryData();
          });
        }

        /**
         * initialize summary data
         */
        function initSummaryData() {
          //  $scope.period.index = ($scope.currentYear === $scope.selectedYear) ? 1 : 0;
          var monthly, weekly, daily, year;
          year = $scope.selectedYear || $scope.years[$scope.years.length - 1];

          if (summary[year].monthly.length === 0 || summary[year].weekly.length === 0 || summary[year].daily.length === 0) {
            monthly = globalSummaryService.getRegistrationData('monthly', year);
            weekly = globalSummaryService.getRegistrationData('weekly', year);
            daily = globalSummaryService.getRegistrationData('daily', year);

            weekly.then(function (data) {
              summary[year].weekly = summary[year].weekly || [];
              summary[year].weekly = data;
              // show monthly chart
              $scope.initRegistrationsChart(1);
              monthly.then(function (data) {
                summary[year].monthly = summary[year].monthly || [];
                summary[year].monthly = data;
              });
              daily.then(function (data) {
                summary[year].daily = summary[year].daily || [];
                summary[year].daily = data.map(function (d, i) {
                  d.registrationDate = moment(d.registrationDate).format("MMM DD");
                  return d;
                })
              });
            });
          } else {
            $scope.initRegistrationsChart(1);
          }
        }

        /**
         * initialize country data
         *
         * @param year
         */
        function initCountryData(type) {
          // var yearKey = year || ALL;
          var yearKey = $scope.selectedYear;
          type = type || 'new';
          $scope.countryUsersType = type;
          if (countryData[yearKey] === undefined) {
            $scope.loadingCountryChart = true;
            globalSummaryService.getCountryData(yearKey).then(function (data) {
              countryData[yearKey] = data;
              googleChartApiPromise.then(function () {
                $scope.loadingCountryChart = false;
              });
              initCountryChart(yearKey, type);
            });
          } else {
            initCountryChart(yearKey, type);
          }
        }

        /**
         * initialize applications data
         */
        function initAppData() {
          globalSummaryService.getAppData($scope.selectedYear).then(function (data) {
            $scope.appData = data;
            $scope.appData.forEach(function (obj, index) {
              obj.achievedPercent = Math.ceil(obj.newUsers / obj.target * 100);
              if (obj.targetAchieved < 70) {
                obj.type = 'danger';
              } else if (obj.targetAchieved < 90) {
                obj.type = 'warning';
              } else {
                obj.type = 'success';
              }
            });
          });
        }

        function initBomData(){
          if($scope.selectedYear === ALL || $scope.selectedYear < 2016){
            return $scope.showBomChart = false;
          }
          $scope.showBomChart = true;
          if(bomData[$scope.selectedYear] !== undefined && bomData[$scope.selectedYear]['weekly'] !== undefined){
            return $scope.initBomChart(1);
          }
          globalSummaryService.getBomSummary('monthly').then(function(data){
            $scope.totalBomData['total'] = Math.ceil((angular.copy(data).reduce(function(prev, next){
              next.bomExported += prev.bomExported;
              return next;
            }, {bomExported: 0}).bomExported) / 1000);
          });
          bomData[$scope.selectedYear] = {};
          ['weekly', 'monthly'].forEach(function(period){
            globalSummaryService.getBomSummary(period, $scope.selectedYear).then(function(apiData){
              var data = angular.copy(apiData);
              for(var attr in data){
                if(data[attr].bomExported !== 0){
                  data[attr].bomExported = Math.ceil(data[attr].bomExported / 1000);
                }
              }
              if(angular.equals(bomData[$scope.selectedYear], {}) && period === 'weekly'){
                bomData[$scope.selectedYear][period] = data;
                $scope.initBomChart(1);
              }else{
                $timeout(function(){
                  bomData[$scope.selectedYear][period] = data;
                }, 0);
                $scope.totalBomData[$scope.selectedYear] =  ((angular.copy(data).reduce(function(prev, next){
                    next.bomExported += prev.bomExported;
                    return next;
                  }, {bomExported: 0}).bomExported)/ 1000).toFixed(1);
              }
            });
          });
        }
        /**
         * set users type selection - new / total
         *
         * @param type
         */
        $scope.setCountryUsersType = function (type) {
          $scope.countryUsersType = type;
        };

        // call init();
        init();

        /**
         *
         * @param year
         */
        $scope.selectYear = function (year) {
          $scope.selectedYear = year;
          if (year !== ALL) {
            var showActiveUsers = (year >= startActiveUsersSummaryFrom);
            $scope.selectedAll = false;
            $scope.period.index = ($scope.currentYear == $scope.selectedYear) ? 1 : 0;
            $scope.userType.index = 0;
            $scope.bomPeriod.index = 0;
            $scope.showActiveUsers = showActiveUsers;
            $scope.showActiveUserByCountry = showActiveUsers;
            $scope.loadingBomSummary = true;
            initSummaryData();
            initCountryData();
            initAppData();
            //initBomData();
            $timeout(initBomData, BOM_CHART_DELAY);
          } else {
            $scope.period.index = 0;
            $scope.selectedAll = true;
            $scope.selectedYear = ALL;
            $scope.showActiveUserByCountry = false;
            $scope.showActiveUsers = true;
            $scope.initRegistrationsChart(3);
            initCountryData();
            initAppData();
            initBomData();
          }
        };

        /**
         * @param {number} period
         * @returns nothing
         */
        $scope.initRegistrationsChart = function (period) {
          var chartData = null;
          $scope.loadingSummary = true;
          var chart = {
            type: "LineChart",
            options: {
              titleTextStyle: {
                fontSize: 16
              },
              theme: 'material',
              width: '100%',
              height: 300,
              legend: {
                position: 'none'
              },
              colors: ['#43459d', '#e2431e'],
              chartArea: {
                height: '80%',
                width: '90%'
              },
              tooltip: {
                isHTML: true
              },
              pointSize: 6,
              pointShape: 'circle',
              hAxis: {
                textPosition: 'out',
                showTextEvery: 2
              },
              vAxis: {
                textPosition: 'out'
              }
            }
          };

          switch (period) {
            case 0:
              if (summary[$scope.selectedYear].daily[0].totalUsers) {
                chartData = gaChartUtil.populateThreeFieldChartData(summary[$scope.selectedYear].daily, 'registrationDate', 'newUsers', 'totalUsers', 'Dates', 'New Users', 'Active Users');
              } else {
                chartData = gaChartUtil.populateTwoFieldChartData(summary[$scope.selectedYear].daily, 'registrationDate', 'newUsers', 'Dates', 'New Users');
              }
              chart.data = chartData;
              break;
            case 1:
              if (summary[$scope.selectedYear].weekly[0].totalUsers) {
                chartData = gaChartUtil.populateThreeFieldChartData(summary[$scope.selectedYear].weekly, 'registrationWeek', 'newUsers', 'totalUsers', 'Weeks', 'New Users', 'Active Users');
              } else {
                chartData = gaChartUtil.populateTwoFieldChartData(summary[$scope.selectedYear].weekly, 'registrationWeek', 'newUsers', 'Dates', 'New Users');
              }
              chart.data = chartData;
              chart.options.hAxis.showTextEvery = parseInt(summary[$scope.selectedYear].weekly.length / 10);
              break;
            case 2:
              if (summary[$scope.selectedYear].monthly[0].totalUsers) {
                chartData = gaChartUtil.populateThreeFieldChartData(summary[$scope.selectedYear].monthly, 'registrationMonth', 'newUsers', 'totalUsers', 'Weeks', 'New Users', 'Active Users');
              } else {
                chartData = gaChartUtil.populateTwoFieldChartData(summary[$scope.selectedYear].monthly, 'registrationMonth', 'newUsers', 'Dates', 'New Users');
              }
              chart.data = chartData;
              chart.options.hAxis.showTextEvery = 1;
              break;
            case 3:
              $scope.selectedAll = true;
              chart.data = gaChartUtil.populateThreeFieldChartData($scope.registrationSummary, 'regYear', 'newUsers', 'totalUsers', 'Years', 'New Users', 'Total Users');
              chart.options.hAxis.showTextEvery = 1;
              break;
          }
          $scope.chartRegistrations = chart;
          googleChartApiPromise.then(function () {
            $scope.loadingSummary = false;
          });
        };

        /**
         *
         * @returns nothing
         */
        function initCountryChart(year, type) {
          var yearStr = $scope.selectedYear.toString();
          var tree = $scope.chartRegistrationByCountries;
          // set rows
          tree.data.rows = [{
            c: [
              {v: yearStr},
              {v: null},
              {v: null}
            ]
          }];
          countryTotalRegistrations = 0;
          tree.options.showTooltips = true;

          if(countryData[year].length === 0){
            countryData[year].push({
              newUsers: 1,
              totalUsers: 1,
              message: 'No data available'
            });
            tree.options.showTooltips = false;
          }
          //console.log('\ninitCountryChart: ', JSON.stringify(countryData[year]));
          countryData[year].forEach(function (obj, i) {
            var users = (type === 'new') ? obj.newUsers : obj.totalUsers;
            countryTotalRegistrations += users;
            var row = {
              c: [
                {v: (obj.message !== undefined) ? obj.message : obj.countryName + " (" + users + ")"},
                {v: yearStr},
                {v: users}]
            };
            tree.data.rows.push(row);
          });
          // $scope.loadingCountryChart = false;
        }

        /**
         *
         * @returns nothing
         */
        function initAppChart() {
          $scope.appChartLoading = true;
          var appChart = $scope.chartApps;
          // show only for current year
          if ($scope.isCurrentYear()) {
            appChart.data.rows = [];
            if ($scope.appData.length > 0) {
              $scope.appData.forEach(function (obj, i) {
                var type = 'default';
                var row = {
                  c: [
                    {v: obj.appName}, {v: obj.target}, {v: obj.noOfUsers}
                  ]
                };
                appChart.data.rows.push(row);
              });
              appChart.options.height = appChart.data.rows.length * 40;
              appChart.options.width = appChart.data.rows.length * 40;
            }
            $scope.setAppType('table');
          }
          $scope.appChartLoading = false;
        }

        $scope.initBomChart = function initBomChart(period) {
          var chartBomExported = {
            type: "AreaChart",
            options: {
              titleTextStyle: {
                fontSize: 16
              },
              theme: 'material',
              width: '100%',
              height: 300,
              legend: {
                position: 'none'
              },
              colors: ['#43459d'],
              chartArea: {
                height: '80%',
                width: '90%'
              },
              tooltip: {
                isHTML: true
              },
              hAxis: {
                textPosition: 'out',
                showTextEvery: 2
              },
              vAxis: {
                textPosition: 'out',
                showTextEvery: 2
              },
              pointSize: 4,
              pointShape: 'circle',
              isStacked: true,
              fill: 20
            }
          };
          switch (period) {
            case 1:
              chartBomExported.data = gaChartUtil.populateTwoFieldChartData(bomData[$scope.selectedYear]['weekly'], 'exportedWeek', 'bomExported', 'Dates', 'BOM Exported');
              chartBomExported.options.hAxis.showTextEvery = parseInt(bomData[$scope.selectedYear].weekly.length / 10);
              break;
            case 2:
              chartBomExported.data = gaChartUtil.populateTwoFieldChartData(bomData[$scope.selectedYear]['monthly'], 'exportedMonth', 'bomExported', 'Dates', 'BOM Exported');
              chartBomExported.options.hAxis.showTextEvery = 1;
              break;
          }
          $scope.chartBomExported = chartBomExported;
         $scope.loadingBomSummary = false;
        };

        $scope.chartReadyHandler = function chartReadyHandler(chartWrapper){
          //console.log("_______Chart Ready______", chartWrapper.ZL.id, $scope.userChartReady);
          if(chartWrapper.ZL.id === 'userChart' && $scope.userChartReady === false){
            $scope.userChartReady = true;
            $timeout(initBomData, BOM_CHART_DELAY / 5);
          }
        };
        /**
         * function to toggle the top bar
         * @returns {undefined}
         */
        $scope.toggleNotes = function () {
          $scope.showNotes = !$scope.showNotes;
          angular.element('#biTopnav').slideToggle(1000);
        };

        /**
         *
         * @returns {undefined}
         */
        $scope.chartReady = function () {
          fixGoogleChartsBarsBootstrap();
        };

        /**
         *
         * @returns {undefined}
         */
        function fixGoogleChartsBarsBootstrap() {
          $(".google-visualization-table-table img[width]").each(function (index, img) {
            $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
          });
        }

        /**
         *
         * @returns {undefined}
         */
        $scope.showCountryTable = function () {
          var parentEl = angular.element(document.body);
          $scope.viewCountryData = countryData[$scope.selectedYear].sort(function (a, b) {
            var ret = 0;
            if (a.countryName > b.countryName) {
              ret = 1;
            } else if (a.countryName < b.countryName) {
              ret = -1;
            }
            return ret;
          });
          // $uibModal.open({
          //   templateUrl: 'views/summaryCountryTable.html',
          //   resolve: {
          //     items: function () {
          //       var sorted = countryData[$scope.selectedYear].sort(function (a, b) {
          //         var ret = 0;
          //         if (a.countryName > b.countryName) {
          //           ret = 1;
          //         } else if (a.countryName < b.countryName) {
          //           ret = -1;
          //         }
          //         return ret;
          //       });
          //       return {data: sorted};
          //     }
          //   },
          //   controller: function ($scope, items) {
          //     $scope.items = items;
          //   }
        };

        $scope.isCurrentYear = function () {
          return $scope.selectedYear === $scope.currentYear;
        };

        $scope.setAppType = function (apType) {
          appType = apType;
        };

        $scope.getAppType = function () {
          return appType;
        };

        $scope.isAppType = function (apType) {
          return appType === apType;
        };

        $scope.showApps = function ($mdMenu, event) {
          $mdMenu.open(event);
        };


        $scope.scrollDown = function () {
          var scrollVal = 0;
          var delta = 5;
          var handle = $interval(function () {
            $window.scrollBy(0, delta);
            scrollVal += delta;
            if (scrollVal > 600) {
              $interval.cancel(handle);
            }
          }, 1);
          // var left = angular.element(".bi-scroll-btn").css("left");
          // var top = angular.element(".bi-scroll-btn").css("top");
        };

        $scope.hasLink = function (appId) {
          return apps.indexOf(appId) > -1;
        }

        // feedback
        $scope.feedback = {
          likedislike: 'like',
          comment: '',
          thanks: 'Thanks for your feedback',
          showThanks: false
        };

        $scope.submitFeedback = function () {
          $scope.feedback.showThanks = true;
          ga('send', {
            hitType: 'event',
            eventCategory: 'feedback',
            eventAction: $scope.feedback.likedislike,
            eventLabel: $scope.feedback.comment,
            hitCallback: function () {
              console.log("Done");
            }
          });
        };
      }
    ])
    .controller('DialogCtrl', function ($scope, $mdDialog) {
    });
})
();
