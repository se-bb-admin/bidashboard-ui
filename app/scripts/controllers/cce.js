/**
 * @ngdoc controller
 * @name shell
 *
 * @description
 *  shell controller includes header, main container and footer controllers. The global required events are exposed in this controller.
 *  Global required function is written in this controller.
 *
 */
(function () {
	'use strict';

	angular.module('se-branding')
			.controller('cceCtrl', cce);

	cce.$inject = ['$rootScope', 'logger', 'paceService', 'appConstants',
		'$routeParams', '$location', 'serverDataValidationUtils'];

	/**
	 *
	 * @param {type} $rootScope
	 * @param {type} logger
	 * @returns {cce_L10.cce}
	 */
	function cce($rootScope, logger, paceService, appConstants, $routeParams,
			$location, serverDataValidationUtils) {

		// #Start Conrroller
		var vm = this;
		vm.showLoader = false;
		vm.showTransparentOverlay = false;
		vm.showGrayOverlay = false;

		vm.publicProperty = "Public Property";
		vm.publicMethod = publicMethod;

		vm.chart = chart1;
		vm.cssStyle = "height:600px; width:100%;";
		vm.chartReady = chartReady;



		init();

		/**
		 * Constractor
		 *
		 * @returns {undefined}
		 */
		function init() {
			logger.log("init :: CCE");

			//Get registered user from the bigining of time
			getRegisteredUsers();
		}

		/**
		 * Get registered user from the bigining of time
		 * @returns {undefined}
		 */
		function getRegisteredUsers() {
			//Get Data From PACE
			//logger.log("........00000000 getRegisteredUsers");
			$rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
			//logger.log("........ getRegisteredUsers");
			paceService.getRegisteredUsers().then(
					function (result) {
						logger.log("CCE::PACE::Result::Success");
						parsePACEDataRegUser(result);
						$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
					},
					function (error) {
						logger.log("CCE::PACE::Result::FAIL");
						$rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
					}
			);
		}

		/**
		 *
		 * @param {type} data
		 * @returns {undefined}
		 */
		function parsePACEDataRegUser(data) {
			vm.paceDataRegUser = {};

			vm.paceDataRegUser.cccAppUsersReg = 0;
			vm.paceDataRegUser.cccAppAndroidUsersReg = 0;
			vm.paceDataRegUser.cccAppIOSUsersReg = 0;
			vm.paceDataRegUser.cccAppCumulative = 0;

			//console.dir(data);
			var applicationStats = angular.copy(data);
			var tempNode = "";
			var projName = "";
//			console.log('==> ccc: applicationStats: ', JSON.stringify(applicationStats));

			for (var i = 0; i < applicationStats.length; i++) {
				tempNode = applicationStats[i];
				projName = "";
				projName = tempNode.projName;

				//logger.log(" APP URL :: " + projName);

				switch (projName) {
					case appConstants.SITE_NAME.CCC_APP:
						vm.paceDataRegUser.cccAppUsersReg = tempNode.users;
						break;
					case appConstants.SITE_NAME.CCC_APP_ANDROID:
						vm.paceDataRegUser.cccAppAndroidUsersReg = tempNode.users;
						break;
					case appConstants.SITE_NAME.CCC_APP_IOS:
						vm.paceDataRegUser.cccAppIOSUsersReg = tempNode.users;
						break;

				}//End Switch Case
			} //End For Loop
			vm.paceDataRegUser.cccAppCumulative = parseInt(vm.paceDataRegUser.cccAppUsersReg) + parseInt(vm.paceDataRegUser.cccAppAndroidUsersReg) + parseInt(vm.paceDataRegUser.cccAppIOSUsersReg);
		}



		function publicMethod() {

		}

		// -----------------------------------------------------------------

		var chart1 = {};
		chart1.type = "AreaChart";
		chart1.displayed = false;
		chart1.data = {
			"cols":
					[
						//{id: "month", label: "Month", type: "string"},
						{id: "date", label: "Date", type: "string"},
						{id: "users", label: "Users", type: "number"}
					],
			"rows":
					[
						{
							c:
									[
										{v: "2015-03-25"},
										{v: 14}
									]
						},
						{
							c:
									[
										{v: "2015-03-26"},
										{v: 50}
									]
						},
						{
							c:
									[
										{v: "2015-03-27"},
										{v: 69}
									]
						},
						{
							c:
									[
										{v: "2015-03-28"},
										{v: 8}
									]
						}
					]
		};

		chart1.options = {
			"title": "Sample google chart for PACE data",
			"isStacked": "true",
			"fill": 20,
			"displayExactValues": true,
			"pointShape": 'circle',
			"vAxis": {
				"title": "No Of Users", "gridlines": {"count": 10}
			},
			"hAxis": {
				"title": "Date"
			},
			"legend": {
				"position": 'top'
			}
		};


		var formatCollection = [
			{
				name: "color",
				format: [
					{
						columnNum: 4,
						formats: [
							{
								from: 0,
								to: 3,
								color: "white",
								bgcolor: "red"
							},
							{
								from: 3,
								to: 5,
								color: "white",
								fromBgColor: "red",
								toBgColor: "blue"
							},
							{
								from: 6,
								to: null,
								color: "black",
								bgcolor: "#33ff33"
							}
						]
					}
				]
			},
			{
				name: "arrow",
				checked: false,
				format: [
					{
						columnNum: 1,
						base: 19
					}
				]
			},
			{
				name: "date",
				format: [
					{
						columnNum: 5,
						formatType: 'long'
					}
				]
			},
			{
				name: "number",
				format: [
					{
						columnNum: 4,
						prefix: '$'
					}
				]
			},
			{
				name: "bar",
				format: [
					{
						columnNum: 1,
						width: 100
					}
				]
			}
		];

		chart1.formatters = {};

		vm.chart = chart1;


		function chartReady() {
			fixGoogleChartsBarsBootstrap();
		}

		function fixGoogleChartsBarsBootstrap() {
			// Google charts uses <img height="12px">, which is incompatible with Twitter
			// * bootstrap in responsive mode, which inserts a css rule for: img { height: auto; }.
			// *
			// * The fix is to use inline style width attributes, ie <img style="height: 12px;">.
			// * BUT we can't change the way Google Charts renders its bars. Nor can we change
			// * the Twitter bootstrap CSS and remain future proof.
			// *
			// * Instead, this function can be called after a Google charts render to "fix" the
			// * issue by setting the style attributes dynamically.

			$(".google-visualization-table-table img[width]").each(function (index, img) {
				$(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
			});
		}
		;


	}// #End


})();
