/**
 * @ngdoc controller
 * @name shell
 *
 * @description
 *  shell controller includes header, main container and footer controllers. The global required events are exposed in this controller.
 *  Global required function is written in this controller.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').controller('reportCtrl', report);

  report.$inject = ['$rootScope', 'logger', 'appConstants', '$routeParams', '$location'];

  /**
   *
   * @param {type} $rootScope
   * @param {type} logger
   * @param {type} appConstants
   * @param {type} $routeParams
   * @param {type} $location
   * @returns {undefined}
   */
  function report($rootScope, logger, appConstants, $routeParams, $location) {

    // #Start Conrroller
    var vm = this;
    vm.showLoader = false;
    vm.showTransparentOverlay = false;
    vm.showGrayOverlay = false;

    vm.pageTitle1 = appConstants.PAGE_TITLE.REPORT.PAGE_1;
    vm.pageTitle2 = appConstants.PAGE_TITLE.REPORT.PAGE_2;
    vm.pageTitle3 = appConstants.PAGE_TITLE.REPORT.PAGE_3;


    vm.loadPage = loadPage;
    vm.isActive = isActive;

    //GET Page NO
    var pageNo = 0;
    vm.pageNo = 0;

    init();


    /**
     * Constractor
     *
     * @returns {undefined}
     */
    function init() {
      logger.log("Init :: Report");

      var pageNo = $routeParams.pageNo;
      vm.pageNo = parseInt((pageNo === null || pageNo === undefined || pageNo === "") ? 1 : pageNo);
      vm.currentPage = pageNo;
    }


    /**
     *
     * @param {type} evnt
     * @param {type} pgno
     * @returns {undefined}
     */
    function loadPage(evnt, pgno) {
      if (evnt !== undefined && evnt !== null) {
        evnt.stopImmediatePropagation();
      }
      vm.currentPage = parseInt(pgno);
      var URL = 'report/' + pgno;
      $location.path(URL);
    }

    /**
     *
     * @param {type} page
     * @returns {String}
     */
    function isActive(page) {
      page = parseInt(page);
      if (page === vm.pageNo) {
        return 'active';
      } else {
        return '';
      }
    }
  }// #End
})();
