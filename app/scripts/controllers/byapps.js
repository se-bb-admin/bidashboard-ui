/**
 * Created by sesa431390 on 3/9/2017.
 */
(function () {
  'use strict';
  angular.module('se-branding')
    .controller('byappsCtrl', ['$scope', 'appConstants', 'commonUtils', 'moment', 'appsDataService', 'globalSummaryService', 'gaChartUtil', '$mdDialog', '$uibModal', '$filter', '$timeout', '$mdMenu', '$mdToast', '$anchorScroll', '$location', '$window', '$document',
      '$interval', '$routeParams', 'googleChartApiPromise', '$q',
      function ($scope, appConstants, commonUtils, moment, appsDataService, globalSummaryService, gaChartUtil, $mdDialog, $uibModal, $filter, $timeout, $mdMenu, $mdToast, $anchorScroll, $location, $window, $document, $interval, $routeParams, googleChartApiPromise, $q) {

        var ALL = 'All';
        var countryTotalUsers = 0;
        var countryData = [];
        var allCountryData = {};
        var usersData = [];
        var appSelected = false;
        var routedAppId = '';
        var gaData = {};
        var gaFeatures = [];
        var gaTrafficData = [];
        var specialNotes = {};
        var gaSourceConfig = {};
        var PAGE_SIZE = 10;
        var colors = ['#43459d', '#e2431e', '#6f9654', '#f1ca3a', '#1c91c0'];

        $scope.dataNotes = appConstants.SUMMARY_NOTES;
        $scope.dataNotes.updatedTill.value = '';
        $scope.currentYear = moment().year();
        $scope.years = [];
        $scope.startYear = moment().year();
        $scope.period = {index: 1};
        $scope.userType = {index: 0};
        $scope.gaPeriod = {index: 1};
        $scope.featureGroups = [];

        var usersSummary = {};
        var periods = ['daily', 'weekly', 'monthly'];

        $scope.apps = [];

        $scope.selectedApp = {};
        $scope.selectedAppId = '';

        $scope.selectedYear = moment().year();
        $scope.selectedCountryCode = ALL;
        $scope.availableCountries = [];
        $scope.appCountryTargets = [];

        $scope.allNewUsers = 9990;
        $scope.allTotalUsers = 99990;
        var startActiveUsersSummaryFrom = 0;
        $scope.showActiveUsers = true;
        $scope.showActiveUserByCountry = true;

        $scope.loadingUsers = false;
        $scope.loadingUsersByCountry = false;
        $scope.loadingGaFeatures = false;
        $scope.loadingGaTraffic = false;

        $scope.countryUsersType = 'new';
        $scope.showGaFeatureChart = false;
        $scope.showGaTrafficChart = false;
        $scope.userChartReady = false;
        $scope.gaTrafficChartReady = false;
        $scope.showFrom = 0;
        $scope.showLimit = $scope.showFrom + PAGE_SIZE;

        $scope.months = ['All', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
          'October', 'November', 'December'];
        var gaChartsApps = [];
        var noTrafficChartApps = [];

        $scope.appLinks = {
          'easy-quote': '#/home/1',
          'global-meter': '#/home/2',
          'closed-loop': '#/home/2',
          'mix-n-match': '#/home/3',
          'my-electrician': '#/home/3',
          'quick-ref': '#/work/1',
          'cbt': '#/work/2',
          'ecoreal-qq': '#/work/2',
          'cad-library': '#/work/3',
          'berrysoft': '#/work/4',
          'smart-selector': '#/work/4',
          'ecoreal-630': '#/work/5',
          'rapsody': '#/work/5',
          'eco-dial-desktop': '#/work/6'
        }

        var countryMapTitles = ['Last 30 days', 'Last 26 weeks', 'Last 12 months'];

        $scope.monthSelected = $scope.months[0];

        $scope.chartUsers = {
          type: 'LineChart',
          data: {
            cols: [
              {id: 'date', label: 'Period', type: 'string'},
              {id: 'newusers', label: 'New', type: 'number'},
              {id: 'activeusers', label: 'Active', type: 'number'}
            ],
            rows: []
          },
          options: {
            titleTextStyle: {
              fontSize: 16
            },
            theme: 'material',
            width: '100%',
            height: 300,
            legend: {
              position: 'none'
            },
            colors: ['#43459d', '#e2431e'],
            chartArea: {
              height: '80%',
              width: '90%'
            },
            tooltip: {
              isHTML: true
            },
            pointSize: 6,
            pointShape: 'circle',
            hAxis: {
              textPosition: 'out',
              showTextEvery: 2
            },
            vAxis: {
              textPosition: 'out'
            }
          }
        };

        var chartGaFeatures = {
          type: 'LineChart',
          data: {
            cols: [
              {id: 'date', label: 'Period', type: 'string'},
              {id: 'newusers', label: 'New', type: 'number'},
              {id: 'activeusers', label: 'Active', type: 'number'}
            ],
            rows: []
          },
          options: {
            titleTextStyle: {
              fontSize: 16
            },
            theme: 'material',
            width: '100%',
            height: 300,
            legend: {
              position: 'top',
              alignment: 'end'
            },
            colors: ['#43459d', '#e2431e'],
            chartArea: {
              height: '80%',
              width: '90%'
            },
            tooltip: {
              isHTML: true
            },
            pointSize: 6,
            pointShape: 'circle',
            hAxis: {
              textPosition: 'out',
              showTextEvery: 2
            },
            vAxis: {
              textPosition: 'out'
            }
          }
        };

        $scope.chartGaTraffic = {
          type: 'LineChart',
          data: {
            cols: [],
            rows: []
          },
          options: {
            titleTextStyle: {
              fontSize: 16
            },
            theme: 'material',
            width: '100%',
            height: 300,
            legend: {
              position: 'top',
              alignment: 'end'
            },
            chartArea: {
              height: '80%',
              width: '90%'
            },
            tooltip: {
              isHTML: true
            },
            pointSize: 6,
            pointShape: 'circle',
            hAxis: {
              textPosition: 'out',
              showTextEvery: 2
            },
            vAxis: {
              textPosition: 'out'
            }
          }
        };


        $scope.countryChartSelectHandler = function (selectedItem) {
          var selectedCountry = $scope.chartUsersByCountries.data.rows[selectedItem.row]['c'];
          $scope.selectCountry({name: selectedCountry[0]['v'].split("(")[0].trim(), code: selectedCountry[3]['v']});
        }
        $scope.countryChartHoverHandler = function (row, col) {
          angular.element('g').css('cursor', 'pointer');
        }
        $scope.countryChartOutHandler = function (row, col) {
          angular.element('g').css('cursor', 'default');
        }

        $scope.chartUsersByCountries = gaChartUtil.populateTreeMapData({field: 'country', label: 'Country'},
          {field: 'year', label: 'Year'}, {field: 'count', label: 'Count'}, 400, 35, 14);

        function showChartTooltip(row, size, value) {
          var d = $scope.chartRegistrationByCountries.data.rows[row];
//                var perc = (value * 100).toFixed(2);
          var perc = (d.c[2].v / countryTotalRegistrations * 100).toFixed(1);
          return perc > 0 ? "<div style='background:#f9f9f9; padding:10px; box-shadow: 0 0 10px #333;font-weight: 600;text-align: center;border-radius: 4px;'><div>" + d.c[0].v + '</div><div>' + perc + "%</div></div>" : "";
        };

        /**
         *
         */
        function init() {
          routedAppId = $routeParams.appid || '';
          globalSummaryService.getAppConfig().then(function(data) {
            gaChartsApps = data.config.gaCharts.apps;
            noTrafficChartApps = data.config.noTrafficChart.apps;
            specialNotes = data.notes;
            gaSourceConfig = data.ga.sources;
            initSummaryData();
          });
        }

        /**
         *
         */
        function initSummaryData() {
          $scope.selectedApp = null;
          appsDataService.getSummaryData().then(function (data) {
          $scope.dataNotes.updatedTill.value = data.lastUpdatedDate;
            $scope.apps = data.apps.map(function (obj, indx) {
              if (obj.appId === routedAppId) {
                $scope.selectedApp = obj;
              }
              return obj;
            });
            $scope.selectedApp = $scope.selectedApp || $scope.apps[0];
            $scope.years = data.years;

            $scope.initData();
          });
        }

        $scope.initData = function () {
          var selectedAppId = $scope.selectedApp.appId;
          $scope.loadingUsers = true;
          $scope.loadingUsersByCountry = true;
          $scope.loadingGaFeatures = true;
          $scope.loadingGaTraffic = true;
          $scope.countryMapTitle = countryMapTitles[$scope.period.index];
          $scope.showGaFeatureChart = (gaChartsApps.indexOf(selectedAppId) !== -1) ? true : false;
          $scope.showGaTrafficChart = (noTrafficChartApps.indexOf(selectedAppId) !== -1) ? false : true;
          $scope.appNote = specialNotes.byapp[selectedAppId] || false;
          $scope.gaNote = specialNotes.ga[selectedAppId] || false;
          $scope.showFrom = 0;
          $scope.showLimit = $scope.showFrom + PAGE_SIZE;

          $scope.getUsersData()
          if($scope.showGaTrafficChart) {
            initGaTrafficData();
          }
          if($scope.showGaFeatureChart){
            initGaFeaturesData();
          }
        };

        $scope.initUsersChart = function (index) {
          var periodIndex = index || $scope.period.index;
          if(index === undefined && ($scope.currentYear != $scope.selectedYear)){
            periodIndex++;
          }
          var appId = $scope.selectedApp.appId;
          var uchart = $scope.chartUsers;
          $scope.userChartReady = false;

          switch (periodIndex) {
            case 0:
              uchart.data = gaChartUtil.populateThreeFieldChartData(usersData[appId].daily, 'accessedDate', 'newUsers', 'activeUsers', 'Dates', 'New Users', 'Active Users');
              break;
            case 1:
              uchart.data = gaChartUtil.populateThreeFieldChartData(usersData[appId].weekly, 'accessedWeek', 'newUsers', 'activeUsers', 'Weeks', 'New Users', 'Active Users');
              uchart.options.hAxis.showTextEvery = parseInt(usersData[appId].weekly.length / 10);
              break;
            case 2:
              uchart.data = gaChartUtil.populateThreeFieldChartData(usersData[appId].monthly, 'accessedMonth', 'newUsers', 'activeUsers', 'Months', 'New Users', 'Active Users');
              break;
          }

          googleChartApiPromise.then(function () {
            console.log("Show User chart");
            $scope.loadingUsers = false;
          });
        };

        $scope.getUsersByCountryData = function () {
          // $scope.loadingUsersByCountry = true;
          var selectedAppId = $scope.selectedApp.appId;
          appsDataService.getUsersByCountryData(selectedAppId, $scope.selectedCountryCode, $scope.selectedYear).then(function (data) {
            countryData = data;

            if($scope.selectedCountryCode === ALL) {
              allCountryData = angular.copy(data);
              appsDataService.getAppCountryTargetData(selectedAppId, $scope.selectedYear).then(function(targetData){
                var appCountryTargets = [];
                var targetList = [];
                var totalNewUsers = 0;
                var totalActiveUsers = 0;

                if(targetData.length === 0){
                  return;
                }
                targetData.forEach(function(tData){
                  var countryData = {newUsers: 0, activeUsers: 0};
                  var countryRecord = $filter('filter')(allCountryData[selectedAppId], function(data){
                    return data.countryCode === tData.countryCode;
                  });

                  if(countryRecord.length > 0){
                    countryData = countryRecord[0];
                  }
                  var record = angular.copy(tData);
                  if(appCountryTargets[record.clusterCode] === undefined){
                    record['newUsers'] = countryData['newUsers'];
                    record['activeUsers'] = countryData['activeUsers'];
                    record['members'] = [record.member];
                    appCountryTargets[record.clusterCode] = record;
                  }else{
                    appCountryTargets[record.clusterCode]['newUsers'] += countryData['newUsers'];
                    appCountryTargets[record.clusterCode]['activeUsers'] += countryData['activeUsers'];
                    appCountryTargets[record.clusterCode]['members'].push(record.member);
                  }
                  totalNewUsers += countryData['newUsers'];
                  totalActiveUsers += countryData['activeUsers'];
                });

                var totalUsers = allCountryData[selectedAppId].reduce(function(prev, next){
                  return [prev[0] + next.newUsers, prev[1] + next.activeUsers];
                }, [0, 0]);
                if(appCountryTargets['OTHER'] !== undefined){
                  appCountryTargets['OTHER']['newUsers'] = totalUsers[0] - totalNewUsers;
                  appCountryTargets['OTHER']['activeUsers'] = totalUsers[1] - totalActiveUsers;
                }
                for(var code in appCountryTargets){
                  var row = appCountryTargets[code];
                  row['targetAchieved'] = calculateProgress(row.newUsers, row.target);
                  row['achievedPercentage'] = Math.ceil((row.newUsers / row.target) * 100);
                  row['type'] = row.targetAchieved < 70 ? 'danger' : (row.targetAchieved < 90 ? 'warning' : 'success');
                  targetList.push(row);
                };
                $scope.appCountryTargets = $filter('orderBy')(targetList, function(data){
                  return data.clusterCode == 'OTHER' ? 1 : -(data.target);
                }, false);
              });
              $scope.availableCountries = countryData[selectedAppId].map(function (row) {
                return {name: row.countryName, code: row.countryCode};
              });
              $scope.availableCountries = $filter('orderBy')($scope.availableCountries, 'name');
              $scope.availableCountries.unshift({name: ALL, code: ALL});
            }
            $scope.initUsersByCountryChart('new');
          });
        };

        $scope.initUsersByCountryChart = function (dataType) {
          $scope.loadingUsersByCountry = true;
          $scope.countryUsersType = dataType || 'new';

          var appId = $scope.selectedApp.appId;
          var year = $scope.selectedYear;
          var tree = $scope.chartUsersByCountries;
          // set rows
          tree.data.rows = [{
            c: [
              {v: year},
              {v: null},
              {v: null}
            ]
          }];
         // tree.options.title = $scope.countryMapTitle;
          tree.options.highlightOnMouseOver = true;
          tree.options.titleTextStyle = {
            fontSize: 14,
            color: '#666',
            bold: false
          };

          countryTotalUsers = 0;
          // populate data and compute total users for all countries
          if(countryData[appId].length === 0){
            countryData[appId].push({
              newUsers: 0,
              activeUsers: 0,
              countryCode: $scope.selectedCountryCode,
              countryName: $filter('filter')($scope.availableCountries, {code: $scope.selectedCountryCode})[0].name
            });
          }
          countryData[appId].forEach(function (obj, i) {
            var users = (dataType === 'new') ? obj.newUsers : obj.activeUsers;
            countryTotalUsers += users;
            var row = {
              c: [
                {v: obj.countryName + " (" + users + ")"},
                {v: year},
                {v: (users == 0 ? users + 1 : users)},
                {v: obj.countryCode}
              ]
            };
            if(users != 0 || countryData[appId].length === 1){
              tree.data.rows.push(row);
            }
          });

          // function to generate tooltip on highlighting a country
          if(countryTotalUsers != 0){
            tree.options.showTooltips = true;
            tree.options.generateTooltip = function (row, size, value) {
              var d = tree.data.rows[row];
              var perc = (d.c[2].v / countryTotalUsers * 100).toFixed(1);
              return perc > 0 ? "<div style='background:#f9f9f9; padding:10px; box-shadow: 0 0 10px #333;font-weight: 600;text-align: center;border-radius: 4px;'><div>" + d.c[0].v + '</div><div>' + perc + "%</div></div>" : "";
            };
          }else{
            tree.options.showTooltips = false;
          }

          googleChartApiPromise.then(function () {
            $scope.loadingUsersByCountry = false;
          });
        };

        /**
         * Get data for particular application
         * @param appName
         */
        $scope.getUsersData = function () {
          var selectedAppId = $scope.selectedApp.appId;
          appsDataService.getUsersData(selectedAppId, $scope.selectedCountryCode, $scope.selectedYear).then(function (data) {
            usersData = angular.copy(data);
            usersData[selectedAppId]['monthly'].map(function (d, i) {
              d.accessedMonth = moment(d.accessedMonth).format("MMM 'YY");
              return d;
            });
            $scope.getUsersByCountryData();
            $scope.initUsersChart();
          });
        };

        /**
         * To select application from dropdown
         *
         * @param app
         */
        $scope.selectApp = function (app) {
          $scope.period.index = 1;
          $scope.gaPeriod.index = 1;
          appSelected = true;
          $scope.selectedApp = app;
          $scope.selectedAppId = app.appId;
          $scope.appSelectFilter = '';
          $scope.selectedCountryCode = ALL;
          $scope.selectedCountry = {};
          $scope.selectedYear = $scope.currentYear;
          $scope.appCountryTargets = [];
          $scope.initData();
          //$location.url($location.url().replace(/([\w]+)\/?(.*)/, '$1/' + app.appId));
        };

        $scope.selectCountry = function (country) {
          $scope.selectedCountry = country;
          $scope.selectedCountryCode = country.code;
          $scope.countrySelectFilter = '';
          $scope.initData();
          $window.scroll(0,0);
        };

        $scope.selectMonth = function (monthno) {
          $scope.monthSelected = $scope.months[monthno];
        };
        $scope.selectYear = function(year){
          var currentYearSelected = ($scope.currentYear == year);
          $scope.selectedYear = year;
          $scope.period.index = currentYearSelected ? 1 : 0;
          $scope.gaPeriod.index = currentYearSelected ? 1 : 0;
          $scope.appCountryTargets = [];
          $scope.initData();
        };

        /**
         *
         * @returns {undefined}
         */
        $scope.showCountryTable = function () {
          $scope.periodTitle = $scope.selectedYear;
          var appId = $scope.selectedApp.appId;
          var viewCountryData = $filter('orderBy')(allCountryData[appId], 'countryName', false);
          //show the data for only the selected year
          $scope.viewCountryData = $filter('filter')(viewCountryData, function(data){
            return data.newUsers != 0 || data.activeUsers != 0;
          });
        };

        $scope.closeOnSelect = function($event) {
          $event.stopPropagation();
        }

        $scope.scrollDown = function () {
          var scrollVal = 0;
          var delta = 5;
          var handle = $interval(function () {
            $window.scrollBy(0, delta);
            scrollVal += delta;
            if (scrollVal > 600) {
              $interval.cancel(handle);
            }
          }, 1);
        };

        function initGaFeaturesData(){
          var appId = $scope.selectedApp.appId;
          appsDataService.getGaFeaturesList(appId).then(function(data){
            gaFeatures = data;
            appsDataService.getAppGaFeatureData(appId, $scope.selectedCountryCode, $scope.selectedYear).then(function (data) {
              gaData[appId] = gaData[appId] || {};
              gaData[appId][$scope.selectedCountryCode] = data;
            });
          });
        }

        function initGaTrafficData(){
          var appId = $scope.selectedApp.appId;
          appsDataService.getAppGaTrafficData(appId, $scope.selectedCountryCode, $scope.selectedYear).then(function (data) {
            gaTrafficData[appId] = gaTrafficData[appId] || {};
            gaTrafficData[appId][$scope.selectedCountryCode] = data;
          });
        }

        $scope.initGaFeatureChart = function (index) {
          var periodIndex = index || $scope.gaPeriod.index;
          if(index === undefined && ($scope.currentYear != $scope.selectedYear)){
            periodIndex++;
          }
          //$scope.loadingGaTraffic = true;
          var graphIndex = 0;
          var featureGroups = [];
          $scope.featureGroups = [];
          $scope.chartGaFeatures = [chartGaFeatures];
          var appId = $scope.selectedApp.appId;
          var chartLabels =[
            {type: 'daily', attr: 'featureDate', label: 'Dates'},
            {type: 'weekly', attr: 'featureWeek', label: 'Weeks'},
            {type: 'monthly', attr: 'featureMonth', label: 'Months'}
          ];
          var period = chartLabels[periodIndex];
          var data = gaData[appId][$scope.selectedCountryCode][period.type].data;


          for(var featureGroup in gaFeatures){
            featureGroups.push(featureGroup);
          }
          setData();

          function setData(){
            $scope.chartGaFeatures[graphIndex] = angular.copy(chartGaFeatures);
            var uchart = $scope.chartGaFeatures[graphIndex];
            var features = {id: [period.attr], label: [period.label]};
            var vAxis = angular.copy(uchart.options.vAxis);
            var vAxes = [angular.copy(vAxis)];
            var series = {};
            var featureGroup = featureGroups[graphIndex];
            var yAxisLabels = [];

            $scope.featureGroups.push(featureGroup);

            gaFeatures[featureGroup].forEach(function(feature, index){
              features['id'].push(feature.id);
              features['label'].push(feature.label);
              series[index] = {targetAxisIndex: 0};

              if(feature['scale'] !== undefined){
                vAxes.push(vAxis);
                series[index].targetAxisIndex = index;
                vAxes[index].title = feature.name;
              }else{
                yAxisLabels.push(feature.name);
              }
            });
            var yAxisText = yAxisLabels.join(', ');

            //multiple Y Axes (currently google charts supports only two)
            if(vAxes.length > 1){
              vAxes[0].title = yAxisText;
              uchart.options.vAxes = vAxes;
              uchart.options.series = series;
              uchart.options.chartArea.width = '85%';
            }

            uchart.data = gaChartUtil.populateMultiLineChartData(data, features['id'], features['label']);
            uchart.options.colors = colors.slice(graphIndex);

            if(featureGroups[++graphIndex] !== undefined){
              console.log("Draw the next graph");
              $timeout(setData, 200);
            }
          }
          $scope.loadingGaFeatures = false;
        };

        $scope.initGaTrafficChart = function (index) {
          var periodIndex = index || $scope.gaPeriod.index;
          if(index === undefined && ($scope.currentYear != $scope.selectedYear)){
            periodIndex++;
          }
          $scope.loadingGaTraffic = true;
          $scope.gaTrafficChartReady = false;
          var appId = $scope.selectedApp.appId;
          var uchart = $scope.chartGaTraffic;
          var sources = [];
          var labels = [];
          var colors = [];
          var chartLabels =[
            {type: 'daily', attr: 'accessedDate', label: 'Dates'},
            {type: 'weekly', attr: 'accessedWeek', label: 'Weeks'},
            {type: 'monthly', attr: 'accessedMonth', label: 'Months'}
          ];
          var period = chartLabels[periodIndex];
          var data = gaTrafficData[appId][$scope.selectedCountryCode][period.type].data;
          //extract only the source attributes from the api response
          var apiKeys = Object.keys(data[0]);
          var attrIndex = apiKeys.indexOf(period.attr);
          var yearIndex = apiKeys.indexOf('year');
          if(yearIndex !== -1 &&  yearIndex < attrIndex){
            apiKeys.splice(yearIndex, 1);
            attrIndex--;
          }
          sources = apiKeys.slice(0, attrIndex).sort();
          if(sources.length === 0){
            sources = Object.keys(gaSourceConfig).slice(0,1);
          }
          sources.forEach(function(source){
            labels.push(gaSourceConfig[source].label);
            colors.push(gaSourceConfig[source].color);
          });
          sources = [period.attr].concat(sources);
          labels = [period.label].concat(labels);

          uchart.options.colors = colors;
          uchart.data = gaChartUtil.populateMultiLineChartData(data, sources, labels);
          $scope.loadingGaTraffic = false;
        };

        $scope.chartReadyHandler = function chartReadyHandler(chartWrapper){
          console.log("_______Chart Ready______", chartWrapper.ZL.id, $scope.userChartReady);
          if(chartWrapper.ZL.id === 'userChart' && !$scope.userChartReady){// && $scope.showGaFeatureChart){
            $scope.userChartReady = true;

            if($scope.showGaTrafficChart){
              $scope.loadingGaTraffic = true;
              $timeout($scope.initGaTrafficChart, 100);
            }else if($scope.showGaFeatureChart){
              $scope.gaTrafficChartReady = true;
              var wait = angular.equals({}, gaData) ? 200 : 100;
              $timeout($scope.initGaFeatureChart, wait);
            }
          }else if(chartWrapper.ZL.id === 'gaTrafficChart' && $scope.showGaFeatureChart && !$scope.gaTrafficChartReady){
            $scope.gaTrafficChartReady = true;
            //$scope.loadingGaFeatures = true;
            var wait = angular.equals({}, gaData) ? 200 : 100;
            $timeout($scope.initGaFeatureChart, wait);
          }
        };

        $scope.showMore = function(end){
          $scope.showFrom += PAGE_SIZE;
          $scope.showLimit = end;
        }

        $scope.showLess = function(){
          $scope.showFrom = 0;
          $scope.showLimit = PAGE_SIZE;
        }

        function calculateProgress(newUsers, target){
          var dayOfYear = moment($scope.dataNotes.updatedTill.value).dayOfYear();
          var DAYS_IN_YEAR = 365;

          return Math.floor(((newUsers / dayOfYear) / (target / DAYS_IN_YEAR)) * 100);
        }
        // invoke init function
        init();

      }]);
})();
