/**
 * @ngdoc controller
 * @name shell
 *
 * @description
 *  shell controller includes header, main container and footer controllers. The global required events are exposed in this controller.
 *  Global required function is written in this controller.
 *
 */
(function () {
  'use strict';

  angular.module('se-branding').controller('workCtrl', work);

//	work.$inject = ['$rootScope', 'logger', 'paceService', 'appConstants', '$routeParams', '$location', 'serverDataValidationUtils'];
  work.$inject = ['$rootScope', 'logger', 'paceService', 'appConstants', '$routeParams', '$location','$mdSidenav'];

  /**
   *
   * @param {type} $rootScope
   * @param {type} logger
   * @param {type} paceService
   * @param {type} appConstants
   * @param {type} $routeParams
   * @param {type} $location
   * @param {type} serverDataValidationUtils
   * @returns {work_L10.work}
   */
  function work($rootScope, logger, paceService, appConstants, $routeParams, $location, $mdSidenav) {
//			$location, serverDataValidationUtils) {

    // #Start Conrroller
    var vm = this;
    vm.showLoader = false;
    vm.showTransparentOverlay = false;
    vm.showGrayOverlay = false;
    vm.paceData = null;
    vm.paceDataRegUser = null;
    vm.paceDataRegUserOverPeriod = null;
    vm.loadPage = loadPage;
    vm.pageTitle1 = appConstants.PAGE_TITLE.WORK.PAGE_1;
    vm.pageTitle2 = appConstants.PAGE_TITLE.WORK.PAGE_2;
    vm.pageTitle3 = appConstants.PAGE_TITLE.WORK.PAGE_3;
    vm.pageTitle4 = appConstants.PAGE_TITLE.WORK.PAGE_4;
    vm.pageTitle5 = appConstants.PAGE_TITLE.WORK.PAGE_5;
    vm.pageTitle6 = appConstants.PAGE_TITLE.WORK.PAGE_6;
    vm.pageTitle7 = appConstants.PAGE_TITLE.WORK.PAGE_7;
    vm.pageTitle8 = appConstants.PAGE_TITLE.WORK.PAGE_8;
    vm.isActive = isActive;

    var PACE_CHART_REG_USER_LBL = appConstants.PACE_CHART.LABEL.REG_USER;
    var MONTH_ARR = appConstants.CHART_OPTIONS.MONTH_ARR;

    //GET Page NO
    var pageNo = $routeParams.pageNo;
    vm.pageNo = parseInt((pageNo === null || pageNo === undefined || pageNo === "") ? 0 : pageNo);

    vm.workProjects = [{
				name: 'Quick Ref | MyPact',
				path: '/work/1'
			},  {
				name: 'Ecoreal QQ 630A | CBT',
				path: '/work/2'
			}, {
				name: 'BLM | CAD Library',
				path: "/work/3"
			}, {
				name: 'Smart Selector | Berrisoft',
				path: '/work/4'
			},  {
				name: 'Ecoreal 630 | Rapsody',
				path: "/work/5"
			}, {
				name: 'Ecodial Desktop | Ecoreach',
				path: '/work/6'
			}, {
				name: 'Ecodial AutoCAD Plugin | Ecodial China',
				path: '/work/7'
			}, {
				name: 'Masterpact MTZ App',
				path: '/work/8'
			}];

    init();


    /**
     * Constractor
     *
     * @returns {undefined}
     */
    function init() {
      logger.log("Init :: Work ::  Page No :: " + vm.pageNo);

      //Get registered user from the bigining of time
      getRegisteredUsers();

      //Get registered user from last one year
      getRegisteredUsersOverPeriod();
    }

    /**
     * Get registered user from the bigining of time
     * @returns {undefined}
     */
    function getRegisteredUsers() {
      //Get Data From PACE
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

      paceService.getRegisteredUsers().then(
              function (result) {
                logger.log("Work::PACE::Result::Success");
                parsePACEDataRegUser(result);
//						console.log('==> work: paceDataRegUser :: ', vm.paceDataRegUser);
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              },
              function (error) {
                logger.log("Work::PACE::Result::FAIL");
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              }
      );
    }


    /**
     *
     * @param {type} data
     * @returns {undefined}
     */
    function parsePACEDataRegUser(data) {
      vm.paceDataRegUser = {};
      vm.paceDataRegUser.blmUsersReg = 0;
      vm.paceDataRegUser.rapsodyUsersReg = 0;
      vm.paceDataRegUser.smartSelectorsUsersReg = 0;
      vm.paceDataRegUser.cbtUsersReg = 0;
      vm.paceDataRegUser.myPactBasicUsersReg = 0;
      vm.paceDataRegUser.quickQuotation630SpainUsersReg = 0;
      vm.paceDataRegUser.quickQuotation630RussiaUsersReg = 0;
      vm.paceDataRegUser.quickQuotation630backupSpainUsersReg = 0;
      vm.paceDataRegUser.quickQuotation630backupRussiaUsersReg = 0;
      vm.paceDataRegUser.unAssignedUser = 0;
      vm.paceDataRegUser.cadLibraryUsersReg = 0;
      vm.paceDataRegUser.ecoreal630UsersReg = 0;
      vm.paceDataRegUser.cccAppUsersReg = 0;
      vm.paceDataRegUser.cccAppAndroidUsersReg = 0;
      vm.paceDataRegUser.cccAppIOSUsersReg = 0;
      vm.paceDataRegUser.cccAppCumulative = 0;
      vm.paceDataRegUser.ecorealMvUsersReg = 0;
      vm.paceDataRegUser.myNovaBuddyUsersReg = 0;

      //console.dir(data);
      var applicationStats = angular.copy(data);
      var tempNode = "";
      var projName = "";
//logger.log('@126: parsePACEDataRegUser(',data,')');

      for (var i = 0; i < applicationStats.length; i++) {
        tempNode = applicationStats[i];
        projName = "";
        projName = tempNode.projName;

        //logger.log(" APP URL :: " + projName);

        switch (projName) {
          case appConstants.SITE_NAME.QUICK_QUOTATION_630_SPAIN:
            vm.paceDataRegUser.quickQuotation630SpainUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.QUICK_QUOTATION_630_RUSSIA:
            vm.paceDataRegUser.quickQuotation630RussiaUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.ECOREAL_630:
            vm.paceDataRegUser.ecoreal630UsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.MY_PACT:
            vm.paceDataRegUser.myPactBasicUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.BLM:
            vm.paceDataRegUser.blmUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.CAD_LIBRARY:
            vm.paceDataRegUser.cadLibraryUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.RAPSODY:
            vm.paceDataRegUser.rapsodyUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.SMART_SELECTORS:
            vm.paceDataRegUser.smartSelectorsUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.BERRI_SOFT:
            vm.paceDataRegUser.berriSoftUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.ECO_DIAL_DESKTOP:
            vm.paceDataRegUser.ecoDialDesktopUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.ECO_DIAL_CHINA:
            vm.paceDataRegUser.ecoDialChinaUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.ECO_DIAL_AUTOCAD_PLUGIN:
            vm.paceDataRegUser.ecoDialAutoCadPluginUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.CBT:
            vm.paceDataRegUser.cbtUsersReg = tempNode.users;
            break;

          case appConstants.SITE_NAME.NOT_LISTED:
            vm.paceDataRegUser.unAssignedUser = tempNode.users;
            break;

          case appConstants.SITE_NAME.CCC_APP:
            vm.paceDataRegUser.cccAppUsersReg = tempNode.users;
            break;
          case appConstants.SITE_NAME.CCC_APP_ANDROID:
            vm.paceDataRegUser.cccAppAndroidUsersReg = tempNode.users;
            break;
          case appConstants.SITE_NAME.CCC_APP_IOS:
            vm.paceDataRegUser.cccAppIOSUsersReg = tempNode.users;
            break;
          case appConstants.SITE_NAME.ECOREALMV:
            vm.paceDataRegUser.ecorealMvUsersReg = tempNode.users;
            break;
          case appConstants.SITE_NAME.MY_NOVA_BUDDY:
            vm.paceDataRegUser.myNovaBuddyUsersReg = tempNode.users;
            break;
        }//End Switch Case
      } //End For Loop
      vm.paceDataRegUser.cccAppCumulative = parseInt(vm.paceDataRegUser.cccAppUsersReg) + parseInt(vm.paceDataRegUser.cccAppAndroidUsersReg) + parseInt(vm.paceDataRegUser.cccAppIOSUsersReg);
    }

    /**
     * Get registered user from the bigining of time
     * @returns {undefined}
     */
    function getRegisteredUsersOverPeriod() {
      //Get Data From PACE
      $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);

      paceService.getRegisteredUsersOverPeriod().then(
              function (result) {
                logger.log("Work::PACE::Result::Success");
                parsePACEDataRegUserOverPeriod(result);

                $rootScope.$broadcast('RegUserOverPeriodDataReady', vm.paceDataRegUserOverPeriod);
              },
              function (error) {
                logger.log("Work::PACE::Result::FAIL");
                $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
              }
      );
    }


    /**
     *
     * @param {type} data
     * @returns {undefined}
     */
    function parsePACEDataRegUserOverPeriod(data) {
      vm.paceDataRegUserOverPeriod = {};
      vm.paceDataRegUserOverPeriod.blmUsersReg = null;
      vm.paceDataRegUserOverPeriod.rapsodyUsersReg = null;
      vm.paceDataRegUserOverPeriod.smartSelectorsUsersReg = null;
      vm.paceDataRegUserOverPeriod.cbtUsersReg = null;
      vm.paceDataRegUserOverPeriod.myPactBasicUsersReg = null;
      vm.paceDataRegUserOverPeriod.quickQuotation630SpainUsersReg = null;
      vm.paceDataRegUserOverPeriod.quickQuotation630RussiaUsersReg = null;
      vm.paceDataRegUserOverPeriod.unAssignedUser = null;
      vm.paceDataRegUserOverPeriod.cadLibraryUsersReg = null;
      vm.paceDataRegUserOverPeriod.cccAppUsersReg = null;
      vm.paceDataRegUserOverPeriod.ecorealMvUsersReg = null;
      vm.paceDataRegUserOverPeriod.myNovaBuddyUsersReg = null;
      vm.paceDataRegUserOverPeriod.ecoreach2UsersReg = null;

      var applicationStats = angular.copy(data);
      var tempNode = "";
      var projName = "";

      for (var i = 0; i < applicationStats.length; i++) {
        tempNode = applicationStats[i];
        projName = tempNode.projName;
        //logger.log(" APP URL :: " + projName);
        switch (projName) {
          case appConstants.SITE_NAME.QUICK_QUOTATION_630_SPAIN:
            vm.paceDataRegUserOverPeriod.quickQuotation630SpainUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.QUICK_QUOTATION_630_RUSSIA:
            vm.paceDataRegUserOverPeriod.quickQuotation630RussiaUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.ECOREAL_630:
            vm.paceDataRegUserOverPeriod.ecoreal630UsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.MY_PACT:
            vm.paceDataRegUserOverPeriod.myPactBasicUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.BLM:
            vm.paceDataRegUserOverPeriod.blmUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.CAD_LIBRARY:
            vm.paceDataRegUserOverPeriod.cadLibraryUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.RAPSODY:
            vm.paceDataRegUserOverPeriod.rapsodyUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.SMART_SELECTORS:
            vm.paceDataRegUserOverPeriod.smartSelectorsUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.BERRI_SOFT:
            vm.paceDataRegUserOverPeriod.berriSoftUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.ECO_DIAL_DESKTOP:
            vm.paceDataRegUserOverPeriod.ecoDialDesktopUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.ECO_DIAL_CHINA:
            vm.paceDataRegUserOverPeriod.ecoDialChinaUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.ECO_DIAL_AUTOCAD_PLUGIN:
            vm.paceDataRegUserOverPeriod.ecoDialAutoCadPluginUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.CBT:
            vm.paceDataRegUserOverPeriod.cbtUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.CCC_APP:
            vm.paceDataRegUserOverPeriod.cccAppUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.ECOREALMV:
            vm.paceDataRegUserOverPeriod.ecorealMvUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.MY_NOVA_BUDDY:
            vm.paceDataRegUserOverPeriod.myNovaBuddyUsersReg = populatePaceUserChartData(tempNode);
            break;

          case appConstants.SITE_NAME.ECO_REACH_2:
            vm.paceDataRegUserOverPeriod.ecoreach2UsersReg = populatePaceUserChartData(tempNode);
//						console.log("\n\nvm.paceDataRegUserOverPeriod.ecoreach2UsersReg: ", JSON.stringify(vm.paceDataRegUserOverPeriod.ecoreach2UsersReg), '\n\n');
            break;
        } //End Switch Case..
      }// End For Loop
    }


    /**
     *
     * @param {type} target
     * @param {type} source
     * @returns {undefined}
     */
    function addUserData(target, source) {
      for (var i = 0; i < target.length; i++) {
        for (var l = 0; l < source.length; l++) {
          if (target[i].date === source[l].date) {
            target[i].users = target[i].users + source[l].users;
          }
        }
      }
    }

    //------------------------------------------------------------------

    /**
     *
     * @param {type} data
     * @returns {undefined}
     */
    function populatePaceUserChartData(data) {
//console.log('@347: populatePaceUserChartData: ',data);
      var chartData = {};

      var dayChartData = populateDayChartData(data.day);
      var weekChartData = populateWeekChartData(data.week);
      var monthChartData = populateMonthChartData(data.month);

      chartData.day = dayChartData;
      chartData.week = weekChartData;
      chartData.month = monthChartData;


      return chartData;
    }


    //------------------------------------------------------------------
    //------------------ Populate Day Chart Data -----------------------
    //------------------------------------------------------------------
    /**
     *
     * @param {type} data
     * @returns {work_L15.populateDayChartData.dayChartData}
     */
    function populateDayChartData(data) {
      var dayChartData = {};
      var columns = getDayChatColumnsData();
      var rows = getDayChatRowsData(data);
      dayChartData.cols = columns;
      dayChartData.rows = rows;

      return dayChartData;

    }

    /**
     *
     * @returns {paceService_L9.getDayChatColumnsData.cols|Array}
     */
    function getDayChatColumnsData() {
      var cols = [];
      var col1 = {};
      var col2 = {};
      //Populate Col1
      col1.id = "date";
      col1.label = "Date";
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = "users";
      col2.label = PACE_CHART_REG_USER_LBL;
      col2.type = "number";
      //Push
      cols.push(col2);

      return cols;
    }

    /**
     *
     *
     * OUTPUT Single Object: {c:[{v: "2015-03-25"},{v: 14}]}
     *
     * @param {type} data
     * @returns {undefined}
     */
    function getDayChatRowsData(data) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = getMonthDateFromDateStr(dataRow.dateFormatted);
        //Push to Array
        row.c.push(rowValDate);

        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow.users;
        //Push to Array
        row.c.push(rowValUser);

        //push into Rows
        rows.push(row);
      }

      return  rows;

    }

    /**
     *
     * @param {type} dateStr
     * @returns {String}
     */
    function getMonthDateFromDateStr(dateStr) {
      var monthArr = MONTH_ARR;
      var dtArr = dateStr.split("-");
      var month = parseInt(dtArr[1]);
      var monthStr = monthArr[(month - 1)];
      var date = dtArr[2];
      return monthStr + " " + date;

    }

    //------------------------------------------------------------------
    //------------------ Populate Week Chart Data ----------------------
    //------------------------------------------------------------------
    /**
     *
     * @param {type} data
     * @returns {work_L15.populateDayChartData.dayChartData}
     */
    function populateWeekChartData(data) {
      var weekChartData = {};
      var columns = getWeekChatColumnsData();
      var rows = getWeekChatRowsData(data);
      weekChartData.cols = columns;
      weekChartData.rows = rows;

      return weekChartData;

    }

    /**
     *
     * @returns {paceService_L9.getDayChatColumnsData.cols|Array}
     */
    function getWeekChatColumnsData() {
      var cols = [];
      var col1 = {};
      var col2 = {};
      //Populate Col1
      col1.id = "date";
      col1.label = "Date";
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = "users";
      col2.label = PACE_CHART_REG_USER_LBL;
      col2.type = "number";
      //Push
      cols.push(col2);

      return cols;
    }

    /**
     *
     *
     * OUTPUT Single Object: {c:[{v: "2015-03-25"},{v: 14}]}
     *
     * @param {type} data
     * @returns {undefined}
     */
    function getWeekChatRowsData(data) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = getWeekYearFromDateStr(dataRow.dateFormatted, dataRow.week);
        //Push to Array
        row.c.push(rowValDate);

        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow.users;
        //Push to Array
        row.c.push(rowValUser);


        //push into Rows
        rows.push(row);
      }

      return  rows;
    }



    function getWeekYearFromDateStr(dateStr, wek) {
      var monthArr = MONTH_ARR;
      var dtArr = dateStr.split("-");
      var month = parseInt(dtArr[1]);
      var monthStr = monthArr[(month - 1)];
      var yearWeek = "";
      var year = dtArr[0];

      var week = parseInt(wek);
      var weekStr = week > 9 ? week : "0" + week;



      yearWeek = year + "-" + weekStr;

      return yearWeek;
    }



    //------------------------------------------------------------------
    //------------------ Populate Month Chart Data ---------------------
    //------------------------------------------------------------------
    /**
     *
     * @param {type} data
     * @returns {work_L15.populateDayChartData.dayChartData}
     */
    function populateMonthChartData(data) {
      var monthChartData = {};
      var columns = getMonthChatColumnsData();
      var rows = getMonthChatRowsData(data);
      monthChartData.cols = columns;
      monthChartData.rows = rows;

      return monthChartData;

    }

    /**
     *
     * @returns {paceService_L9.getDayChatColumnsData.cols|Array}
     */
    function getMonthChatColumnsData() {
      var cols = [];
      var col1 = {};
      var col2 = {};
      //Populate Col1
      col1.id = "date";
      col1.label = "Date";
      col1.type = "string";
      //Push
      cols.push(col1);

      //Populate Col1
      col2.id = "users";
      col2.label = PACE_CHART_REG_USER_LBL;
      col2.type = "number";
      //Push
      cols.push(col2);

      return cols;
    }

    /**
     *
     *
     * OUTPUT Single Object: {c:[{v: "2015-03-25"},{v: 14}]}
     *
     * @param {type} data
     * @returns {undefined}
     */
    function getMonthChatRowsData(data) {
      var rows = [];
      var dataLen = data.length;
      var dataRow = null;
      var row = {};
      var rowValDate = {};
      var rowValUser = {};

      for (var i = 0; i < dataLen; i++) {
        row = {};
        dataRow = data[i];
        row.c = [];

        //Add Date
        rowValDate = {};
        rowValDate.v = getYearMonthFromDateStr(dataRow.dateFormatted);
        //Push to Array
        row.c.push(rowValDate);

        //Add user Value
        rowValUser = {};
        rowValUser.v = dataRow.users;
        //Push to Array
        row.c.push(rowValUser);


        //push into Rows
        rows.push(row);
      }

      return  rows;
    }


    function getYearMonthFromDateStr(dateStr) {
      var monthArr = MONTH_ARR;
      var dtArr = dateStr.split("-");
      var month = parseInt(dtArr[1]);
      var monthStr = monthArr[(month - 1)];
      var year = dtArr[0];
      var yearMonth = "";

      yearMonth = monthStr + "'" + year.substring(2, 4);

      return yearMonth;
    }


    //------------------------------------------------------------------








    //------------------------------------------------------------------
    /**
     *
     * @param {type} evnt
     * @param {type} pgno
     * @returns {undefined}
     */
    function loadPage(evnt, pgno) {
      if (evnt !== undefined && evnt !== null) {
        evnt.stopImmediatePropagation();
      }
      vm.currentPage = parseInt(pgno);
      var URL = 'work/' + pgno;
      $location.path(URL);
    }


    /**
     *
     * @returns {undefined}
     function selectPageNo() {
     // TOP Pagination
     $("#workPageUl1").find("li").removeClass("active");
     var pageDivLiTop = "#workPageLi1_" + vm.pageNo;
     $(pageDivLiTop).addClass("active");
     // Bottom Pagination
     $("#workPageUl2").find("li").removeClass("active");
     var pageDivLiBottom = "#workPageLi2_" + vm.pageNo;
     $(pageDivLiBottom).addClass("active");
     }*/


    /**
     *
     * @param {type} page
     * @returns {String}
     */
    function isActive(page) {
      page = parseInt(page);
      //logger.log("Page ::" + page + " Current Page:: " + vm.pageNo);
      if (page === vm.pageNo) {
        return 'active';
      } else {
        return '';
      }
    }
    vm.openLeftMenu = function() {
      $mdSidenav('work').toggle();
    };


  }// #End


})();
