'use strict';
angular.module('se-branding')
        .controller('homeCtrl',
//			function ($scope, $mdToast, sdkTemplateServices, $http, $window, gettextCatalog, toasterService, $log,
//					$sce, authorizationService) {
                function ($rootScope, logger, $routeParams, $location, appConstants, paceService) {

                  // #Start Conrroller
                  var vm = this;
                  vm.showLoader = false;
                  vm.showTransparentOverlay = false;
                  vm.showGrayOverlay = false;
                  vm.paceDataRegUser = null;
                  vm.publicProperty = "Public Property";
                  vm.loadPage = loadPage;
                  vm.isActive = isActive;
                  //GET Page NO
                  var pageNo = $routeParams.pageNo;

                  vm.homeProjects = [{
                      name: 'Easy Mobile Quote | Clipsal Wishlist',
                      path: "/home/1"

                    }, {
                      name: '',
                      path: "/home/1"
                    }, {
                      name: 'Global Meter App | Closed Loop',
                      path: "/home/2"
                    }, {
                      name: 'Mix n Match | My Electrician  App',
                      path: "/home/3"
                    }, {
                      name: 'WiseUp',
                      path: "/home/4"
                    }];

                  vm.users = [
                    {id: 1, name: 'Bob'},
                    {id: 2, name: 'Alice'},
                    {id: 3, name: 'Steve'}
                  ];
                  vm.selectedUser = {id: 1, name: 'Bob'};

                  vm.pageNo = parseInt((pageNo === null || pageNo === undefined || pageNo === "") ? 0 : pageNo);
                  init();
                  /**
                   * Constractor
                   *
                   * @returns {undefined}
                   */
                  function init() {
                    logger.log("init :: Home");
                    getRegisteredUsers();
                  }

                  function getRegisteredUsers() {
                    //Get Data From PACE
                    $rootScope.$broadcast(appConstants.EVENTS.SHOW_LOADER, null);
                    paceService.getRegisteredUsers().then(
                            function (result) {
                              logger.log("Home::PACE::Result::Success");
                              parsePACEDataRegUser(result);
                              $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                            },
                            function (error) {
                              logger.log("Home::PACE::Result::FAIL");
                              $rootScope.$broadcast(appConstants.EVENTS.HIDE_LOADER, null);
                            }
                    );
                  }


                  function parsePACEDataRegUser(data) {
                    vm.paceDataRegUser = {};
                    vm.paceDataRegUser.meaUsersReg = 0;
                    vm.paceDataRegUser.mesUsersReg = 0;
                    var applicationStats = angular.copy(data);
                    var tempNode = "";
                    var projName = "";
                    for (var i = 0; i < applicationStats.length; i++) {
                      tempNode = applicationStats[i];
                      projName = "";
                      projName = tempNode.projName;
                      switch (projName) {
                        case appConstants.SITE_NAME.MEA:
                          vm.paceDataRegUser.meaUsersReg = tempNode.users;
                          break;
                        case appConstants.SITE_NAME.MES:
                          vm.paceDataRegUser.mesUsersReg = tempNode.users;
                          break;
                      }
                    }
                  }
                  /**
                   *
                   * @param {type} evnt
                   * @param {type} pgno
                   * @returns {undefined}
                   */
                  function loadPage(evnt, pgno) {
                    if (evnt !== undefined && evnt !== null) {
                      evnt.stopImmediatePropagation();
                    }
                    vm.currentPage = parseInt(pgno);
                    var URL = 'home/' + pgno;
                    $location.path(URL);
                  }

                  /**
                   *
                   * @param {type} page
                   * @returns {String}
                   */
                  function isActive(page) {
                    page = parseInt(page);
                    //logger.log("Page ::" + page + " Current Page:: " + vm.pageNo);
                    if (page === vm.pageNo) {
                      return 'active';
                    } else {
                      return '';
                    }
                  }


                });// #End