/**
 * Created by sesa431390 on 3/9/2017.
 */
(function() {
  'use strict';

  angular.module('se-branding')
    .filter('alink', function() {
      return function(input) {

        var strLink = input;
        if (apps.indexOf(input) > -1) {
          strLink = "<span class='bi-link'>" + input + "</span>";
        }
        return strLink;
      }
    })
})();
