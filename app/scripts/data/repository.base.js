/**
 * @ngdoc Repository
 * @name repository.BaseRepository
 *
 * @description
 *  This is the Repository Class for Base Repository
 *  Store & Map data into Data Store
 *
 */

(function () {
  'use strict';

  var factoryId = 'BaseRepository';

  angular.module('repositoryModule').factory(factoryId, [
    '$q', '$injector', 'logger', 'DataStore',
    function ($q, $injector, logger, DataStore) {

      //#region Factory Construction

      //# Private Property Store All Data in a central Location
      var _dataStore = new DataStore();

      function Ctor() {
      }

      /*
       // Required for inheritance.
       Ctor.extend = function(repoCtor) {
       repoCtor.prototype = new Ctor();
       repoCtor.prototype.constructor = repoCtor;
       repoCtor.prototype.setData = setData;
       };
       */


      //#region Factory definition
      Ctor.prototype.resourceName = '';
      Ctor.prototype.setData = setData;
      Ctor.prototype.getDataById = getDataById;
      Ctor.prototype.update = update;
      Ctor.prototype.add = add;
      Ctor.prototype.remove = remove;
      Ctor.prototype.failureHandler = failureHandler;

      return Ctor;

      //------------------------------------------------------------------
      //---------------------- Public Methods ----------------------------
      //------------------------------------------------------------------

      /**
       *
       * @param {type} objData
       * @returns {undefined}
       */
      function setData(objData) {
        if (objData.id !== undefined && objData.id !== null) {
          var resourceName = objData.id;
          _dataStore.set(resourceName, objData);
        } else {
          throw new Error('Unable to set data:: id is not defined.');
        }
      }

      /**
       *
       * @param {type} id
       * @returns {Entity}
       */
      function getDataById(id) {
        var deferred = $q.defer();
        var result = null;
        try {
          result = _dataStore.get(id);
          deferred.resolve(result);
        } catch (e) {
          deferred.reject(result);
        }
        return deferred.promise;
      }



      /**
       *
       * @param {type} entity
       * @returns {undefined}
       */
      function update(entity) {
      }

      /**
       *
       * @param {type} entity
       * @returns {undefined}
       */
      function add(entity) {
      }

      /**
       *
       * @param {type} id
       * @returns {undefined}
       */
      function remove(id) {
      }

      //------------------------------------------------------------------
      //------------------- Private Methods --------------------------
      //------------------------------------------------------------------

      function failureHandler(error, deferred) {
        logger.error('Repository Action Failed', factoryId, error, false);
        if (deferred !== null)
          deferred.reject(error);
      }

    }]);
})();