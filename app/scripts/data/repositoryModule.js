/**
 * @ngdoc Module
 * @name repositoryModule
 *
 * @description
 *   Define separate module for repository
 *
 */
(function () {
  'use strict';

  var moduleId = 'repositoryModule';
  //Inject dataModelModule for Logger
  var repositoryModule = angular.module(moduleId, ['commonUtilsModule']);
}
)();
