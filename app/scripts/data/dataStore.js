/**
 * @name DataStore
 *
 * @description
 *      This is the base class of Repository.
 *
 */
(function () {
  'use strict';
  var factoryId = 'DataStore';

  angular.module('repositoryModule').factory(factoryId, [
    "$q", 'logger',
    function ($q, logger) {

      /**
       * @private
       * @property {objData} name description
       */
      var _data = {};


      /**
       * @public
       * @constructor
       * @returns {_L7.Ctor}
       */
      function Ctor() {
        this.set = setData;
        this.get = getData;
        this.find = findData;
        this.update = updateData;
        this.save = saveData;
        this.destroy = destroyData;

      }
      //#endregion
      return Ctor;


      //------------------------------------------------------------------
      //----------------------- Public Methods ---------------------------
      //------------------------------------------------------------------


      /**
       *
       * @param {type} resourceName
       * @param {type} objData
       * @returns {undefined}
       */
      function setData(resourceName, objData) {
        _data[resourceName] = {};
        _data[resourceName] = objData;
        logger.log('Set Data :: resourceName :: ' + resourceName);
      }

      /**
       *
       * @param {type} resourceName
       * @returns {unresolved}
       */
      function getData(resourceName) {
        var returnVal = null;
        if (_data[resourceName] === undefined) {
          returnVal = null;
        } else {
          returnVal = _data[resourceName];
        }
        logger.log('Get Data :: resourceName :: ' + resourceName);
        return returnVal;
      }

      function findData() {
        //TODO Definen later
      }
      function saveData() {
        //TODO Definen later
      }

      function destroyData() {
        //TODO Definen later
      }

      function updateData() {

      }


    }
  ]);

})();