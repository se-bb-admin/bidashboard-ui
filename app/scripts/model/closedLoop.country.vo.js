/**
 * @ngdoc Model
 * @name GlobalMeterAppCountryVO
 *
 * @description
 *  Global Meter App Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'ClosedLoopCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (GlobalMeterAppCountryVO)
        angular.extend(this, countryVO);
        this.installationsNumber = (data.installationsNumber === undefined || data.installationsNumber === null) ? null : data.installationsNumber;
        this.opportunitiesWon = (data.opportunitiesWon === undefined || data.opportunitiesWon === null) ? null : data.opportunitiesWon;
        this.opportunitiesInstalled = (data.opportunitiesInstalled === undefined || data.opportunitiesInstalled === null) ? null : data.opportunitiesInstalled;
        this.projectsWonVal = (data.ProjectsWonVal === undefined || data.ProjectsWonVal === null) ? null : data.ProjectsWonVal;
      }
      return Ctor;
    }
  ]);
})();





