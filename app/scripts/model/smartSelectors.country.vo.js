/**
 * @ngdoc Model
 * @name BLMCountryVO
 *
 * @description
 *  BLM Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'SmartSelectorsCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (SmartSelectorsCountryVO)
        angular.extend(this, countryVO);
        this.smartSelectorsLaunchDays = (data.smartSelectorsLaunchDays === undefined || data.smartSelectorsLaunchDays === null) ? null : data.smartSelectorsLaunchDays;
      }

      return Ctor;

    }
  ]);
})();





