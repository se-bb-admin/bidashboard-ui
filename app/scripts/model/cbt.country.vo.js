/**
 * @ngdoc Model
 * @name CbtCountryVO
 *
 * @description
 *  CBT Ref Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'CBTCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (CBTCountryVO)
        angular.extend(this, countryVO);
        this.noOfProjCreated = (data.noOfProjCreated === undefined || data.noOfProjCreated === null) ? null : data.noOfProjCreated;
        this.noOfChecksCal = (data.noOfChecksCal === undefined || data.noOfChecksCal === null) ? null : data.noOfChecksCal;
        this.aveCalPerSess = (data.aveCalPerSess === undefined || data.aveCalPerSess === null) ? null : data.aveCalPerSess;
        this.noOfProjCreatedChart = (data.noOfProjCreatedChart === undefined || data.noOfProjCreatedChart === null) ? null : data.noOfProjCreatedChart;
        this.noOfChecksCalChart = (data.noOfChecksCalChart === undefined || data.noOfChecksCalChart === null) ? null : data.noOfChecksCalChart;
        this.aveCalPerSessChart = (data.aveCalPerSessChart === undefined || data.aveCalPerSessChart === null) ? null : data.aveCalPerSessChart;
      }

      return Ctor;

    }
  ]);
})();





