/**
 * @ngdoc Model
 * @name CountryVO
 *
 * @description
 *  Base Class of Country
 *
 */

(function () {
  'use strict';

  var factoryId = 'CountryVO';

  angular.module('dataModelModule').factory(factoryId, [
    function () {
      var self;
      function Ctor(data) {
        self = this;
        //angular.extend(this, data);
        this.countryName = (data.countryName === undefined || data.countryName === null) ? null : data.countryName;
        this.countryGaId = (data.countryGaId === undefined || data.countryGaId === null) ? null : data.countryGaId;
        this.launchDate = (data.launchDate === undefined || data.launchDate === null) ? null : data.launchDate;
        this.launchDateStr = (data.launchDateStr === undefined || data.launchDateStr === null) ? null : data.launchDateStr;
        this.regUsers = (data.regUsers === undefined || data.regUsers === null) ? null : data.regUsers;
        this.uniqueActiveUsers = (data.uniqueActiveUsers === undefined || data.uniqueActiveUsers === null) ? null : data.uniqueActiveUsers;
        this.uniqueActiveUsersChart = (data.uniqueActiveUsersChart === undefined || data.uniqueActiveUsersChart === null) ? null : data.uniqueActiveUsersChart;
      }

      return Ctor;

    }
  ]);
})();


