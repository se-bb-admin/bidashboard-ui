/**
 * @ngdoc Model
 * @name CCCAppCountryVO
 *
 * @description
 *  CCC APP Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'CCCAppCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (SmartSelectorsCountryVO)
        angular.extend(this, countryVO);
        this.cccAppLaunchDays = (data.cccAppLaunchDays === undefined || data.cccAppLaunchDays === null) ? null : data.cccAppLaunchDays;
      }

      return Ctor;

    }
  ]);
})();





