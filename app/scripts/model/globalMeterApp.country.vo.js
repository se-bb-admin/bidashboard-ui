/**
 * @ngdoc Model
 * @name GlobalMeterAppCountryVO
 *
 * @description
 *  Global Meter App Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'GlobalMeterAppCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (GlobalMeterAppCountryVO)
        angular.extend(this, countryVO);
        this.productSearches = (data.projectAction === undefined || data.projectAction === null) ? null : data.projectAction;
        this.productViews = (data.projectView === undefined || data.projectView === null) ? null : data.projectView;
      }
      return Ctor;
    }
  ]);
})();





