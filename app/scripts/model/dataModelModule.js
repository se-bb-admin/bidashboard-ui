/**
 * @ngdoc Module
 * @name dataModelModule
 *
 * @description
 *   Define separate module for dataModel
 *
 */
(function () {
  'use strict';

  var moduleId = 'dataModelModule';

  //Inject commonUtilsModule for Logger
  var dataModelModule = angular.module(moduleId, ['commonUtilsModule']);
}
)();


