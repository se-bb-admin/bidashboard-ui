/**
 * @ngdoc Model
 * @name CadLibraryCountryVO
 *
 * @description
 *  Cad Library Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'CadLibraryCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (CadLibraryCountryVO)
        angular.extend(this, countryVO);
        this.productDownloads = (data.productDownloads === undefined || data.productDownloads === null) ? null : data.productDownloads;
        this.returningUsers = (data.returningUsers === undefined || data.returningUsers === null) ? null : data.returningUsers;
        this.visits = (data.visits === undefined || data.visits === null) ? null : data.visits;
        this.pageViews = (data.pageViews === undefined || data.pageViews === null) ? null : data.pageViews;
      }

      return Ctor;

    }
  ]);
})();





