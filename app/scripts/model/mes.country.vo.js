/**
 * @ngdoc Model
 * @name MESCountryVO
 *
 * @description
 *  MESCountryVO Ref Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'MESCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (CBTCountryVO)
        angular.extend(this, countryVO);
        this.uniqueActiveUsersChart = (data.uniqueActiveUsersChart === undefined || data.uniqueActiveUsersChart === null) ? null : data.uniqueActiveUsersChart;
      }
      return Ctor;
    }
  ]);
})();





