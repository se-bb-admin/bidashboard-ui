/**
 * @ngdoc Model
 * @name ClipsalWishListCountryVO
 *
 * @description
 *  Quick Ref Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'ClipsalWishlistCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (ClipsalWishListCountryVO)
        angular.extend(this, countryVO);
        this.productsAddedToBOM = (data.productsAddedToBOM === undefined || data.productsAddedToBOM === null) ? null : data.productsAddedToBOM;
        this.projectsCreated = (data.projectsCreated === undefined || data.projectsCreated === null) ? null : data.projectsCreated;
        this.projectsSentToElectricians = (data.projectsSentToElectricians === undefined || data.projectsSentToElectricians === null) ? null : data.projectsSentToElectricians;
        this.bomSentToElectricians = (data.bomSentToElectricians === undefined || data.bomSentToElectricians === null) ? null : data.bomSentToElectricians;
      }

      return Ctor;

    }
  ]);
})();





