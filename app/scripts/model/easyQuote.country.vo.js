/**
 * @ngdoc Model
 * @name EasyQuoteCountryVO
 *
 * @description
 *  EasyQuoteCountryVO Ref Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'EasyQuoteCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (CBTCountryVO)
        angular.extend(this, countryVO);
        this.loginUsers = (data.loginUsers === undefined || data.loginUsers === null) ? null : data.loginUsers;
        this.createProject = (data.createProject === undefined || data.createProject === null) ? null : data.createProject;
        this.projectCompleted = (data.projectCompleted === undefined || data.projectCompleted === null) ? null : data.projectCompleted;
        this.bomPrice = (data.bomPrice === undefined || data.bomPrice === null) ? null : data.bomPrice;
        this.referenceCount = (data.referenceCount === undefined || data.referenceCount === null) ? null : data.referenceCount;
        this.sendMail = (data.sendMail === undefined || data.sendMail === null) ? null : data.sendMail;
        this.regUsersChart = (data.regUsersChart === undefined || data.regUsersChart === null) ? null : data.regUsersChart;
        this.uniqueActiveUsersChart = (data.uniqueActiveUsersChart === undefined || data.uniqueActiveUsersChart === null) ? null : data.uniqueActiveUsersChart;
        this.loginUsersChart = (data.loginUsersChart === undefined || data.loginUsersChart === null) ? null : data.loginUsersChart;
        this.createProjectChart = (data.createProjectChart === undefined || data.createProjectChart === null) ? null : data.createProjectChart;
        this.projectCompletedChart = (data.projectCompletedChart === undefined || data.projectCompletedChart === null) ? null : data.projectCompletedChart;
        this.bomPriceChart = (data.bomPriceChart === undefined || data.bomPriceChart === null) ? null : data.bomPriceChart;
        this.referenceCountChart = (data.referenceCountChart === undefined || data.referenceCountChart === null) ? null : data.referenceCountChart;
        this.sendMailChart = (data.sendMailChart === undefined || data.sendMailChart === null) ? null : data.sendMailChart;
      }
      return Ctor;
    }
  ]);
})();





