/**
 * @ngdoc Model
 * @name BLMCountryVO
 *
 * @description
 *  BLM Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'BLMCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (QuickRefCountryVO)
        angular.extend(this, countryVO);
        this.projectsCreated = (data.projectsCreated === undefined || data.projectsCreated === null) ? null : data.projectsCreated;
      }

      return Ctor;

    }
  ]);
})();





