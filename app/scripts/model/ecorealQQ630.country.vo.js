/**
 * @ngdoc Model
 * @name EcorealQQ630CountryVO
 *
 * @description
 *  EcorealQQ630CountryVO Ref Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'EcorealQQ630CountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (CBTCountryVO)
        angular.extend(this, countryVO);
        this.productsAdded = (data.productsAdded === undefined || data.productsAdded === null) ? null : data.productsAdded;
        this.projectCreated = (data.projectCreated === undefined || data.projectCreated === null) ? null : data.projectCreated;
        this.bomExportedNo = (data.bomExportedNo === undefined || data.bomExportedNo === null) ? null : data.bomExportedNo;
        this.bomExportedVal = (data.bomExportedVal === undefined || data.bomExportedVal === null) ? null : data.bomExportedVal;
        this.productsAddedChart = (data.productsAddedChart === undefined || data.productsAddedChart === null) ? null : data.productsAddedChart;
        this.projectCreatedChart = (data.projectCreatedChart === undefined || data.projectCreatedChart === null) ? null : data.projectCreatedChart;
        this.bomExportedNoChart = (data.bomExportedNoChart === undefined || data.bomExportedNoChart === null) ? null : data.bomExportedNoChart;
        this.bomExportedValChart = (data.bomExportedValChart === undefined || data.bomExportedValChart === null) ? null : data.bomExportedValChart;
      }

      return Ctor;

    }
  ]);
})();
