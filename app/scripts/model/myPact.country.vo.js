/**
 * @ngdoc Model
 * @name MyPactCountryVO
 *
 * @description
 *  Mypact Ref Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'MyPactCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (CBTCountryVO)
        angular.extend(this, countryVO);
        this.uniqueCountryWiseActvUsers = (data.uniqueCountryWiseActvUsers === undefined || data.uniqueCountryWiseActvUsers === null) ? null : data.uniqueCountryWiseActvUsers;
        this.countrySubName = (data.countrySubName === undefined || data.countrySubName === null) ? null : data.countrySubName;
        this.noSelectionLaunched = (data.noSelectionLaunched === undefined || data.noSelectionLaunched === null) ? null : data.noSelectionLaunched;
        this.noCountryWiseSelectionLaunched = (data.noCountryWiseSelectionLaunched === undefined || data.noCountryWiseSelectionLaunched === null) ? null : data.noCountryWiseSelectionLaunched;
        this.noSelectionSaved = (data.noSelectionSaved === undefined || data.noSelectionSaved === null) ? null : data.noSelectionSaved;
        this.noCountryWiseSelectionSaved = (data.noCountryWiseSelectionSaved === undefined || data.noCountryWiseSelectionSaved === null) ? null : data.noCountryWiseSelectionSaved;
      }

      return Ctor;

    }
  ]);
})();





