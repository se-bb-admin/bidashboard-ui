/**
 * @ngdoc Model
 * @name QuickRefCountryVO
 *
 * @description
 *  Quick Ref Country VO
 *
 */

(function () {
  'use strict';

  var factoryId = 'QuickRefCountryVO';

  angular.module('dataModelModule').factory(factoryId, ['CountryVO',
    function (CountryVO) {

      var self;
      var countryVO;

      function Ctor(data) {
        self = this;
        if (data === undefined || data === null) {
          data = {};
        }
        countryVO = new CountryVO(data);
        // Copy All the property of County VO to this (QuickRefCountryVO)
        angular.extend(this, countryVO);
        this.configLaunched = (data.configLaunched === undefined || data.configLaunched === null) ? null : data.configLaunched;
      }

      return Ctor;

    }
  ]);
})();





